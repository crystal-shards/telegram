abstract class TD::Base
  macro object_type(type)
    def initialize(__json : JSON::Any? = nil)
    end

    @[JSON::Field(key: "@type")]
    property object_type : ::String = {{type.id.stringify}}
  end

  macro object_attributes(type)
    {% params = [] of String %}
    {% for field, field_type in type %}
      {% params << "@#{field} : #{"::".id if field_type.is_a?(Generic) && field_type.name.stringify == "Array"}#{field_type}#{" = false".id if field_type.is_a?(Path) && field_type.id.stringify.includes?("Bool")}" %}
      {% if field_type.is_a?(Generic) %}
      property {{field.id}}
      @{{field.id}} = uninitialized {{"::".id if field_type.name.stringify == "Array"}}{{field_type.id}}
      {% else %}
      property {{field.id}}
      @{{field.id}} : {{field_type.id}}
      {% end %}
    {% end %}
    {% params = params.sort_by {|el| el.includes?("= false") ? 1 : 0 }%}

    def initialize({{params.join(", ").id}})
    end

    def initialize(__json : JSON::Any? = nil)
      __json = __json.not_nil!
      {% for field, field_type in type %}
      {% if field_type.is_a?(Generic) %}
        # String?, Array(String), Array(String)?, Array(Array(String)), Array(Array(String))?
        {{nilable = field_type.type_vars.size == 2}}
        {{nested_type = field_type.type_vars[0]}}
        {% if nested_type.is_a?(Path) %}
          # String?, Array(String)
          {% if nilable %}
            # String?
            {% if TD::Types::ABSTRACT_TYPES.keys.includes?(nested_type) %}
              @{{field.id}} = extract_abstract_type(__json[{{field.id.stringify}}]).as({{nested_type.id}}) if __json[{{field.id.stringify}}]?
            {% else %}
              @{{field.id}} = extract_type(__json[{{field.id.stringify}}], {{nested_type.id}}).as({{nested_type.id}}) if __json[{{field.id.stringify}}]?
            {% end %}
          {% else %}
            # Array(String)
            {% if TD::Types::ABSTRACT_TYPES.keys.includes?(nested_type) %}
              @{{field.id}} = __json[{{field.id.stringify}}].as_a.map {|el| extract_abstract_type(el).as({{nested_type.id}}) }
            {% else %}
              @{{field.id}} = __json[{{field.id.stringify}}].as_a.map {|el| extract_type(el, {{nested_type.id}}).as({{nested_type.id}}) }
            {% end %}
          {% end %}
        {% else %}
          # Array(String)?, Array(Array(String)), Array(Array(String))?
          {{double_nested_type = nested_type.type_vars[0]}}
          {{double_nilable = nested_type.type_vars.size == 2}}
          {% if double_nested_type.is_a?(Path) %}
            {% if nilable %}
              # Array(String)?
              {% if TD::Types::ABSTRACT_TYPES.keys.includes?(double_nested_type) %}
                @{{field.id}} = __json[{{field.id.stringify}}].as_a.map {|el| extract_abstract_type(el).as({{double_nested_type.id}}) } if __json[{{field.id.stringify}}]?
              {% else %}
                @{{field.id}} = __json[{{field.id.stringify}}].as_a.map {|el| extract_type(el, {{double_nested_type.id}}).as({{double_nested_type.id}}) } if __json[{{field.id.stringify}}]?
              {% end %}
            {% else %}
              # Array(Array(String))
              {% if TD::Types::ABSTRACT_TYPES.keys.includes?(double_nested_type) %}
                @{{field.id}} = __json[{{field.id.stringify}}].as_a.map do |parent_el|
                  parent_el.as_a.map {|el| extract_abstract_type(el).as({{double_nested_type.id}})  } 
                end
              {% else %}
                @{{field.id}} = __json[{{field.id.stringify}}].as_a.map do |parent_el|
                  parent_el.as_a.map {|el| extract_type(el, {{double_nested_type.id}}).as({{double_nested_type.id}})  } 
                end 
              {% end %}
            {% end %}
          {% else %}
            # Array(Array(String))?
            {% if TD::Types::ABSTRACT_TYPES.keys.includes?(double_nested_type.type_vars[0]) %}
              @{{field.id}} = __json[{{field.id.stringify}}].as_a.map do |parent_el|
                parent_el.as_a.map {|el| extract_abstract_type(el).as({{double_nested_type.type_vars[0].id}})  } 
              end if __json[{{field.id.stringify}}]?
            {% else %}
              @{{field.id}} = __json[{{field.id.stringify}}].as_a.map do |parent_el|
                parent_el.as_a.map {|el| extract_type(el, {{double_nested_type.type_vars[0].id}}).as({{double_nested_type.type_vars[0].id}})  } 
              end if __json[{{field.id.stringify}}]?
            {% end %}
          {% end %}
        {% end %}
      {% else %}
        # String
        {% if TD::Types::ABSTRACT_TYPES.keys.includes?(field_type) %}
          @{{field.id}} = extract_abstract_type(__json[{{field.id.stringify}}]).as({{field_type.id}}) 
        {% else %}
          @{{field.id}} = extract_type(__json[{{field.id.stringify}}], {{field_type.id}}).as({{field_type.id}}) 
        {% end %}
      {% end %}
      {% end %}
    end
  end

  def extract_type(__json : JSON::Any, class_type : Class)
    case class_type
    when Int32.class then __json.as_i
    when Int64.class then __json.as_i64
    when Float64.class then __json.as_f
    when String.class then __json.as_s
    when Bool.class then __json.as_bool
    else
      class_type.new(__json: __json)
    end
  end

  def extract_abstract_type(__json : JSON::Any)
    type = __json["@type"].as_s
    TD::Types::TABLE[type].new(__json: __json)
  end
end