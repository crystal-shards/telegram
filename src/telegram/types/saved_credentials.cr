# Contains information about saved card credentials
#
# @id : String (Unique identifier of the saved credentials)
# @title : String (Title of the saved credentials)

class TD::SavedCredentials < TD::Base
  include JSON::Serializable

  object_type "savedCredentials"

  object_attributes({
    id: String,
    title: String
  })
end