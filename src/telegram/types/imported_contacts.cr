# Represents the result of an ImportContacts request
#
# @user_ids : Array(Int64) (User identifiers of the imported contacts in the same order as they were specified in the request; 0 if the contact is not yet a registered user)
# @importer_count : Array(Int32) (The number of users that imported the corresponding contact; 0 for already registered users or if unavailable)

class TD::ImportedContacts < TD::Base
  include JSON::Serializable

  object_type "importedContacts"

  object_attributes({
    user_ids: Array(Int64),
    importer_count: Array(Int32)
  })
end