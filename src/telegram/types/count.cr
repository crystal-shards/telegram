# Contains a counter
#
# @count : Int32 (Count)

class TD::Count < TD::Base
  include JSON::Serializable

  object_type "count"

  object_attributes({
    count: Int32
  })
end