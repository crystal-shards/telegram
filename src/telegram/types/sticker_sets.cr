# Represents a list of sticker sets
#
# @total_count : Int32 (Approximate total number of sticker sets found)
# @sets : Array(TD::StickerSetInfo) (List of sticker sets)

class TD::StickerSets < TD::Base
  include JSON::Serializable

  object_type "stickerSets"

  object_attributes({
    total_count: Int32,
    sets: Array(TD::StickerSetInfo)
  })
end