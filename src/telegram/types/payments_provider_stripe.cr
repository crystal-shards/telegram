# Stripe payment provider
#
# @publishable_key : String (Stripe API publishable key)
# @need_country : Bool (True, if the user country must be provided)
# @need_postal_code : Bool (True, if the user ZIP)
# @need_cardholder_name : Bool (True, if the cardholder name must be provided)

class TD::PaymentsProviderStripe < TD::Base
  include JSON::Serializable

  object_type "paymentsProviderStripe"

  object_attributes({
    publishable_key: String,
    need_country: Bool,
    need_postal_code: Bool,
    need_cardholder_name: Bool
  })
end