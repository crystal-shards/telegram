# A Telegram Passport element containing the user's phone number
#
# @phone_number : String (Phone number)

class TD::PassportElement::PhoneNumber < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementPhoneNumber"

  object_attributes({
    phone_number: String
  })
end