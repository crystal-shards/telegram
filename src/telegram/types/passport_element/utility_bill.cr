# A Telegram Passport element containing the user's utility bill
#
# @utility_bill : TD::PersonalDocument (Utility bill)

class TD::PassportElement::UtilityBill < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementUtilityBill"

  object_attributes({
    utility_bill: TD::PersonalDocument
  })
end