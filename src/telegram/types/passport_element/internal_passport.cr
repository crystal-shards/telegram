# A Telegram Passport element containing the user's internal passport
#
# @internal_passport : TD::IdentityDocument (Internal passport)

class TD::PassportElement::InternalPassport < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementInternalPassport"

  object_attributes({
    internal_passport: TD::IdentityDocument
  })
end