# A Telegram Passport element containing the user's address
#
# @address : TD::Address (Address)

class TD::PassportElement::Address < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementAddress"

  object_attributes({
    address: TD::Address
  })
end