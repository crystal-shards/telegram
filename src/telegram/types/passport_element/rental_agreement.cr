# A Telegram Passport element containing the user's rental agreement
#
# @rental_agreement : TD::PersonalDocument (Rental agreement)

class TD::PassportElement::RentalAgreement < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementRentalAgreement"

  object_attributes({
    rental_agreement: TD::PersonalDocument
  })
end