# A Telegram Passport element containing the user's temporary registration
#
# @temporary_registration : TD::PersonalDocument (Temporary registration)

class TD::PassportElement::TemporaryRegistration < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementTemporaryRegistration"

  object_attributes({
    temporary_registration: TD::PersonalDocument
  })
end