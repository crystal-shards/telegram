# A Telegram Passport element containing the user's passport
#
# @passport : TD::IdentityDocument (Passport)

class TD::PassportElement::Passport < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementPassport"

  object_attributes({
    passport: TD::IdentityDocument
  })
end