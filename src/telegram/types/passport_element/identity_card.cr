# A Telegram Passport element containing the user's identity card
#
# @identity_card : TD::IdentityDocument (Identity card)

class TD::PassportElement::IdentityCard < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementIdentityCard"

  object_attributes({
    identity_card: TD::IdentityDocument
  })
end