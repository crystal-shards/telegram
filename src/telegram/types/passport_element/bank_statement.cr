# A Telegram Passport element containing the user's bank statement
#
# @bank_statement : TD::PersonalDocument (Bank statement)

class TD::PassportElement::BankStatement < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementBankStatement"

  object_attributes({
    bank_statement: TD::PersonalDocument
  })
end