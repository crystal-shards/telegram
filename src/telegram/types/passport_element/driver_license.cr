# A Telegram Passport element containing the user's driver license
#
# @driver_license : TD::IdentityDocument (Driver license)

class TD::PassportElement::DriverLicense < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementDriverLicense"

  object_attributes({
    driver_license: TD::IdentityDocument
  })
end