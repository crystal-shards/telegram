# A Telegram Passport element containing the user's passport registration pages
#
# @passport_registration : TD::PersonalDocument (Passport registration pages)

class TD::PassportElement::PassportRegistration < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementPassportRegistration"

  object_attributes({
    passport_registration: TD::PersonalDocument
  })
end