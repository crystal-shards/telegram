# A Telegram Passport element containing the user's email address
#
# @email_address : String (Email address)

class TD::PassportElement::EmailAddress < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementEmailAddress"

  object_attributes({
    email_address: String
  })
end