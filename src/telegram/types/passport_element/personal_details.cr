# A Telegram Passport element containing the user's personal details
#
# @personal_details : TD::PersonalDetails (Personal details of the user)

class TD::PassportElement::PersonalDetails < TD::PassportElement
  include JSON::Serializable

  object_type "passportElementPersonalDetails"

  object_attributes({
    personal_details: TD::PersonalDetails
  })
end