# Represents the scope to which bot commands are relevant
#
# Default: A scope covering all users
# AllPrivateChats: A scope covering all private chats
# AllGroupChats: A scope covering all group and supergroup chats
# AllChatAdministrators: A scope covering all group and supergroup chat administrators
# Chat: A scope covering all members of a chat
# ChatAdministrators: A scope covering all administrators of a chat
# ChatMember: A scope covering a member of a chat

abstract class TD::BotCommandScope < TD::Base
end