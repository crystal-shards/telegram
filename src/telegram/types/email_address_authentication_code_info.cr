# Information about the email address authentication code that was sent
#
# @email_address_pattern : String (Pattern of the email address to which an authentication code was sent)
# @length : Int32 (Length of the code; 0 if unknown)

class TD::EmailAddressAuthenticationCodeInfo < TD::Base
  include JSON::Serializable

  object_type "emailAddressAuthenticationCodeInfo"

  object_attributes({
    email_address_pattern: String,
    length: Int32
  })
end