# Represents a list of chat administrators
#
# @administrators : Array(TD::ChatAdministrator) (A list of chat administrators)

class TD::ChatAdministrators < TD::Base
  include JSON::Serializable

  object_type "chatAdministrators"

  object_attributes({
    administrators: Array(TD::ChatAdministrator)
  })
end