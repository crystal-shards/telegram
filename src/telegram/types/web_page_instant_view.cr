# Describes an instant view page for a web page
#
# @page_blocks : Array(TD::PageBlock) (Content of the web page)
# @view_count : Int32 (Number of the instant view views; 0 if unknown)
# @version : Int32 (Version of the instant view; currently, can be 1 or 2)
# @is_rtl : Bool (True, if the instant view must be shown from right to left)
# @is_full : Bool (True, if the instant view contains the full page. A network request might be needed to get the full web page instant view)
# @feedback_link : TD::InternalLinkType (An internal link to be opened to leave feedback about the instant view)

class TD::WebPageInstantView < TD::Base
  include JSON::Serializable

  object_type "webPageInstantView"

  object_attributes({
    page_blocks: Array(TD::PageBlock),
    view_count: Int32,
    version: Int32,
    is_rtl: Bool,
    is_full: Bool,
    feedback_link: TD::InternalLinkType
  })
end