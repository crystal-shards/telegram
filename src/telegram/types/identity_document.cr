# An identity document
#
# @number : String (Document number; 1-24 characters)
# @expiry_date : TD::Date? (Document expiry date; may be null if not applicable)
# @front_side : TD::DatedFile (Front side of the document)
# @reverse_side : TD::DatedFile? (Reverse side of the document; only for driver license and identity card; may be null)
# @selfie : TD::DatedFile? (Selfie with the document; may be null)
# @translation : Array(TD::DatedFile) (List of files containing a certified English translation of the document)

class TD::IdentityDocument < TD::Base
  include JSON::Serializable

  object_type "identityDocument"

  object_attributes({
    number: String,
    expiry_date: TD::Date?,
    front_side: TD::DatedFile,
    reverse_side: TD::DatedFile?,
    selfie: TD::DatedFile?,
    translation: Array(TD::DatedFile)
  })
end