# A Telegram Passport element to be saved containing the user's address
#
# @address : TD::Address (The address to be saved)

class TD::InputPassportElement::Address < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementAddress"

  object_attributes({
    address: TD::Address
  })
end