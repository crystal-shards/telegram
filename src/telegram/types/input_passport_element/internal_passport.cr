# A Telegram Passport element to be saved containing the user's internal passport
#
# @internal_passport : TD::InputIdentityDocument (The internal passport to be saved)

class TD::InputPassportElement::InternalPassport < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementInternalPassport"

  object_attributes({
    internal_passport: TD::InputIdentityDocument
  })
end