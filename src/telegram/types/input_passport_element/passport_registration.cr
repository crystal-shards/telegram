# A Telegram Passport element to be saved containing the user's passport registration
#
# @passport_registration : TD::InputPersonalDocument (The passport registration page to be saved)

class TD::InputPassportElement::PassportRegistration < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementPassportRegistration"

  object_attributes({
    passport_registration: TD::InputPersonalDocument
  })
end