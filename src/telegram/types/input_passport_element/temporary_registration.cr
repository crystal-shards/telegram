# A Telegram Passport element to be saved containing the user's temporary registration
#
# @temporary_registration : TD::InputPersonalDocument (The temporary registration document to be saved)

class TD::InputPassportElement::TemporaryRegistration < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementTemporaryRegistration"

  object_attributes({
    temporary_registration: TD::InputPersonalDocument
  })
end