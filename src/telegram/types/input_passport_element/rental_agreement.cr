# A Telegram Passport element to be saved containing the user's rental agreement
#
# @rental_agreement : TD::InputPersonalDocument (The rental agreement to be saved)

class TD::InputPassportElement::RentalAgreement < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementRentalAgreement"

  object_attributes({
    rental_agreement: TD::InputPersonalDocument
  })
end