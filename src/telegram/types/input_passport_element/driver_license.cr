# A Telegram Passport element to be saved containing the user's driver license
#
# @driver_license : TD::InputIdentityDocument (The driver license to be saved)

class TD::InputPassportElement::DriverLicense < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementDriverLicense"

  object_attributes({
    driver_license: TD::InputIdentityDocument
  })
end