# A Telegram Passport element to be saved containing the user's identity card
#
# @identity_card : TD::InputIdentityDocument (The identity card to be saved)

class TD::InputPassportElement::IdentityCard < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementIdentityCard"

  object_attributes({
    identity_card: TD::InputIdentityDocument
  })
end