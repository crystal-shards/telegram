# A Telegram Passport element to be saved containing the user's personal details
#
# @personal_details : TD::PersonalDetails (Personal details of the user)

class TD::InputPassportElement::PersonalDetails < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementPersonalDetails"

  object_attributes({
    personal_details: TD::PersonalDetails
  })
end