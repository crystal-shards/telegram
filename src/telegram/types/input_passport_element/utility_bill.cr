# A Telegram Passport element to be saved containing the user's utility bill
#
# @utility_bill : TD::InputPersonalDocument (The utility bill to be saved)

class TD::InputPassportElement::UtilityBill < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementUtilityBill"

  object_attributes({
    utility_bill: TD::InputPersonalDocument
  })
end