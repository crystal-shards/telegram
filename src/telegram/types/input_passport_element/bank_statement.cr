# A Telegram Passport element to be saved containing the user's bank statement
#
# @bank_statement : TD::InputPersonalDocument (The bank statement to be saved)

class TD::InputPassportElement::BankStatement < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementBankStatement"

  object_attributes({
    bank_statement: TD::InputPersonalDocument
  })
end