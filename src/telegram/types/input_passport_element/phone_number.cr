# A Telegram Passport element to be saved containing the user's phone number
#
# @phone_number : String (The phone number to be saved)

class TD::InputPassportElement::PhoneNumber < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementPhoneNumber"

  object_attributes({
    phone_number: String
  })
end