# A Telegram Passport element to be saved containing the user's passport
#
# @passport : TD::InputIdentityDocument (The passport to be saved)

class TD::InputPassportElement::Passport < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementPassport"

  object_attributes({
    passport: TD::InputIdentityDocument
  })
end