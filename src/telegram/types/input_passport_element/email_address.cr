# A Telegram Passport element to be saved containing the user's email address
#
# @email_address : String (The email address to be saved)

class TD::InputPassportElement::EmailAddress < TD::InputPassportElement
  include JSON::Serializable

  object_type "inputPassportElementEmailAddress"

  object_attributes({
    email_address: String
  })
end