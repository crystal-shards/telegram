# Contains the description of an error in a Telegram Passport element
#
# Unspecified: The element contains an error in an unspecified place. The error will be considered resolved when new data is added
# DataField: One of the data fields contains an error. The error will be considered resolved when the value of the field changes
# FrontSide: The front side of the document contains an error. The error will be considered resolved when the file with the front side changes
# ReverseSide: The reverse side of the document contains an error. The error will be considered resolved when the file with the reverse side changes
# Selfie: The selfie with the document contains an error. The error will be considered resolved when the file with the selfie changes
# TranslationFile: One of files with the translation of the document contains an error. The error will be considered resolved when the file changes
# TranslationFiles: The translation of the document contains an error. The error will be considered resolved when the list of translation files changes
# File: The file contains an error. The error will be considered resolved when the file changes
# Files: The list of attached files contains an error. The error will be considered resolved when the list of files changes

abstract class TD::PassportElementErrorSource < TD::Base
end