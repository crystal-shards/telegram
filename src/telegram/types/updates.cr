# Contains a list of updates
#
# @updates : Array(TD::Update) (List of updates)

class TD::Updates < TD::Base
  include JSON::Serializable

  object_type "updates"

  object_attributes({
    updates: Array(TD::Update)
  })
end