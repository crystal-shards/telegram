# Represents a chat event
#
# MessageEdited: A message was edited
# MessageDeleted: A message was deleted
# PollStopped: A poll in a message was stopped
# MessagePinned: A message was pinned
# MessageUnpinned: A message was unpinned
# MemberJoined: A new member joined the chat
# MemberJoinedByInviteLink: A new member joined the chat via an invite link
# MemberJoinedByRequest: A new member was accepted to the chat by an administrator
# MemberLeft: A member left the chat
# MemberInvited: A new chat member was invited
# MemberPromoted: A chat member has gained
# MemberRestricted: A chat member was restricted
# TitleChanged: The chat title was changed
# PermissionsChanged: The chat permissions was changed
# DescriptionChanged: The chat description was changed
# UsernameChanged: The chat username was changed
# PhotoChanged: The chat photo was changed
# InvitesToggled: The can_invite_users permission of a supergroup chat was toggled
# LinkedChatChanged: The linked chat of a supergroup was changed
# SlowModeDelayChanged: The slow_mode_delay setting of a supergroup was changed
# MessageTtlChanged: The message TTL was changed
# SignMessagesToggled: The sign_messages setting of a channel was toggled
# HasProtectedContentToggled: The has_protected_content setting of a channel was toggled
# StickerSetChanged: The supergroup sticker set was changed
# LocationChanged: The supergroup location was changed
# IsAllHistoryAvailableToggled: The is_all_history_available setting of a supergroup was toggled
# InviteLinkEdited: A chat invite link was edited
# InviteLinkRevoked: A chat invite link was revoked
# InviteLinkDeleted: A revoked chat invite link was deleted
# VideoChatCreated: A video chat was created
# VideoChatEnded: A video chat was ended
# VideoChatParticipantIsMutedToggled: A video chat participant was muted or unmuted
# VideoChatParticipantVolumeLevelChanged: A video chat participant volume level was changed
# VideoChatMuteNewParticipantsToggled: The mute_new_participants setting of a video chat was toggled

abstract class TD::ChatEventAction < TD::Base
end