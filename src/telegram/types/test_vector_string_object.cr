# A simple object containing a vector of objects that hold a string; for testing only
#
# @value : Array(TD::TestString) (Vector of objects)

class TD::TestVectorStringObject < TD::Base
  include JSON::Serializable

  object_type "testVectorStringObject"

  object_attributes({
    value: Array(TD::TestString)
  })
end