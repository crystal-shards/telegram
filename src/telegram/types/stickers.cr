# Represents a list of stickers
#
# @stickers : Array(TD::Sticker) (List of stickers)

class TD::Stickers < TD::Base
  include JSON::Serializable

  object_type "stickers"

  object_attributes({
    stickers: Array(TD::Sticker)
  })
end