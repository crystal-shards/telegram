# Thumbnail image of a very poor quality and low resolution
#
# @width : Int32 (Thumbnail width, usually doesn't exceed 40)
# @height : Int32 (Thumbnail height, usually doesn't exceed 40)
# @data : String (The thumbnail in JPEG format)

class TD::Minithumbnail < TD::Base
  include JSON::Serializable

  object_type "minithumbnail"

  object_attributes({
    width: Int32,
    height: Int32,
    data: String
  })
end