# Represents a file
#
# @id : Int32 (Unique file identifier)
# @size : Int32 (File size, in bytes; 0 if unknown)
# @expected_size : Int32 (Approximate file size in bytes in case the exact file size is unknown. Can be used to show download)
# @local : TD::LocalFile (Information about the local copy of the file)
# @remote : TD::RemoteFile (Information about the remote copy of the file)

class TD::File < TD::Base
  include JSON::Serializable

  object_type "file"

  object_attributes({
    id: Int32,
    size: Int32,
    expected_size: Int32,
    local: TD::LocalFile,
    remote: TD::RemoteFile
  })
end