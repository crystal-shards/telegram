# Represents a supergroup or channel with zero or more members (subscribers in the case of channels). From the point of view of the system, a channel is a special kind of a supergroup: only administrators can post and see the list of members, and posts from all administrators use the name and photo of the channel instead of individual names and profile photos. Unlike supergroups, channels can have an unlimited number of subscribers
#
# @id : Int64 (Supergroup or channel identifier)
# @username : String (Username of the supergroup or channel; empty for private supergroups or channels)
# @date : Int32 (Point in time (Unix timestamp) when the current user joined, or the point in time when the supergroup or channel was created, in case the user is not a member)
# @status : TD::ChatMemberStatus (Status of the current user in the supergroup or channel; custom title will be always empty)
# @member_count : Int32 (Number of members in the supergroup or channel; 0 if unknown. Currently, it is guaranteed to be known only if the supergroup or channel was received through searchPublicChats, searchChatsNearby, getInactiveSupergroupChats, getSuitableDiscussionChats, getGroupsInCommon, or getUserPrivacySettingRules)
# @has_linked_chat : Bool (True, if the channel has a discussion group, or the supergroup is the designated discussion group for a channel)
# @has_location : Bool (True, if the supergroup is connected to a location, i.e. the supergroup is a location-based supergroup)
# @sign_messages : Bool (True, if messages sent to the channel need to contain information about the sender. This field is only applicable to channels)
# @is_slow_mode_enabled : Bool (True, if the slow mode is enabled in the supergroup)
# @is_channel : Bool (True, if the supergroup is a channel)
# @is_broadcast_group : Bool (True, if the supergroup is a broadcast group, i.e. only administrators can send messages and there is no limit on the number of members)
# @is_verified : Bool (True, if the supergroup or channel is verified)
# @restriction_reason : String (If non-empty, contains a human-readable description of the reason why access to this supergroup or channel must be restricted)
# @is_scam : Bool (True, if many users reported this supergroup or channel as a scam)
# @is_fake : Bool (True, if many users reported this supergroup or channel as a fake account)

class TD::Supergroup < TD::Base
  include JSON::Serializable

  object_type "supergroup"

  object_attributes({
    id: Int64,
    username: String,
    date: Int32,
    status: TD::ChatMemberStatus,
    member_count: Int32,
    has_linked_chat: Bool,
    has_location: Bool,
    sign_messages: Bool,
    is_slow_mode_enabled: Bool,
    is_channel: Bool,
    is_broadcast_group: Bool,
    is_verified: Bool,
    restriction_reason: String,
    is_scam: Bool,
    is_fake: Bool
  })
end