# Represents a cell of a table
#
# @text : TD::RichText? (Cell text; may be null. If the text is null, then the cell must be invisible)
# @is_header : Bool (True, if it is a header cell)
# @colspan : Int32 (The number of columns the cell spans)
# @rowspan : Int32 (The number of rows the cell spans)
# @align : TD::PageBlockHorizontalAlignment (Horizontal cell content alignment)
# @valign : TD::PageBlockVerticalAlignment (Vertical cell content alignment)

class TD::PageBlockTableCell < TD::Base
  include JSON::Serializable

  object_type "pageBlockTableCell"

  object_attributes({
    text: TD::RichText?,
    is_header: Bool,
    colspan: Int32,
    rowspan: Int32,
    align: TD::PageBlockHorizontalAlignment,
    valign: TD::PageBlockVerticalAlignment
  })
end