# Contains a list of backgrounds
#
# @backgrounds : Array(TD::Background) (A list of backgrounds)

class TD::Backgrounds < TD::Base
  include JSON::Serializable

  object_type "backgrounds"

  object_attributes({
    backgrounds: Array(TD::Background)
  })
end