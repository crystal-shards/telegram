# Describes a server for relaying call data
#
# @id : String (Server identifier)
# @ip_address : String (Server IPv4 address)
# @ipv6_address : String (Server IPv6 address)
# @port : Int32 (Server port number)
# @type : TD::CallServerType (Server type)

class TD::CallServer < TD::Base
  include JSON::Serializable

  object_type "callServer"

  object_attributes({
    id: String,
    ip_address: String,
    ipv6_address: String,
    port: Int32,
    type: TD::CallServerType
  })
end