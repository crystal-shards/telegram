# Contains an HTTPS link to a message in a supergroup or channel
#
# @link : String (Message link)
# @is_public : Bool (True, if the link will work for non-members of the chat)

class TD::MessageLink < TD::Base
  include JSON::Serializable

  object_type "messageLink"

  object_attributes({
    link: String,
    is_public: Bool
  })
end