# Represents result of checking whether the current session can be used to transfer a chat ownership to another user
#
# Ok: The session can be used
# PasswordNeeded: The 2-step verification needs to be enabled first
# PasswordTooFresh: The 2-step verification was enabled recently, user needs to wait
# SessionTooFresh: The session was created recently, user needs to wait

abstract class TD::CanTransferOwnershipResult < TD::Base
end