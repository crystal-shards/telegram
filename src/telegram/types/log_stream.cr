# Describes a stream to which TDLib internal log is written
#
# Default: The log is written to stderr or an OS specific log
# File: The log is written to a file
# Empty: The log is written nowhere

abstract class TD::LogStream < TD::Base
end