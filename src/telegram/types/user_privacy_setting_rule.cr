# Represents a single rule for managing privacy settings
#
# AllowAll: A rule to allow all users to do something
# AllowContacts: A rule to allow all of a user's contacts to do something
# AllowUsers: A rule to allow certain specified users to do something
# AllowChatMembers: A rule to allow all members of certain specified basic groups and supergroups to doing something
# RestrictAll: A rule to restrict all users from doing something
# RestrictContacts: A rule to restrict all contacts of a user from doing something
# RestrictUsers: A rule to restrict all specified users from doing something
# RestrictChatMembers: A rule to restrict all members of specified basic groups and supergroups from doing something

abstract class TD::UserPrivacySettingRule < TD::Base
end