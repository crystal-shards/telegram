# The message Time To Live setting for a chat was changed
#
# @chat_id : Int64 (Chat identifier)
# @message_ttl : Int32 (New value of message_ttl)

class TD::Update::ChatMessageTtl < TD::Update
  include JSON::Serializable

  object_type "updateChatMessageTtl"

  object_attributes({
    chat_id: Int64,
    message_ttl: Int32
  })
end