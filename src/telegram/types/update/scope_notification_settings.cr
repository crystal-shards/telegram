# Notification settings for some type of chats were updated
#
# @scope : TD::NotificationSettingsScope (Types of chats for which notification settings were updated)
# @notification_settings : TD::ScopeNotificationSettings (The new notification settings)

class TD::Update::ScopeNotificationSettings < TD::Update
  include JSON::Serializable

  object_type "updateScopeNotificationSettings"

  object_attributes({
    scope: TD::NotificationSettingsScope,
    notification_settings: TD::ScopeNotificationSettings
  })
end