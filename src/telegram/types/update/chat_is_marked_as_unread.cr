# A chat was marked as unread or was read
#
# @chat_id : Int64 (Chat identifier)
# @is_marked_as_unread : Bool (New value of is_marked_as_unread)

class TD::Update::ChatIsMarkedAsUnread < TD::Update
  include JSON::Serializable

  object_type "updateChatIsMarkedAsUnread"

  object_attributes({
    chat_id: Int64,
    is_marked_as_unread: Bool
  })
end