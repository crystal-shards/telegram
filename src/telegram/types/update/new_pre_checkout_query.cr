# A new incoming pre-checkout query; for bots only. Contains full information about a checkout
#
# @id : String (Unique query identifier)
# @sender_user_id : Int64 (Identifier of the user who sent the query)
# @currency : String (Currency for the product price)
# @total_amount : Int64 (Total price for the product, in the smallest units of the currency)
# @invoice_payload : String (Invoice payload)
# @shipping_option_id : String (Identifier of a shipping option chosen by the user; may be empty if not applicable)
# @order_info : TD::OrderInfo? (Information about the order; may be null)

class TD::Update::NewPreCheckoutQuery < TD::Update
  include JSON::Serializable

  object_type "updateNewPreCheckoutQuery"

  object_attributes({
    id: String,
    sender_user_id: Int64,
    currency: String,
    total_amount: Int64,
    invoice_payload: String,
    shipping_option_id: String,
    order_info: TD::OrderInfo?
  })
end