# The information about interactions with a message has changed
#
# @chat_id : Int64 (Chat identifier)
# @message_id : Int64 (Message identifier)
# @interaction_info : TD::MessageInteractionInfo? (New information about interactions with the message; may be null)

class TD::Update::MessageInteractionInfo < TD::Update
  include JSON::Serializable

  object_type "updateMessageInteractionInfo"

  object_attributes({
    chat_id: Int64,
    message_id: Int64,
    interaction_info: TD::MessageInteractionInfo?
  })
end