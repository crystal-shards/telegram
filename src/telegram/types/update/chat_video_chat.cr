# A chat video chat state has changed
#
# @chat_id : Int64 (Chat identifier)
# @video_chat : TD::VideoChat (New value of video_chat)

class TD::Update::ChatVideoChat < TD::Update
  include JSON::Serializable

  object_type "updateChatVideoChat"

  object_attributes({
    chat_id: Int64,
    video_chat: TD::VideoChat
  })
end