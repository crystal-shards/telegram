# The message content has changed
#
# @chat_id : Int64 (Chat identifier)
# @message_id : Int64 (Message identifier)
# @new_content : TD::MessageContent (New message content)

class TD::Update::MessageContent < TD::Update
  include JSON::Serializable

  object_type "updateMessageContent"

  object_attributes({
    chat_id: Int64,
    message_id: Int64,
    new_content: TD::MessageContent
  })
end