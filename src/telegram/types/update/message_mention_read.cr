# A message with an unread mention was read
#
# @chat_id : Int64 (Chat identifier)
# @message_id : Int64 (Message identifier)
# @unread_mention_count : Int32 (The new number of unread mention messages left in the chat)

class TD::Update::MessageMentionRead < TD::Update
  include JSON::Serializable

  object_type "updateMessageMentionRead"

  object_attributes({
    chat_id: Int64,
    message_id: Int64,
    unread_mention_count: Int32
  })
end