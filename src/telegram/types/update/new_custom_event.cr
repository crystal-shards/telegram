# A new incoming event; for bots only
#
# @event : String (A JSON-serialized event)

class TD::Update::NewCustomEvent < TD::Update
  include JSON::Serializable

  object_type "updateNewCustomEvent"

  object_attributes({
    event: String
  })
end