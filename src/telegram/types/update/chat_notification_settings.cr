# Notification settings for a chat were changed
#
# @chat_id : Int64 (Chat identifier)
# @notification_settings : TD::ChatNotificationSettings (The new notification settings)

class TD::Update::ChatNotificationSettings < TD::Update
  include JSON::Serializable

  object_type "updateChatNotificationSettings"

  object_attributes({
    chat_id: Int64,
    notification_settings: TD::ChatNotificationSettings
  })
end