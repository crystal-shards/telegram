# Information about a group call participant was changed. The updates are sent only after the group call is received through getGroupCall and only if the call is joined or being joined
#
# @group_call_id : Int32 (Identifier of group call)
# @participant : TD::GroupCallParticipant (New data about a participant)

class TD::Update::GroupCallParticipant < TD::Update
  include JSON::Serializable

  object_type "updateGroupCallParticipant"

  object_attributes({
    group_call_id: Int32,
    participant: TD::GroupCallParticipant
  })
end