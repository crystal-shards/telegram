# A new incoming inline query; for bots only
#
# @id : String (Unique query identifier)
# @sender_user_id : Int64 (Identifier of the user who sent the query)
# @user_location : TD::Location? (User location; may be null)
# @chat_type : TD::ChatType? (The type of the chat, from which the query originated; may be null if unknown)
# @query : String (Text of the query)
# @offset : String (Offset of the first entry to return)

class TD::Update::NewInlineQuery < TD::Update
  include JSON::Serializable

  object_type "updateNewInlineQuery"

  object_attributes({
    id: String,
    sender_user_id: Int64,
    user_location: TD::Location?,
    chat_type: TD::ChatType?,
    query: String,
    offset: String
  })
end