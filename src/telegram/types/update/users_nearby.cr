# The list of users nearby has changed. The update is guaranteed to be sent only 60 seconds after a successful searchChatsNearby request
#
# @users_nearby : Array(TD::ChatNearby) (The new list of users nearby)

class TD::Update::UsersNearby < TD::Update
  include JSON::Serializable

  object_type "updateUsersNearby"

  object_attributes({
    users_nearby: Array(TD::ChatNearby)
  })
end