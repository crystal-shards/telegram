# The list of saved animations was updated
#
# @animation_ids : Array(Int32) (The new list of file identifiers of saved animations)

class TD::Update::SavedAnimations < TD::Update
  include JSON::Serializable

  object_type "updateSavedAnimations"

  object_attributes({
    animation_ids: Array(Int32)
  })
end