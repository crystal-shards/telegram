# The message sender that is selected to send messages in a chat has changed
#
# @chat_id : Int64 (Chat identifier)
# @message_sender_id : TD::MessageSender? (New value of message_sender_id; may be null if the user can't change message sender)

class TD::Update::ChatMessageSender < TD::Update
  include JSON::Serializable

  object_type "updateChatMessageSender"

  object_attributes({
    chat_id: Int64,
    message_sender_id: TD::MessageSender?
  })
end