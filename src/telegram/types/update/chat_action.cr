# A message sender activity in the chat has changed
#
# @chat_id : Int64 (Chat identifier)
# @message_thread_id : Int64 (If not 0, a message thread identifier in which the action was performed)
# @sender_id : TD::MessageSender (Identifier of a message sender performing the action)
# @action : TD::ChatAction (The action)

class TD::Update::ChatAction < TD::Update
  include JSON::Serializable

  object_type "updateChatAction"

  object_attributes({
    chat_id: Int64,
    message_thread_id: Int64,
    sender_id: TD::MessageSender,
    action: TD::ChatAction
  })
end