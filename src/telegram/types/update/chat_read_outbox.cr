# Outgoing messages were read
#
# @chat_id : Int64 (Chat identifier)
# @last_read_outbox_message_id : Int64 (Identifier of last read outgoing message)

class TD::Update::ChatReadOutbox < TD::Update
  include JSON::Serializable

  object_type "updateChatReadOutbox"

  object_attributes({
    chat_id: Int64,
    last_read_outbox_message_id: Int64
  })
end