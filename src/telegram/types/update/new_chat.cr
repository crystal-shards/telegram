# A new chat has been loaded
#
# @chat : TD::Chat (The chat)

class TD::Update::NewChat < TD::Update
  include JSON::Serializable

  object_type "updateNewChat"

  object_attributes({
    chat: TD::Chat
  })
end