# New terms of service must be accepted by the user. If the terms of service are declined, then the deleteAccount method must be called with the reason "Decline ToS update"
#
# @terms_of_service_id : String (Identifier of the terms of service)
# @terms_of_service : TD::TermsOfService (The new terms of service)

class TD::Update::TermsOfService < TD::Update
  include JSON::Serializable

  object_type "updateTermsOfService"

  object_attributes({
    terms_of_service_id: String,
    terms_of_service: TD::TermsOfService
  })
end