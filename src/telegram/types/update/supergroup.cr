# Some data of a supergroup or a channel has changed. This update is guaranteed to come before the supergroup identifier is returned to the application
#
# @supergroup : TD::Supergroup (New data about the supergroup)

class TD::Update::Supergroup < TD::Update
  include JSON::Serializable

  object_type "updateSupergroup"

  object_attributes({
    supergroup: TD::Supergroup
  })
end