# Describes whether there are some pending notification updates. Can be used to prevent application from killing, while there are some pending notifications
#
# @have_delayed_notifications : Bool (True, if there are some delayed notification updates, which will be sent soon)
# @have_unreceived_notifications : Bool (True, if there can be some yet unreceived notifications, which are being fetched from the server)

class TD::Update::HavePendingNotifications < TD::Update
  include JSON::Serializable

  object_type "updateHavePendingNotifications"

  object_attributes({
    have_delayed_notifications: Bool,
    have_unreceived_notifications: Bool
  })
end