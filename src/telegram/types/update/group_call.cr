# Information about a group call was updated
#
# @group_call : TD::GroupCall (New data about a group call)

class TD::Update::GroupCall < TD::Update
  include JSON::Serializable

  object_type "updateGroupCall"

  object_attributes({
    group_call: TD::GroupCall
  })
end