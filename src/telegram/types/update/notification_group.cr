# A list of active notifications in a notification group has changed
#
# @notification_group_id : Int32 (Unique notification group identifier)
# @type : TD::NotificationGroupType (New type of the notification group)
# @chat_id : Int64 (Identifier of a chat to which all notifications in the group belong)
# @notification_settings_chat_id : Int64 (Chat identifier, which notification settings must be applied to the added notifications)
# @is_silent : Bool (True, if the notifications must be shown without sound)
# @total_count : Int32 (Total number of unread notifications in the group, can be bigger than number of active notifications)
# @added_notifications : Array(TD::Notification) (List of added group notifications, sorted by notification ID)
# @removed_notification_ids : Array(Int32) (Identifiers of removed group notifications, sorted by notification ID)

class TD::Update::NotificationGroup < TD::Update
  include JSON::Serializable

  object_type "updateNotificationGroup"

  object_attributes({
    notification_group_id: Int32,
    type: TD::NotificationGroupType,
    chat_id: Int64,
    notification_settings_chat_id: Int64,
    is_silent: Bool,
    total_count: Int32,
    added_notifications: Array(TD::Notification),
    removed_notification_ids: Array(Int32)
  })
end