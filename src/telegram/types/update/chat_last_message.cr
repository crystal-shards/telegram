# The last message of a chat was changed. If last_message is null, then the last message in the chat became unknown. Some new unknown messages might be added to the chat in this case
#
# @chat_id : Int64 (Chat identifier)
# @last_message : TD::Message? (The new last message in the chat; may be null)
# @positions : Array(TD::ChatPosition) (The new chat positions in the chat lists)

class TD::Update::ChatLastMessage < TD::Update
  include JSON::Serializable

  object_type "updateChatLastMessage"

  object_attributes({
    chat_id: Int64,
    last_message: TD::Message?,
    positions: Array(TD::ChatPosition)
  })
end