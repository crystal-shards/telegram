# The selected background has changed
#
# @for_dark_theme : Bool (True, if background for dark theme has changed)
# @background : TD::Background? (The new selected background; may be null)

class TD::Update::SelectedBackground < TD::Update
  include JSON::Serializable

  object_type "updateSelectedBackground"

  object_attributes({
    for_dark_theme: Bool,
    background: TD::Background?
  })
end