# A message was edited. Changes in the message content will come in a separate updateMessageContent
#
# @chat_id : Int64 (Chat identifier)
# @message_id : Int64 (Message identifier)
# @edit_date : Int32 (Point in time (Unix timestamp) when the message was edited)
# @reply_markup : TD::ReplyMarkup? (New message reply markup; may be null)

class TD::Update::MessageEdited < TD::Update
  include JSON::Serializable

  object_type "updateMessageEdited"

  object_attributes({
    chat_id: Int64,
    message_id: Int64,
    edit_date: Int32,
    reply_markup: TD::ReplyMarkup?
  })
end