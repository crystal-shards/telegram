# New call signaling data arrived
#
# @call_id : Int32 (The call identifier)
# @data : String (The data)

class TD::Update::NewCallSignalingData < TD::Update
  include JSON::Serializable

  object_type "updateNewCallSignalingData"

  object_attributes({
    call_id: Int32,
    data: String
  })
end