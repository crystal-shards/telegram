# The list of favorite stickers was updated
#
# @sticker_ids : Array(Int32) (The new list of file identifiers of favorite stickers)

class TD::Update::FavoriteStickers < TD::Update
  include JSON::Serializable

  object_type "updateFavoriteStickers"

  object_attributes({
    sticker_ids: Array(Int32)
  })
end