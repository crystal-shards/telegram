# Number of unread messages in a chat list has changed. This update is sent only if the message database is used
#
# @chat_list : TD::ChatList (The chat list with changed number of unread messages)
# @unread_count : Int32 (Total number of unread messages)
# @unread_unmuted_count : Int32 (Total number of unread messages in unmuted chats)

class TD::Update::UnreadMessageCount < TD::Update
  include JSON::Serializable

  object_type "updateUnreadMessageCount"

  object_attributes({
    chat_list: TD::ChatList,
    unread_count: Int32,
    unread_unmuted_count: Int32
  })
end