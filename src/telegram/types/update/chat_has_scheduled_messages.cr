# A chat's has_scheduled_messages field has changed
#
# @chat_id : Int64 (Chat identifier)
# @has_scheduled_messages : Bool (New value of has_scheduled_messages)

class TD::Update::ChatHasScheduledMessages < TD::Update
  include JSON::Serializable

  object_type "updateChatHasScheduledMessages"

  object_attributes({
    chat_id: Int64,
    has_scheduled_messages: Bool
  })
end