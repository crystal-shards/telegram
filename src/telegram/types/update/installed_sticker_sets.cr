# The list of installed sticker sets was updated
#
# @is_masks : Bool (True, if the list of installed mask sticker sets was updated)
# @sticker_set_ids : Array(String) (The new list of installed ordinary sticker sets)

class TD::Update::InstalledStickerSets < TD::Update
  include JSON::Serializable

  object_type "updateInstalledStickerSets"

  object_attributes({
    is_masks: Bool,
    sticker_set_ids: Array(String)
  })
end