# A message has been successfully sent
#
# @message : TD::Message (The sent message. Usually only the message identifier, date, and content are changed, but almost all other fields can also change)
# @old_message_id : Int64 (The previous temporary message identifier)

class TD::Update::MessageSendSucceeded < TD::Update
  include JSON::Serializable

  object_type "updateMessageSendSucceeded"

  object_attributes({
    message: TD::Message,
    old_message_id: Int64
  })
end