# Number of unread chats, i.e. with unread messages or marked as unread, has changed. This update is sent only if the message database is used
#
# @chat_list : TD::ChatList (The chat list with changed number of unread messages)
# @total_count : Int32 (Approximate total number of chats in the chat list)
# @unread_count : Int32 (Total number of unread chats)
# @unread_unmuted_count : Int32 (Total number of unread unmuted chats)
# @marked_as_unread_count : Int32 (Total number of chats marked as unread)
# @marked_as_unread_unmuted_count : Int32 (Total number of unmuted chats marked as unread)

class TD::Update::UnreadChatCount < TD::Update
  include JSON::Serializable

  object_type "updateUnreadChatCount"

  object_attributes({
    chat_list: TD::ChatList,
    total_count: Int32,
    unread_count: Int32,
    unread_unmuted_count: Int32,
    marked_as_unread_count: Int32,
    marked_as_unread_unmuted_count: Int32
  })
end