# A user sent a join request to a chat; for bots only
#
# @chat_id : Int64 (Chat identifier)
# @request : TD::ChatJoinRequest (Join request)
# @invite_link : TD::ChatInviteLink? (The invite link, which was used to send join request; may be null)

class TD::Update::NewChatJoinRequest < TD::Update
  include JSON::Serializable

  object_type "updateNewChatJoinRequest"

  object_attributes({
    chat_id: Int64,
    request: TD::ChatJoinRequest,
    invite_link: TD::ChatInviteLink?
  })
end