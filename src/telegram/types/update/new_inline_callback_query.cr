# A new incoming callback query from a message sent via a bot; for bots only
#
# @id : String (Unique query identifier)
# @sender_user_id : Int64 (Identifier of the user who sent the query)
# @inline_message_id : String (Identifier of the inline message, from which the query originated)
# @chat_instance : String (An identifier uniquely corresponding to the chat a message was sent to)
# @payload : TD::CallbackQueryPayload (Query payload)

class TD::Update::NewInlineCallbackQuery < TD::Update
  include JSON::Serializable

  object_type "updateNewInlineCallbackQuery"

  object_attributes({
    id: String,
    sender_user_id: Int64,
    inline_message_id: String,
    chat_instance: String,
    payload: TD::CallbackQueryPayload
  })
end