# Some data in userFullInfo has been changed
#
# @user_id : Int64 (User identifier)
# @user_full_info : TD::UserFullInfo (New full information about the user)

class TD::Update::UserFullInfo < TD::Update
  include JSON::Serializable

  object_type "updateUserFullInfo"

  object_attributes({
    user_id: Int64,
    user_full_info: TD::UserFullInfo
  })
end