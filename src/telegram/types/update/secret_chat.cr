# Some data of a secret chat has changed. This update is guaranteed to come before the secret chat identifier is returned to the application
#
# @secret_chat : TD::SecretChat (New data about the secret chat)

class TD::Update::SecretChat < TD::Update
  include JSON::Serializable

  object_type "updateSecretChat"

  object_attributes({
    secret_chat: TD::SecretChat
  })
end