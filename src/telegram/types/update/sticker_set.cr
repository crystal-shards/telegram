# A sticker set has changed
#
# @sticker_set : TD::StickerSet (The sticker set)

class TD::Update::StickerSet < TD::Update
  include JSON::Serializable

  object_type "updateStickerSet"

  object_attributes({
    sticker_set: TD::StickerSet
  })
end