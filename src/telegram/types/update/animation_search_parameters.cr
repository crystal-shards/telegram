# The parameters of animation search through GetOption("animation_search_bot_username") bot has changed
#
# @provider : String (Name of the animation search provider)
# @emojis : Array(String) (The new list of emojis suggested for searching)

class TD::Update::AnimationSearchParameters < TD::Update
  include JSON::Serializable

  object_type "updateAnimationSearchParameters"

  object_attributes({
    provider: String,
    emojis: Array(String)
  })
end