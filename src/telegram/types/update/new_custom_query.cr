# A new incoming query; for bots only
#
# @id : String (The query identifier)
# @data : String (JSON-serialized query data)
# @timeout : Int32 (Query timeout)

class TD::Update::NewCustomQuery < TD::Update
  include JSON::Serializable

  object_type "updateNewCustomQuery"

  object_attributes({
    id: String,
    data: String,
    timeout: Int32
  })
end