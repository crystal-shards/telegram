# The list of available chat themes has changed
#
# @chat_themes : Array(TD::ChatTheme) (The new list of chat themes)

class TD::Update::ChatThemes < TD::Update
  include JSON::Serializable

  object_type "updateChatThemes"

  object_attributes({
    chat_themes: Array(TD::ChatTheme)
  })
end