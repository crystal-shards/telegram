# A service notification from the server was received. Upon receiving this the application must show a popup with the content of the notification
#
# @type : String (Notification type. If type begins with "AUTH_KEY_DROP_", then two buttons "Cancel" and "Log out" must be shown under notification; if user presses the second, all local data must be destroyed using Destroy method)
# @content : TD::MessageContent (Notification content)

class TD::Update::ServiceNotification < TD::Update
  include JSON::Serializable

  object_type "updateServiceNotification"

  object_attributes({
    type: String,
    content: TD::MessageContent
  })
end