# A poll was updated; for bots only
#
# @poll : TD::Poll (New data about the poll)

class TD::Update::Poll < TD::Update
  include JSON::Serializable

  object_type "updatePoll"

  object_attributes({
    poll: TD::Poll
  })
end