# The list of recently used stickers was updated
#
# @is_attached : Bool (True, if the list of stickers attached to photo or video files was updated, otherwise the list of sent stickers is updated)
# @sticker_ids : Array(Int32) (The new list of file identifiers of recently used stickers)

class TD::Update::RecentStickers < TD::Update
  include JSON::Serializable

  object_type "updateRecentStickers"

  object_attributes({
    is_attached: Bool,
    sticker_ids: Array(Int32)
  })
end