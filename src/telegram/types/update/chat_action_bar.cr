# The chat action bar was changed
#
# @chat_id : Int64 (Chat identifier)
# @action_bar : TD::ChatActionBar? (The new value of the action bar; may be null)

class TD::Update::ChatActionBar < TD::Update
  include JSON::Serializable

  object_type "updateChatActionBar"

  object_attributes({
    chat_id: Int64,
    action_bar: TD::ChatActionBar?
  })
end