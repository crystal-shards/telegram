# A message failed to send. Be aware that some messages being sent can be irrecoverably deleted, in which case updateDeleteMessages will be received instead of this update
#
# @message : TD::Message (The failed to send message)
# @old_message_id : Int64 (The previous temporary message identifier)
# @error_code : Int32 (An error code)
# @error_message : String (Error message)

class TD::Update::MessageSendFailed < TD::Update
  include JSON::Serializable

  object_type "updateMessageSendFailed"

  object_attributes({
    message: TD::Message,
    old_message_id: Int64,
    error_code: Int32,
    error_message: String
  })
end