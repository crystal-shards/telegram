# The default chat reply markup was changed. Can occur because new messages with reply markup were received or because an old reply markup was hidden by the user
#
# @chat_id : Int64 (Chat identifier)
# @reply_markup_message_id : Int64 (Identifier of the message from which reply markup needs to be used; 0 if there is no default custom reply markup in the chat)

class TD::Update::ChatReplyMarkup < TD::Update
  include JSON::Serializable

  object_type "updateChatReplyMarkup"

  object_attributes({
    chat_id: Int64,
    reply_markup_message_id: Int64
  })
end