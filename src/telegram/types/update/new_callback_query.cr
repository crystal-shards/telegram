# A new incoming callback query; for bots only
#
# @id : String (Unique query identifier)
# @sender_user_id : Int64 (Identifier of the user who sent the query)
# @chat_id : Int64 (Identifier of the chat where the query was sent)
# @message_id : Int64 (Identifier of the message, from which the query originated)
# @chat_instance : String (Identifier that uniquely corresponds to the chat to which the message was sent)
# @payload : TD::CallbackQueryPayload (Query payload)

class TD::Update::NewCallbackQuery < TD::Update
  include JSON::Serializable

  object_type "updateNewCallbackQuery"

  object_attributes({
    id: String,
    sender_user_id: Int64,
    chat_id: Int64,
    message_id: Int64,
    chat_instance: String,
    payload: TD::CallbackQueryPayload
  })
end