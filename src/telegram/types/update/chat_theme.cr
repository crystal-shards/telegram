# The chat theme was changed
#
# @chat_id : Int64 (Chat identifier)
# @theme_name : String (The new name of the chat theme; may be empty if theme was reset to default)

class TD::Update::ChatTheme < TD::Update
  include JSON::Serializable

  object_type "updateChatTheme"

  object_attributes({
    chat_id: Int64,
    theme_name: String
  })
end