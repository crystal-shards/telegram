# The user authorization state has changed
#
# @authorization_state : TD::AuthorizationState (New authorization state)

class TD::Update::AuthorizationState < TD::Update
  include JSON::Serializable

  object_type "updateAuthorizationState"

  object_attributes({
    authorization_state: TD::AuthorizationState
  })
end