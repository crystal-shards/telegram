# The position of a chat in a chat list has changed. Instead of this update updateChatLastMessage or updateChatDraftMessage might be sent
#
# @chat_id : Int64 (Chat identifier)
# @position : TD::ChatPosition (New chat position. If new order is 0, then the chat needs to be removed from the list)

class TD::Update::ChatPosition < TD::Update
  include JSON::Serializable

  object_type "updateChatPosition"

  object_attributes({
    chat_id: Int64,
    position: TD::ChatPosition
  })
end