# The chat unread_mention_count has changed
#
# @chat_id : Int64 (Chat identifier)
# @unread_mention_count : Int32 (The number of unread mention messages left in the chat)

class TD::Update::ChatUnreadMentionCount < TD::Update
  include JSON::Serializable

  object_type "updateChatUnreadMentionCount"

  object_attributes({
    chat_id: Int64,
    unread_mention_count: Int32
  })
end