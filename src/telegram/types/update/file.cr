# Information about a file was updated
#
# @file : TD::File (New data about the file)

class TD::Update::File < TD::Update
  include JSON::Serializable

  object_type "updateFile"

  object_attributes({
    file: TD::File
  })
end