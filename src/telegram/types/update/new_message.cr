# A new message was received; can also be an outgoing message
#
# @message : TD::Message (The new message)

class TD::Update::NewMessage < TD::Update
  include JSON::Serializable

  object_type "updateNewMessage"

  object_attributes({
    message: TD::Message
  })
end