# The user went online or offline
#
# @user_id : Int64 (User identifier)
# @status : TD::UserStatus (New status of the user)

class TD::Update::UserStatus < TD::Update
  include JSON::Serializable

  object_type "updateUserStatus"

  object_attributes({
    user_id: Int64,
    status: TD::UserStatus
  })
end