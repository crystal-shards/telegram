# Chat permissions was changed
#
# @chat_id : Int64 (Chat identifier)
# @permissions : TD::ChatPermissions (The new chat permissions)

class TD::Update::ChatPermissions < TD::Update
  include JSON::Serializable

  object_type "updateChatPermissions"

  object_attributes({
    chat_id: Int64,
    permissions: TD::ChatPermissions
  })
end