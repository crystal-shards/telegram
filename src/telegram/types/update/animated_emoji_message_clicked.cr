# Some animated emoji message was clicked and a big animated sticker must be played if the message is visible on the screen. chatActionWatchingAnimations with the text of the message needs to be sent if the sticker is played
#
# @chat_id : Int64 (Chat identifier)
# @message_id : Int64 (Message identifier)
# @sticker : TD::Sticker (The animated sticker to be played)

class TD::Update::AnimatedEmojiMessageClicked < TD::Update
  include JSON::Serializable

  object_type "updateAnimatedEmojiMessageClicked"

  object_attributes({
    chat_id: Int64,
    message_id: Int64,
    sticker: TD::Sticker
  })
end