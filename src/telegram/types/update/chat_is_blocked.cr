# A chat was blocked or unblocked
#
# @chat_id : Int64 (Chat identifier)
# @is_blocked : Bool (New value of is_blocked)

class TD::Update::ChatIsBlocked < TD::Update
  include JSON::Serializable

  object_type "updateChatIsBlocked"

  object_attributes({
    chat_id: Int64,
    is_blocked: Bool
  })
end