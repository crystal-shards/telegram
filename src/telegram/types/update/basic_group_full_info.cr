# Some data in basicGroupFullInfo has been changed
#
# @basic_group_id : Int64 (Identifier of a basic group)
# @basic_group_full_info : TD::BasicGroupFullInfo (New full information about the group)

class TD::Update::BasicGroupFullInfo < TD::Update
  include JSON::Serializable

  object_type "updateBasicGroupFullInfo"

  object_attributes({
    basic_group_id: Int64,
    basic_group_full_info: TD::BasicGroupFullInfo
  })
end