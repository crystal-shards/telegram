# A message with a live location was viewed. When the update is received, the application is supposed to update the live location
#
# @chat_id : Int64 (Identifier of the chat with the live location message)
# @message_id : Int64 (Identifier of the message with live location)

class TD::Update::MessageLiveLocationViewed < TD::Update
  include JSON::Serializable

  object_type "updateMessageLiveLocationViewed"

  object_attributes({
    chat_id: Int64,
    message_id: Int64
  })
end