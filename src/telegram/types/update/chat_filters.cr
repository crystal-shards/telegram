# The list of chat filters or a chat filter has changed
#
# @chat_filters : Array(TD::ChatFilterInfo) (The new list of chat filters)

class TD::Update::ChatFilters < TD::Update
  include JSON::Serializable

  object_type "updateChatFilters"

  object_attributes({
    chat_filters: Array(TD::ChatFilterInfo)
  })
end