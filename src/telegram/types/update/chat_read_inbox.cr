# Incoming messages were read or the number of unread messages has been changed
#
# @chat_id : Int64 (Chat identifier)
# @last_read_inbox_message_id : Int64 (Identifier of the last read incoming message)
# @unread_count : Int32 (The number of unread messages left in the chat)

class TD::Update::ChatReadInbox < TD::Update
  include JSON::Serializable

  object_type "updateChatReadInbox"

  object_attributes({
    chat_id: Int64,
    last_read_inbox_message_id: Int64,
    unread_count: Int32
  })
end