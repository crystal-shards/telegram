# User rights changed in a chat; for bots only
#
# @chat_id : Int64 (Chat identifier)
# @actor_user_id : Int64 (Identifier of the user, changing the rights)
# @date : Int32 (Point in time (Unix timestamp) when the user rights was changed)
# @invite_link : TD::ChatInviteLink? (If user has joined the chat using an invite link, the invite link; may be null)
# @old_chat_member : TD::ChatMember (Previous chat member)
# @new_chat_member : TD::ChatMember (New chat member)

class TD::Update::ChatMember < TD::Update
  include JSON::Serializable

  object_type "updateChatMember"

  object_attributes({
    chat_id: Int64,
    actor_user_id: Int64,
    date: Int32,
    invite_link: TD::ChatInviteLink?,
    old_chat_member: TD::ChatMember,
    new_chat_member: TD::ChatMember
  })
end