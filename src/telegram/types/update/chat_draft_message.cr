# A chat draft has changed. Be aware that the update may come in the currently opened chat but with old content of the draft. If the user has changed the content of the draft, this update mustn't be applied
#
# @chat_id : Int64 (Chat identifier)
# @draft_message : TD::DraftMessage? (The new draft message; may be null)
# @positions : Array(TD::ChatPosition) (The new chat positions in the chat lists)

class TD::Update::ChatDraftMessage < TD::Update
  include JSON::Serializable

  object_type "updateChatDraftMessage"

  object_attributes({
    chat_id: Int64,
    draft_message: TD::DraftMessage?,
    positions: Array(TD::ChatPosition)
  })
end