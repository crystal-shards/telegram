# The chat pending join requests were changed
#
# @chat_id : Int64 (Chat identifier)
# @pending_join_requests : TD::ChatJoinRequestsInfo? (The new data about pending join requests; may be null)

class TD::Update::ChatPendingJoinRequests < TD::Update
  include JSON::Serializable

  object_type "updateChatPendingJoinRequests"

  object_attributes({
    chat_id: Int64,
    pending_join_requests: TD::ChatJoinRequestsInfo?
  })
end