# The list of supported dice emojis has changed
#
# @emojis : Array(String) (The new list of supported dice emojis)

class TD::Update::DiceEmojis < TD::Update
  include JSON::Serializable

  object_type "updateDiceEmojis"

  object_attributes({
    emojis: Array(String)
  })
end