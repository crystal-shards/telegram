# The list of trending sticker sets was updated or some of them were viewed
#
# @sticker_sets : TD::StickerSets (The prefix of the list of trending sticker sets with the newest trending sticker sets)

class TD::Update::TrendingStickerSets < TD::Update
  include JSON::Serializable

  object_type "updateTrendingStickerSets"

  object_attributes({
    sticker_sets: TD::StickerSets
  })
end