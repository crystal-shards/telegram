# Some data of a user has changed. This update is guaranteed to come before the user identifier is returned to the application
#
# @user : TD::User (New data about the user)

class TD::Update::User < TD::Update
  include JSON::Serializable

  object_type "updateUser"

  object_attributes({
    user: TD::User
  })
end