# New call was created or information about a call was updated
#
# @call : TD::Call (New data about a call)

class TD::Update::Call < TD::Update
  include JSON::Serializable

  object_type "updateCall"

  object_attributes({
    call: TD::Call
  })
end