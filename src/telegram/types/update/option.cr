# An option changed its value
#
# @name : String (The option name)
# @value : TD::OptionValue (The new option value)

class TD::Update::Option < TD::Update
  include JSON::Serializable

  object_type "updateOption"

  object_attributes({
    name: String,
    value: TD::OptionValue
  })
end