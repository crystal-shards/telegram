# The title of a chat was changed
#
# @chat_id : Int64 (Chat identifier)
# @title : String (The new chat title)

class TD::Update::ChatTitle < TD::Update
  include JSON::Serializable

  object_type "updateChatTitle"

  object_attributes({
    chat_id: Int64,
    title: String
  })
end