# A new incoming shipping query; for bots only. Only for invoices with flexible price
#
# @id : String (Unique query identifier)
# @sender_user_id : Int64 (Identifier of the user who sent the query)
# @invoice_payload : String (Invoice payload)
# @shipping_address : TD::Address (User shipping address)

class TD::Update::NewShippingQuery < TD::Update
  include JSON::Serializable

  object_type "updateNewShippingQuery"

  object_attributes({
    id: String,
    sender_user_id: Int64,
    invoice_payload: String,
    shipping_address: TD::Address
  })
end