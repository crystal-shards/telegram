# The user has chosen a result of an inline query; for bots only
#
# @sender_user_id : Int64 (Identifier of the user who sent the query)
# @user_location : TD::Location? (User location; may be null)
# @query : String (Text of the query)
# @result_id : String (Identifier of the chosen result)
# @inline_message_id : String (Identifier of the sent inline message, if known)

class TD::Update::NewChosenInlineResult < TD::Update
  include JSON::Serializable

  object_type "updateNewChosenInlineResult"

  object_attributes({
    sender_user_id: Int64,
    user_location: TD::Location?,
    query: String,
    result_id: String,
    inline_message_id: String
  })
end