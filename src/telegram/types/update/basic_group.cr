# Some data of a basic group has changed. This update is guaranteed to come before the basic group identifier is returned to the application
#
# @basic_group : TD::BasicGroup (New data about the group)

class TD::Update::BasicGroup < TD::Update
  include JSON::Serializable

  object_type "updateBasicGroup"

  object_attributes({
    basic_group: TD::BasicGroup
  })
end