# A chat photo was changed
#
# @chat_id : Int64 (Chat identifier)
# @photo : TD::ChatPhotoInfo? (The new chat photo; may be null)

class TD::Update::ChatPhoto < TD::Update
  include JSON::Serializable

  object_type "updateChatPhoto"

  object_attributes({
    chat_id: Int64,
    photo: TD::ChatPhotoInfo?
  })
end