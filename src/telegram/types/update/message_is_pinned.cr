# The message pinned state was changed
#
# @chat_id : Int64 (Chat identifier)
# @message_id : Int64 (The message identifier)
# @is_pinned : Bool (True, if the message is pinned)

class TD::Update::MessageIsPinned < TD::Update
  include JSON::Serializable

  object_type "updateMessageIsPinned"

  object_attributes({
    chat_id: Int64,
    message_id: Int64,
    is_pinned: Bool
  })
end