# The connection state has changed. This update must be used only to show a human-readable description of the connection state
#
# @state : TD::ConnectionState (The new connection state)

class TD::Update::ConnectionState < TD::Update
  include JSON::Serializable

  object_type "updateConnectionState"

  object_attributes({
    state: TD::ConnectionState
  })
end