# A chat content was allowed or restricted for saving
#
# @chat_id : Int64 (Chat identifier)
# @has_protected_content : Bool (New value of has_protected_content)

class TD::Update::ChatHasProtectedContent < TD::Update
  include JSON::Serializable

  object_type "updateChatHasProtectedContent"

  object_attributes({
    chat_id: Int64,
    has_protected_content: Bool
  })
end