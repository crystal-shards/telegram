# A notification was changed
#
# @notification_group_id : Int32 (Unique notification group identifier)
# @notification : TD::Notification (Changed notification)

class TD::Update::Notification < TD::Update
  include JSON::Serializable

  object_type "updateNotification"

  object_attributes({
    notification_group_id: Int32,
    notification: TD::Notification
  })
end