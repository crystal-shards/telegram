# Some privacy setting rules have been changed
#
# @setting : TD::UserPrivacySetting (The privacy setting)
# @rules : TD::UserPrivacySettingRules (New privacy rules)

class TD::Update::UserPrivacySettingRules < TD::Update
  include JSON::Serializable

  object_type "updateUserPrivacySettingRules"

  object_attributes({
    setting: TD::UserPrivacySetting,
    rules: TD::UserPrivacySettingRules
  })
end