# Some messages were deleted
#
# @chat_id : Int64 (Chat identifier)
# @message_ids : Array(Int64) (Identifiers of the deleted messages)
# @is_permanent : Bool (True, if the messages are permanently deleted by a user (as opposed to just becoming inaccessible))
# @from_cache : Bool (True, if the messages are deleted only from the cache and can possibly be retrieved again in the future)

class TD::Update::DeleteMessages < TD::Update
  include JSON::Serializable

  object_type "updateDeleteMessages"

  object_attributes({
    chat_id: Int64,
    message_ids: Array(Int64),
    is_permanent: Bool,
    from_cache: Bool
  })
end