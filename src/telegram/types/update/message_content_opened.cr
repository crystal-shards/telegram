# The message content was opened. Updates voice note messages to "listened", video note messages to "viewed" and starts the TTL timer for self-destructing messages
#
# @chat_id : Int64 (Chat identifier)
# @message_id : Int64 (Message identifier)

class TD::Update::MessageContentOpened < TD::Update
  include JSON::Serializable

  object_type "updateMessageContentOpened"

  object_attributes({
    chat_id: Int64,
    message_id: Int64
  })
end