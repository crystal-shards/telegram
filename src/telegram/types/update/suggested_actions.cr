# The list of suggested to the user actions has changed
#
# @added_actions : Array(TD::SuggestedAction) (Added suggested actions)
# @removed_actions : Array(TD::SuggestedAction) (Removed suggested actions)

class TD::Update::SuggestedActions < TD::Update
  include JSON::Serializable

  object_type "updateSuggestedActions"

  object_attributes({
    added_actions: Array(TD::SuggestedAction),
    removed_actions: Array(TD::SuggestedAction)
  })
end