# File generation is no longer needed
#
# @generation_id : String (Unique identifier for the generation process)

class TD::Update::FileGenerationStop < TD::Update
  include JSON::Serializable

  object_type "updateFileGenerationStop"

  object_attributes({
    generation_id: String
  })
end