# A user changed the answer to a poll; for bots only
#
# @poll_id : String (Unique poll identifier)
# @user_id : Int64 (The user, who changed the answer to the poll)
# @option_ids : Array(Int32) (0-based identifiers of answer options, chosen by the user)

class TD::Update::PollAnswer < TD::Update
  include JSON::Serializable

  object_type "updatePollAnswer"

  object_attributes({
    poll_id: String,
    user_id: Int64,
    option_ids: Array(Int32)
  })
end