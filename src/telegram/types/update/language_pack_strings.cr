# Some language pack strings have been updated
#
# @localization_target : String (Localization target to which the language pack belongs)
# @language_pack_id : String (Identifier of the updated language pack)
# @strings : Array(TD::LanguagePackString) (List of changed language pack strings)

class TD::Update::LanguagePackStrings < TD::Update
  include JSON::Serializable

  object_type "updateLanguagePackStrings"

  object_attributes({
    localization_target: String,
    language_pack_id: String,
    strings: Array(TD::LanguagePackString)
  })
end