# Contains active notifications that was shown on previous application launches. This update is sent only if the message database is used. In that case it comes once before any updateNotification and updateNotificationGroup update
#
# @groups : Array(TD::NotificationGroup) (Lists of active notification groups)

class TD::Update::ActiveNotifications < TD::Update
  include JSON::Serializable

  object_type "updateActiveNotifications"

  object_attributes({
    groups: Array(TD::NotificationGroup)
  })
end