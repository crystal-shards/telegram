# Some data in supergroupFullInfo has been changed
#
# @supergroup_id : Int64 (Identifier of the supergroup or channel)
# @supergroup_full_info : TD::SupergroupFullInfo (New full information about the supergroup)

class TD::Update::SupergroupFullInfo < TD::Update
  include JSON::Serializable

  object_type "updateSupergroupFullInfo"

  object_attributes({
    supergroup_id: Int64,
    supergroup_full_info: TD::SupergroupFullInfo
  })
end