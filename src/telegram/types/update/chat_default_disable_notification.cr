# The value of the default disable_notification parameter, used when a message is sent to the chat, was changed
#
# @chat_id : Int64 (Chat identifier)
# @default_disable_notification : Bool (The new default_disable_notification value)

class TD::Update::ChatDefaultDisableNotification < TD::Update
  include JSON::Serializable

  object_type "updateChatDefaultDisableNotification"

  object_attributes({
    chat_id: Int64,
    default_disable_notification: Bool
  })
end