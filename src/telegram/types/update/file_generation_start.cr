# The file generation process needs to be started by the application
#
# @generation_id : String (Unique identifier for the generation process)
# @original_path : String (The path to a file from which a new file is generated; may be empty)
# @destination_path : String (The path to a file that must be created and where the new file is generated)
# @conversion : String (String specifying the conversion applied to the original file. If conversion is ")

class TD::Update::FileGenerationStart < TD::Update
  include JSON::Serializable

  object_type "updateFileGenerationStart"

  object_attributes({
    generation_id: String,
    original_path: String,
    destination_path: String,
    conversion: String
  })
end