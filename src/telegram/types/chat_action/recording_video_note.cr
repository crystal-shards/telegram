# The user is recording a video note
#

class TD::ChatAction::RecordingVideoNote < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionRecordingVideoNote"
end