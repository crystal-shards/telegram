# The user is uploading a document
#
# @progress : Int32 (Upload progress, as a percentage)

class TD::ChatAction::UploadingDocument < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionUploadingDocument"

  object_attributes({
    progress: Int32
  })
end