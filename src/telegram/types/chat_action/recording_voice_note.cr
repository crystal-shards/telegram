# The user is recording a voice note
#

class TD::ChatAction::RecordingVoiceNote < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionRecordingVoiceNote"
end