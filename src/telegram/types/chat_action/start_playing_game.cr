# The user has started to play a game
#

class TD::ChatAction::StartPlayingGame < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionStartPlayingGame"
end