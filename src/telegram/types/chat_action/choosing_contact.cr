# The user is picking a contact to send
#

class TD::ChatAction::ChoosingContact < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionChoosingContact"
end