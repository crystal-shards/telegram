# The user has canceled the previous action
#

class TD::ChatAction::Cancel < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionCancel"
end