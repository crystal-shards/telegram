# The user is picking a location or venue to send
#

class TD::ChatAction::ChoosingLocation < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionChoosingLocation"
end