# The user is picking a sticker to send
#

class TD::ChatAction::ChoosingSticker < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionChoosingSticker"
end