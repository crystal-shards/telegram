# The user is uploading a video note
#
# @progress : Int32 (Upload progress, as a percentage)

class TD::ChatAction::UploadingVideoNote < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionUploadingVideoNote"

  object_attributes({
    progress: Int32
  })
end