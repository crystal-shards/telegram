# The user is watching animations sent by the other party by clicking on an animated emoji
#
# @emoji : String (The animated emoji)

class TD::ChatAction::WatchingAnimations < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionWatchingAnimations"

  object_attributes({
    emoji: String
  })
end