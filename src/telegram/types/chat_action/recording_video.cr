# The user is recording a video
#

class TD::ChatAction::RecordingVideo < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionRecordingVideo"
end