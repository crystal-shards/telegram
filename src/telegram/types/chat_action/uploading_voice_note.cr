# The user is uploading a voice note
#
# @progress : Int32 (Upload progress, as a percentage)

class TD::ChatAction::UploadingVoiceNote < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionUploadingVoiceNote"

  object_attributes({
    progress: Int32
  })
end