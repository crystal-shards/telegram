# The user is uploading a video
#
# @progress : Int32 (Upload progress, as a percentage)

class TD::ChatAction::UploadingVideo < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionUploadingVideo"

  object_attributes({
    progress: Int32
  })
end