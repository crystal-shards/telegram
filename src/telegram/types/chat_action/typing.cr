# The user is typing a message
#

class TD::ChatAction::Typing < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionTyping"
end