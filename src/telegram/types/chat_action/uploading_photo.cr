# The user is uploading a photo
#
# @progress : Int32 (Upload progress, as a percentage)

class TD::ChatAction::UploadingPhoto < TD::ChatAction
  include JSON::Serializable

  object_type "chatActionUploadingPhoto"

  object_attributes({
    progress: Int32
  })
end