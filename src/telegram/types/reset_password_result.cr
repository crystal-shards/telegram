# Represents result of 2-step verification password reset
#
# Ok: The password was reset
# Pending: The password reset request is pending
# Declined: The password reset request was declined

abstract class TD::ResetPasswordResult < TD::Base
end