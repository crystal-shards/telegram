# Represents one member of a JSON object
#
# @key : String (Member's key)
# @value : TD::JsonValue (Member's value)

class TD::JsonObjectMember < TD::Base
  include JSON::Serializable

  object_type "jsonObjectMember"

  object_attributes({
    key: String,
    value: TD::JsonValue
  })
end