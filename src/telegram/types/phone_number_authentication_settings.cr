# Contains settings for the authentication of the user's phone number
#
# @allow_flash_call : Bool (Pass true if the authentication code may be sent via a flash call to the specified phone number)
# @allow_missed_call : Bool (Pass true if the authentication code may be sent via a missed call to the specified phone number)
# @is_current_phone_number : Bool (Pass true if the authenticated phone number is used on the current device)
# @allow_sms_retriever_api : Bool (For official applications only. True, if the application can use Android SMS Retriever API (requires Google Play Services)
# @authentication_tokens : Array(String) (List of up to 20 authentication tokens, recently received in updateOption("authentication_token") in previously logged out sessions)

class TD::PhoneNumberAuthenticationSettings < TD::Base
  include JSON::Serializable

  object_type "phoneNumberAuthenticationSettings"

  object_attributes({
    allow_flash_call: Bool,
    allow_missed_call: Bool,
    is_current_phone_number: Bool,
    allow_sms_retriever_api: Bool,
    authentication_tokens: Array(String)
  })
end