# Contains information about a related article
#
# @url : String (Related article URL)
# @title : String (Article title; may be empty)
# @description : String (Article description; may be empty)
# @photo : TD::Photo? (Article photo; may be null)
# @author : String (Article author; may be empty)
# @publish_date : Int32 (Point in time (Unix timestamp) when the article was published; 0 if unknown)

class TD::PageBlockRelatedArticle < TD::Base
  include JSON::Serializable

  object_type "pageBlockRelatedArticle"

  object_attributes({
    url: String,
    title: String,
    description: String,
    photo: TD::Photo?,
    author: String,
    publish_date: Int32
  })
end