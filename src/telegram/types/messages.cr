# Contains a list of messages
#
# @total_count : Int32 (Approximate total count of messages found)
# @messages : Array(TD::Message)? (List of messages; messages may be null)

class TD::Messages < TD::Base
  include JSON::Serializable

  object_type "messages"

  object_attributes({
    total_count: Int32,
    messages: Array(TD::Message)?
  })
end