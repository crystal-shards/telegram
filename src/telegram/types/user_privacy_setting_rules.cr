# A list of privacy rules. Rules are matched in the specified order. The first matched rule defines the privacy setting for a given user. If no rule matches, the action is not allowed
#
# @rules : Array(TD::UserPrivacySettingRule) (A list of rules)

class TD::UserPrivacySettingRules < TD::Base
  include JSON::Serializable

  object_type "userPrivacySettingRules"

  object_attributes({
    rules: Array(TD::UserPrivacySettingRule)
  })
end