# Contains information about an inline button of type inlineKeyboardButtonTypeLoginUrl
#
# Open: An HTTP url needs to be open
# RequestConfirmation: An authorization confirmation dialog needs to be shown to the user

abstract class TD::LoginUrlInfo < TD::Base
end