# Contains information about the sending state of the message
#
# Pending: The message is being sent now, but has not yet been delivered to the server
# Failed: The message failed to be sent

abstract class TD::MessageSendingState < TD::Base
end