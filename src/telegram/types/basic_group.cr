# Represents a basic group of 0-200 users (must be upgraded to a supergroup to accommodate more than 200 users)
#
# @id : Int64 (Group identifier)
# @member_count : Int32 (Number of members in the group)
# @status : TD::ChatMemberStatus (Status of the current user in the group)
# @is_active : Bool (True, if the group is active)
# @upgraded_to_supergroup_id : Int64 (Identifier of the supergroup to which this group was upgraded; 0 if none)

class TD::BasicGroup < TD::Base
  include JSON::Serializable

  object_type "basicGroup"

  object_attributes({
    id: Int64,
    member_count: Int32,
    status: TD::ChatMemberStatus,
    is_active: Bool,
    upgraded_to_supergroup_id: Int64
  })
end