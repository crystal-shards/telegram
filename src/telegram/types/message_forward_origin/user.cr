# The message was originally sent by a known user
#
# @sender_user_id : Int64 (Identifier of the user that originally sent the message)

class TD::MessageForwardOrigin::User < TD::MessageForwardOrigin
  include JSON::Serializable

  object_type "messageForwardOriginUser"

  object_attributes({
    sender_user_id: Int64
  })
end