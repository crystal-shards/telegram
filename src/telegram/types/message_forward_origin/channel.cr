# The message was originally a post in a channel
#
# @chat_id : Int64 (Identifier of the chat from which the message was originally forwarded)
# @message_id : Int64 (Message identifier of the original message)
# @author_signature : String (Original post author signature)

class TD::MessageForwardOrigin::Channel < TD::MessageForwardOrigin
  include JSON::Serializable

  object_type "messageForwardOriginChannel"

  object_attributes({
    chat_id: Int64,
    message_id: Int64,
    author_signature: String
  })
end