# The message was imported from an exported message history
#
# @sender_name : String (Name of the sender)

class TD::MessageForwardOrigin::MessageImport < TD::MessageForwardOrigin
  include JSON::Serializable

  object_type "messageForwardOriginMessageImport"

  object_attributes({
    sender_name: String
  })
end