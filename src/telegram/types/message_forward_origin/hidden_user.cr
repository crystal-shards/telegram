# The message was originally sent by a user, which is hidden by their privacy settings
#
# @sender_name : String (Name of the sender)

class TD::MessageForwardOrigin::HiddenUser < TD::MessageForwardOrigin
  include JSON::Serializable

  object_type "messageForwardOriginHiddenUser"

  object_attributes({
    sender_name: String
  })
end