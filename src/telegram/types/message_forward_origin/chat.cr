# The message was originally sent on behalf of a chat
#
# @sender_chat_id : Int64 (Identifier of the chat that originally sent the message)
# @author_signature : String (For messages originally sent by an anonymous chat administrator, original message author signature)

class TD::MessageForwardOrigin::Chat < TD::MessageForwardOrigin
  include JSON::Serializable

  object_type "messageForwardOriginChat"

  object_attributes({
    sender_chat_id: Int64,
    author_signature: String
  })
end