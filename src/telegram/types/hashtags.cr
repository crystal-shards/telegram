# Contains a list of hashtags
#
# @hashtags : Array(String) (A list of hashtags)

class TD::Hashtags < TD::Base
  include JSON::Serializable

  object_type "hashtags"

  object_attributes({
    hashtags: Array(String)
  })
end