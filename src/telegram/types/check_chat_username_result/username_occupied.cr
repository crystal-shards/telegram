# The username is occupied
#

class TD::CheckChatUsernameResult::UsernameOccupied < TD::CheckChatUsernameResult
  include JSON::Serializable

  object_type "checkChatUsernameResultUsernameOccupied"
end