# The username is invalid
#

class TD::CheckChatUsernameResult::UsernameInvalid < TD::CheckChatUsernameResult
  include JSON::Serializable

  object_type "checkChatUsernameResultUsernameInvalid"
end