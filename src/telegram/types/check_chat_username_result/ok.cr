# The username can be set
#

class TD::CheckChatUsernameResult::Ok < TD::CheckChatUsernameResult
  include JSON::Serializable

  object_type "checkChatUsernameResultOk"
end