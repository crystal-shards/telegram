# The user can't be a member of a public supergroup
#

class TD::CheckChatUsernameResult::PublicGroupsUnavailable < TD::CheckChatUsernameResult
  include JSON::Serializable

  object_type "checkChatUsernameResultPublicGroupsUnavailable"
end