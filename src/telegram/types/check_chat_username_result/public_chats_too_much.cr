# The user has too much chats with username, one of them must be made private first
#

class TD::CheckChatUsernameResult::PublicChatsTooMuch < TD::CheckChatUsernameResult
  include JSON::Serializable

  object_type "checkChatUsernameResultPublicChatsTooMuch"
end