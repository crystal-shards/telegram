# Represents a URL linking to an internal Telegram entity
#
# @url : String (URL)
# @type : TD::TMeUrlType (Type of the URL)

class TD::TMeUrl < TD::Base
  include JSON::Serializable

  object_type "tMeUrl"

  object_attributes({
    url: String,
    type: TD::TMeUrlType
  })
end