# Describes a venue
#
# @location : TD::Location (Venue location; as defined by the sender)
# @title : String (Venue name; as defined by the sender)
# @address : String (Venue address; as defined by the sender)
# @provider : String (Provider of the venue database; as defined by the sender. Currently, only "foursquare" and "gplaces" (Google Places) need to be supported)
# @id : String (Identifier of the venue in the provider database; as defined by the sender)
# @type : String (Type of the venue in the provider database; as defined by the sender)

class TD::Venue < TD::Base
  include JSON::Serializable

  object_type "venue"

  object_attributes({
    location: TD::Location,
    title: String,
    address: String,
    provider: String,
    id: String,
    type: String
  })
end