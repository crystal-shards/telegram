# Contains information about a Telegram Passport element
#
# PersonalDetails: A Telegram Passport element containing the user's personal details
# Passport: A Telegram Passport element containing the user's passport
# DriverLicense: A Telegram Passport element containing the user's driver license
# IdentityCard: A Telegram Passport element containing the user's identity card
# InternalPassport: A Telegram Passport element containing the user's internal passport
# Address: A Telegram Passport element containing the user's address
# UtilityBill: A Telegram Passport element containing the user's utility bill
# BankStatement: A Telegram Passport element containing the user's bank statement
# RentalAgreement: A Telegram Passport element containing the user's rental agreement
# PassportRegistration: A Telegram Passport element containing the user's passport registration pages
# TemporaryRegistration: A Telegram Passport element containing the user's temporary registration
# PhoneNumber: A Telegram Passport element containing the user's phone number
# EmailAddress: A Telegram Passport element containing the user's email address

abstract class TD::PassportElement < TD::Base
end