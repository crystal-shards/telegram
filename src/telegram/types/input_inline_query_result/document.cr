# Represents a link to a file
#
# @id : String (Unique identifier of the query result)
# @title : String (Title of the resulting file)
# @description : String (Short description of the result, if known)
# @document_url : String (URL of the file)
# @mime_type : String (MIME type of the file content; only "application)
# @thumbnail_url : String (The URL of the file thumbnail, if it exists)
# @thumbnail_width : Int32 (Width of the thumbnail)
# @thumbnail_height : Int32 (Height of the thumbnail)
# @reply_markup : TD::ReplyMarkup (The message reply markup; pass null if none. Must be of type replyMarkupInlineKeyboard or null)
# @input_message_content : TD::InputMessageContent (The content of the message to be sent. Must be one of the following types: inputMessageText, inputMessageDocument, inputMessageInvoice, inputMessageLocation, inputMessageVenue or inputMessageContact)

class TD::InputInlineQueryResult::Document < TD::InputInlineQueryResult
  include JSON::Serializable

  object_type "inputInlineQueryResultDocument"

  object_attributes({
    id: String,
    title: String,
    description: String,
    document_url: String,
    mime_type: String,
    thumbnail_url: String,
    thumbnail_width: Int32,
    thumbnail_height: Int32,
    reply_markup: TD::ReplyMarkup,
    input_message_content: TD::InputMessageContent
  })
end