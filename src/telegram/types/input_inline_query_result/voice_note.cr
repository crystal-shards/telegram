# Represents a link to an opus-encoded audio file within an OGG container, single channel audio
#
# @id : String (Unique identifier of the query result)
# @title : String (Title of the voice note)
# @voice_note_url : String (The URL of the voice note file)
# @voice_note_duration : Int32 (Duration of the voice note, in seconds)
# @reply_markup : TD::ReplyMarkup (The message reply markup; pass null if none. Must be of type replyMarkupInlineKeyboard or null)
# @input_message_content : TD::InputMessageContent (The content of the message to be sent. Must be one of the following types: inputMessageText, inputMessageVoiceNote, inputMessageInvoice, inputMessageLocation, inputMessageVenue or inputMessageContact)

class TD::InputInlineQueryResult::VoiceNote < TD::InputInlineQueryResult
  include JSON::Serializable

  object_type "inputInlineQueryResultVoiceNote"

  object_attributes({
    id: String,
    title: String,
    voice_note_url: String,
    voice_note_duration: Int32,
    reply_markup: TD::ReplyMarkup,
    input_message_content: TD::InputMessageContent
  })
end