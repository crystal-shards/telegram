# Represents a game
#
# @id : String (Unique identifier of the query result)
# @game_short_name : String (Short name of the game)
# @reply_markup : TD::ReplyMarkup (The message reply markup; pass null if none. Must be of type replyMarkupInlineKeyboard or null)

class TD::InputInlineQueryResult::Game < TD::InputInlineQueryResult
  include JSON::Serializable

  object_type "inputInlineQueryResultGame"

  object_attributes({
    id: String,
    game_short_name: String,
    reply_markup: TD::ReplyMarkup
  })
end