# Represents a link to a page containing an embedded video player or a video file
#
# @id : String (Unique identifier of the query result)
# @title : String (Title of the result)
# @description : String (A short description of the result, if known)
# @thumbnail_url : String (The URL of the video thumbnail (JPEG), if it exists)
# @video_url : String (URL of the embedded video player or video file)
# @mime_type : String (MIME type of the content of the video URL, only "text)
# @video_width : Int32 (Width of the video)
# @video_height : Int32 (Height of the video)
# @video_duration : Int32 (Video duration, in seconds)
# @reply_markup : TD::ReplyMarkup (The message reply markup; pass null if none. Must be of type replyMarkupInlineKeyboard or null)
# @input_message_content : TD::InputMessageContent (The content of the message to be sent. Must be one of the following types: inputMessageText, inputMessageVideo, inputMessageInvoice, inputMessageLocation, inputMessageVenue or inputMessageContact)

class TD::InputInlineQueryResult::Video < TD::InputInlineQueryResult
  include JSON::Serializable

  object_type "inputInlineQueryResultVideo"

  object_attributes({
    id: String,
    title: String,
    description: String,
    thumbnail_url: String,
    video_url: String,
    mime_type: String,
    video_width: Int32,
    video_height: Int32,
    video_duration: Int32,
    reply_markup: TD::ReplyMarkup,
    input_message_content: TD::InputMessageContent
  })
end