# Represents a link to a WEBP or TGS sticker
#
# @id : String (Unique identifier of the query result)
# @thumbnail_url : String (URL of the sticker thumbnail, if it exists)
# @sticker_url : String (The URL of the WEBP or TGS sticker (sticker file size must not exceed 5MB))
# @sticker_width : Int32 (Width of the sticker)
# @sticker_height : Int32 (Height of the sticker)
# @reply_markup : TD::ReplyMarkup (The message reply markup; pass null if none. Must be of type replyMarkupInlineKeyboard or null)
# @input_message_content : TD::InputMessageContent (The content of the message to be sent. Must be one of the following types: inputMessageText, inputMessageSticker, inputMessageInvoice, inputMessageLocation, inputMessageVenue or inputMessageContact)

class TD::InputInlineQueryResult::Sticker < TD::InputInlineQueryResult
  include JSON::Serializable

  object_type "inputInlineQueryResultSticker"

  object_attributes({
    id: String,
    thumbnail_url: String,
    sticker_url: String,
    sticker_width: Int32,
    sticker_height: Int32,
    reply_markup: TD::ReplyMarkup,
    input_message_content: TD::InputMessageContent
  })
end