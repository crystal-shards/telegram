# Represents link to a JPEG image
#
# @id : String (Unique identifier of the query result)
# @title : String (Title of the result, if known)
# @description : String (A short description of the result, if known)
# @thumbnail_url : String (URL of the photo thumbnail, if it exists)
# @photo_url : String (The URL of the JPEG photo (photo size must not exceed 5MB))
# @photo_width : Int32 (Width of the photo)
# @photo_height : Int32 (Height of the photo)
# @reply_markup : TD::ReplyMarkup (The message reply markup; pass null if none. Must be of type replyMarkupInlineKeyboard or null)
# @input_message_content : TD::InputMessageContent (The content of the message to be sent. Must be one of the following types: inputMessageText, inputMessagePhoto, inputMessageInvoice, inputMessageLocation, inputMessageVenue or inputMessageContact)

class TD::InputInlineQueryResult::Photo < TD::InputInlineQueryResult
  include JSON::Serializable

  object_type "inputInlineQueryResultPhoto"

  object_attributes({
    id: String,
    title: String,
    description: String,
    thumbnail_url: String,
    photo_url: String,
    photo_width: Int32,
    photo_height: Int32,
    reply_markup: TD::ReplyMarkup,
    input_message_content: TD::InputMessageContent
  })
end