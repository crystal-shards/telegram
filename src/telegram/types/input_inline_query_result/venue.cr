# Represents information about a venue
#
# @id : String (Unique identifier of the query result)
# @venue : TD::Venue (Venue result)
# @thumbnail_url : String (URL of the result thumbnail, if it exists)
# @thumbnail_width : Int32 (Thumbnail width, if known)
# @thumbnail_height : Int32 (Thumbnail height, if known)
# @reply_markup : TD::ReplyMarkup (The message reply markup; pass null if none. Must be of type replyMarkupInlineKeyboard or null)
# @input_message_content : TD::InputMessageContent (The content of the message to be sent. Must be one of the following types: inputMessageText, inputMessageInvoice, inputMessageLocation, inputMessageVenue or inputMessageContact)

class TD::InputInlineQueryResult::Venue < TD::InputInlineQueryResult
  include JSON::Serializable

  object_type "inputInlineQueryResultVenue"

  object_attributes({
    id: String,
    venue: TD::Venue,
    thumbnail_url: String,
    thumbnail_width: Int32,
    thumbnail_height: Int32,
    reply_markup: TD::ReplyMarkup,
    input_message_content: TD::InputMessageContent
  })
end