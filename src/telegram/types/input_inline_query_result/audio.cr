# Represents a link to an MP3 audio file
#
# @id : String (Unique identifier of the query result)
# @title : String (Title of the audio file)
# @performer : String (Performer of the audio file)
# @audio_url : String (The URL of the audio file)
# @audio_duration : Int32 (Audio file duration, in seconds)
# @reply_markup : TD::ReplyMarkup (The message reply markup; pass null if none. Must be of type replyMarkupInlineKeyboard or null)
# @input_message_content : TD::InputMessageContent (The content of the message to be sent. Must be one of the following types: inputMessageText, inputMessageAudio, inputMessageInvoice, inputMessageLocation, inputMessageVenue or inputMessageContact)

class TD::InputInlineQueryResult::Audio < TD::InputInlineQueryResult
  include JSON::Serializable

  object_type "inputInlineQueryResultAudio"

  object_attributes({
    id: String,
    title: String,
    performer: String,
    audio_url: String,
    audio_duration: Int32,
    reply_markup: TD::ReplyMarkup,
    input_message_content: TD::InputMessageContent
  })
end