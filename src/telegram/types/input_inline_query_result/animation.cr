# Represents a link to an animated GIF or an animated (i.e., without sound) H.264
#
# @id : String (Unique identifier of the query result)
# @title : String (Title of the query result)
# @thumbnail_url : String (URL of the result thumbnail (JPEG, GIF, or MPEG4), if it exists)
# @thumbnail_mime_type : String (MIME type of the video thumbnail. If non-empty, must be one of "image)
# @video_url : String (The URL of the video file (file size must not exceed 1MB))
# @video_mime_type : String (MIME type of the video file. Must be one of "image)
# @video_duration : Int32 (Duration of the video, in seconds)
# @video_width : Int32 (Width of the video)
# @video_height : Int32 (Height of the video)
# @reply_markup : TD::ReplyMarkup (The message reply markup; pass null if none. Must be of type replyMarkupInlineKeyboard or null)
# @input_message_content : TD::InputMessageContent (The content of the message to be sent. Must be one of the following types: inputMessageText, inputMessageAnimation, inputMessageInvoice, inputMessageLocation, inputMessageVenue or inputMessageContact)

class TD::InputInlineQueryResult::Animation < TD::InputInlineQueryResult
  include JSON::Serializable

  object_type "inputInlineQueryResultAnimation"

  object_attributes({
    id: String,
    title: String,
    thumbnail_url: String,
    thumbnail_mime_type: String,
    video_url: String,
    video_mime_type: String,
    video_duration: Int32,
    video_width: Int32,
    video_height: Int32,
    reply_markup: TD::ReplyMarkup,
    input_message_content: TD::InputMessageContent
  })
end