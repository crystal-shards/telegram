# Contains information about a chat invite link
#
# @chat_id : Int64 (Chat identifier of the invite link; 0 if the user has no access to the chat before joining)
# @accessible_for : Int32 (If non-zero, the amount of time for which read access to the chat will remain available, in seconds)
# @type : TD::ChatType (Type of the chat)
# @title : String (Title of the chat)
# @photo : TD::ChatPhotoInfo? (Chat photo; may be null)
# @description : String (Chat description)
# @member_count : Int32 (Number of members in the chat)
# @member_user_ids : Array(Int64) (User identifiers of some chat members that may be known to the current user)
# @creates_join_request : Bool (True, if the link only creates join request)
# @is_public : Bool (True, if the chat is a public supergroup or channel, i.e. it has a username or it is a location-based supergroup)

class TD::ChatInviteLinkInfo < TD::Base
  include JSON::Serializable

  object_type "chatInviteLinkInfo"

  object_attributes({
    chat_id: Int64,
    accessible_for: Int32,
    type: TD::ChatType,
    title: String,
    photo: TD::ChatPhotoInfo?,
    description: String,
    member_count: Int32,
    member_user_ids: Array(Int64),
    creates_join_request: Bool,
    is_public: Bool
  })
end