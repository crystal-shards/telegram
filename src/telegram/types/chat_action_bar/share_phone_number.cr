# The chat is a private or secret chat with a mutual contact and the user's phone number can be shared with the other user using the method sharePhoneNumber
#

class TD::ChatActionBar::SharePhoneNumber < TD::ChatActionBar
  include JSON::Serializable

  object_type "chatActionBarSharePhoneNumber"
end