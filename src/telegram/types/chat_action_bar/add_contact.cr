# The chat is a private or secret chat and the other user can be added to the contact list using the method addContact
#

class TD::ChatActionBar::AddContact < TD::ChatActionBar
  include JSON::Serializable

  object_type "chatActionBarAddContact"
end