# The chat can be reported as spam using the method reportChat with the reason chatReportReasonSpam
#
# @can_unarchive : Bool (If true, the chat was automatically archived and can be moved back to the main chat list using addChatToList simultaneously with setting chat notification settings to default using setChatNotificationSettings)

class TD::ChatActionBar::ReportSpam < TD::ChatActionBar
  include JSON::Serializable

  object_type "chatActionBarReportSpam"

  object_attributes({
    can_unarchive: Bool
  })
end