# The chat is a location-based supergroup, which can be reported as having unrelated location using the method reportChat with the reason chatReportReasonUnrelatedLocation
#

class TD::ChatActionBar::ReportUnrelatedLocation < TD::ChatActionBar
  include JSON::Serializable

  object_type "chatActionBarReportUnrelatedLocation"
end