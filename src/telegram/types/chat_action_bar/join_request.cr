# The chat is a private chat with an administrator of a chat to which the user sent join request
#
# @title : String (Title of the chat to which the join request was sent)
# @is_channel : Bool (True, if the join request was sent to a channel chat)
# @request_date : Int32 (Point in time (Unix timestamp) when the join request was sent)

class TD::ChatActionBar::JoinRequest < TD::ChatActionBar
  include JSON::Serializable

  object_type "chatActionBarJoinRequest"

  object_attributes({
    title: String,
    is_channel: Bool,
    request_date: Int32
  })
end