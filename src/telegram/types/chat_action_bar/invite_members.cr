# The chat is a recently created group chat to which new members can be invited
#

class TD::ChatActionBar::InviteMembers < TD::ChatActionBar
  include JSON::Serializable

  object_type "chatActionBarInviteMembers"
end