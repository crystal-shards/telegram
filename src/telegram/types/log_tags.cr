# Contains a list of available TDLib internal log tags
#
# @tags : Array(String) (List of log tags)

class TD::LogTags < TD::Base
  include JSON::Serializable

  object_type "logTags"

  object_attributes({
    tags: Array(String)
  })
end