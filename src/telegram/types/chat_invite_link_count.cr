# Describes a chat administrator with a number of active and revoked chat invite links
#
# @user_id : Int64 (Administrator's user identifier)
# @invite_link_count : Int32 (Number of active invite links)
# @revoked_invite_link_count : Int32 (Number of revoked invite links)

class TD::ChatInviteLinkCount < TD::Base
  include JSON::Serializable

  object_type "chatInviteLinkCount"

  object_attributes({
    user_id: Int64,
    invite_link_count: Int32,
    revoked_invite_link_count: Int32
  })
end