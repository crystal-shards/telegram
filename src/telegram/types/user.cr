# Represents a user
#
# @id : Int64 (User identifier)
# @first_name : String (First name of the user)
# @last_name : String (Last name of the user)
# @username : String (Username of the user)
# @phone_number : String (Phone number of the user)
# @status : TD::UserStatus (Current online status of the user)
# @profile_photo : TD::ProfilePhoto? (Profile photo of the user; may be null)
# @is_contact : Bool (The user is a contact of the current user)
# @is_mutual_contact : Bool (The user is a contact of the current user and the current user is a contact of the user)
# @is_verified : Bool (True, if the user is verified)
# @is_support : Bool (True, if the user is Telegram support account)
# @restriction_reason : String (If non-empty, it contains a human-readable description of the reason why access to this user must be restricted)
# @is_scam : Bool (True, if many users reported this user as a scam)
# @is_fake : Bool (True, if many users reported this user as a fake account)
# @have_access : Bool (If false, the user is inaccessible, and the only information known about the user is inside this class. It can't be passed to any method except GetUser)
# @type : TD::UserType (Type of the user)
# @language_code : String (IETF language tag of the user's language; only available to bots)

class TD::User < TD::Base
  include JSON::Serializable

  object_type "user"

  object_attributes({
    id: Int64,
    first_name: String,
    last_name: String,
    username: String,
    phone_number: String,
    status: TD::UserStatus,
    profile_photo: TD::ProfilePhoto?,
    is_contact: Bool,
    is_mutual_contact: Bool,
    is_verified: Bool,
    is_support: Bool,
    restriction_reason: String,
    is_scam: Bool,
    is_fake: Bool,
    have_access: Bool,
    type: TD::UserType,
    language_code: String
  })
end