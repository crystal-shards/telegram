# Describes a list of chats
#
# Main: A main list of chats
# Archive: A list of chats usually located at the top of the main chat list. Unmuted chats are automatically moved from the Archive to the Main chat list when a new message arrives
# Filter: A list of chats belonging to a chat filter

abstract class TD::ChatList < TD::Base
end