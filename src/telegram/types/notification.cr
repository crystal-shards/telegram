# Contains information about a notification
#
# @id : Int32 (Unique persistent identifier of this notification)
# @date : Int32 (Notification date)
# @is_silent : Bool (True, if the notification was initially silent)
# @type : TD::NotificationType (Notification type)

class TD::Notification < TD::Base
  include JSON::Serializable

  object_type "notification"

  object_attributes({
    id: Int32,
    date: Int32,
    is_silent: Bool,
    type: TD::NotificationType
  })
end