# Represents an audio file
#
# @id : String (Unique identifier of the query result)
# @audio : TD::Audio (Audio file)

class TD::InlineQueryResult::Audio < TD::InlineQueryResult
  include JSON::Serializable

  object_type "inlineQueryResultAudio"

  object_attributes({
    id: String,
    audio: TD::Audio
  })
end