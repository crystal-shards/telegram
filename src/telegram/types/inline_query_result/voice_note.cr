# Represents a voice note
#
# @id : String (Unique identifier of the query result)
# @voice_note : TD::VoiceNote (Voice note)
# @title : String (Title of the voice note)

class TD::InlineQueryResult::VoiceNote < TD::InlineQueryResult
  include JSON::Serializable

  object_type "inlineQueryResultVoiceNote"

  object_attributes({
    id: String,
    voice_note: TD::VoiceNote,
    title: String
  })
end