# Represents information about a game
#
# @id : String (Unique identifier of the query result)
# @game : TD::Game (Game result)

class TD::InlineQueryResult::Game < TD::InlineQueryResult
  include JSON::Serializable

  object_type "inlineQueryResultGame"

  object_attributes({
    id: String,
    game: TD::Game
  })
end