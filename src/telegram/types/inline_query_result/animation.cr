# Represents an animation file
#
# @id : String (Unique identifier of the query result)
# @animation : TD::Animation (Animation file)
# @title : String (Animation title)

class TD::InlineQueryResult::Animation < TD::InlineQueryResult
  include JSON::Serializable

  object_type "inlineQueryResultAnimation"

  object_attributes({
    id: String,
    animation: TD::Animation,
    title: String
  })
end