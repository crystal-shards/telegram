# Represents a document
#
# @id : String (Unique identifier of the query result)
# @document : TD::Document (Document)
# @title : String (Document title)
# @description : String (Document description)

class TD::InlineQueryResult::Document < TD::InlineQueryResult
  include JSON::Serializable

  object_type "inlineQueryResultDocument"

  object_attributes({
    id: String,
    document: TD::Document,
    title: String,
    description: String
  })
end