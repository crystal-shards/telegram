# Represents a photo
#
# @id : String (Unique identifier of the query result)
# @photo : TD::Photo (Photo)
# @title : String (Title of the result, if known)
# @description : String (A short description of the result, if known)

class TD::InlineQueryResult::Photo < TD::InlineQueryResult
  include JSON::Serializable

  object_type "inlineQueryResultPhoto"

  object_attributes({
    id: String,
    photo: TD::Photo,
    title: String,
    description: String
  })
end