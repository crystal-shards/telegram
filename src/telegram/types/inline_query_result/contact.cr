# Represents a user contact
#
# @id : String (Unique identifier of the query result)
# @contact : TD::Contact (A user contact)
# @thumbnail : TD::Thumbnail? (Result thumbnail in JPEG format; may be null)

class TD::InlineQueryResult::Contact < TD::InlineQueryResult
  include JSON::Serializable

  object_type "inlineQueryResultContact"

  object_attributes({
    id: String,
    contact: TD::Contact,
    thumbnail: TD::Thumbnail?
  })
end