# Represents a link to an article or web page
#
# @id : String (Unique identifier of the query result)
# @url : String (URL of the result, if it exists)
# @hide_url : Bool (True, if the URL must be not shown)
# @title : String (Title of the result)
# @description : String (A short description of the result)
# @thumbnail : TD::Thumbnail? (Result thumbnail in JPEG format; may be null)

class TD::InlineQueryResult::Article < TD::InlineQueryResult
  include JSON::Serializable

  object_type "inlineQueryResultArticle"

  object_attributes({
    id: String,
    url: String,
    hide_url: Bool,
    title: String,
    description: String,
    thumbnail: TD::Thumbnail?
  })
end