# Represents a video
#
# @id : String (Unique identifier of the query result)
# @video : TD::Video (Video)
# @title : String (Title of the video)
# @description : String (Description of the video)

class TD::InlineQueryResult::Video < TD::InlineQueryResult
  include JSON::Serializable

  object_type "inlineQueryResultVideo"

  object_attributes({
    id: String,
    video: TD::Video,
    title: String,
    description: String
  })
end