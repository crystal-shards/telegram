# Represents a point on the map
#
# @id : String (Unique identifier of the query result)
# @location : TD::Location (Location result)
# @title : String (Title of the result)
# @thumbnail : TD::Thumbnail? (Result thumbnail in JPEG format; may be null)

class TD::InlineQueryResult::Location < TD::InlineQueryResult
  include JSON::Serializable

  object_type "inlineQueryResultLocation"

  object_attributes({
    id: String,
    location: TD::Location,
    title: String,
    thumbnail: TD::Thumbnail?
  })
end