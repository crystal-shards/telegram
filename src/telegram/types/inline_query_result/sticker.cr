# Represents a sticker
#
# @id : String (Unique identifier of the query result)
# @sticker : TD::Sticker (Sticker)

class TD::InlineQueryResult::Sticker < TD::InlineQueryResult
  include JSON::Serializable

  object_type "inlineQueryResultSticker"

  object_attributes({
    id: String,
    sticker: TD::Sticker
  })
end