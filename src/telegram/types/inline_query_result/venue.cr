# Represents information about a venue
#
# @id : String (Unique identifier of the query result)
# @venue : TD::Venue (Venue result)
# @thumbnail : TD::Thumbnail? (Result thumbnail in JPEG format; may be null)

class TD::InlineQueryResult::Venue < TD::InlineQueryResult
  include JSON::Serializable

  object_type "inlineQueryResultVenue"

  object_attributes({
    id: String,
    venue: TD::Venue,
    thumbnail: TD::Thumbnail?
  })
end