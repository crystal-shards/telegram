# Describes the type of a URL linking to an internal Telegram entity
#
# User: A URL linking to a user
# Supergroup: A URL linking to a public supergroup or channel
# ChatInvite: A chat invite link
# StickerSet: A URL linking to a sticker set

abstract class TD::TMeUrlType < TD::Base
end