# An authentication code is delivered via a phone call to the specified phone number
#
# @length : Int32 (Length of the code)

class TD::AuthenticationCodeType::Call < TD::AuthenticationCodeType
  include JSON::Serializable

  object_type "authenticationCodeTypeCall"

  object_attributes({
    length: Int32
  })
end