# An authentication code is delivered by an immediately canceled call to the specified phone number. The phone number that calls is the code that must be entered automatically
#
# @pattern : String (Pattern of the phone number from which the call will be made)

class TD::AuthenticationCodeType::FlashCall < TD::AuthenticationCodeType
  include JSON::Serializable

  object_type "authenticationCodeTypeFlashCall"

  object_attributes({
    pattern: String
  })
end