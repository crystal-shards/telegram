# An authentication code is delivered via an SMS message to the specified phone number
#
# @length : Int32 (Length of the code)

class TD::AuthenticationCodeType::Sms < TD::AuthenticationCodeType
  include JSON::Serializable

  object_type "authenticationCodeTypeSms"

  object_attributes({
    length: Int32
  })
end