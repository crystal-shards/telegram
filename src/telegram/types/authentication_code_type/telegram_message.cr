# An authentication code is delivered via a private Telegram message, which can be viewed from another active session
#
# @length : Int32 (Length of the code)

class TD::AuthenticationCodeType::TelegramMessage < TD::AuthenticationCodeType
  include JSON::Serializable

  object_type "authenticationCodeTypeTelegramMessage"

  object_attributes({
    length: Int32
  })
end