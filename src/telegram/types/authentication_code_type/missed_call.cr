# An authentication code is delivered by an immediately canceled call to the specified phone number. The last digits of the phone number that calls are the code that must be entered manually by the user
#
# @phone_number_prefix : String (Prefix of the phone number from which the call will be made)
# @length : Int32 (Number of digits in the code, excluding the prefix)

class TD::AuthenticationCodeType::MissedCall < TD::AuthenticationCodeType
  include JSON::Serializable

  object_type "authenticationCodeTypeMissedCall"

  object_attributes({
    phone_number_prefix: String,
    length: Int32
  })
end