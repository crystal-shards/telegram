# Represents a group call participant
#
# @participant_id : TD::MessageSender (Identifier of the group call participant)
# @audio_source_id : Int32 (User's audio channel synchronization source identifier)
# @screen_sharing_audio_source_id : Int32 (User's screen sharing audio channel synchronization source identifier)
# @video_info : TD::GroupCallParticipantVideoInfo? (Information about user's video channel; may be null if there is no active video)
# @screen_sharing_video_info : TD::GroupCallParticipantVideoInfo? (Information about user's screen sharing video channel; may be null if there is no active screen sharing video)
# @bio : String (The participant user's bio or the participant chat's description)
# @is_current_user : Bool (True, if the participant is the current user)
# @is_speaking : Bool (True, if the participant is speaking as set by setGroupCallParticipantIsSpeaking)
# @is_hand_raised : Bool (True, if the participant hand is raised)
# @can_be_muted_for_all_users : Bool (True, if the current user can mute the participant for all other group call participants)
# @can_be_unmuted_for_all_users : Bool (True, if the current user can allow the participant to unmute themselves or unmute the participant (if the participant is the current user))
# @can_be_muted_for_current_user : Bool (True, if the current user can mute the participant only for self)
# @can_be_unmuted_for_current_user : Bool (True, if the current user can unmute the participant for self)
# @is_muted_for_all_users : Bool (True, if the participant is muted for all users)
# @is_muted_for_current_user : Bool (True, if the participant is muted for the current user)
# @can_unmute_self : Bool (True, if the participant is muted for all users, but can unmute themselves)
# @volume_level : Int32 (Participant's volume level; 1-20000 in hundreds of percents)
# @order : String (User's order in the group call participant list. Orders must be compared lexicographically. The bigger is order, the higher is user in the list. If order is empty, the user must be removed from the participant list)

class TD::GroupCallParticipant < TD::Base
  include JSON::Serializable

  object_type "groupCallParticipant"

  object_attributes({
    participant_id: TD::MessageSender,
    audio_source_id: Int32,
    screen_sharing_audio_source_id: Int32,
    video_info: TD::GroupCallParticipantVideoInfo?,
    screen_sharing_video_info: TD::GroupCallParticipantVideoInfo?,
    bio: String,
    is_current_user: Bool,
    is_speaking: Bool,
    is_hand_raised: Bool,
    can_be_muted_for_all_users: Bool,
    can_be_unmuted_for_all_users: Bool,
    can_be_muted_for_current_user: Bool,
    can_be_unmuted_for_current_user: Bool,
    is_muted_for_all_users: Bool,
    is_muted_for_current_user: Bool,
    can_unmute_self: Bool,
    volume_level: Int32,
    order: String
  })
end