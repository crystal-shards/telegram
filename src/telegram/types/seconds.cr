# Contains a value representing a number of seconds
#
# @seconds : Float64 (Number of seconds)

class TD::Seconds < TD::Base
  include JSON::Serializable

  object_type "seconds"

  object_attributes({
    seconds: Float64
  })
end