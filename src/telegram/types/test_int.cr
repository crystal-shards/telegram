# A simple object containing a number; for testing only
#
# @value : Int32 (Number)

class TD::TestInt < TD::Base
  include JSON::Serializable

  object_type "testInt"

  object_attributes({
    value: Int32
  })
end