# The call was ended before the conversation started. It was declined by the other party
#

class TD::CallDiscardReason::Declined < TD::CallDiscardReason
  include JSON::Serializable

  object_type "callDiscardReasonDeclined"
end