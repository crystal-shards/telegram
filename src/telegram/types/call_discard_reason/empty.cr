# The call wasn't discarded, or the reason is unknown
#

class TD::CallDiscardReason::Empty < TD::CallDiscardReason
  include JSON::Serializable

  object_type "callDiscardReasonEmpty"
end