# The call was ended before the conversation started. It was canceled by the caller or missed by the other party
#

class TD::CallDiscardReason::Missed < TD::CallDiscardReason
  include JSON::Serializable

  object_type "callDiscardReasonMissed"
end