# The call was ended because one of the parties hung up
#

class TD::CallDiscardReason::HungUp < TD::CallDiscardReason
  include JSON::Serializable

  object_type "callDiscardReasonHungUp"
end