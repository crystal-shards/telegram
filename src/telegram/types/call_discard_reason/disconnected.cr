# The call was ended during the conversation because the users were disconnected
#

class TD::CallDiscardReason::Disconnected < TD::CallDiscardReason
  include JSON::Serializable

  object_type "callDiscardReasonDisconnected"
end