# Contains Telegram terms of service
#
# @text : TD::FormattedText (Text of the terms of service)
# @min_user_age : Int32 (The minimum age of a user to be able to accept the terms; 0 if any)
# @show_popup : Bool (True, if a blocking popup with terms of service must be shown to the user)

class TD::TermsOfService < TD::Base
  include JSON::Serializable

  object_type "termsOfService"

  object_attributes({
    text: TD::FormattedText,
    min_user_age: Int32,
    show_popup: Bool
  })
end