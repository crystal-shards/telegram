# The user is a member of the chat and has some additional privileges. In basic groups, administrators can edit and delete messages sent by others, add new members, ban unprivileged members, and manage video chats. In supergroups and channels, there are more detailed options for administrator privileges
#
# @custom_title : String (A custom title of the administrator; 0-16 characters without emojis; applicable to supergroups only)
# @can_be_edited : Bool (True, if the current user can edit the administrator privileges for the called user)
# @can_manage_chat : Bool (True, if the administrator can get chat event log, get chat statistics, get message statistics in channels, get channel members, see anonymous administrators in supergroups and ignore slow mode. Implied by any other privilege; applicable to supergroups and channels only)
# @can_change_info : Bool (True, if the administrator can change the chat title, photo, and other settings)
# @can_post_messages : Bool (True, if the administrator can create channel posts; applicable to channels only)
# @can_edit_messages : Bool (True, if the administrator can edit messages of other users and pin messages; applicable to channels only)
# @can_delete_messages : Bool (True, if the administrator can delete messages of other users)
# @can_invite_users : Bool (True, if the administrator can invite new users to the chat)
# @can_restrict_members : Bool (True, if the administrator can restrict, ban, or unban chat members; always true for channels)
# @can_pin_messages : Bool (True, if the administrator can pin messages; applicable to basic groups and supergroups only)
# @can_promote_members : Bool (True, if the administrator can add new administrators with a subset of their own privileges or demote administrators that were directly or indirectly promoted by them)
# @can_manage_video_chats : Bool (True, if the administrator can manage video chats)
# @is_anonymous : Bool (True, if the administrator isn't shown in the chat member list and sends messages anonymously; applicable to supergroups only)

class TD::ChatMemberStatus::Administrator < TD::ChatMemberStatus
  include JSON::Serializable

  object_type "chatMemberStatusAdministrator"

  object_attributes({
    custom_title: String,
    can_be_edited: Bool,
    can_manage_chat: Bool,
    can_change_info: Bool,
    can_post_messages: Bool,
    can_edit_messages: Bool,
    can_delete_messages: Bool,
    can_invite_users: Bool,
    can_restrict_members: Bool,
    can_pin_messages: Bool,
    can_promote_members: Bool,
    can_manage_video_chats: Bool,
    is_anonymous: Bool
  })
end