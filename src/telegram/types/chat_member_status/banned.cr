# The user or the chat was banned (and hence is not a member of the chat). Implies the user can't return to the chat, view messages, or be used as a participant identifier to join a video chat of the chat
#
# @banned_until_date : Int32 (Point in time (Unix timestamp) when the user will be unbanned; 0 if never. If the user is banned for more than 366 days or for less than 30 seconds from the current time, the user is considered to be banned forever. Always 0 in basic groups)

class TD::ChatMemberStatus::Banned < TD::ChatMemberStatus
  include JSON::Serializable

  object_type "chatMemberStatusBanned"

  object_attributes({
    banned_until_date: Int32
  })
end