# The user is the owner of the chat and has all the administrator privileges
#
# @custom_title : String (A custom title of the owner; 0-16 characters without emojis; applicable to supergroups only)
# @is_anonymous : Bool (True, if the creator isn't shown in the chat member list and sends messages anonymously; applicable to supergroups only)
# @is_member : Bool (True, if the user is a member of the chat)

class TD::ChatMemberStatus::Creator < TD::ChatMemberStatus
  include JSON::Serializable

  object_type "chatMemberStatusCreator"

  object_attributes({
    custom_title: String,
    is_anonymous: Bool,
    is_member: Bool
  })
end