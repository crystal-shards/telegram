# The user is under certain restrictions in the chat. Not supported in basic groups and channels
#
# @is_member : Bool (True, if the user is a member of the chat)
# @restricted_until_date : Int32 (Point in time (Unix timestamp) when restrictions will be lifted from the user; 0 if never. If the user is restricted for more than 366 days or for less than 30 seconds from the current time, the user is considered to be restricted forever)
# @permissions : TD::ChatPermissions (User permissions in the chat)

class TD::ChatMemberStatus::Restricted < TD::ChatMemberStatus
  include JSON::Serializable

  object_type "chatMemberStatusRestricted"

  object_attributes({
    is_member: Bool,
    restricted_until_date: Int32,
    permissions: TD::ChatPermissions
  })
end