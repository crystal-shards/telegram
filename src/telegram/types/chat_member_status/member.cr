# The user is a member of the chat, without any additional privileges or restrictions
#

class TD::ChatMemberStatus::Member < TD::ChatMemberStatus
  include JSON::Serializable

  object_type "chatMemberStatusMember"
end