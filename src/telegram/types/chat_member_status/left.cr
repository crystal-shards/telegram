# The user or the chat is not a chat member
#

class TD::ChatMemberStatus::Left < TD::ChatMemberStatus
  include JSON::Serializable

  object_type "chatMemberStatusLeft"
end