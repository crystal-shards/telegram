# Contains the call identifier
#
# @id : Int32 (Call identifier)

class TD::CallId < TD::Base
  include JSON::Serializable

  object_type "callId"

  object_attributes({
    id: Int32
  })
end