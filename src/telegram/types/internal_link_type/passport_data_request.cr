# The link contains a request of Telegram passport data. Call getPassportAuthorizationForm with the given parameters to process the link if the link was received from outside of the app, otherwise ignore it
#
# @bot_user_id : Int64 (User identifier of the service's bot)
# @scope : String (Telegram Passport element types requested by the service)
# @public_key : String (Service's public key)
# @nonce : String (Unique request identifier provided by the service)
# @callback_url : String (An HTTP URL to open once the request is finished or canceled with the parameter tg_passport)

class TD::InternalLinkType::PassportDataRequest < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypePassportDataRequest"

  object_attributes({
    bot_user_id: Int64,
    scope: String,
    public_key: String,
    nonce: String,
    callback_url: String
  })
end