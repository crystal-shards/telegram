# The link is a link to a sticker set. Call searchStickerSet with the given sticker set name to process the link and show the sticker set
#
# @sticker_set_name : String (Name of the sticker set)

class TD::InternalLinkType::StickerSet < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeStickerSet"

  object_attributes({
    sticker_set_name: String
  })
end