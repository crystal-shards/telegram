# The link is a link to a Telegram message. Call getMessageLinkInfo with the given URL to process the link
#
# @url : String (URL to be passed to getMessageLinkInfo)

class TD::InternalLinkType::Message < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeMessage"

  object_attributes({
    url: String
  })
end