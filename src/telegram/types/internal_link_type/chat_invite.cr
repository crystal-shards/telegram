# The link is a chat invite link. Call checkChatInviteLink with the given invite link to process the link
#
# @invite_link : String (Internal representation of the invite link)

class TD::InternalLinkType::ChatInvite < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeChatInvite"

  object_attributes({
    invite_link: String
  })
end