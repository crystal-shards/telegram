# The link is a link to app settings
#

class TD::InternalLinkType::Settings < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeSettings"
end