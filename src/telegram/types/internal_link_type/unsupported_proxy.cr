# The link is a link to an unsupported proxy. An alert can be shown to the user
#

class TD::InternalLinkType::UnsupportedProxy < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeUnsupportedProxy"
end