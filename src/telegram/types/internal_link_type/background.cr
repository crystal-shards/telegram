# The link is a link to a background. Call searchBackground with the given background name to process the link
#
# @background_name : String (Name of the background)

class TD::InternalLinkType::Background < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeBackground"

  object_attributes({
    background_name: String
  })
end