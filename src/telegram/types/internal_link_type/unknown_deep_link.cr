# The link is an unknown tg: link. Call getDeepLinkInfo to process the link
#
# @link : String (Link to be passed to getDeepLinkInfo)

class TD::InternalLinkType::UnknownDeepLink < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeUnknownDeepLink"

  object_attributes({
    link: String
  })
end