# The link contains a message draft text. A share screen needs to be shown to the user, then the chosen chat must be opened and the text is added to the input field
#
# @text : TD::FormattedText (Message draft text)
# @contains_link : Bool (True, if the first line of the text contains a link. If true, the input field needs to be focused and the text after the link must be selected)

class TD::InternalLinkType::MessageDraft < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeMessageDraft"

  object_attributes({
    text: TD::FormattedText,
    contains_link: Bool
  })
end