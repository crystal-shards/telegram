# The link is a link to the theme settings section of the app
#

class TD::InternalLinkType::ThemeSettings < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeThemeSettings"
end