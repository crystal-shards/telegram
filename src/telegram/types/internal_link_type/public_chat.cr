# The link is a link to a chat by its username. Call searchPublicChat with the given chat username to process the link
#
# @chat_username : String (Username of the chat)

class TD::InternalLinkType::PublicChat < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypePublicChat"

  object_attributes({
    chat_username: String
  })
end