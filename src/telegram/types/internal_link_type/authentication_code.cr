# The link contains an authentication code. Call checkAuthenticationCode with the code if the current authorization state is authorizationStateWaitCode
#
# @code : String (The authentication code)

class TD::InternalLinkType::AuthenticationCode < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeAuthenticationCode"

  object_attributes({
    code: String
  })
end