# The link is a link to a video chat. Call searchPublicChat with the given chat username, and then joinGoupCall with the given invite hash to process the link
#
# @chat_username : String (Username of the chat with the video chat)
# @invite_hash : String (If non-empty, invite hash to be used to join the video chat without being muted by administrators)
# @is_live_stream : Bool (True, if the video chat is expected to be a live stream in a channel or a broadcast group)

class TD::InternalLinkType::VideoChat < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeVideoChat"

  object_attributes({
    chat_username: String,
    invite_hash: String,
    is_live_stream: Bool
  })
end