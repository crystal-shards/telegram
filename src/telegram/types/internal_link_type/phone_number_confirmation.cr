# The link can be used to confirm ownership of a phone number to prevent account deletion. Call sendPhoneNumberConfirmationCode with the given hash and phone number to process the link
#
# @hash : String (Hash value from the link)
# @phone_number : String (Phone number value from the link)

class TD::InternalLinkType::PhoneNumberConfirmation < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypePhoneNumberConfirmation"

  object_attributes({
    hash: String,
    phone_number: String
  })
end