# The link is a link to a theme. TDLib has no theme support yet
#
# @theme_name : String (Name of the theme)

class TD::InternalLinkType::Theme < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeTheme"

  object_attributes({
    theme_name: String
  })
end