# The link is a link to a game. Call searchPublicChat with the given bot username, check that the user is a bot, ask the current user to select a chat to send the game, and then call sendMessage with inputMessageGame
#
# @bot_username : String (Username of the bot that owns the game)
# @game_short_name : String (Short name of the game)

class TD::InternalLinkType::Game < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeGame"

  object_attributes({
    bot_username: String,
    game_short_name: String
  })
end