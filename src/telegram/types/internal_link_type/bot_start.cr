# The link is a link to a chat with a Telegram bot. Call searchPublicChat with the given bot username, check that the user is a bot, show START button in the chat with the bot,and then call sendBotStartMessage with the given start parameter after the button is pressed
#
# @bot_username : String (Username of the bot)
# @start_parameter : String (The parameter to be passed to sendBotStartMessage)

class TD::InternalLinkType::BotStart < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeBotStart"

  object_attributes({
    bot_username: String,
    start_parameter: String
  })
end