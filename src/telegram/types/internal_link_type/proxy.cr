# The link is a link to a proxy. Call addProxy with the given parameters to process the link and add the proxy
#
# @server : String (Proxy server IP address)
# @port : Int32 (Proxy server port)
# @type : TD::ProxyType (Type of the proxy)

class TD::InternalLinkType::Proxy < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeProxy"

  object_attributes({
    server: String,
    port: Int32,
    type: TD::ProxyType
  })
end