# The link is a link to a language pack. Call getLanguagePackInfo with the given language pack identifier to process the link
#
# @language_pack_id : String (Language pack identifier)

class TD::InternalLinkType::LanguagePack < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeLanguagePack"

  object_attributes({
    language_pack_id: String
  })
end