# The link is a link to the filter settings section of the app
#

class TD::InternalLinkType::FilterSettings < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeFilterSettings"
end