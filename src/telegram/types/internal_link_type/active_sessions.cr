# The link is a link to the active sessions section of the app. Use getActiveSessions to handle the link
#

class TD::InternalLinkType::ActiveSessions < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeActiveSessions"
end