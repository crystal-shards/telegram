# The link is a link to the change phone number section of the app
#

class TD::InternalLinkType::ChangePhoneNumber < TD::InternalLinkType
  include JSON::Serializable

  object_type "internalLinkTypeChangePhoneNumber"
end