# New message was received
#
# @message : TD::Message (The message)

class TD::NotificationType::NewMessage < TD::NotificationType
  include JSON::Serializable

  object_type "notificationTypeNewMessage"

  object_attributes({
    message: TD::Message
  })
end