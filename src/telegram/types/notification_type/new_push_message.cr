# New message was received through a push notification
#
# @message_id : Int64 (The message identifier. The message will not be available in the chat history, but the ID can be used in viewMessages, or as reply_to_message_id)
# @sender_id : TD::MessageSender (Identifier of the sender of the message. Corresponding user or chat may be inaccessible)
# @sender_name : String (Name of the sender)
# @is_outgoing : Bool (True, if the message is outgoing)
# @content : TD::PushMessageContent (Push message content)

class TD::NotificationType::NewPushMessage < TD::NotificationType
  include JSON::Serializable

  object_type "notificationTypeNewPushMessage"

  object_attributes({
    message_id: Int64,
    sender_id: TD::MessageSender,
    sender_name: String,
    is_outgoing: Bool,
    content: TD::PushMessageContent
  })
end