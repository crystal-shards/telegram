# New secret chat was created
#

class TD::NotificationType::NewSecretChat < TD::NotificationType
  include JSON::Serializable

  object_type "notificationTypeNewSecretChat"
end