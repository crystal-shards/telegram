# New call was received
#
# @call_id : Int32 (Call identifier)

class TD::NotificationType::NewCall < TD::NotificationType
  include JSON::Serializable

  object_type "notificationTypeNewCall"

  object_attributes({
    call_id: Int32
  })
end