# Represents a date range
#
# @start_date : Int32 (Point in time (Unix timestamp) at which the date range begins)
# @end_date : Int32 (Point in time (Unix timestamp) at which the date range ends)

class TD::DateRange < TD::Base
  include JSON::Serializable

  object_type "dateRange"

  object_attributes({
    start_date: Int32,
    end_date: Int32
  })
end