# A token for Simple Push API for Firefox OS
#
# @endpoint : String (Absolute URL exposed by the push service where the application server can send push messages; may be empty to deregister a device)

class TD::DeviceToken::SimplePush < TD::DeviceToken
  include JSON::Serializable

  object_type "deviceTokenSimplePush"

  object_attributes({
    endpoint: String
  })
end