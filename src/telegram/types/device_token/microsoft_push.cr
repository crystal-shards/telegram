# A token for Microsoft Push Notification Service
#
# @channel_uri : String (Push notification channel URI; may be empty to deregister a device)

class TD::DeviceToken::MicrosoftPush < TD::DeviceToken
  include JSON::Serializable

  object_type "deviceTokenMicrosoftPush"

  object_attributes({
    channel_uri: String
  })
end