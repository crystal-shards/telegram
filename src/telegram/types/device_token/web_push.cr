# A token for web Push API
#
# @endpoint : String (Absolute URL exposed by the push service where the application server can send push messages; may be empty to deregister a device)
# @p256dh_base64url : String (Base64url-encoded P-256 elliptic curve Diffie-Hellman public key)
# @auth_base64url : String (Base64url-encoded authentication secret)

class TD::DeviceToken::WebPush < TD::DeviceToken
  include JSON::Serializable

  object_type "deviceTokenWebPush"

  object_attributes({
    endpoint: String,
    p256dh_base64url: String,
    auth_base64url: String
  })
end