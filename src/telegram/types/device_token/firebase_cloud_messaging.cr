# A token for Firebase Cloud Messaging
#
# @token : String (Device registration token; may be empty to deregister a device)
# @encrypt : Bool (True, if push notifications must be additionally encrypted)

class TD::DeviceToken::FirebaseCloudMessaging < TD::DeviceToken
  include JSON::Serializable

  object_type "deviceTokenFirebaseCloudMessaging"

  object_attributes({
    token: String,
    encrypt: Bool
  })
end