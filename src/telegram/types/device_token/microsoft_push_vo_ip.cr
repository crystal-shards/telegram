# A token for Microsoft Push Notification Service VoIP channel
#
# @channel_uri : String (Push notification channel URI; may be empty to deregister a device)

class TD::DeviceToken::MicrosoftPushVoIP < TD::DeviceToken
  include JSON::Serializable

  object_type "deviceTokenMicrosoftPushVoIP"

  object_attributes({
    channel_uri: String
  })
end