# A token for Ubuntu Push Client service
#
# @token : String (Token; may be empty to deregister a device)

class TD::DeviceToken::UbuntuPush < TD::DeviceToken
  include JSON::Serializable

  object_type "deviceTokenUbuntuPush"

  object_attributes({
    token: String
  })
end