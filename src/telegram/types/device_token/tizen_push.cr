# A token for Tizen Push Service
#
# @reg_id : String (Push service registration identifier; may be empty to deregister a device)

class TD::DeviceToken::TizenPush < TD::DeviceToken
  include JSON::Serializable

  object_type "deviceTokenTizenPush"

  object_attributes({
    reg_id: String
  })
end