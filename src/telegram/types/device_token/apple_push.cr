# A token for Apple Push Notification service
#
# @device_token : String (Device token; may be empty to deregister a device)
# @is_app_sandbox : Bool (True, if App Sandbox is enabled)

class TD::DeviceToken::ApplePush < TD::DeviceToken
  include JSON::Serializable

  object_type "deviceTokenApplePush"

  object_attributes({
    device_token: String,
    is_app_sandbox: Bool
  })
end