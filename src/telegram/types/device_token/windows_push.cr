# A token for Windows Push Notification Services
#
# @access_token : String (The access token that will be used to send notifications; may be empty to deregister a device)

class TD::DeviceToken::WindowsPush < TD::DeviceToken
  include JSON::Serializable

  object_type "deviceTokenWindowsPush"

  object_attributes({
    access_token: String
  })
end