# A token for Apple Push Notification service VoIP notifications
#
# @device_token : String (Device token; may be empty to deregister a device)
# @is_app_sandbox : Bool (True, if App Sandbox is enabled)
# @encrypt : Bool (True, if push notifications must be additionally encrypted)

class TD::DeviceToken::ApplePushVoIP < TD::DeviceToken
  include JSON::Serializable

  object_type "deviceTokenApplePushVoIP"

  object_attributes({
    device_token: String,
    is_app_sandbox: Bool,
    encrypt: Bool
  })
end