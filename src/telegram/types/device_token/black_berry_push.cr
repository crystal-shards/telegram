# A token for BlackBerry Push Service
#
# @token : String (Token; may be empty to deregister a device)

class TD::DeviceToken::BlackBerryPush < TD::DeviceToken
  include JSON::Serializable

  object_type "deviceTokenBlackBerryPush"

  object_attributes({
    token: String
  })
end