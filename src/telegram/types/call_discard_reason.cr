# Describes the reason why a call was discarded
#
# Empty: The call wasn't discarded, or the reason is unknown
# Missed: The call was ended before the conversation started. It was canceled by the caller or missed by the other party
# Declined: The call was ended before the conversation started. It was declined by the other party
# Disconnected: The call was ended during the conversation because the users were disconnected
# HungUp: The call was ended because one of the parties hung up

abstract class TD::CallDiscardReason < TD::Base
end