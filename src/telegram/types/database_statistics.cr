# Contains database statistics
#
# @statistics : String (Database statistics in an unspecified human-readable format)

class TD::DatabaseStatistics < TD::Base
  include JSON::Serializable

  object_type "databaseStatistics"

  object_attributes({
    statistics: String
  })
end