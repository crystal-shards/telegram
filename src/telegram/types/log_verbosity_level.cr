# Contains a TDLib internal log verbosity level
#
# @verbosity_level : Int32 (Log verbosity level)

class TD::LogVerbosityLevel < TD::Base
  include JSON::Serializable

  object_type "logVerbosityLevel"

  object_attributes({
    verbosity_level: Int32
  })
end