# A subscript rich text
#
# @text : TD::RichText (Text)

class TD::RichText::Subscript < TD::RichText
  include JSON::Serializable

  object_type "richTextSubscript"

  object_attributes({
    text: TD::RichText
  })
end