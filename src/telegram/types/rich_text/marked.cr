# A marked rich text
#
# @text : TD::RichText (Text)

class TD::RichText::Marked < TD::RichText
  include JSON::Serializable

  object_type "richTextMarked"

  object_attributes({
    text: TD::RichText
  })
end