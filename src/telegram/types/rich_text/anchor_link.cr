# A link to an anchor on the same web page
#
# @text : TD::RichText (The link text)
# @anchor_name : String (The anchor name. If the name is empty, the link must bring back to top)
# @url : String (An HTTP URL, opening the anchor)

class TD::RichText::AnchorLink < TD::RichText
  include JSON::Serializable

  object_type "richTextAnchorLink"

  object_attributes({
    text: TD::RichText,
    anchor_name: String,
    url: String
  })
end