# A superscript rich text
#
# @text : TD::RichText (Text)

class TD::RichText::Superscript < TD::RichText
  include JSON::Serializable

  object_type "richTextSuperscript"

  object_attributes({
    text: TD::RichText
  })
end