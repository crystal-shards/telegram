# A reference to a richTexts object on the same web page
#
# @text : TD::RichText (The text)
# @anchor_name : String (The name of a richTextAnchor object, which is the first element of the target richTexts object)
# @url : String (An HTTP URL, opening the reference)

class TD::RichText::Reference < TD::RichText
  include JSON::Serializable

  object_type "richTextReference"

  object_attributes({
    text: TD::RichText,
    anchor_name: String,
    url: String
  })
end