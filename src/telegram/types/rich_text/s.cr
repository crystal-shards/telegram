# A concatenation of rich texts
#
# @texts : Array(TD::RichText) (Texts)

class TD::RichText::S < TD::RichText
  include JSON::Serializable

  object_type "richTexts"

  object_attributes({
    texts: Array(TD::RichText)
  })
end