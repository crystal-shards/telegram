# A rich text email link
#
# @text : TD::RichText (Text)
# @email_address : String (Email address)

class TD::RichText::EmailAddress < TD::RichText
  include JSON::Serializable

  object_type "richTextEmailAddress"

  object_attributes({
    text: TD::RichText,
    email_address: String
  })
end