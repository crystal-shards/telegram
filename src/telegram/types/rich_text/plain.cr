# A plain text
#
# @text : String (Text)

class TD::RichText::Plain < TD::RichText
  include JSON::Serializable

  object_type "richTextPlain"

  object_attributes({
    text: String
  })
end