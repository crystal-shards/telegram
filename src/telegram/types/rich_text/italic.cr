# An italicized rich text
#
# @text : TD::RichText (Text)

class TD::RichText::Italic < TD::RichText
  include JSON::Serializable

  object_type "richTextItalic"

  object_attributes({
    text: TD::RichText
  })
end