# A bold rich text
#
# @text : TD::RichText (Text)

class TD::RichText::Bold < TD::RichText
  include JSON::Serializable

  object_type "richTextBold"

  object_attributes({
    text: TD::RichText
  })
end