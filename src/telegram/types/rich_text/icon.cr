# A small image inside the text
#
# @document : TD::Document (The image represented as a document. The image can be in GIF, JPEG or PNG format)
# @width : Int32 (Width of a bounding box in which the image must be shown; 0 if unknown)
# @height : Int32 (Height of a bounding box in which the image must be shown; 0 if unknown)

class TD::RichText::Icon < TD::RichText
  include JSON::Serializable

  object_type "richTextIcon"

  object_attributes({
    document: TD::Document,
    width: Int32,
    height: Int32
  })
end