# A rich text phone number
#
# @text : TD::RichText (Text)
# @phone_number : String (Phone number)

class TD::RichText::PhoneNumber < TD::RichText
  include JSON::Serializable

  object_type "richTextPhoneNumber"

  object_attributes({
    text: TD::RichText,
    phone_number: String
  })
end