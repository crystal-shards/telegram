# A strikethrough rich text
#
# @text : TD::RichText (Text)

class TD::RichText::Strikethrough < TD::RichText
  include JSON::Serializable

  object_type "richTextStrikethrough"

  object_attributes({
    text: TD::RichText
  })
end