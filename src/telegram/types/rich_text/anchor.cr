# An anchor
#
# @name : String (Anchor name)

class TD::RichText::Anchor < TD::RichText
  include JSON::Serializable

  object_type "richTextAnchor"

  object_attributes({
    name: String
  })
end