# A rich text URL link
#
# @text : TD::RichText (Text)
# @url : String (URL)
# @is_cached : Bool (True, if the URL has cached instant view server-side)

class TD::RichText::Url < TD::RichText
  include JSON::Serializable

  object_type "richTextUrl"

  object_attributes({
    text: TD::RichText,
    url: String,
    is_cached: Bool
  })
end