# An underlined rich text
#
# @text : TD::RichText (Text)

class TD::RichText::Underline < TD::RichText
  include JSON::Serializable

  object_type "richTextUnderline"

  object_attributes({
    text: TD::RichText
  })
end