# A fixed-width rich text
#
# @text : TD::RichText (Text)

class TD::RichText::Fixed < TD::RichText
  include JSON::Serializable

  object_type "richTextFixed"

  object_attributes({
    text: TD::RichText
  })
end