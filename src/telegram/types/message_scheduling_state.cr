# Contains information about the time when a scheduled message will be sent
#
# SendAtDate: The message will be sent at the specified date
# SendWhenOnline: The message will be sent when the peer will be online. Applicable to private chats only and when the exact online status of the peer is known

abstract class TD::MessageSchedulingState < TD::Base
end