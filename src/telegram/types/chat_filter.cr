# Represents a filter of user chats
#
# @title : String (The title of the filter; 1-12 characters without line feeds)
# @icon_name : String (The chosen icon name for short filter representation. If non-empty, must be one of "All", "Unread", "Unmuted", "Bots", "Channels", "Groups", "Private", "Custom", "Setup", "Cat", "Crown", "Favorite", "Flower", "Game", "Home", "Love", "Mask", "Party", "Sport", "Study", "Trade", "Travel", "Work".If empty, use getChatFilterDefaultIconName to get default icon name for the filter)
# @pinned_chat_ids : Array(Int64) (The chat identifiers of pinned chats in the filtered chat list)
# @included_chat_ids : Array(Int64) (The chat identifiers of always included chats in the filtered chat list)
# @excluded_chat_ids : Array(Int64) (The chat identifiers of always excluded chats in the filtered chat list)
# @exclude_muted : Bool (True, if muted chats need to be excluded)
# @exclude_read : Bool (True, if read chats need to be excluded)
# @exclude_archived : Bool (True, if archived chats need to be excluded)
# @include_contacts : Bool (True, if contacts need to be included)
# @include_non_contacts : Bool (True, if non-contact users need to be included)
# @include_bots : Bool (True, if bots need to be included)
# @include_groups : Bool (True, if basic groups and supergroups need to be included)
# @include_channels : Bool (True, if channels need to be included)

class TD::ChatFilter < TD::Base
  include JSON::Serializable

  object_type "chatFilter"

  object_attributes({
    title: String,
    icon_name: String,
    pinned_chat_ids: Array(Int64),
    included_chat_ids: Array(Int64),
    excluded_chat_ids: Array(Int64),
    exclude_muted: Bool,
    exclude_read: Bool,
    exclude_archived: Bool,
    include_contacts: Bool,
    include_non_contacts: Bool,
    include_bots: Bool,
    include_groups: Bool,
    include_channels: Bool
  })
end