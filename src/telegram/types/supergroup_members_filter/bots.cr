# Returns bot members of the supergroup or channel
#

class TD::SupergroupMembersFilter::Bots < TD::SupergroupMembersFilter
  include JSON::Serializable

  object_type "supergroupMembersFilterBots"
end