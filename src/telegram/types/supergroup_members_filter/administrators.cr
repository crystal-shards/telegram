# Returns the owner and administrators
#

class TD::SupergroupMembersFilter::Administrators < TD::SupergroupMembersFilter
  include JSON::Serializable

  object_type "supergroupMembersFilterAdministrators"
end