# Used to search for supergroup or channel members via a (string) query
#
# @query : String (Query to search for)

class TD::SupergroupMembersFilter::Search < TD::SupergroupMembersFilter
  include JSON::Serializable

  object_type "supergroupMembersFilterSearch"

  object_attributes({
    query: String
  })
end