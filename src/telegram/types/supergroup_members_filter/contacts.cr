# Returns contacts of the user, which are members of the supergroup or channel
#
# @query : String (Query to search for)

class TD::SupergroupMembersFilter::Contacts < TD::SupergroupMembersFilter
  include JSON::Serializable

  object_type "supergroupMembersFilterContacts"

  object_attributes({
    query: String
  })
end