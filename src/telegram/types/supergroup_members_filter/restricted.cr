# Returns restricted supergroup members; can be used only by administrators
#
# @query : String (Query to search for)

class TD::SupergroupMembersFilter::Restricted < TD::SupergroupMembersFilter
  include JSON::Serializable

  object_type "supergroupMembersFilterRestricted"

  object_attributes({
    query: String
  })
end