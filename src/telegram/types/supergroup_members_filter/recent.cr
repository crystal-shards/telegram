# Returns recently active users in reverse chronological order
#

class TD::SupergroupMembersFilter::Recent < TD::SupergroupMembersFilter
  include JSON::Serializable

  object_type "supergroupMembersFilterRecent"
end