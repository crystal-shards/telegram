# Returns users banned from the supergroup or channel; can be used only by administrators
#
# @query : String (Query to search for)

class TD::SupergroupMembersFilter::Banned < TD::SupergroupMembersFilter
  include JSON::Serializable

  object_type "supergroupMembersFilterBanned"

  object_attributes({
    query: String
  })
end