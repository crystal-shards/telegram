# Returns users which can be mentioned in the supergroup
#
# @query : String (Query to search for)
# @message_thread_id : Int64 (If non-zero, the identifier of the current message thread)

class TD::SupergroupMembersFilter::Mention < TD::SupergroupMembersFilter
  include JSON::Serializable

  object_type "supergroupMembersFilterMention"

  object_attributes({
    query: String,
    message_thread_id: Int64
  })
end