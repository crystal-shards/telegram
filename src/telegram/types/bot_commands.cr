# Contains a list of bot commands
#
# @bot_user_id : Int64 (Bot's user identifier)
# @commands : Array(TD::BotCommand) (List of bot commands)

class TD::BotCommands < TD::Base
  include JSON::Serializable

  object_type "botCommands"

  object_attributes({
    bot_user_id: Int64,
    commands: Array(TD::BotCommand)
  })
end