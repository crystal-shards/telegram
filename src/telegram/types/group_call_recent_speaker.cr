# Describes a recently speaking participant in a group call
#
# @participant_id : TD::MessageSender (Group call participant identifier)
# @is_speaking : Bool (True, is the user has spoken recently)

class TD::GroupCallRecentSpeaker < TD::Base
  include JSON::Serializable

  object_type "groupCallRecentSpeaker"

  object_attributes({
    participant_id: TD::MessageSender,
    is_speaking: Bool
  })
end