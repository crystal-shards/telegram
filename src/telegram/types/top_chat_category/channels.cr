# A category containing frequently used channels
#

class TD::TopChatCategory::Channels < TD::TopChatCategory
  include JSON::Serializable

  object_type "topChatCategoryChannels"
end