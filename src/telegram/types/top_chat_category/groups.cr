# A category containing frequently used basic groups and supergroups
#

class TD::TopChatCategory::Groups < TD::TopChatCategory
  include JSON::Serializable

  object_type "topChatCategoryGroups"
end