# A category containing frequently used chats used for calls
#

class TD::TopChatCategory::Calls < TD::TopChatCategory
  include JSON::Serializable

  object_type "topChatCategoryCalls"
end