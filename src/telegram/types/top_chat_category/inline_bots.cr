# A category containing frequently used chats with inline bots sorted by their usage in inline mode
#

class TD::TopChatCategory::InlineBots < TD::TopChatCategory
  include JSON::Serializable

  object_type "topChatCategoryInlineBots"
end