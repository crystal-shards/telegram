# A category containing frequently used private chats with non-bot users
#

class TD::TopChatCategory::Users < TD::TopChatCategory
  include JSON::Serializable

  object_type "topChatCategoryUsers"
end