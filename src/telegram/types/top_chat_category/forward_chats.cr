# A category containing frequently used chats used to forward messages
#

class TD::TopChatCategory::ForwardChats < TD::TopChatCategory
  include JSON::Serializable

  object_type "topChatCategoryForwardChats"
end