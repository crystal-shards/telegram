# A category containing frequently used private chats with bot users
#

class TD::TopChatCategory::Bots < TD::TopChatCategory
  include JSON::Serializable

  object_type "topChatCategoryBots"
end