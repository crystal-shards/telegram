# Contains information about a link to a message in a chat
#
# @is_public : Bool (True, if the link is a public link for a message in a chat)
# @chat_id : Int64 (If found, identifier of the chat to which the message belongs, 0 otherwise)
# @message : TD::Message? (If found, the linked message; may be null)
# @media_timestamp : Int32 (Timestamp from which the video)
# @for_album : Bool (True, if the whole media album to which the message belongs is linked)
# @for_comment : Bool (True, if the message is linked as a channel post comment or from a message thread)

class TD::MessageLinkInfo < TD::Base
  include JSON::Serializable

  object_type "messageLinkInfo"

  object_attributes({
    is_public: Bool,
    chat_id: Int64,
    message: TD::Message?,
    media_timestamp: Int32,
    for_album: Bool,
    for_comment: Bool
  })
end