# A new member joined the chat via an invite link
#

class TD::MessageContent::ChatJoinByLink < TD::MessageContent
  include JSON::Serializable

  object_type "messageChatJoinByLink"
end