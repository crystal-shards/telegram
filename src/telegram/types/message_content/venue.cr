# A message with information about a venue
#
# @venue : TD::Venue (The venue description)

class TD::MessageContent::Venue < TD::MessageContent
  include JSON::Serializable

  object_type "messageVenue"

  object_attributes({
    venue: TD::Venue
  })
end