# A deleted chat photo
#

class TD::MessageContent::ChatDeletePhoto < TD::MessageContent
  include JSON::Serializable

  object_type "messageChatDeletePhoto"
end