# Message content that is not supported in the current TDLib version
#

class TD::MessageContent::Unsupported < TD::MessageContent
  include JSON::Serializable

  object_type "messageUnsupported"
end