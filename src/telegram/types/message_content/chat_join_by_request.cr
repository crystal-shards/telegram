# A new member was accepted to the chat by an administrator
#

class TD::MessageContent::ChatJoinByRequest < TD::MessageContent
  include JSON::Serializable

  object_type "messageChatJoinByRequest"
end