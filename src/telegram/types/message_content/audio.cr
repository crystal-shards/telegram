# An audio message
#
# @audio : TD::Audio (The audio description)
# @caption : TD::FormattedText (Audio caption)

class TD::MessageContent::Audio < TD::MessageContent
  include JSON::Serializable

  object_type "messageAudio"

  object_attributes({
    audio: TD::Audio,
    caption: TD::FormattedText
  })
end