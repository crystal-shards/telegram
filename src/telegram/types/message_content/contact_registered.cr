# A contact has registered with Telegram
#

class TD::MessageContent::ContactRegistered < TD::MessageContent
  include JSON::Serializable

  object_type "messageContactRegistered"
end