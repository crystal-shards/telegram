# A newly created video chat
#
# @group_call_id : Int32 (Identifier of the video chat. The video chat can be received through the method getGroupCall)

class TD::MessageContent::VideoChatStarted < TD::MessageContent
  include JSON::Serializable

  object_type "messageVideoChatStarted"

  object_attributes({
    group_call_id: Int32
  })
end