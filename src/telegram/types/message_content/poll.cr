# A message with a poll
#
# @poll : TD::Poll (The poll description)

class TD::MessageContent::Poll < TD::MessageContent
  include JSON::Serializable

  object_type "messagePoll"

  object_attributes({
    poll: TD::Poll
  })
end