# A sticker message
#
# @sticker : TD::Sticker (The sticker description)

class TD::MessageContent::Sticker < TD::MessageContent
  include JSON::Serializable

  object_type "messageSticker"

  object_attributes({
    sticker: TD::Sticker
  })
end