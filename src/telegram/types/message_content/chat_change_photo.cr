# An updated chat photo
#
# @photo : TD::ChatPhoto (New chat photo)

class TD::MessageContent::ChatChangePhoto < TD::MessageContent
  include JSON::Serializable

  object_type "messageChatChangePhoto"

  object_attributes({
    photo: TD::ChatPhoto
  })
end