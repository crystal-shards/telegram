# A payment has been completed
#
# @invoice_chat_id : Int64 (Identifier of the chat, containing the corresponding invoice message; 0 if unknown)
# @invoice_message_id : Int64 (Identifier of the message with the corresponding invoice; can be an identifier of a deleted message)
# @currency : String (Currency for the price of the product)
# @total_amount : Int64 (Total price for the product, in the smallest units of the currency)

class TD::MessageContent::PaymentSuccessful < TD::MessageContent
  include JSON::Serializable

  object_type "messagePaymentSuccessful"

  object_attributes({
    invoice_chat_id: Int64,
    invoice_message_id: Int64,
    currency: String,
    total_amount: Int64
  })
end