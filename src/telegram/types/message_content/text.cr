# A text message
#
# @text : TD::FormattedText (Text of the message)
# @web_page : TD::WebPage? (A preview of the web page that's mentioned in the text; may be null)

class TD::MessageContent::Text < TD::MessageContent
  include JSON::Serializable

  object_type "messageText"

  object_attributes({
    text: TD::FormattedText,
    web_page: TD::WebPage?
  })
end