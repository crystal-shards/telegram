# A message with information about an ended call
#
# @is_video : Bool (True, if the call was a video call)
# @discard_reason : TD::CallDiscardReason (Reason why the call was discarded)
# @duration : Int32 (Call duration, in seconds)

class TD::MessageContent::Call < TD::MessageContent
  include JSON::Serializable

  object_type "messageCall"

  object_attributes({
    is_video: Bool,
    discard_reason: TD::CallDiscardReason,
    duration: Int32
  })
end