# A message with an invoice from a bot
#
# @title : String (Product title)
# @description : String (Product description)
# @photo : TD::Photo? (Product photo; may be null)
# @currency : String (Currency for the product price)
# @total_amount : Int64 (Product total price in the smallest units of the currency)
# @start_parameter : String (Unique invoice bot start_parameter. To share an invoice use the URL https:)
# @is_test : Bool (True, if the invoice is a test invoice)
# @need_shipping_address : Bool (True, if the shipping address must be specified)
# @receipt_message_id : Int64 (The identifier of the message with the receipt, after the product has been purchased)

class TD::MessageContent::Invoice < TD::MessageContent
  include JSON::Serializable

  object_type "messageInvoice"

  object_attributes({
    title: String,
    description: String,
    photo: TD::Photo?,
    currency: String,
    total_amount: Int64,
    start_parameter: String,
    is_test: Bool,
    need_shipping_address: Bool,
    receipt_message_id: Int64
  })
end