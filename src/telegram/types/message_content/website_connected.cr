# The current user has connected a website by logging in using Telegram Login Widget on it
#
# @domain_name : String (Domain name of the connected website)

class TD::MessageContent::WebsiteConnected < TD::MessageContent
  include JSON::Serializable

  object_type "messageWebsiteConnected"

  object_attributes({
    domain_name: String
  })
end