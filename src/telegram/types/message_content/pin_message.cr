# A message has been pinned
#
# @message_id : Int64 (Identifier of the pinned message, can be an identifier of a deleted message or 0)

class TD::MessageContent::PinMessage < TD::MessageContent
  include JSON::Serializable

  object_type "messagePinMessage"

  object_attributes({
    message_id: Int64
  })
end