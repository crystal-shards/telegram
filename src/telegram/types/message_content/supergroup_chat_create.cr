# A newly created supergroup or channel
#
# @title : String (Title of the supergroup or channel)

class TD::MessageContent::SupergroupChatCreate < TD::MessageContent
  include JSON::Serializable

  object_type "messageSupergroupChatCreate"

  object_attributes({
    title: String
  })
end