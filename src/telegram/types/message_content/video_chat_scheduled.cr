# A new video chat was scheduled
#
# @group_call_id : Int32 (Identifier of the video chat. The video chat can be received through the method getGroupCall)
# @start_date : Int32 (Point in time (Unix timestamp) when the group call is supposed to be started by an administrator)

class TD::MessageContent::VideoChatScheduled < TD::MessageContent
  include JSON::Serializable

  object_type "messageVideoChatScheduled"

  object_attributes({
    group_call_id: Int32,
    start_date: Int32
  })
end