# A message with a location
#
# @location : TD::Location (The location description)
# @live_period : Int32 (Time relative to the message send date, for which the location can be updated, in seconds)
# @expires_in : Int32 (Left time for which the location can be updated, in seconds. updateMessageContent is not sent when this field changes)
# @heading : Int32 (For live locations, a direction in which the location moves, in degrees; 1-360. If 0 the direction is unknown)
# @proximity_alert_radius : Int32 (For live locations, a maximum distance to another chat member for proximity alerts, in meters (0-100000). 0 if the notification is disabled. Available only for the message sender)

class TD::MessageContent::Location < TD::MessageContent
  include JSON::Serializable

  object_type "messageLocation"

  object_attributes({
    location: TD::Location,
    live_period: Int32,
    expires_in: Int32,
    heading: Int32,
    proximity_alert_radius: Int32
  })
end