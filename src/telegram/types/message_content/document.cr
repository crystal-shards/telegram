# A document message (general file)
#
# @document : TD::Document (The document description)
# @caption : TD::FormattedText (Document caption)

class TD::MessageContent::Document < TD::MessageContent
  include JSON::Serializable

  object_type "messageDocument"

  object_attributes({
    document: TD::Document,
    caption: TD::FormattedText
  })
end