# A message with information about an invite to a video chat
#
# @group_call_id : Int32 (Identifier of the video chat. The video chat can be received through the method getGroupCall)
# @user_ids : Array(Int64) (Invited user identifiers)

class TD::MessageContent::InviteVideoChatParticipants < TD::MessageContent
  include JSON::Serializable

  object_type "messageInviteVideoChatParticipants"

  object_attributes({
    group_call_id: Int32,
    user_ids: Array(Int64)
  })
end