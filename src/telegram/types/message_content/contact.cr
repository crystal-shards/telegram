# A message with a user contact
#
# @contact : TD::Contact (The contact description)

class TD::MessageContent::Contact < TD::MessageContent
  include JSON::Serializable

  object_type "messageContact"

  object_attributes({
    contact: TD::Contact
  })
end