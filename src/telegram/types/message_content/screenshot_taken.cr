# A screenshot of a message in the chat has been taken
#

class TD::MessageContent::ScreenshotTaken < TD::MessageContent
  include JSON::Serializable

  object_type "messageScreenshotTaken"
end