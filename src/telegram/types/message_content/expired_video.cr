# An expired video message (self-destructed after TTL has elapsed)
#

class TD::MessageContent::ExpiredVideo < TD::MessageContent
  include JSON::Serializable

  object_type "messageExpiredVideo"
end