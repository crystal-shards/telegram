# A photo message
#
# @photo : TD::Photo (The photo description)
# @caption : TD::FormattedText (Photo caption)
# @is_secret : Bool (True, if the photo must be blurred and must be shown only while tapped)

class TD::MessageContent::Photo < TD::MessageContent
  include JSON::Serializable

  object_type "messagePhoto"

  object_attributes({
    photo: TD::Photo,
    caption: TD::FormattedText,
    is_secret: Bool
  })
end