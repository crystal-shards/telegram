# A user in the chat came within proximity alert range
#
# @traveler_id : TD::MessageSender (The identifier of a user or chat that triggered the proximity alert)
# @watcher_id : TD::MessageSender (The identifier of a user or chat that subscribed for the proximity alert)
# @distance : Int32 (The distance between the users)

class TD::MessageContent::ProximityAlertTriggered < TD::MessageContent
  include JSON::Serializable

  object_type "messageProximityAlertTriggered"

  object_attributes({
    traveler_id: TD::MessageSender,
    watcher_id: TD::MessageSender,
    distance: Int32
  })
end