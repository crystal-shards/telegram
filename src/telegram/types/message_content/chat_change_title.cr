# An updated chat title
#
# @title : String (New chat title)

class TD::MessageContent::ChatChangeTitle < TD::MessageContent
  include JSON::Serializable

  object_type "messageChatChangeTitle"

  object_attributes({
    title: String
  })
end