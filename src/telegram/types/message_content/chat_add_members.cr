# New chat members were added
#
# @member_user_ids : Array(Int64) (User identifiers of the new members)

class TD::MessageContent::ChatAddMembers < TD::MessageContent
  include JSON::Serializable

  object_type "messageChatAddMembers"

  object_attributes({
    member_user_ids: Array(Int64)
  })
end