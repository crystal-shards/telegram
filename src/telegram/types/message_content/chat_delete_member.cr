# A chat member was deleted
#
# @user_id : Int64 (User identifier of the deleted chat member)

class TD::MessageContent::ChatDeleteMember < TD::MessageContent
  include JSON::Serializable

  object_type "messageChatDeleteMember"

  object_attributes({
    user_id: Int64
  })
end