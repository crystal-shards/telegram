# A message with a game
#
# @game : TD::Game (The game description)

class TD::MessageContent::Game < TD::MessageContent
  include JSON::Serializable

  object_type "messageGame"

  object_attributes({
    game: TD::Game
  })
end