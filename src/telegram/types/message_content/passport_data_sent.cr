# Telegram Passport data has been sent
#
# @types : Array(TD::PassportElementType) (List of Telegram Passport element types sent)

class TD::MessageContent::PassportDataSent < TD::MessageContent
  include JSON::Serializable

  object_type "messagePassportDataSent"

  object_attributes({
    types: Array(TD::PassportElementType)
  })
end