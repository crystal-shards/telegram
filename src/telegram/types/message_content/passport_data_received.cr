# Telegram Passport data has been received; for bots only
#
# @elements : Array(TD::EncryptedPassportElement) (List of received Telegram Passport elements)
# @credentials : TD::EncryptedCredentials (Encrypted data credentials)

class TD::MessageContent::PassportDataReceived < TD::MessageContent
  include JSON::Serializable

  object_type "messagePassportDataReceived"

  object_attributes({
    elements: Array(TD::EncryptedPassportElement),
    credentials: TD::EncryptedCredentials
  })
end