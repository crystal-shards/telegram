# A basic group was upgraded to a supergroup and was deactivated as the result
#
# @supergroup_id : Int64 (Identifier of the supergroup to which the basic group was upgraded)

class TD::MessageContent::ChatUpgradeTo < TD::MessageContent
  include JSON::Serializable

  object_type "messageChatUpgradeTo"

  object_attributes({
    supergroup_id: Int64
  })
end