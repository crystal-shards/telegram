# The TTL (Time To Live) setting for messages in the chat has been changed
#
# @ttl : Int32 (New message TTL)

class TD::MessageContent::ChatSetTtl < TD::MessageContent
  include JSON::Serializable

  object_type "messageChatSetTtl"

  object_attributes({
    ttl: Int32
  })
end