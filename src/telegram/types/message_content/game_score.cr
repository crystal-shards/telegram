# A new high score was achieved in a game
#
# @game_message_id : Int64 (Identifier of the message with the game, can be an identifier of a deleted message)
# @game_id : String (Identifier of the game; may be different from the games presented in the message with the game)
# @score : Int32 (New score)

class TD::MessageContent::GameScore < TD::MessageContent
  include JSON::Serializable

  object_type "messageGameScore"

  object_attributes({
    game_message_id: Int64,
    game_id: String,
    score: Int32
  })
end