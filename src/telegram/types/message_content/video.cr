# A video message
#
# @video : TD::Video (The video description)
# @caption : TD::FormattedText (Video caption)
# @is_secret : Bool (True, if the video thumbnail must be blurred and the video must be shown only while tapped)

class TD::MessageContent::Video < TD::MessageContent
  include JSON::Serializable

  object_type "messageVideo"

  object_attributes({
    video: TD::Video,
    caption: TD::FormattedText,
    is_secret: Bool
  })
end