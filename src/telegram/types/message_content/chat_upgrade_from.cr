# A supergroup has been created from a basic group
#
# @title : String (Title of the newly created supergroup)
# @basic_group_id : Int64 (The identifier of the original basic group)

class TD::MessageContent::ChatUpgradeFrom < TD::MessageContent
  include JSON::Serializable

  object_type "messageChatUpgradeFrom"

  object_attributes({
    title: String,
    basic_group_id: Int64
  })
end