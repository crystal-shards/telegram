# A video note message
#
# @video_note : TD::VideoNote (The video note description)
# @is_viewed : Bool (True, if at least one of the recipients has viewed the video note)
# @is_secret : Bool (True, if the video note thumbnail must be blurred and the video note must be shown only while tapped)

class TD::MessageContent::VideoNote < TD::MessageContent
  include JSON::Serializable

  object_type "messageVideoNote"

  object_attributes({
    video_note: TD::VideoNote,
    is_viewed: Bool,
    is_secret: Bool
  })
end