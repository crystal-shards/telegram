# A message with an animated emoji
#
# @animated_emoji : TD::AnimatedEmoji (The animated emoji)
# @emoji : String (The corresponding emoji)

class TD::MessageContent::AnimatedEmoji < TD::MessageContent
  include JSON::Serializable

  object_type "messageAnimatedEmoji"

  object_attributes({
    animated_emoji: TD::AnimatedEmoji,
    emoji: String
  })
end