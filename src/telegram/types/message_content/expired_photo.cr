# An expired photo message (self-destructed after TTL has elapsed)
#

class TD::MessageContent::ExpiredPhoto < TD::MessageContent
  include JSON::Serializable

  object_type "messageExpiredPhoto"
end