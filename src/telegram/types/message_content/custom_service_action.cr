# A non-standard action has happened in the chat
#
# @text : String (Message text to be shown in the chat)

class TD::MessageContent::CustomServiceAction < TD::MessageContent
  include JSON::Serializable

  object_type "messageCustomServiceAction"

  object_attributes({
    text: String
  })
end