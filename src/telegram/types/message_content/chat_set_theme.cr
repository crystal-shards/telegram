# A theme in the chat has been changed
#
# @theme_name : String (If non-empty, name of a new theme, set for the chat. Otherwise chat theme was reset to the default one)

class TD::MessageContent::ChatSetTheme < TD::MessageContent
  include JSON::Serializable

  object_type "messageChatSetTheme"

  object_attributes({
    theme_name: String
  })
end