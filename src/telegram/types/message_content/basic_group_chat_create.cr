# A newly created basic group
#
# @title : String (Title of the basic group)
# @member_user_ids : Array(Int64) (User identifiers of members in the basic group)

class TD::MessageContent::BasicGroupChatCreate < TD::MessageContent
  include JSON::Serializable

  object_type "messageBasicGroupChatCreate"

  object_attributes({
    title: String,
    member_user_ids: Array(Int64)
  })
end