# A payment has been completed; for bots only
#
# @currency : String (Currency for price of the product)
# @total_amount : Int64 (Total price for the product, in the smallest units of the currency)
# @invoice_payload : String (Invoice payload)
# @shipping_option_id : String (Identifier of the shipping option chosen by the user; may be empty if not applicable)
# @order_info : TD::OrderInfo? (Information about the order; may be null)
# @telegram_payment_charge_id : String (Telegram payment identifier)
# @provider_payment_charge_id : String (Provider payment identifier)

class TD::MessageContent::PaymentSuccessfulBot < TD::MessageContent
  include JSON::Serializable

  object_type "messagePaymentSuccessfulBot"

  object_attributes({
    currency: String,
    total_amount: Int64,
    invoice_payload: String,
    shipping_option_id: String,
    order_info: TD::OrderInfo?,
    telegram_payment_charge_id: String,
    provider_payment_charge_id: String
  })
end