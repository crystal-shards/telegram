# An animation message (GIF-style).
#
# @animation : TD::Animation (The animation description)
# @caption : TD::FormattedText (Animation caption)
# @is_secret : Bool (True, if the animation thumbnail must be blurred and the animation must be shown only while tapped)

class TD::MessageContent::Animation < TD::MessageContent
  include JSON::Serializable

  object_type "messageAnimation"

  object_attributes({
    animation: TD::Animation,
    caption: TD::FormattedText,
    is_secret: Bool
  })
end