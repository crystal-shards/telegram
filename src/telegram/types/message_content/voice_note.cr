# A voice note message
#
# @voice_note : TD::VoiceNote (The voice note description)
# @caption : TD::FormattedText (Voice note caption)
# @is_listened : Bool (True, if at least one of the recipients has listened to the voice note)

class TD::MessageContent::VoiceNote < TD::MessageContent
  include JSON::Serializable

  object_type "messageVoiceNote"

  object_attributes({
    voice_note: TD::VoiceNote,
    caption: TD::FormattedText,
    is_listened: Bool
  })
end