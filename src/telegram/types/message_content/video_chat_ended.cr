# A message with information about an ended video chat
#
# @duration : Int32 (Call duration, in seconds)

class TD::MessageContent::VideoChatEnded < TD::MessageContent
  include JSON::Serializable

  object_type "messageVideoChatEnded"

  object_attributes({
    duration: Int32
  })
end