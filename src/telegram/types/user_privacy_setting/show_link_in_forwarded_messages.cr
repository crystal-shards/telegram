# A privacy setting for managing whether a link to the user's account is included in forwarded messages
#

class TD::UserPrivacySetting::ShowLinkInForwardedMessages < TD::UserPrivacySetting
  include JSON::Serializable

  object_type "userPrivacySettingShowLinkInForwardedMessages"
end