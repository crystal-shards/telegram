# A privacy setting for managing whether the user can be invited to chats
#

class TD::UserPrivacySetting::AllowChatInvites < TD::UserPrivacySetting
  include JSON::Serializable

  object_type "userPrivacySettingAllowChatInvites"
end