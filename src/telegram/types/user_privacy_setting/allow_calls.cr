# A privacy setting for managing whether the user can be called
#

class TD::UserPrivacySetting::AllowCalls < TD::UserPrivacySetting
  include JSON::Serializable

  object_type "userPrivacySettingAllowCalls"
end