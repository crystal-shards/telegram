# A privacy setting for managing whether the user's profile photo is visible
#

class TD::UserPrivacySetting::ShowProfilePhoto < TD::UserPrivacySetting
  include JSON::Serializable

  object_type "userPrivacySettingShowProfilePhoto"
end