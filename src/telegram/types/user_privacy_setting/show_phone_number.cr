# A privacy setting for managing whether the user's phone number is visible
#

class TD::UserPrivacySetting::ShowPhoneNumber < TD::UserPrivacySetting
  include JSON::Serializable

  object_type "userPrivacySettingShowPhoneNumber"
end