# A privacy setting for managing whether the user's online status is visible
#

class TD::UserPrivacySetting::ShowStatus < TD::UserPrivacySetting
  include JSON::Serializable

  object_type "userPrivacySettingShowStatus"
end