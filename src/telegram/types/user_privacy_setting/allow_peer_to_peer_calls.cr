# A privacy setting for managing whether peer-to-peer connections can be used for calls
#

class TD::UserPrivacySetting::AllowPeerToPeerCalls < TD::UserPrivacySetting
  include JSON::Serializable

  object_type "userPrivacySettingAllowPeerToPeerCalls"
end