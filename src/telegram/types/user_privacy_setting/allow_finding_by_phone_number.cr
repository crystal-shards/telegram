# A privacy setting for managing whether the user can be found by their phone number. Checked only if the phone number is not known to the other user. Can be set only to "Allow contacts" or "Allow all"
#

class TD::UserPrivacySetting::AllowFindingByPhoneNumber < TD::UserPrivacySetting
  include JSON::Serializable

  object_type "userPrivacySettingAllowFindingByPhoneNumber"
end