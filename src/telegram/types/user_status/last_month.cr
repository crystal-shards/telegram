# The user is offline, but was online last month
#

class TD::UserStatus::LastMonth < TD::UserStatus
  include JSON::Serializable

  object_type "userStatusLastMonth"
end