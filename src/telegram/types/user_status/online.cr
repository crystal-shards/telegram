# The user is online
#
# @expires : Int32 (Point in time (Unix timestamp) when the user's online status will expire)

class TD::UserStatus::Online < TD::UserStatus
  include JSON::Serializable

  object_type "userStatusOnline"

  object_attributes({
    expires: Int32
  })
end