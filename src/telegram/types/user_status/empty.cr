# The user status was never changed
#

class TD::UserStatus::Empty < TD::UserStatus
  include JSON::Serializable

  object_type "userStatusEmpty"
end