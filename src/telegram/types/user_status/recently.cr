# The user was online recently
#

class TD::UserStatus::Recently < TD::UserStatus
  include JSON::Serializable

  object_type "userStatusRecently"
end