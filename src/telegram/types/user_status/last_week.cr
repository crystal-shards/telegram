# The user is offline, but was online last week
#

class TD::UserStatus::LastWeek < TD::UserStatus
  include JSON::Serializable

  object_type "userStatusLastWeek"
end