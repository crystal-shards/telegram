# The user is offline
#
# @was_online : Int32 (Point in time (Unix timestamp) when the user was last online)

class TD::UserStatus::Offline < TD::UserStatus
  include JSON::Serializable

  object_type "userStatusOffline"

  object_attributes({
    was_online: Int32
  })
end