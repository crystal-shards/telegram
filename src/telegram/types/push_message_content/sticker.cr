# A message with a sticker
#
# @sticker : TD::Sticker? (Message content; may be null)
# @emoji : String (Emoji corresponding to the sticker; may be empty)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Sticker < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentSticker"

  object_attributes({
    sticker: TD::Sticker?,
    emoji: String,
    is_pinned: Bool
  })
end