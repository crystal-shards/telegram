# A message with a location
#
# @is_live : Bool (True, if the location is live)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Location < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentLocation"

  object_attributes({
    is_live: Bool,
    is_pinned: Bool
  })
end