# A message with a game
#
# @title : String (Game title, empty for pinned game message)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Game < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentGame"

  object_attributes({
    title: String,
    is_pinned: Bool
  })
end