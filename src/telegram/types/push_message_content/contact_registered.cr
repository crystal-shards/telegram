# A contact has registered with Telegram
#

class TD::PushMessageContent::ContactRegistered < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentContactRegistered"
end