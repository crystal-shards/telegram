# A screenshot of a message in the chat has been taken
#

class TD::PushMessageContent::ScreenshotTaken < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentScreenshotTaken"
end