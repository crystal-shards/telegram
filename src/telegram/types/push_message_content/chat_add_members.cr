# New chat members were invited to a group
#
# @member_name : String (Name of the added member)
# @is_current_user : Bool (True, if the current user was added to the group)
# @is_returned : Bool (True, if the user has returned to the group themselves)

class TD::PushMessageContent::ChatAddMembers < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentChatAddMembers"

  object_attributes({
    member_name: String,
    is_current_user: Bool,
    is_returned: Bool
  })
end