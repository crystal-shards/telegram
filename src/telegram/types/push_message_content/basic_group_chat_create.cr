# A newly created basic group
#

class TD::PushMessageContent::BasicGroupChatCreate < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentBasicGroupChatCreate"
end