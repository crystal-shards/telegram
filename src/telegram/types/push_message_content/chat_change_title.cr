# A chat title was edited
#
# @title : String (New chat title)

class TD::PushMessageContent::ChatChangeTitle < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentChatChangeTitle"

  object_attributes({
    title: String
  })
end