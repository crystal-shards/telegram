# A message with a user contact
#
# @name : String (Contact's name)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Contact < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentContact"

  object_attributes({
    name: String,
    is_pinned: Bool
  })
end