# A photo message
#
# @photo : TD::Photo? (Message content; may be null)
# @caption : String (Photo caption)
# @is_secret : Bool (True, if the photo is secret)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Photo < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentPhoto"

  object_attributes({
    photo: TD::Photo?,
    caption: String,
    is_secret: Bool,
    is_pinned: Bool
  })
end