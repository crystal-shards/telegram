# A chat member was deleted
#
# @member_name : String (Name of the deleted member)
# @is_current_user : Bool (True, if the current user was deleted from the group)
# @is_left : Bool (True, if the user has left the group themselves)

class TD::PushMessageContent::ChatDeleteMember < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentChatDeleteMember"

  object_attributes({
    member_name: String,
    is_current_user: Bool,
    is_left: Bool
  })
end