# A forwarded messages
#
# @total_count : Int32 (Number of forwarded messages)

class TD::PushMessageContent::MessageForwards < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentMessageForwards"

  object_attributes({
    total_count: Int32
  })
end