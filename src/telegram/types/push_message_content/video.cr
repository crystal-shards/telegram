# A video message
#
# @video : TD::Video? (Message content; may be null)
# @caption : String (Video caption)
# @is_secret : Bool (True, if the video is secret)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Video < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentVideo"

  object_attributes({
    video: TD::Video?,
    caption: String,
    is_secret: Bool,
    is_pinned: Bool
  })
end