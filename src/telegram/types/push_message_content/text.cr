# A text message
#
# @text : String (Message text)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Text < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentText"

  object_attributes({
    text: String,
    is_pinned: Bool
  })
end