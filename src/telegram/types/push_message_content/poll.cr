# A message with a poll
#
# @question : String (Poll question)
# @is_regular : Bool (True, if the poll is regular and not in quiz mode)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Poll < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentPoll"

  object_attributes({
    question: String,
    is_regular: Bool,
    is_pinned: Bool
  })
end