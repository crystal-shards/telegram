# A video note message
#
# @video_note : TD::VideoNote? (Message content; may be null)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::VideoNote < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentVideoNote"

  object_attributes({
    video_note: TD::VideoNote?,
    is_pinned: Bool
  })
end