# An audio message
#
# @audio : TD::Audio? (Message content; may be null)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Audio < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentAudio"

  object_attributes({
    audio: TD::Audio?,
    is_pinned: Bool
  })
end