# A media album
#
# @total_count : Int32 (Number of messages in the album)
# @has_photos : Bool (True, if the album has at least one photo)
# @has_videos : Bool (True, if the album has at least one video)
# @has_audios : Bool (True, if the album has at least one audio file)
# @has_documents : Bool (True, if the album has at least one document)

class TD::PushMessageContent::MediaAlbum < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentMediaAlbum"

  object_attributes({
    total_count: Int32,
    has_photos: Bool,
    has_videos: Bool,
    has_audios: Bool,
    has_documents: Bool
  })
end