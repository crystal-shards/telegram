# A new high score was achieved in a game
#
# @title : String (Game title, empty for pinned message)
# @score : Int32 (New score, 0 for pinned message)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::GameScore < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentGameScore"

  object_attributes({
    title: String,
    score: Int32,
    is_pinned: Bool
  })
end