# A message with an invoice from a bot
#
# @price : String (Product price)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Invoice < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentInvoice"

  object_attributes({
    price: String,
    is_pinned: Bool
  })
end