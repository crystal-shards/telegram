# A new member was accepted to the chat by an administrator
#

class TD::PushMessageContent::ChatJoinByRequest < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentChatJoinByRequest"
end