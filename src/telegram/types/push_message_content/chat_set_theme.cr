# A chat theme was edited
#
# @theme_name : String (If non-empty, name of a new theme, set for the chat. Otherwise chat theme was reset to the default one)

class TD::PushMessageContent::ChatSetTheme < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentChatSetTheme"

  object_attributes({
    theme_name: String
  })
end