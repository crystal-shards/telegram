# A voice note message
#
# @voice_note : TD::VoiceNote? (Message content; may be null)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::VoiceNote < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentVoiceNote"

  object_attributes({
    voice_note: TD::VoiceNote?,
    is_pinned: Bool
  })
end