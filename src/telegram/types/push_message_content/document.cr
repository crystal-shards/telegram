# A document message (a general file)
#
# @document : TD::Document? (Message content; may be null)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Document < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentDocument"

  object_attributes({
    document: TD::Document?,
    is_pinned: Bool
  })
end