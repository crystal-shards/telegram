# A chat photo was edited
#

class TD::PushMessageContent::ChatChangePhoto < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentChatChangePhoto"
end