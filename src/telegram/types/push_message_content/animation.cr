# An animation message (GIF-style).
#
# @animation : TD::Animation? (Message content; may be null)
# @caption : String (Animation caption)
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Animation < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentAnimation"

  object_attributes({
    animation: TD::Animation?,
    caption: String,
    is_pinned: Bool
  })
end