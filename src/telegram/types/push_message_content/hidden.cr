# A general message with hidden content
#
# @is_pinned : Bool (True, if the message is a pinned message with the specified content)

class TD::PushMessageContent::Hidden < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentHidden"

  object_attributes({
    is_pinned: Bool
  })
end