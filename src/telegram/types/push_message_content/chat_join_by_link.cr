# A new member joined the chat via an invite link
#

class TD::PushMessageContent::ChatJoinByLink < TD::PushMessageContent
  include JSON::Serializable

  object_type "pushMessageContentChatJoinByLink"
end