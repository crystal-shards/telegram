# Returns only messages with mentions of the current user, or messages that are replies to their messages
#

class TD::SearchMessagesFilter::Mention < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterMention"
end