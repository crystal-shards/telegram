# Returns only pinned messages
#

class TD::SearchMessagesFilter::Pinned < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterPinned"
end