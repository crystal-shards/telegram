# Returns only video messages
#

class TD::SearchMessagesFilter::Video < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterVideo"
end