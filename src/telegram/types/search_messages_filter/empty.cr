# Returns all found messages, no filter is applied
#

class TD::SearchMessagesFilter::Empty < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterEmpty"
end