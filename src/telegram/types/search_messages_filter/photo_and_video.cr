# Returns only photo and video messages
#

class TD::SearchMessagesFilter::PhotoAndVideo < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterPhotoAndVideo"
end