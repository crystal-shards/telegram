# Returns only audio messages
#

class TD::SearchMessagesFilter::Audio < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterAudio"
end