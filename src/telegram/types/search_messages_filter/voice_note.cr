# Returns only voice note messages
#

class TD::SearchMessagesFilter::VoiceNote < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterVoiceNote"
end