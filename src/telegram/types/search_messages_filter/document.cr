# Returns only document messages
#

class TD::SearchMessagesFilter::Document < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterDocument"
end