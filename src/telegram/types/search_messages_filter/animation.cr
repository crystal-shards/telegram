# Returns only animation messages
#

class TD::SearchMessagesFilter::Animation < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterAnimation"
end