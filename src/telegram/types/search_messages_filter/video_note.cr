# Returns only video note messages
#

class TD::SearchMessagesFilter::VideoNote < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterVideoNote"
end