# Returns only messages containing chat photos
#

class TD::SearchMessagesFilter::ChatPhoto < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterChatPhoto"
end