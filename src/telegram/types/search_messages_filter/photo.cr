# Returns only photo messages
#

class TD::SearchMessagesFilter::Photo < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterPhoto"
end