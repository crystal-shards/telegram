# Returns only messages containing URLs
#

class TD::SearchMessagesFilter::Url < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterUrl"
end