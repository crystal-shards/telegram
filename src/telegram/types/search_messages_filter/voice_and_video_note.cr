# Returns only voice and video note messages
#

class TD::SearchMessagesFilter::VoiceAndVideoNote < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterVoiceAndVideoNote"
end