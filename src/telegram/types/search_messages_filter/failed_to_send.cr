# Returns only failed to send messages. This filter can be used only if the message database is used
#

class TD::SearchMessagesFilter::FailedToSend < TD::SearchMessagesFilter
  include JSON::Serializable

  object_type "searchMessagesFilterFailedToSend"
end