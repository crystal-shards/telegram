# The speech was distorted
#

class TD::CallProblem::DistortedSpeech < TD::CallProblem
  include JSON::Serializable

  object_type "callProblemDistortedSpeech"
end