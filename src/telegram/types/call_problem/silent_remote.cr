# The other side couldn't hear the user
#

class TD::CallProblem::SilentRemote < TD::CallProblem
  include JSON::Serializable

  object_type "callProblemSilentRemote"
end