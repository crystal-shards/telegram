# The user heard their own voice
#

class TD::CallProblem::Echo < TD::CallProblem
  include JSON::Serializable

  object_type "callProblemEcho"
end