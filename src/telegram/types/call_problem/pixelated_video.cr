# The video was pixelated
#

class TD::CallProblem::PixelatedVideo < TD::CallProblem
  include JSON::Serializable

  object_type "callProblemPixelatedVideo"
end