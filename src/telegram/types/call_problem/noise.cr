# The user heard background noise
#

class TD::CallProblem::Noise < TD::CallProblem
  include JSON::Serializable

  object_type "callProblemNoise"
end