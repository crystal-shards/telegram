# The video was distorted
#

class TD::CallProblem::DistortedVideo < TD::CallProblem
  include JSON::Serializable

  object_type "callProblemDistortedVideo"
end