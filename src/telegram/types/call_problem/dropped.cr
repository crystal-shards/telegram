# The call ended unexpectedly
#

class TD::CallProblem::Dropped < TD::CallProblem
  include JSON::Serializable

  object_type "callProblemDropped"
end