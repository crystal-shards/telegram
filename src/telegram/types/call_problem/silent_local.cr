# The user couldn't hear the other side
#

class TD::CallProblem::SilentLocal < TD::CallProblem
  include JSON::Serializable

  object_type "callProblemSilentLocal"
end