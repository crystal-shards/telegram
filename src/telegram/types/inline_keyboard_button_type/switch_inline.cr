# A button that forces an inline query to the bot to be inserted in the input field
#
# @query : String (Inline query to be sent to the bot)
# @in_current_chat : Bool (True, if the inline query must be sent from the current chat)

class TD::InlineKeyboardButtonType::SwitchInline < TD::InlineKeyboardButtonType
  include JSON::Serializable

  object_type "inlineKeyboardButtonTypeSwitchInline"

  object_attributes({
    query: String,
    in_current_chat: Bool
  })
end