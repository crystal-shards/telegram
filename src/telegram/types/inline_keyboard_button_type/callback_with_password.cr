# A button that asks for password of the current user and then sends a callback query to a bot
#
# @data : String (Data to be sent to the bot via a callback query)

class TD::InlineKeyboardButtonType::CallbackWithPassword < TD::InlineKeyboardButtonType
  include JSON::Serializable

  object_type "inlineKeyboardButtonTypeCallbackWithPassword"

  object_attributes({
    data: String
  })
end