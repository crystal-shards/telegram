# A button to buy something. This button must be in the first column and row of the keyboard and can be attached only to a message with content of the type messageInvoice
#

class TD::InlineKeyboardButtonType::Buy < TD::InlineKeyboardButtonType
  include JSON::Serializable

  object_type "inlineKeyboardButtonTypeBuy"
end