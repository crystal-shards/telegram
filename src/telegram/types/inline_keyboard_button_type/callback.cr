# A button that sends a callback query to a bot
#
# @data : String (Data to be sent to the bot via a callback query)

class TD::InlineKeyboardButtonType::Callback < TD::InlineKeyboardButtonType
  include JSON::Serializable

  object_type "inlineKeyboardButtonTypeCallback"

  object_attributes({
    data: String
  })
end