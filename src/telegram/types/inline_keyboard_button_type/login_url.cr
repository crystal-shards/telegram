# A button that opens a specified URL and automatically authorize the current user if allowed to do so
#
# @url : String (An HTTP URL to open)
# @id : Int64 (Unique button identifier)
# @forward_text : String (If non-empty, new text of the button in forwarded messages)

class TD::InlineKeyboardButtonType::LoginUrl < TD::InlineKeyboardButtonType
  include JSON::Serializable

  object_type "inlineKeyboardButtonTypeLoginUrl"

  object_attributes({
    url: String,
    id: Int64,
    forward_text: String
  })
end