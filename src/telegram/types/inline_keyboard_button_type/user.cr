# A button with a user reference to be handled in the same way as textEntityTypeMentionName entities
#
# @user_id : Int64 (User identifier)

class TD::InlineKeyboardButtonType::User < TD::InlineKeyboardButtonType
  include JSON::Serializable

  object_type "inlineKeyboardButtonTypeUser"

  object_attributes({
    user_id: Int64
  })
end