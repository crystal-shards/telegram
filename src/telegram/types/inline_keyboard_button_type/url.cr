# A button that opens a specified URL
#
# @url : String (HTTP or tg:)

class TD::InlineKeyboardButtonType::Url < TD::InlineKeyboardButtonType
  include JSON::Serializable

  object_type "inlineKeyboardButtonTypeUrl"

  object_attributes({
    url: String
  })
end