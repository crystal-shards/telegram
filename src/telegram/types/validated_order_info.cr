# Contains a temporary identifier of validated order information, which is stored for one hour. Also contains the available shipping options
#
# @order_info_id : String (Temporary identifier of the order information)
# @shipping_options : Array(TD::ShippingOption) (Available shipping options)

class TD::ValidatedOrderInfo < TD::Base
  include JSON::Serializable

  object_type "validatedOrderInfo"

  object_attributes({
    order_info_id: String,
    shipping_options: Array(TD::ShippingOption)
  })
end