# A basic group (a chat with 0-200 other users)
#
# @basic_group_id : Int64 (Basic group identifier)

class TD::ChatType::BasicGroup < TD::ChatType
  include JSON::Serializable

  object_type "chatTypeBasicGroup"

  object_attributes({
    basic_group_id: Int64
  })
end