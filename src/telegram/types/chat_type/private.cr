# An ordinary chat with a user
#
# @user_id : Int64 (User identifier)

class TD::ChatType::Private < TD::ChatType
  include JSON::Serializable

  object_type "chatTypePrivate"

  object_attributes({
    user_id: Int64
  })
end