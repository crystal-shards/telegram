# A supergroup or channel (with unlimited members)
#
# @supergroup_id : Int64 (Supergroup or channel identifier)
# @is_channel : Bool (True, if the supergroup is a channel)

class TD::ChatType::Supergroup < TD::ChatType
  include JSON::Serializable

  object_type "chatTypeSupergroup"

  object_attributes({
    supergroup_id: Int64,
    is_channel: Bool
  })
end