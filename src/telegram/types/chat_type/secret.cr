# A secret chat with a user
#
# @secret_chat_id : Int32 (Secret chat identifier)
# @user_id : Int64 (User identifier of the secret chat peer)

class TD::ChatType::Secret < TD::ChatType
  include JSON::Serializable

  object_type "chatTypeSecret"

  object_attributes({
    secret_chat_id: Int32,
    user_id: Int64
  })
end