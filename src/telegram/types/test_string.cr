# A simple object containing a string; for testing only
#
# @value : String (String)

class TD::TestString < TD::Base
  include JSON::Serializable

  object_type "testString"

  object_attributes({
    value: String
  })
end