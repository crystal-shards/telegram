# Contains an HTTP URL
#
# @url : String (The URL)

class TD::HttpUrl < TD::Base
  include JSON::Serializable

  object_type "httpUrl"

  object_attributes({
    url: String
  })
end