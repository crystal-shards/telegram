# Describes a video chat
#
# @group_call_id : Int32 (Group call identifier of an active video chat; 0 if none. Full information about the video chat can be received through the method getGroupCall)
# @has_participants : Bool (True, if the video chat has participants)
# @default_participant_id : TD::MessageSender? (Default group call participant identifier to join the video chat; may be null)

class TD::VideoChat < TD::Base
  include JSON::Serializable

  object_type "videoChat"

  object_attributes({
    group_call_id: Int32,
    has_participants: Bool,
    default_participant_id: TD::MessageSender?
  })
end