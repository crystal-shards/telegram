# Contains information about saved Telegram Passport elements
#
# @elements : Array(TD::PassportElement) (Telegram Passport elements)

class TD::PassportElements < TD::Base
  include JSON::Serializable

  object_type "passportElements"

  object_attributes({
    elements: Array(TD::PassportElement)
  })
end