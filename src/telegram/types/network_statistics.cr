# A full list of available network statistic entries
#
# @since_date : Int32 (Point in time (Unix timestamp) from which the statistics are collected)
# @entries : Array(TD::NetworkStatisticsEntry) (Network statistics entries)

class TD::NetworkStatistics < TD::Base
  include JSON::Serializable

  object_type "networkStatistics"

  object_attributes({
    since_date: Int32,
    entries: Array(TD::NetworkStatisticsEntry)
  })
end