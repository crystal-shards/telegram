# Describes available user privacy settings
#
# ShowStatus: A privacy setting for managing whether the user's online status is visible
# ShowProfilePhoto: A privacy setting for managing whether the user's profile photo is visible
# ShowLinkInForwardedMessages: A privacy setting for managing whether a link to the user's account is included in forwarded messages
# ShowPhoneNumber: A privacy setting for managing whether the user's phone number is visible
# AllowChatInvites: A privacy setting for managing whether the user can be invited to chats
# AllowCalls: A privacy setting for managing whether the user can be called
# AllowPeerToPeerCalls: A privacy setting for managing whether peer-to-peer connections can be used for calls
# AllowFindingByPhoneNumber: A privacy setting for managing whether the user can be found by their phone number. Checked only if the phone number is not known to the other user. Can be set only to "Allow contacts" or "Allow all"

abstract class TD::UserPrivacySetting < TD::Base
end