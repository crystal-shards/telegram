# The name is occupied
#

class TD::CheckStickerSetNameResult::NameOccupied < TD::CheckStickerSetNameResult
  include JSON::Serializable

  object_type "checkStickerSetNameResultNameOccupied"
end