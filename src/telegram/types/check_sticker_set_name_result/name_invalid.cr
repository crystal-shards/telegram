# The name is invalid
#

class TD::CheckStickerSetNameResult::NameInvalid < TD::CheckStickerSetNameResult
  include JSON::Serializable

  object_type "checkStickerSetNameResultNameInvalid"
end