# The name can be set
#

class TD::CheckStickerSetNameResult::Ok < TD::CheckStickerSetNameResult
  include JSON::Serializable

  object_type "checkStickerSetNameResultOk"
end