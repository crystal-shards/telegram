# An MTProto proxy server
#
# @secret : String (The proxy's secret in hexadecimal encoding)

class TD::ProxyType::Mtproto < TD::ProxyType
  include JSON::Serializable

  object_type "proxyTypeMtproto"

  object_attributes({
    secret: String
  })
end