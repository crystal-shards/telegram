# A HTTP transparent proxy server
#
# @username : String (Username for logging in; may be empty)
# @password : String (Password for logging in; may be empty)
# @http_only : Bool (Pass true if the proxy supports only HTTP requests and doesn't support transparent TCP connections via HTTP CONNECT method)

class TD::ProxyType::Http < TD::ProxyType
  include JSON::Serializable

  object_type "proxyTypeHttp"

  object_attributes({
    username: String,
    password: String,
    http_only: Bool
  })
end