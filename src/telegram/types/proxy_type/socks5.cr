# A SOCKS5 proxy server
#
# @username : String (Username for logging in; may be empty)
# @password : String (Password for logging in; may be empty)

class TD::ProxyType::Socks5 < TD::ProxyType
  include JSON::Serializable

  object_type "proxyTypeSocks5"

  object_attributes({
    username: String,
    password: String
  })
end