# Represents the value of an option
#
# Boolean: Represents a boolean option
# Empty: Represents an unknown option or an option which has a default value
# Integer: Represents an integer option
# String: Represents a string option

abstract class TD::OptionValue < TD::Base
end