# Represents a boolean JSON value
#
# @value : Bool (The value)

class TD::JsonValue::Boolean < TD::JsonValue
  include JSON::Serializable

  object_type "jsonValueBoolean"

  object_attributes({
    value: Bool
  })
end