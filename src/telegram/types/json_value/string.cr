# Represents a string JSON value
#
# @value : String (The value)

class TD::JsonValue::String < TD::JsonValue
  include JSON::Serializable

  object_type "jsonValueString"

  object_attributes({
    value: String
  })
end