# Represents a JSON array
#
# @values : Array(TD::JsonValue) (The list of array elements)

class TD::JsonValue::Array < TD::JsonValue
  include JSON::Serializable

  object_type "jsonValueArray"

  object_attributes({
    values: Array(TD::JsonValue)
  })
end