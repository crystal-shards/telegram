# Represents a JSON object
#
# @members : Array(TD::JsonObjectMember) (The list of object members)

class TD::JsonValue::Object < TD::JsonValue
  include JSON::Serializable

  object_type "jsonValueObject"

  object_attributes({
    members: Array(TD::JsonObjectMember)
  })
end