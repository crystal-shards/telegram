# Represents a numeric JSON value
#
# @value : Float64 (The value)

class TD::JsonValue::Number < TD::JsonValue
  include JSON::Serializable

  object_type "jsonValueNumber"

  object_attributes({
    value: Float64
  })
end