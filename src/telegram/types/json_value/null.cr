# Represents a null JSON value
#

class TD::JsonValue::Null < TD::JsonValue
  include JSON::Serializable

  object_type "jsonValueNull"
end