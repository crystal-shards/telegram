# Contains information about found messages, split by days according to the option "utc_time_offset"
#
# @total_count : Int32 (Total number of found messages)
# @days : Array(TD::MessageCalendarDay) (Information about messages sent)

class TD::MessageCalendar < TD::Base
  include JSON::Serializable

  object_type "messageCalendar"

  object_attributes({
    total_count: Int32,
    days: Array(TD::MessageCalendarDay)
  })
end