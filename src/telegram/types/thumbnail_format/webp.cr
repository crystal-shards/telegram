# The thumbnail is in WEBP format. It will be used only for some stickers
#

class TD::ThumbnailFormat::Webp < TD::ThumbnailFormat
  include JSON::Serializable

  object_type "thumbnailFormatWebp"
end