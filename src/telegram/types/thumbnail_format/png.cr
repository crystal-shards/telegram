# The thumbnail is in PNG format. It will be used only for background patterns
#

class TD::ThumbnailFormat::Png < TD::ThumbnailFormat
  include JSON::Serializable

  object_type "thumbnailFormatPng"
end