# The thumbnail is in TGS format. It will be used only for animated sticker sets
#

class TD::ThumbnailFormat::Tgs < TD::ThumbnailFormat
  include JSON::Serializable

  object_type "thumbnailFormatTgs"
end