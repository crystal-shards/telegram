# The thumbnail is in static GIF format. It will be used only for some bot inline results
#

class TD::ThumbnailFormat::Gif < TD::ThumbnailFormat
  include JSON::Serializable

  object_type "thumbnailFormatGif"
end