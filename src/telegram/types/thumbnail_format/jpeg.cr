# The thumbnail is in JPEG format
#

class TD::ThumbnailFormat::Jpeg < TD::ThumbnailFormat
  include JSON::Serializable

  object_type "thumbnailFormatJpeg"
end