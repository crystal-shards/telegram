# The thumbnail is in MPEG4 format. It will be used only for some animations and videos
#

class TD::ThumbnailFormat::Mpeg4 < TD::ThumbnailFormat
  include JSON::Serializable

  object_type "thumbnailFormatMpeg4"
end