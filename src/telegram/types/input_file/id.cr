# A file defined by its unique ID
#
# @id : Int32 (Unique file identifier)

class TD::InputFile::Id < TD::InputFile
  include JSON::Serializable

  object_type "inputFileId"

  object_attributes({
    id: Int32
  })
end