# A file defined by a local path
#
# @path : String (Local path to the file)

class TD::InputFile::Local < TD::InputFile
  include JSON::Serializable

  object_type "inputFileLocal"

  object_attributes({
    path: String
  })
end