# A file generated by the application
#
# @original_path : String (Local path to a file from which the file is generated; may be empty if there is no such file)
# @conversion : String (String specifying the conversion applied to the original file; must be persistent across application restarts. Conversions beginning with ')
# @expected_size : Int32 (Expected size of the generated file, in bytes; 0 if unknown)

class TD::InputFile::Generated < TD::InputFile
  include JSON::Serializable

  object_type "inputFileGenerated"

  object_attributes({
    original_path: String,
    conversion: String,
    expected_size: Int32
  })
end