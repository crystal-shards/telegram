# The message was sent by a known user
#
# @user_id : Int64 (Identifier of the user that sent the message)

class TD::MessageSender::User < TD::MessageSender
  include JSON::Serializable

  object_type "messageSenderUser"

  object_attributes({
    user_id: Int64
  })
end