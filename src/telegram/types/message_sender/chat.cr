# The message was sent on behalf of a chat
#
# @chat_id : Int64 (Identifier of the chat that sent the message)

class TD::MessageSender::Chat < TD::MessageSender
  include JSON::Serializable

  object_type "messageSenderChat"

  object_attributes({
    chat_id: Int64
  })
end