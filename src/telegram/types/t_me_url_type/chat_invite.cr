# A chat invite link
#
# @info : TD::ChatInviteLinkInfo (Chat invite link info)

class TD::TMeUrlType::ChatInvite < TD::TMeUrlType
  include JSON::Serializable

  object_type "tMeUrlTypeChatInvite"

  object_attributes({
    info: TD::ChatInviteLinkInfo
  })
end