# A URL linking to a user
#
# @user_id : Int64 (Identifier of the user)

class TD::TMeUrlType::User < TD::TMeUrlType
  include JSON::Serializable

  object_type "tMeUrlTypeUser"

  object_attributes({
    user_id: Int64
  })
end