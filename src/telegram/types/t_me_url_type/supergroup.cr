# A URL linking to a public supergroup or channel
#
# @supergroup_id : Int64 (Identifier of the supergroup or channel)

class TD::TMeUrlType::Supergroup < TD::TMeUrlType
  include JSON::Serializable

  object_type "tMeUrlTypeSupergroup"

  object_attributes({
    supergroup_id: Int64
  })
end