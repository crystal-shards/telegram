# A URL linking to a sticker set
#
# @sticker_set_id : String (Identifier of the sticker set)

class TD::TMeUrlType::StickerSet < TD::TMeUrlType
  include JSON::Serializable

  object_type "tMeUrlTypeStickerSet"

  object_attributes({
    sticker_set_id: String
  })
end