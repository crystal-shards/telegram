# An object of this type is returned on a successful function call for certain functions
#

class TD::Ok < TD::Base
  include JSON::Serializable

  object_type "ok"
end