# Represents one language pack string
#
# @key : String (String key)
# @value : TD::LanguagePackStringValue (String value; pass null if the string needs to be taken from the built-in English language pack)

class TD::LanguagePackString < TD::Base
  include JSON::Serializable

  object_type "languagePackString"

  object_attributes({
    key: String,
    value: TD::LanguagePackStringValue
  })
end