# A list of chats belonging to a chat filter
#
# @chat_filter_id : Int32 (Chat filter identifier)

class TD::ChatList::Filter < TD::ChatList
  include JSON::Serializable

  object_type "chatListFilter"

  object_attributes({
    chat_filter_id: Int32
  })
end