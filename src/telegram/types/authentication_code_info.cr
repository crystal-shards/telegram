# Information about the authentication code that was sent
#
# @phone_number : String (A phone number that is being authenticated)
# @type : TD::AuthenticationCodeType (The way the code was sent to the user)
# @next_type : TD::AuthenticationCodeType? (The way the next code will be sent to the user; may be null)
# @timeout : Int32 (Timeout before the code can be re-sent, in seconds)

class TD::AuthenticationCodeInfo < TD::Base
  include JSON::Serializable

  object_type "authenticationCodeInfo"

  object_attributes({
    phone_number: String,
    type: TD::AuthenticationCodeType,
    next_type: TD::AuthenticationCodeType?,
    timeout: Int32
  })
end