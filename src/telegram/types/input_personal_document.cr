# A personal document to be saved to Telegram Passport
#
# @files : Array(TD::InputFile) (List of files containing the pages of the document)
# @translation : Array(TD::InputFile) (List of files containing a certified English translation of the document)

class TD::InputPersonalDocument < TD::Base
  include JSON::Serializable

  object_type "inputPersonalDocument"

  object_attributes({
    files: Array(TD::InputFile),
    translation: Array(TD::InputFile)
  })
end