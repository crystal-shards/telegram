# Options to be used when a message is sent
#
# @disable_notification : Bool (Pass true to disable notification for the message)
# @from_background : Bool (Pass true if the message is sent from the background)
# @scheduling_state : TD::MessageSchedulingState (Message scheduling state; pass null to send message immediately. Messages sent to a secret chat, live location messages and self-destructing messages can't be scheduled)

class TD::MessageSendOptions < TD::Base
  include JSON::Serializable

  object_type "messageSendOptions"

  object_attributes({
    disable_notification: Bool,
    from_background: Bool,
    scheduling_state: TD::MessageSchedulingState
  })
end