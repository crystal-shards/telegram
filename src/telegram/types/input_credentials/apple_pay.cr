# Applies if a user enters new credentials using Apple Pay
#
# @data : String (JSON-encoded data with the credential identifier)

class TD::InputCredentials::ApplePay < TD::InputCredentials
  include JSON::Serializable

  object_type "inputCredentialsApplePay"

  object_attributes({
    data: String
  })
end