# Applies if a user enters new credentials on a payment provider website
#
# @data : String (JSON-encoded data with the credential identifier from the payment provider)
# @allow_save : Bool (True, if the credential identifier can be saved on the server side)

class TD::InputCredentials::New < TD::InputCredentials
  include JSON::Serializable

  object_type "inputCredentialsNew"

  object_attributes({
    data: String,
    allow_save: Bool
  })
end