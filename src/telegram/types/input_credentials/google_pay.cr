# Applies if a user enters new credentials using Google Pay
#
# @data : String (JSON-encoded data with the credential identifier)

class TD::InputCredentials::GooglePay < TD::InputCredentials
  include JSON::Serializable

  object_type "inputCredentialsGooglePay"

  object_attributes({
    data: String
  })
end