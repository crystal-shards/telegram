# Applies if a user chooses some previously saved payment credentials. To use their previously saved credentials, the user must have a valid temporary password
#
# @saved_credentials_id : String (Identifier of the saved credentials)

class TD::InputCredentials::Saved < TD::InputCredentials
  include JSON::Serializable

  object_type "inputCredentialsSaved"

  object_attributes({
    saved_credentials_id: String
  })
end