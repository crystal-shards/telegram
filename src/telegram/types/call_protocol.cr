# Specifies the supported call protocols
#
# @udp_p2p : Bool (True, if UDP peer-to-peer connections are supported)
# @udp_reflector : Bool (True, if connection through UDP reflectors is supported)
# @min_layer : Int32 (The minimum supported API layer; use 65)
# @max_layer : Int32 (The maximum supported API layer; use 65)
# @library_versions : Array(String) (List of supported tgcalls versions)

class TD::CallProtocol < TD::Base
  include JSON::Serializable

  object_type "callProtocol"

  object_attributes({
    udp_p2p: Bool,
    udp_reflector: Bool,
    min_layer: Int32,
    max_layer: Int32,
    library_versions: Array(String)
  })
end