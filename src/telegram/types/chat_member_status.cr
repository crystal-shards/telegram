# Provides information about the status of a member in a chat
#
# Creator: The user is the owner of the chat and has all the administrator privileges
# Administrator: The user is a member of the chat and has some additional privileges. In basic groups, administrators can edit and delete messages sent by others, add new members, ban unprivileged members, and manage video chats. In supergroups and channels, there are more detailed options for administrator privileges
# Member: The user is a member of the chat, without any additional privileges or restrictions
# Restricted: The user is under certain restrictions in the chat. Not supported in basic groups and channels
# Left: The user or the chat is not a chat member
# Banned: The user or the chat was banned (and hence is not a member of the chat). Implies the user can't return to the chat, view messages, or be used as a participant identifier to join a video chat of the chat

abstract class TD::ChatMemberStatus < TD::Base
end