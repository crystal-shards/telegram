# The call has been answered and encryption keys are being exchanged
#

class TD::CallState::ExchangingKeys < TD::CallState
  include JSON::Serializable

  object_type "callStateExchangingKeys"
end