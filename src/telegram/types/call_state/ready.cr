# The call is ready to use
#
# @protocol : TD::CallProtocol (Call protocols supported by the peer)
# @servers : Array(TD::CallServer) (List of available call servers)
# @config : String (A JSON-encoded call config)
# @encryption_key : String (Call encryption key)
# @emojis : Array(String) (Encryption key emojis fingerprint)
# @allow_p2p : Bool (True, if peer-to-peer connection is allowed by users privacy settings)

class TD::CallState::Ready < TD::CallState
  include JSON::Serializable

  object_type "callStateReady"

  object_attributes({
    protocol: TD::CallProtocol,
    servers: Array(TD::CallServer),
    config: String,
    encryption_key: String,
    emojis: Array(String),
    allow_p2p: Bool
  })
end