# The call has ended with an error
#
# @error : TD::Error (Error. An error with the code 4005000 will be returned if an outgoing call is missed because of an expired timeout)

class TD::CallState::Error < TD::CallState
  include JSON::Serializable

  object_type "callStateError"

  object_attributes({
    error: TD::Error
  })
end