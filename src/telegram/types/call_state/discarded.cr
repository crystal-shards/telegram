# The call has ended successfully
#
# @reason : TD::CallDiscardReason (The reason, why the call has ended)
# @need_rating : Bool (True, if the call rating must be sent to the server)
# @need_debug_information : Bool (True, if the call debug information must be sent to the server)

class TD::CallState::Discarded < TD::CallState
  include JSON::Serializable

  object_type "callStateDiscarded"

  object_attributes({
    reason: TD::CallDiscardReason,
    need_rating: Bool,
    need_debug_information: Bool
  })
end