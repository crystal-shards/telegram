# The call is pending, waiting to be accepted by a user
#
# @is_created : Bool (True, if the call has already been created by the server)
# @is_received : Bool (True, if the call has already been received by the other party)

class TD::CallState::Pending < TD::CallState
  include JSON::Serializable

  object_type "callStatePending"

  object_attributes({
    is_created: Bool,
    is_received: Bool
  })
end