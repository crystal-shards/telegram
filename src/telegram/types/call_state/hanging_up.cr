# The call is hanging up after discardCall has been called
#

class TD::CallState::HangingUp < TD::CallState
  include JSON::Serializable

  object_type "callStateHangingUp"
end