# Describes the type of a background
#
# Wallpaper: A wallpaper in JPEG format
# Pattern: A PNG or TGV (gzipped subset of SVG with MIME type "application
# Fill: A filled background

abstract class TD::BackgroundType < TD::Base
end