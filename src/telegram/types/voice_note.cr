# Describes a voice note. The voice note must be encoded with the Opus codec, and stored inside an OGG container. Voice notes can have only a single audio channel
#
# @duration : Int32 (Duration of the voice note, in seconds; as defined by the sender)
# @waveform : String (A waveform representation of the voice note in 5-bit format)
# @mime_type : String (MIME type of the file; as defined by the sender)
# @voice : TD::File (File containing the voice note)

class TD::VoiceNote < TD::Base
  include JSON::Serializable

  object_type "voiceNote"

  object_attributes({
    duration: Int32,
    waveform: String,
    mime_type: String,
    voice: TD::File
  })
end