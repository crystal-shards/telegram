# Describes a web page preview
#
# @url : String (Original URL of the link)
# @display_url : String (URL to display)
# @type : String (Type of the web page. Can be: article, photo, audio, video, document, profile, app, or something else)
# @site_name : String (Short name of the site (e.g., Google Docs, App Store))
# @title : String (Title of the content)
# @description : TD::FormattedText (Description of the content)
# @photo : TD::Photo? (Image representing the content; may be null)
# @embed_url : String (URL to show in the embedded preview)
# @embed_type : String (MIME type of the embedded preview, (e.g., text)
# @embed_width : Int32 (Width of the embedded preview)
# @embed_height : Int32 (Height of the embedded preview)
# @duration : Int32 (Duration of the content, in seconds)
# @author : String (Author of the content)
# @animation : TD::Animation? (Preview of the content as an animation, if available; may be null)
# @audio : TD::Audio? (Preview of the content as an audio file, if available; may be null)
# @document : TD::Document? (Preview of the content as a document, if available; may be null)
# @sticker : TD::Sticker? (Preview of the content as a sticker for small WEBP files, if available; may be null)
# @video : TD::Video? (Preview of the content as a video, if available; may be null)
# @video_note : TD::VideoNote? (Preview of the content as a video note, if available; may be null)
# @voice_note : TD::VoiceNote? (Preview of the content as a voice note, if available; may be null)
# @instant_view_version : Int32 (Version of instant view, available for the web page (currently, can be 1 or 2), 0 if none)

class TD::WebPage < TD::Base
  include JSON::Serializable

  object_type "webPage"

  object_attributes({
    url: String,
    display_url: String,
    type: String,
    site_name: String,
    title: String,
    description: TD::FormattedText,
    photo: TD::Photo?,
    embed_url: String,
    embed_type: String,
    embed_width: Int32,
    embed_height: Int32,
    duration: Int32,
    author: String,
    animation: TD::Animation?,
    audio: TD::Audio?,
    document: TD::Document?,
    sticker: TD::Sticker?,
    video: TD::Video?,
    video_note: TD::VideoNote?,
    voice_note: TD::VoiceNote?,
    instant_view_version: Int32
  })
end