# Contains statistics about interactions with a message
#
# @message_id : Int64 (Message identifier)
# @view_count : Int32 (Number of times the message was viewed)
# @forward_count : Int32 (Number of times the message was forwarded)

class TD::ChatStatisticsMessageInteractionInfo < TD::Base
  include JSON::Serializable

  object_type "chatStatisticsMessageInteractionInfo"

  object_attributes({
    message_id: Int64,
    view_count: Int32,
    forward_count: Int32
  })
end