# Describes a Vertical alignment of a table cell content
#
# Top: The content must be top-aligned
# Middle: The content must be middle-aligned
# Bottom: The content must be bottom-aligned

abstract class TD::PageBlockVerticalAlignment < TD::Base
end