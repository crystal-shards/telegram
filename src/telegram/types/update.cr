# Contains notifications about data changes
#
# AuthorizationState: The user authorization state has changed
# NewMessage: A new message was received; can also be an outgoing message
# MessageSendAcknowledged: A request to send a message has reached the Telegram server. This doesn't mean that the message will be sent successfully or even that the send message request will be processed. This update will be sent only if the option "use_quick_ack" is set to true. This update may be sent multiple times for the same message
# MessageSendSucceeded: A message has been successfully sent
# MessageSendFailed: A message failed to send. Be aware that some messages being sent can be irrecoverably deleted, in which case updateDeleteMessages will be received instead of this update
# MessageContent: The message content has changed
# MessageEdited: A message was edited. Changes in the message content will come in a separate updateMessageContent
# MessageIsPinned: The message pinned state was changed
# MessageInteractionInfo: The information about interactions with a message has changed
# MessageContentOpened: The message content was opened. Updates voice note messages to "listened", video note messages to "viewed" and starts the TTL timer for self-destructing messages
# MessageMentionRead: A message with an unread mention was read
# MessageLiveLocationViewed: A message with a live location was viewed. When the update is received, the application is supposed to update the live location
# NewChat: A new chat has been loaded
# ChatTitle: The title of a chat was changed
# ChatPhoto: A chat photo was changed
# ChatPermissions: Chat permissions was changed
# ChatLastMessage: The last message of a chat was changed. If last_message is null, then the last message in the chat became unknown. Some new unknown messages might be added to the chat in this case
# ChatPosition: The position of a chat in a chat list has changed. Instead of this update updateChatLastMessage or updateChatDraftMessage might be sent
# ChatReadInbox: Incoming messages were read or the number of unread messages has been changed
# ChatReadOutbox: Outgoing messages were read
# ChatActionBar: The chat action bar was changed
# ChatDraftMessage: A chat draft has changed. Be aware that the update may come in the currently opened chat but with old content of the draft. If the user has changed the content of the draft, this update mustn't be applied
# ChatMessageSender: The message sender that is selected to send messages in a chat has changed
# ChatMessageTtl: The message Time To Live setting for a chat was changed
# ChatNotificationSettings: Notification settings for a chat were changed
# ChatPendingJoinRequests: The chat pending join requests were changed
# ChatReplyMarkup: The default chat reply markup was changed. Can occur because new messages with reply markup were received or because an old reply markup was hidden by the user
# ChatTheme: The chat theme was changed
# ChatUnreadMentionCount: The chat unread_mention_count has changed
# ChatVideoChat: A chat video chat state has changed
# ChatDefaultDisableNotification: The value of the default disable_notification parameter, used when a message is sent to the chat, was changed
# ChatHasProtectedContent: A chat content was allowed or restricted for saving
# ChatHasScheduledMessages: A chat's has_scheduled_messages field has changed
# ChatIsBlocked: A chat was blocked or unblocked
# ChatIsMarkedAsUnread: A chat was marked as unread or was read
# ChatFilters: The list of chat filters or a chat filter has changed
# ChatOnlineMemberCount: The number of online group members has changed. This update with non-zero count is sent only for currently opened chats. There is no guarantee that it will be sent just after the count has changed
# ScopeNotificationSettings: Notification settings for some type of chats were updated
# Notification: A notification was changed
# NotificationGroup: A list of active notifications in a notification group has changed
# ActiveNotifications: Contains active notifications that was shown on previous application launches. This update is sent only if the message database is used. In that case it comes once before any updateNotification and updateNotificationGroup update
# HavePendingNotifications: Describes whether there are some pending notification updates. Can be used to prevent application from killing, while there are some pending notifications
# DeleteMessages: Some messages were deleted
# ChatAction: A message sender activity in the chat has changed
# UserStatus: The user went online or offline
# User: Some data of a user has changed. This update is guaranteed to come before the user identifier is returned to the application
# BasicGroup: Some data of a basic group has changed. This update is guaranteed to come before the basic group identifier is returned to the application
# Supergroup: Some data of a supergroup or a channel has changed. This update is guaranteed to come before the supergroup identifier is returned to the application
# SecretChat: Some data of a secret chat has changed. This update is guaranteed to come before the secret chat identifier is returned to the application
# UserFullInfo: Some data in userFullInfo has been changed
# BasicGroupFullInfo: Some data in basicGroupFullInfo has been changed
# SupergroupFullInfo: Some data in supergroupFullInfo has been changed
# ServiceNotification: A service notification from the server was received. Upon receiving this the application must show a popup with the content of the notification
# File: Information about a file was updated
# FileGenerationStart: The file generation process needs to be started by the application
# FileGenerationStop: File generation is no longer needed
# Call: New call was created or information about a call was updated
# GroupCall: Information about a group call was updated
# GroupCallParticipant: Information about a group call participant was changed. The updates are sent only after the group call is received through getGroupCall and only if the call is joined or being joined
# NewCallSignalingData: New call signaling data arrived
# UserPrivacySettingRules: Some privacy setting rules have been changed
# UnreadMessageCount: Number of unread messages in a chat list has changed. This update is sent only if the message database is used
# UnreadChatCount: Number of unread chats, i.e. with unread messages or marked as unread, has changed. This update is sent only if the message database is used
# Option: An option changed its value
# StickerSet: A sticker set has changed
# InstalledStickerSets: The list of installed sticker sets was updated
# TrendingStickerSets: The list of trending sticker sets was updated or some of them were viewed
# RecentStickers: The list of recently used stickers was updated
# FavoriteStickers: The list of favorite stickers was updated
# SavedAnimations: The list of saved animations was updated
# SelectedBackground: The selected background has changed
# ChatThemes: The list of available chat themes has changed
# LanguagePackStrings: Some language pack strings have been updated
# ConnectionState: The connection state has changed. This update must be used only to show a human-readable description of the connection state
# TermsOfService: New terms of service must be accepted by the user. If the terms of service are declined, then the deleteAccount method must be called with the reason "Decline ToS update"
# UsersNearby: The list of users nearby has changed. The update is guaranteed to be sent only 60 seconds after a successful searchChatsNearby request
# DiceEmojis: The list of supported dice emojis has changed
# AnimatedEmojiMessageClicked: Some animated emoji message was clicked and a big animated sticker must be played if the message is visible on the screen. chatActionWatchingAnimations with the text of the message needs to be sent if the sticker is played
# AnimationSearchParameters: The parameters of animation search through GetOption("animation_search_bot_username") bot has changed
# SuggestedActions: The list of suggested to the user actions has changed
# NewInlineQuery: A new incoming inline query; for bots only
# NewChosenInlineResult: The user has chosen a result of an inline query; for bots only
# NewCallbackQuery: A new incoming callback query; for bots only
# NewInlineCallbackQuery: A new incoming callback query from a message sent via a bot; for bots only
# NewShippingQuery: A new incoming shipping query; for bots only. Only for invoices with flexible price
# NewPreCheckoutQuery: A new incoming pre-checkout query; for bots only. Contains full information about a checkout
# NewCustomEvent: A new incoming event; for bots only
# NewCustomQuery: A new incoming query; for bots only
# Poll: A poll was updated; for bots only
# PollAnswer: A user changed the answer to a poll; for bots only
# ChatMember: User rights changed in a chat; for bots only
# NewChatJoinRequest: A user sent a join request to a chat; for bots only

abstract class TD::Update < TD::Base
end