# Order information
#
# @name : String (Name of the user)
# @phone_number : String (Phone number of the user)
# @email_address : String (Email address of the user)
# @shipping_address : TD::Address? (Shipping address for this order; may be null)

class TD::OrderInfo < TD::Base
  include JSON::Serializable

  object_type "orderInfo"

  object_attributes({
    name: String,
    phone_number: String,
    email_address: String,
    shipping_address: TD::Address?
  })
end