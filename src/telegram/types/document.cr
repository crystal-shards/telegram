# Describes a document of any type
#
# @file_name : String (Original name of the file; as defined by the sender)
# @mime_type : String (MIME type of the file; as defined by the sender)
# @minithumbnail : TD::Minithumbnail? (Document minithumbnail; may be null)
# @thumbnail : TD::Thumbnail? (Document thumbnail in JPEG or PNG format (PNG will be used only for background patterns); as defined by the sender; may be null)
# @document : TD::File (File containing the document)

class TD::Document < TD::Base
  include JSON::Serializable

  object_type "document"

  object_attributes({
    file_name: String,
    mime_type: String,
    minithumbnail: TD::Minithumbnail?,
    thumbnail: TD::Thumbnail?,
    document: TD::File
  })
end