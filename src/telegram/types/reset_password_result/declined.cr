# The password reset request was declined
#
# @retry_date : Int32 (Point in time (Unix timestamp) when the password reset can be retried)

class TD::ResetPasswordResult::Declined < TD::ResetPasswordResult
  include JSON::Serializable

  object_type "resetPasswordResultDeclined"

  object_attributes({
    retry_date: Int32
  })
end