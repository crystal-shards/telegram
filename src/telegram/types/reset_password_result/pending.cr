# The password reset request is pending
#
# @pending_reset_date : Int32 (Point in time (Unix timestamp) after which the password can be reset immediately using resetPassword)

class TD::ResetPasswordResult::Pending < TD::ResetPasswordResult
  include JSON::Serializable

  object_type "resetPasswordResultPending"

  object_attributes({
    pending_reset_date: Int32
  })
end