# The password was reset
#

class TD::ResetPasswordResult::Ok < TD::ResetPasswordResult
  include JSON::Serializable

  object_type "resetPasswordResultOk"
end