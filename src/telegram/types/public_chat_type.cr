# Describes a type of public chats
#
# HasUsername: The chat is public, because it has username
# IsLocationBased: The chat is public, because it is a location-based supergroup

abstract class TD::PublicChatType < TD::Base
end