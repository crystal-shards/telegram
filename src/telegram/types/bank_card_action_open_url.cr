# Describes an action associated with a bank card number
#
# @text : String (Action text)
# @url : String (The URL to be opened)

class TD::BankCardActionOpenUrl < TD::Base
  include JSON::Serializable

  object_type "bankCardActionOpenUrl"

  object_attributes({
    text: String,
    url: String
  })
end