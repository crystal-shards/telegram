# An identity document to be saved to Telegram Passport
#
# @number : String (Document number; 1-24 characters)
# @expiry_date : TD::Date (Document expiry date; pass null if not applicable)
# @front_side : TD::InputFile (Front side of the document)
# @reverse_side : TD::InputFile (Reverse side of the document; only for driver license and identity card; pass null otherwise)
# @selfie : TD::InputFile (Selfie with the document; pass null if unavailable)
# @translation : Array(TD::InputFile) (List of files containing a certified English translation of the document)

class TD::InputIdentityDocument < TD::Base
  include JSON::Serializable

  object_type "inputIdentityDocument"

  object_attributes({
    number: String,
    expiry_date: TD::Date,
    front_side: TD::InputFile,
    reverse_side: TD::InputFile,
    selfie: TD::InputFile,
    translation: Array(TD::InputFile)
  })
end