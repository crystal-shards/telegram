# A detailed statistics about a channel chat
#
# @period : TD::DateRange (A period to which the statistics applies)
# @member_count : TD::StatisticalValue (Number of members in the chat)
# @mean_view_count : TD::StatisticalValue (Mean number of times the recently sent messages was viewed)
# @mean_share_count : TD::StatisticalValue (Mean number of times the recently sent messages was shared)
# @enabled_notifications_percentage : Float64 (A percentage of users with enabled notifications for the chat)
# @member_count_graph : TD::StatisticalGraph (A graph containing number of members in the chat)
# @join_graph : TD::StatisticalGraph (A graph containing number of members joined and left the chat)
# @mute_graph : TD::StatisticalGraph (A graph containing number of members muted and unmuted the chat)
# @view_count_by_hour_graph : TD::StatisticalGraph (A graph containing number of message views in a given hour in the last two weeks)
# @view_count_by_source_graph : TD::StatisticalGraph (A graph containing number of message views per source)
# @join_by_source_graph : TD::StatisticalGraph (A graph containing number of new member joins per source)
# @language_graph : TD::StatisticalGraph (A graph containing number of users viewed chat messages per language)
# @message_interaction_graph : TD::StatisticalGraph (A graph containing number of chat message views and shares)
# @instant_view_interaction_graph : TD::StatisticalGraph (A graph containing number of views of associated with the chat instant views)
# @recent_message_interactions : Array(TD::ChatStatisticsMessageInteractionInfo) (Detailed statistics about number of views and shares of recently sent messages)

class TD::ChatStatistics::Channel < TD::ChatStatistics
  include JSON::Serializable

  object_type "chatStatisticsChannel"

  object_attributes({
    period: TD::DateRange,
    member_count: TD::StatisticalValue,
    mean_view_count: TD::StatisticalValue,
    mean_share_count: TD::StatisticalValue,
    enabled_notifications_percentage: Float64,
    member_count_graph: TD::StatisticalGraph,
    join_graph: TD::StatisticalGraph,
    mute_graph: TD::StatisticalGraph,
    view_count_by_hour_graph: TD::StatisticalGraph,
    view_count_by_source_graph: TD::StatisticalGraph,
    join_by_source_graph: TD::StatisticalGraph,
    language_graph: TD::StatisticalGraph,
    message_interaction_graph: TD::StatisticalGraph,
    instant_view_interaction_graph: TD::StatisticalGraph,
    recent_message_interactions: Array(TD::ChatStatisticsMessageInteractionInfo)
  })
end