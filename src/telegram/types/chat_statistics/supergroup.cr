# A detailed statistics about a supergroup chat
#
# @period : TD::DateRange (A period to which the statistics applies)
# @member_count : TD::StatisticalValue (Number of members in the chat)
# @message_count : TD::StatisticalValue (Number of messages sent to the chat)
# @viewer_count : TD::StatisticalValue (Number of users who viewed messages in the chat)
# @sender_count : TD::StatisticalValue (Number of users who sent messages to the chat)
# @member_count_graph : TD::StatisticalGraph (A graph containing number of members in the chat)
# @join_graph : TD::StatisticalGraph (A graph containing number of members joined and left the chat)
# @join_by_source_graph : TD::StatisticalGraph (A graph containing number of new member joins per source)
# @language_graph : TD::StatisticalGraph (A graph containing distribution of active users per language)
# @message_content_graph : TD::StatisticalGraph (A graph containing distribution of sent messages by content type)
# @action_graph : TD::StatisticalGraph (A graph containing number of different actions in the chat)
# @day_graph : TD::StatisticalGraph (A graph containing distribution of message views per hour)
# @week_graph : TD::StatisticalGraph (A graph containing distribution of message views per day of week)
# @top_senders : Array(TD::ChatStatisticsMessageSenderInfo) (List of users sent most messages in the last week)
# @top_administrators : Array(TD::ChatStatisticsAdministratorActionsInfo) (List of most active administrators in the last week)
# @top_inviters : Array(TD::ChatStatisticsInviterInfo) (List of most active inviters of new members in the last week)

class TD::ChatStatistics::Supergroup < TD::ChatStatistics
  include JSON::Serializable

  object_type "chatStatisticsSupergroup"

  object_attributes({
    period: TD::DateRange,
    member_count: TD::StatisticalValue,
    message_count: TD::StatisticalValue,
    viewer_count: TD::StatisticalValue,
    sender_count: TD::StatisticalValue,
    member_count_graph: TD::StatisticalGraph,
    join_graph: TD::StatisticalGraph,
    join_by_source_graph: TD::StatisticalGraph,
    language_graph: TD::StatisticalGraph,
    message_content_graph: TD::StatisticalGraph,
    action_graph: TD::StatisticalGraph,
    day_graph: TD::StatisticalGraph,
    week_graph: TD::StatisticalGraph,
    top_senders: Array(TD::ChatStatisticsMessageSenderInfo),
    top_administrators: Array(TD::ChatStatisticsAdministratorActionsInfo),
    top_inviters: Array(TD::ChatStatisticsInviterInfo)
  })
end