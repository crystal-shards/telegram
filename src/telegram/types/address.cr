# Describes an address
#
# @country_code : String (A two-letter ISO 3166-1 alpha-2 country code)
# @state : String? (State, if applicable)
# @city : String (City)
# @street_line1 : String (First line of the address)
# @street_line2 : String (Second line of the address)
# @postal_code : String (Address postal code)

class TD::Address < TD::Base
  include JSON::Serializable

  object_type "address"

  object_attributes({
    country_code: String,
    state: String?,
    city: String,
    street_line1: String,
    street_line2: String,
    postal_code: String
  })
end