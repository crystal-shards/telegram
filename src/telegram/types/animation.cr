# Describes an animation file. The animation must be encoded in GIF or MPEG4 format
#
# @duration : Int32 (Duration of the animation, in seconds; as defined by the sender)
# @width : Int32 (Width of the animation)
# @height : Int32 (Height of the animation)
# @file_name : String (Original name of the file; as defined by the sender)
# @mime_type : String (MIME type of the file, usually "image)
# @has_stickers : Bool (True, if stickers were added to the animation. The list of corresponding sticker set can be received using getAttachedStickerSets)
# @minithumbnail : TD::Minithumbnail? (Animation minithumbnail; may be null)
# @thumbnail : TD::Thumbnail? (Animation thumbnail in JPEG or MPEG4 format; may be null)
# @animation : TD::File (File containing the animation)

class TD::Animation < TD::Base
  include JSON::Serializable

  object_type "animation"

  object_attributes({
    duration: Int32,
    width: Int32,
    height: Int32,
    file_name: String,
    mime_type: String,
    has_stickers: Bool,
    minithumbnail: TD::Minithumbnail?,
    thumbnail: TD::Thumbnail?,
    animation: TD::File
  })
end