# Contains information about an encrypted Telegram Passport element; for bots only
#
# @type : TD::PassportElementType (Type of Telegram Passport element)
# @data : String (Encrypted JSON-encoded data about the user)
# @front_side : TD::DatedFile (The front side of an identity document)
# @reverse_side : TD::DatedFile? (The reverse side of an identity document; may be null)
# @selfie : TD::DatedFile? (Selfie with the document; may be null)
# @translation : Array(TD::DatedFile) (List of files containing a certified English translation of the document)
# @files : Array(TD::DatedFile) (List of attached files)
# @value : String (Unencrypted data, phone number or email address)
# @hash : String (Hash of the entire element)

class TD::EncryptedPassportElement < TD::Base
  include JSON::Serializable

  object_type "encryptedPassportElement"

  object_attributes({
    type: TD::PassportElementType,
    data: String,
    front_side: TD::DatedFile,
    reverse_side: TD::DatedFile?,
    selfie: TD::DatedFile?,
    translation: Array(TD::DatedFile),
    files: Array(TD::DatedFile),
    value: String,
    hash: String
  })
end