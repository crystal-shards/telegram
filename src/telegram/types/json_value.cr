# Represents a JSON value
#
# Null: Represents a null JSON value
# Boolean: Represents a boolean JSON value
# Number: Represents a numeric JSON value
# String: Represents a string JSON value
# Array: Represents a JSON array
# Object: Represents a JSON object

abstract class TD::JsonValue < TD::Base
end