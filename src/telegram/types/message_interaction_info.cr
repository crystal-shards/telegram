# Contains information about interactions with a message
#
# @view_count : Int32 (Number of times the message was viewed)
# @forward_count : Int32 (Number of times the message was forwarded)
# @reply_info : TD::MessageReplyInfo? (Information about direct or indirect replies to the message; may be null. Currently, available only in channels with a discussion supergroup and discussion supergroups for messages, which are not replies itself)

class TD::MessageInteractionInfo < TD::Base
  include JSON::Serializable

  object_type "messageInteractionInfo"

  object_attributes({
    view_count: Int32,
    forward_count: Int32,
    reply_info: TD::MessageReplyInfo?
  })
end