# Contains information about the origin of a forwarded message
#
# User: The message was originally sent by a known user
# Chat: The message was originally sent on behalf of a chat
# HiddenUser: The message was originally sent by a user, which is hidden by their privacy settings
# Channel: The message was originally a post in a channel
# MessageImport: The message was imported from an exported message history

abstract class TD::MessageForwardOrigin < TD::Base
end