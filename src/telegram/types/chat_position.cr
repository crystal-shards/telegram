# Describes a position of a chat in a chat list
#
# @list : TD::ChatList (The chat list)
# @order : String (A parameter used to determine order of the chat in the chat list. Chats must be sorted by the pair (order, chat.id) in descending order)
# @is_pinned : Bool (True, if the chat is pinned in the chat list)
# @source : TD::ChatSource? (Source of the chat in the chat list; may be null)

class TD::ChatPosition < TD::Base
  include JSON::Serializable

  object_type "chatPosition"

  object_attributes({
    list: TD::ChatList,
    order: String,
    is_pinned: Bool,
    source: TD::ChatSource?
  })
end