# Contains information about a chat administrator
#
# @user_id : Int64 (User identifier of the administrator)
# @custom_title : String (Custom title of the administrator)
# @is_owner : Bool (True, if the user is the owner of the chat)

class TD::ChatAdministrator < TD::Base
  include JSON::Serializable

  object_type "chatAdministrator"

  object_attributes({
    user_id: Int64,
    custom_title: String,
    is_owner: Bool
  })
end