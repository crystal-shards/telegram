# Contains the storage usage statistics for a specific chat
#
# @chat_id : Int64 (Chat identifier; 0 if none)
# @size : Int64 (Total size of the files in the chat, in bytes)
# @count : Int32 (Total number of files in the chat)
# @by_file_type : Array(TD::StorageStatisticsByFileType) (Statistics split by file types)

class TD::StorageStatisticsByChat < TD::Base
  include JSON::Serializable

  object_type "storageStatisticsByChat"

  object_attributes({
    chat_id: Int64,
    size: Int64,
    count: Int32,
    by_file_type: Array(TD::StorageStatisticsByFileType)
  })
end