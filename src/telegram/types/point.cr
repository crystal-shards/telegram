# A point on a Cartesian plane
#
# @x : Float64 (The point's first coordinate)
# @y : Float64 (The point's second coordinate)

class TD::Point < TD::Base
  include JSON::Serializable

  object_type "point"

  object_attributes({
    x: Float64,
    y: Float64
  })
end