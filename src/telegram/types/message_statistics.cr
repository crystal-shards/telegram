# A detailed statistics about a message
#
# @message_interaction_graph : TD::StatisticalGraph (A graph containing number of message views and shares)

class TD::MessageStatistics < TD::Base
  include JSON::Serializable

  object_type "messageStatistics"

  object_attributes({
    message_interaction_graph: TD::StatisticalGraph
  })
end