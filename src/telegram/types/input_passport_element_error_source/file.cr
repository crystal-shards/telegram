# The file contains an error. The error is considered resolved when the file changes
#
# @file_hash : String (Current hash of the file which has the error)

class TD::InputPassportElementErrorSource::File < TD::InputPassportElementErrorSource
  include JSON::Serializable

  object_type "inputPassportElementErrorSourceFile"

  object_attributes({
    file_hash: String
  })
end