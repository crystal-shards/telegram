# The element contains an error in an unspecified place. The error will be considered resolved when new data is added
#
# @element_hash : String (Current hash of the entire element)

class TD::InputPassportElementErrorSource::Unspecified < TD::InputPassportElementErrorSource
  include JSON::Serializable

  object_type "inputPassportElementErrorSourceUnspecified"

  object_attributes({
    element_hash: String
  })
end