# The selfie contains an error. The error is considered resolved when the file with the selfie changes
#
# @file_hash : String (Current hash of the file containing the selfie)

class TD::InputPassportElementErrorSource::Selfie < TD::InputPassportElementErrorSource
  include JSON::Serializable

  object_type "inputPassportElementErrorSourceSelfie"

  object_attributes({
    file_hash: String
  })
end