# The list of attached files contains an error. The error is considered resolved when the file list changes
#
# @file_hashes : Array(String) (Current hashes of all attached files)

class TD::InputPassportElementErrorSource::Files < TD::InputPassportElementErrorSource
  include JSON::Serializable

  object_type "inputPassportElementErrorSourceFiles"

  object_attributes({
    file_hashes: Array(String)
  })
end