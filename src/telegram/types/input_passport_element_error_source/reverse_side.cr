# The reverse side of the document contains an error. The error is considered resolved when the file with the reverse side of the document changes
#
# @file_hash : String (Current hash of the file containing the reverse side)

class TD::InputPassportElementErrorSource::ReverseSide < TD::InputPassportElementErrorSource
  include JSON::Serializable

  object_type "inputPassportElementErrorSourceReverseSide"

  object_attributes({
    file_hash: String
  })
end