# One of the files containing the translation of the document contains an error. The error is considered resolved when the file with the translation changes
#
# @file_hash : String (Current hash of the file containing the translation)

class TD::InputPassportElementErrorSource::TranslationFile < TD::InputPassportElementErrorSource
  include JSON::Serializable

  object_type "inputPassportElementErrorSourceTranslationFile"

  object_attributes({
    file_hash: String
  })
end