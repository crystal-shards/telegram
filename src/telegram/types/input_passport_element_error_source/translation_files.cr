# The translation of the document contains an error. The error is considered resolved when the list of files changes
#
# @file_hashes : Array(String) (Current hashes of all files with the translation)

class TD::InputPassportElementErrorSource::TranslationFiles < TD::InputPassportElementErrorSource
  include JSON::Serializable

  object_type "inputPassportElementErrorSourceTranslationFiles"

  object_attributes({
    file_hashes: Array(String)
  })
end