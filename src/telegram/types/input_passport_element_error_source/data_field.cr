# A data field contains an error. The error is considered resolved when the field's value changes
#
# @field_name : String (Field name)
# @data_hash : String (Current data hash)

class TD::InputPassportElementErrorSource::DataField < TD::InputPassportElementErrorSource
  include JSON::Serializable

  object_type "inputPassportElementErrorSourceDataField"

  object_attributes({
    field_name: String,
    data_hash: String
  })
end