# The front side of the document contains an error. The error is considered resolved when the file with the front side of the document changes
#
# @file_hash : String (Current hash of the file containing the front side)

class TD::InputPassportElementErrorSource::FrontSide < TD::InputPassportElementErrorSource
  include JSON::Serializable

  object_type "inputPassportElementErrorSourceFrontSide"

  object_attributes({
    file_hash: String
  })
end