# The message is being sent now, but has not yet been delivered to the server
#

class TD::MessageSendingState::Pending < TD::MessageSendingState
  include JSON::Serializable

  object_type "messageSendingStatePending"
end