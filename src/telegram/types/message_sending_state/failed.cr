# The message failed to be sent
#
# @error_code : Int32 (An error code; 0 if unknown)
# @error_message : String (Error message)
# @can_retry : Bool (True, if the message can be re-sent)
# @need_another_sender : Bool (True, if the message can be re-sent only on behalf of a different sender)
# @retry_after : Float64 (Time left before the message can be re-sent, in seconds. No update is sent when this field changes)

class TD::MessageSendingState::Failed < TD::MessageSendingState
  include JSON::Serializable

  object_type "messageSendingStateFailed"

  object_attributes({
    error_code: Int32,
    error_message: String,
    can_retry: Bool,
    need_another_sender: Bool,
    retry_after: Float64
  })
end