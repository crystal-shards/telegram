# Suggests the user to check whether they still remember their 2-step verification password
#

class TD::SuggestedAction::CheckPassword < TD::SuggestedAction
  include JSON::Serializable

  object_type "suggestedActionCheckPassword"
end