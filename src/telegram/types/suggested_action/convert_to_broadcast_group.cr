# Suggests the user to convert specified supergroup to a broadcast group
#
# @supergroup_id : Int64 (Supergroup identifier)

class TD::SuggestedAction::ConvertToBroadcastGroup < TD::SuggestedAction
  include JSON::Serializable

  object_type "suggestedActionConvertToBroadcastGroup"

  object_attributes({
    supergroup_id: Int64
  })
end