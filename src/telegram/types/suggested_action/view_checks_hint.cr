# Suggests the user to view a hint about the meaning of one and two check marks on sent messages
#

class TD::SuggestedAction::ViewChecksHint < TD::SuggestedAction
  include JSON::Serializable

  object_type "suggestedActionViewChecksHint"
end