# Suggests the user to set a 2-step verification password to be able to log in again
#
# @authorization_delay : Int32 (The number of days to pass between consecutive authorizations if the user declines to set password)

class TD::SuggestedAction::SetPassword < TD::SuggestedAction
  include JSON::Serializable

  object_type "suggestedActionSetPassword"

  object_attributes({
    authorization_delay: Int32
  })
end