# Suggests the user to enable "archive_and_mute_new_chats_from_unknown_users" option
#

class TD::SuggestedAction::EnableArchiveAndMuteNewChats < TD::SuggestedAction
  include JSON::Serializable

  object_type "suggestedActionEnableArchiveAndMuteNewChats"
end