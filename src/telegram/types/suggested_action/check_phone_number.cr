# Suggests the user to check whether authorization phone number is correct and change the phone number if it is inaccessible
#

class TD::SuggestedAction::CheckPhoneNumber < TD::SuggestedAction
  include JSON::Serializable

  object_type "suggestedActionCheckPhoneNumber"
end