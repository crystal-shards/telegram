# Represents a set of filters used to obtain a chat event log
#
# @message_edits : Bool (True, if message edits need to be returned)
# @message_deletions : Bool (True, if message deletions need to be returned)
# @message_pins : Bool (True, if pin)
# @member_joins : Bool (True, if members joining events need to be returned)
# @member_leaves : Bool (True, if members leaving events need to be returned)
# @member_invites : Bool (True, if invited member events need to be returned)
# @member_promotions : Bool (True, if member promotion)
# @member_restrictions : Bool (True, if member restricted)
# @info_changes : Bool (True, if changes in chat information need to be returned)
# @setting_changes : Bool (True, if changes in chat settings need to be returned)
# @invite_link_changes : Bool (True, if changes to invite links need to be returned)
# @video_chat_changes : Bool (True, if video chat actions need to be returned)

class TD::ChatEventLogFilters < TD::Base
  include JSON::Serializable

  object_type "chatEventLogFilters"

  object_attributes({
    message_edits: Bool,
    message_deletions: Bool,
    message_pins: Bool,
    member_joins: Bool,
    member_leaves: Bool,
    member_invites: Bool,
    member_promotions: Bool,
    member_restrictions: Bool,
    info_changes: Bool,
    setting_changes: Bool,
    invite_link_changes: Bool,
    video_chat_changes: Bool
  })
end