# Instructs application to force a reply to this message
#
# @is_personal : Bool (True, if a forced reply must automatically be shown to the current user. For outgoing messages, specify true to show the forced reply only for the mentioned users and for the target user of a reply)
# @input_field_placeholder : String (If non-empty, the placeholder to be shown in the input field when the reply is active; 0-64 characters)

class TD::ReplyMarkup::ForceReply < TD::ReplyMarkup
  include JSON::Serializable

  object_type "replyMarkupForceReply"

  object_attributes({
    is_personal: Bool,
    input_field_placeholder: String
  })
end