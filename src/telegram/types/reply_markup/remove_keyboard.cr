# Instructs application to remove the keyboard once this message has been received. This kind of keyboard can't be received in an incoming message; instead, UpdateChatReplyMarkup with message_id
#
# @is_personal : Bool (True, if the keyboard is removed only for the mentioned users or the target user of a reply)

class TD::ReplyMarkup::RemoveKeyboard < TD::ReplyMarkup
  include JSON::Serializable

  object_type "replyMarkupRemoveKeyboard"

  object_attributes({
    is_personal: Bool
  })
end