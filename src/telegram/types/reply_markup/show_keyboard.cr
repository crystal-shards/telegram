# Contains a custom keyboard layout to quickly reply to bots
#
# @rows : Array(Array(TD::KeyboardButton)) (A list of rows of bot keyboard buttons)
# @resize_keyboard : Bool (True, if the application needs to resize the keyboard vertically)
# @one_time : Bool (True, if the application needs to hide the keyboard after use)
# @is_personal : Bool (True, if the keyboard must automatically be shown to the current user. For outgoing messages, specify true to show the keyboard only for the mentioned users and for the target user of a reply)
# @input_field_placeholder : String (If non-empty, the placeholder to be shown in the input field when the keyboard is active; 0-64 characters)

class TD::ReplyMarkup::ShowKeyboard < TD::ReplyMarkup
  include JSON::Serializable

  object_type "replyMarkupShowKeyboard"

  object_attributes({
    rows: Array(Array(TD::KeyboardButton)),
    resize_keyboard: Bool,
    one_time: Bool,
    is_personal: Bool,
    input_field_placeholder: String
  })
end