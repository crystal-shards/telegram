# Contains an inline keyboard layout
#
# @rows : Array(Array(TD::InlineKeyboardButton)) (A list of rows of inline keyboard buttons)

class TD::ReplyMarkup::InlineKeyboard < TD::ReplyMarkup
  include JSON::Serializable

  object_type "replyMarkupInlineKeyboard"

  object_attributes({
    rows: Array(Array(TD::InlineKeyboardButton))
  })
end