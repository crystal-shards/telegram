# A regular poll
#
# @allow_multiple_answers : Bool (True, if multiple answer options can be chosen simultaneously)

class TD::PollType::Regular < TD::PollType
  include JSON::Serializable

  object_type "pollTypeRegular"

  object_attributes({
    allow_multiple_answers: Bool
  })
end