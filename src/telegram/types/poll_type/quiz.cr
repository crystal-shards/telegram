# A poll in quiz mode, which has exactly one correct answer option and can be answered only once
#
# @correct_option_id : Int32 (0-based identifier of the correct answer option; -1 for a yet unanswered poll)
# @explanation : TD::FormattedText (Text that is shown when the user chooses an incorrect answer or taps on the lamp icon; 0-200 characters with at most 2 line feeds; empty for a yet unanswered poll)

class TD::PollType::Quiz < TD::PollType
  include JSON::Serializable

  object_type "pollTypeQuiz"

  object_attributes({
    correct_option_id: Int32,
    explanation: TD::FormattedText
  })
end