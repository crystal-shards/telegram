# Contains information about the payment method chosen by the user
#
# Saved: Applies if a user chooses some previously saved payment credentials. To use their previously saved credentials, the user must have a valid temporary password
# New: Applies if a user enters new credentials on a payment provider website
# ApplePay: Applies if a user enters new credentials using Apple Pay
# GooglePay: Applies if a user enters new credentials using Google Pay

abstract class TD::InputCredentials < TD::Base
end