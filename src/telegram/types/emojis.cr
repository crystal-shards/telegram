# Represents a list of emoji
#
# @emojis : Array(String) (List of emojis)

class TD::Emojis < TD::Base
  include JSON::Serializable

  object_type "emojis"

  object_attributes({
    emojis: Array(String)
  })
end