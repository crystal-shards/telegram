# A group containing a notification of type notificationTypeNewSecretChat
#

class TD::NotificationGroupType::SecretChat < TD::NotificationGroupType
  include JSON::Serializable

  object_type "notificationGroupTypeSecretChat"
end