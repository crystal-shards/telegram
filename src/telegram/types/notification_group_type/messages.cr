# A group containing notifications of type notificationTypeNewMessage and notificationTypeNewPushMessage with ordinary unread messages
#

class TD::NotificationGroupType::Messages < TD::NotificationGroupType
  include JSON::Serializable

  object_type "notificationGroupTypeMessages"
end