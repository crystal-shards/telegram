# A group containing notifications of type notificationTypeNewCall
#

class TD::NotificationGroupType::Calls < TD::NotificationGroupType
  include JSON::Serializable

  object_type "notificationGroupTypeCalls"
end