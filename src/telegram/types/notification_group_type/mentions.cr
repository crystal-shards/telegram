# A group containing notifications of type notificationTypeNewMessage and notificationTypeNewPushMessage with unread mentions of the current user, replies to their messages, or a pinned message
#

class TD::NotificationGroupType::Mentions < TD::NotificationGroupType
  include JSON::Serializable

  object_type "notificationGroupTypeMentions"
end