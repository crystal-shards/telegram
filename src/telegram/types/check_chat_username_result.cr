# Represents result of checking whether a username can be set for a chat
#
# Ok: The username can be set
# UsernameInvalid: The username is invalid
# UsernameOccupied: The username is occupied
# PublicChatsTooMuch: The user has too much chats with username, one of them must be made private first
# PublicGroupsUnavailable: The user can't be a member of a public supergroup

abstract class TD::CheckChatUsernameResult < TD::Base
end