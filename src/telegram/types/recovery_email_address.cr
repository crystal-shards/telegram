# Contains information about the current recovery email address
#
# @recovery_email_address : String (Recovery email address)

class TD::RecoveryEmailAddress < TD::Base
  include JSON::Serializable

  object_type "recoveryEmailAddress"

  object_attributes({
    recovery_email_address: String
  })
end