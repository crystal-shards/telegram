# Contains information about a message in a specific position
#
# @position : Int32 (0-based message position in the full list of suitable messages)
# @message_id : Int64 (Message identifier)
# @date : Int32 (Point in time (Unix timestamp) when the message was sent)

class TD::MessagePosition < TD::Base
  include JSON::Serializable

  object_type "messagePosition"

  object_attributes({
    position: Int32,
    message_id: Int64,
    date: Int32
  })
end