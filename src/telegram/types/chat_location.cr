# Represents a location to which a chat is connected
#
# @location : TD::Location (The location)
# @address : String (Location address; 1-64 characters, as defined by the chat owner)

class TD::ChatLocation < TD::Base
  include JSON::Serializable

  object_type "chatLocation"

  object_attributes({
    location: TD::Location,
    address: String
  })
end