# Represents a list of users
#
# @total_count : Int32 (Approximate total count of users found)
# @user_ids : Array(Int64) (A list of user identifiers)

class TD::Users < TD::Base
  include JSON::Serializable

  object_type "users"

  object_attributes({
    total_count: Int32,
    user_ids: Array(Int64)
  })
end