# A simple object containing a sequence of bytes; for testing only
#
# @value : String (Bytes)

class TD::TestBytes < TD::Base
  include JSON::Serializable

  object_type "testBytes"

  object_attributes({
    value: String
  })
end