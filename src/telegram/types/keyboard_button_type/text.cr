# A simple button, with text that must be sent when the button is pressed
#

class TD::KeyboardButtonType::Text < TD::KeyboardButtonType
  include JSON::Serializable

  object_type "keyboardButtonTypeText"
end