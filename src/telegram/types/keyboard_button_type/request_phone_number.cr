# A button that sends the user's phone number when pressed; available only in private chats
#

class TD::KeyboardButtonType::RequestPhoneNumber < TD::KeyboardButtonType
  include JSON::Serializable

  object_type "keyboardButtonTypeRequestPhoneNumber"
end