# A button that allows the user to create and send a poll when pressed; available only in private chats
#
# @force_regular : Bool (If true, only regular polls must be allowed to create)
# @force_quiz : Bool (If true, only polls in quiz mode must be allowed to create)

class TD::KeyboardButtonType::RequestPoll < TD::KeyboardButtonType
  include JSON::Serializable

  object_type "keyboardButtonTypeRequestPoll"

  object_attributes({
    force_regular: Bool,
    force_quiz: Bool
  })
end