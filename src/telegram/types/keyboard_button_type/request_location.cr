# A button that sends the user's location when pressed; available only in private chats
#

class TD::KeyboardButtonType::RequestLocation < TD::KeyboardButtonType
  include JSON::Serializable

  object_type "keyboardButtonTypeRequestLocation"
end