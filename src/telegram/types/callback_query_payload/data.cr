# The payload for a general callback button
#
# @data : String (Data that was attached to the callback button)

class TD::CallbackQueryPayload::Data < TD::CallbackQueryPayload
  include JSON::Serializable

  object_type "callbackQueryPayloadData"

  object_attributes({
    data: String
  })
end