# The payload for a game callback button
#
# @game_short_name : String (A short name of the game that was attached to the callback button)

class TD::CallbackQueryPayload::Game < TD::CallbackQueryPayload
  include JSON::Serializable

  object_type "callbackQueryPayloadGame"

  object_attributes({
    game_short_name: String
  })
end