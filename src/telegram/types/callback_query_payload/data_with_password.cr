# The payload for a callback button requiring password
#
# @password : String (The password for the current user)
# @data : String (Data that was attached to the callback button)

class TD::CallbackQueryPayload::DataWithPassword < TD::CallbackQueryPayload
  include JSON::Serializable

  object_type "callbackQueryPayloadDataWithPassword"

  object_attributes({
    password: String,
    data: String
  })
end