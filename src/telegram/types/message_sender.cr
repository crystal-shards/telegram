# Contains information about the sender of a message
#
# User: The message was sent by a known user
# Chat: The message was sent on behalf of a chat

abstract class TD::MessageSender < TD::Base
end