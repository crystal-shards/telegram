# Contains the group call identifier
#
# @id : Int32 (Group call identifier)

class TD::GroupCallId < TD::Base
  include JSON::Serializable

  object_type "groupCallId"

  object_attributes({
    id: Int32
  })
end