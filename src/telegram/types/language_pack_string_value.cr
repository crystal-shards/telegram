# Represents the value of a string in a language pack
#
# Ordinary: An ordinary language pack string
# Pluralized: A language pack string which has different forms based on the number of some object it mentions. See https:
# Deleted: A deleted language pack string, the value must be taken from the built-in English language pack

abstract class TD::LanguagePackStringValue < TD::Base
end