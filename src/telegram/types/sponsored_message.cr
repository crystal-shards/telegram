# Describes a sponsored message
#
# @message_id : Int64 (Message identifier; unique for the chat to which the sponsored message belongs among both ordinary and sponsored messages)
# @sponsor_chat_id : Int64 (Chat identifier)
# @link : TD::InternalLinkType? (An internal link to be opened when the sponsored message is clicked; may be null. If null, the sponsor chat needs to be opened instead)
# @content : TD::MessageContent (Content of the message. Currently, can be only of the type messageText)

class TD::SponsoredMessage < TD::Base
  include JSON::Serializable

  object_type "sponsoredMessage"

  object_attributes({
    message_id: Int64,
    sponsor_chat_id: Int64,
    link: TD::InternalLinkType?,
    content: TD::MessageContent
  })
end