# Contains the description of an error in a Telegram Passport element; for bots only
#
# Unspecified: The element contains an error in an unspecified place. The error will be considered resolved when new data is added
# DataField: A data field contains an error. The error is considered resolved when the field's value changes
# FrontSide: The front side of the document contains an error. The error is considered resolved when the file with the front side of the document changes
# ReverseSide: The reverse side of the document contains an error. The error is considered resolved when the file with the reverse side of the document changes
# Selfie: The selfie contains an error. The error is considered resolved when the file with the selfie changes
# TranslationFile: One of the files containing the translation of the document contains an error. The error is considered resolved when the file with the translation changes
# TranslationFiles: The translation of the document contains an error. The error is considered resolved when the list of files changes
# File: The file contains an error. The error is considered resolved when the file changes
# Files: The list of attached files contains an error. The error is considered resolved when the file list changes

abstract class TD::InputPassportElementErrorSource < TD::Base
end