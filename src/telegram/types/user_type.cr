# Represents the type of a user. The following types are possible: regular users, deleted users and bots
#
# Regular: A regular user
# Deleted: A deleted user or deleted bot. No information on the user besides the user identifier is available. It is not possible to perform any active actions on this type of user
# Bot: A bot (see https:
# Unknown: No information on the user besides the user identifier is available, yet this user has not been deleted. This object is extremely rare and must be handled like a deleted user. It is not possible to perform any actions on users of this type

abstract class TD::UserType < TD::Base
end