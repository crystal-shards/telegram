# A WebRTC server
#
# @username : String (Username to be used for authentication)
# @password : String (Authentication password)
# @supports_turn : Bool (True, if the server supports TURN)
# @supports_stun : Bool (True, if the server supports STUN)

class TD::CallServerType::Webrtc < TD::CallServerType
  include JSON::Serializable

  object_type "callServerTypeWebrtc"

  object_attributes({
    username: String,
    password: String,
    supports_turn: Bool,
    supports_stun: Bool
  })
end