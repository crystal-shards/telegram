# A Telegram call reflector
#
# @peer_tag : String (A peer tag to be used with the reflector)

class TD::CallServerType::TelegramReflector < TD::CallServerType
  include JSON::Serializable

  object_type "callServerTypeTelegramReflector"

  object_attributes({
    peer_tag: String
  })
end