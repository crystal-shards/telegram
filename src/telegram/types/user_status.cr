# Describes the last time the user was online
#
# Empty: The user status was never changed
# Online: The user is online
# Offline: The user is offline
# Recently: The user was online recently
# LastWeek: The user is offline, but was online last week
# LastMonth: The user is offline, but was online last month

abstract class TD::UserStatus < TD::Base
end