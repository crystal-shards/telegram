# Describes the current secret chat state
#
# Pending: The secret chat is not yet created; waiting for the other user to get online
# Ready: The secret chat is ready to use
# Closed: The secret chat is closed

abstract class TD::SecretChatState < TD::Base
end