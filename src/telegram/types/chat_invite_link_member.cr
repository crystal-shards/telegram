# Describes a chat member joined a chat via an invite link
#
# @user_id : Int64 (User identifier)
# @joined_chat_date : Int32 (Point in time (Unix timestamp) when the user joined the chat)
# @approver_user_id : Int64 (User identifier of the chat administrator, approved user join request)

class TD::ChatInviteLinkMember < TD::Base
  include JSON::Serializable

  object_type "chatInviteLinkMember"

  object_attributes({
    user_id: Int64,
    joined_chat_date: Int32,
    approver_user_id: Int64
  })
end