# Describes a user or a chat as a member of another chat
#
# @member_id : TD::MessageSender (Identifier of the chat member. Currently, other chats can be only Left or Banned. Only supergroups and channels can have other chats as Left or Banned members and these chats must be supergroups or channels)
# @inviter_user_id : Int64 (Identifier of a user that invited)
# @joined_chat_date : Int32 (Point in time (Unix timestamp) when the user joined the chat)
# @status : TD::ChatMemberStatus (Status of the member in the chat)

class TD::ChatMember < TD::Base
  include JSON::Serializable

  object_type "chatMember"

  object_attributes({
    member_id: TD::MessageSender,
    inviter_user_id: Int64,
    joined_chat_date: Int32,
    status: TD::ChatMemberStatus
  })
end