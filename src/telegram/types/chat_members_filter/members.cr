# Returns all chat members, including restricted chat members
#

class TD::ChatMembersFilter::Members < TD::ChatMembersFilter
  include JSON::Serializable

  object_type "chatMembersFilterMembers"
end