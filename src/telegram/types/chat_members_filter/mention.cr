# Returns users which can be mentioned in the chat
#
# @message_thread_id : Int64 (If non-zero, the identifier of the current message thread)

class TD::ChatMembersFilter::Mention < TD::ChatMembersFilter
  include JSON::Serializable

  object_type "chatMembersFilterMention"

  object_attributes({
    message_thread_id: Int64
  })
end