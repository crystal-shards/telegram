# Returns the owner and administrators
#

class TD::ChatMembersFilter::Administrators < TD::ChatMembersFilter
  include JSON::Serializable

  object_type "chatMembersFilterAdministrators"
end