# Returns users banned from the chat; can be used only by administrators in a supergroup or in a channel
#

class TD::ChatMembersFilter::Banned < TD::ChatMembersFilter
  include JSON::Serializable

  object_type "chatMembersFilterBanned"
end