# Returns contacts of the user
#

class TD::ChatMembersFilter::Contacts < TD::ChatMembersFilter
  include JSON::Serializable

  object_type "chatMembersFilterContacts"
end