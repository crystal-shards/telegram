# Returns bot members of the chat
#

class TD::ChatMembersFilter::Bots < TD::ChatMembersFilter
  include JSON::Serializable

  object_type "chatMembersFilterBots"
end