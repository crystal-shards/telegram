# Returns users under certain restrictions in the chat; can be used only by administrators in a supergroup
#

class TD::ChatMembersFilter::Restricted < TD::ChatMembersFilter
  include JSON::Serializable

  object_type "chatMembersFilterRestricted"
end