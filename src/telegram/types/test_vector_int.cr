# A simple object containing a vector of numbers; for testing only
#
# @value : Array(Int32) (Vector of numbers)

class TD::TestVectorInt < TD::Base
  include JSON::Serializable

  object_type "testVectorInt"

  object_attributes({
    value: Array(Int32)
  })
end