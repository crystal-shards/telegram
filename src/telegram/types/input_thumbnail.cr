# A thumbnail to be sent along with a file; must be in JPEG or WEBP format for stickers, and less than 200 KB in size
#
# @thumbnail : TD::InputFile (Thumbnail file to send. Sending thumbnails by file_id is currently not supported)
# @width : Int32 (Thumbnail width, usually shouldn't exceed 320. Use 0 if unknown)
# @height : Int32 (Thumbnail height, usually shouldn't exceed 320. Use 0 if unknown)

class TD::InputThumbnail < TD::Base
  include JSON::Serializable

  object_type "inputThumbnail"

  object_attributes({
    thumbnail: TD::InputFile,
    width: Int32,
    height: Int32
  })
end