# Represents a filter for message search results
#
# Empty: Returns all found messages, no filter is applied
# Animation: Returns only animation messages
# Audio: Returns only audio messages
# Document: Returns only document messages
# Photo: Returns only photo messages
# Video: Returns only video messages
# VoiceNote: Returns only voice note messages
# PhotoAndVideo: Returns only photo and video messages
# Url: Returns only messages containing URLs
# ChatPhoto: Returns only messages containing chat photos
# VideoNote: Returns only video note messages
# VoiceAndVideoNote: Returns only voice and video note messages
# Mention: Returns only messages with mentions of the current user, or messages that are replies to their messages
# UnreadMention: Returns only messages with unread mentions of the current user, or messages that are replies to their messages. When using this filter the results can't be additionally filtered by a query, a message thread or by the sending user
# FailedToSend: Returns only failed to send messages. This filter can be used only if the message database is used
# Pinned: Returns only pinned messages

abstract class TD::SearchMessagesFilter < TD::Base
end