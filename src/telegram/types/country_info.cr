# Contains information about a country
#
# @country_code : String (A two-letter ISO 3166-1 alpha-2 country code)
# @name : String (Native name of the country)
# @english_name : String (English name of the country)
# @is_hidden : Bool (True, if the country must be hidden from the list of all countries)
# @calling_codes : Array(String) (List of country calling codes)

class TD::CountryInfo < TD::Base
  include JSON::Serializable

  object_type "countryInfo"

  object_attributes({
    country_code: String,
    name: String,
    english_name: String,
    is_hidden: Bool,
    calling_codes: Array(String)
  })
end