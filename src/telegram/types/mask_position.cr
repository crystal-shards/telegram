# Position on a photo where a mask is placed
#
# @point : TD::MaskPoint (Part of the face, relative to which the mask is placed)
# @x_shift : Float64 (Shift by X-axis measured in widths of the mask scaled to the face size, from left to right. (For example, -1.0 will place the mask just to the left of the default mask position))
# @y_shift : Float64 (Shift by Y-axis measured in heights of the mask scaled to the face size, from top to bottom. (For example, 1.0 will place the mask just below the default mask position))
# @scale : Float64 (Mask scaling coefficient. (For example, 2.0 means a doubled size))

class TD::MaskPosition < TD::Base
  include JSON::Serializable

  object_type "maskPosition"

  object_attributes({
    point: TD::MaskPoint,
    x_shift: Float64,
    y_shift: Float64,
    scale: Float64
  })
end