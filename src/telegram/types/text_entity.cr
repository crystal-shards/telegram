# Represents a part of the text that needs to be formatted in some unusual way
#
# @offset : Int32 (Offset of the entity, in UTF-16 code units)
# @length : Int32 (Length of the entity, in UTF-16 code units)
# @type : TD::TextEntityType (Type of the entity)

class TD::TextEntity < TD::Base
  include JSON::Serializable

  object_type "textEntity"

  object_attributes({
    offset: Int32,
    length: Int32,
    type: TD::TextEntityType
  })
end