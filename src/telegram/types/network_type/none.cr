# The network is not available
#

class TD::NetworkType::None < TD::NetworkType
  include JSON::Serializable

  object_type "networkTypeNone"
end