# A different network type (e.g., Ethernet network)
#

class TD::NetworkType::Other < TD::NetworkType
  include JSON::Serializable

  object_type "networkTypeOther"
end