# A Wi-Fi network
#

class TD::NetworkType::WiFi < TD::NetworkType
  include JSON::Serializable

  object_type "networkTypeWiFi"
end