# A mobile network
#

class TD::NetworkType::Mobile < TD::NetworkType
  include JSON::Serializable

  object_type "networkTypeMobile"
end