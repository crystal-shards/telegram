# A mobile roaming network
#

class TD::NetworkType::MobileRoaming < TD::NetworkType
  include JSON::Serializable

  object_type "networkTypeMobileRoaming"
end