# Portion of the price of a product (e.g., "delivery cost", "tax amount")
#
# @label : String (Label for this portion of the product price)
# @amount : Int64 (Currency amount in the smallest units of the currency)

class TD::LabeledPricePart < TD::Base
  include JSON::Serializable

  object_type "labeledPricePart"

  object_attributes({
    label: String,
    amount: Int64
  })
end