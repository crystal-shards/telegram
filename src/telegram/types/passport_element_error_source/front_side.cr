# The front side of the document contains an error. The error will be considered resolved when the file with the front side changes
#

class TD::PassportElementErrorSource::FrontSide < TD::PassportElementErrorSource
  include JSON::Serializable

  object_type "passportElementErrorSourceFrontSide"
end