# The selfie with the document contains an error. The error will be considered resolved when the file with the selfie changes
#

class TD::PassportElementErrorSource::Selfie < TD::PassportElementErrorSource
  include JSON::Serializable

  object_type "passportElementErrorSourceSelfie"
end