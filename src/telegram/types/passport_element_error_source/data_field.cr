# One of the data fields contains an error. The error will be considered resolved when the value of the field changes
#
# @field_name : String (Field name)

class TD::PassportElementErrorSource::DataField < TD::PassportElementErrorSource
  include JSON::Serializable

  object_type "passportElementErrorSourceDataField"

  object_attributes({
    field_name: String
  })
end