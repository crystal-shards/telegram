# The reverse side of the document contains an error. The error will be considered resolved when the file with the reverse side changes
#

class TD::PassportElementErrorSource::ReverseSide < TD::PassportElementErrorSource
  include JSON::Serializable

  object_type "passportElementErrorSourceReverseSide"
end