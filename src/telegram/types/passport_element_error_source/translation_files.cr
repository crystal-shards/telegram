# The translation of the document contains an error. The error will be considered resolved when the list of translation files changes
#

class TD::PassportElementErrorSource::TranslationFiles < TD::PassportElementErrorSource
  include JSON::Serializable

  object_type "passportElementErrorSourceTranslationFiles"
end