# The list of attached files contains an error. The error will be considered resolved when the list of files changes
#

class TD::PassportElementErrorSource::Files < TD::PassportElementErrorSource
  include JSON::Serializable

  object_type "passportElementErrorSourceFiles"
end