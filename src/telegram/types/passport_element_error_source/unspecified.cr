# The element contains an error in an unspecified place. The error will be considered resolved when new data is added
#

class TD::PassportElementErrorSource::Unspecified < TD::PassportElementErrorSource
  include JSON::Serializable

  object_type "passportElementErrorSourceUnspecified"
end