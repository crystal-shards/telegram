# The file contains an error. The error will be considered resolved when the file changes
#
# @file_index : Int32 (Index of a file with the error)

class TD::PassportElementErrorSource::File < TD::PassportElementErrorSource
  include JSON::Serializable

  object_type "passportElementErrorSourceFile"

  object_attributes({
    file_index: Int32
  })
end