# One of files with the translation of the document contains an error. The error will be considered resolved when the file changes
#
# @file_index : Int32 (Index of a file with the error)

class TD::PassportElementErrorSource::TranslationFile < TD::PassportElementErrorSource
  include JSON::Serializable

  object_type "passportElementErrorSourceTranslationFile"

  object_attributes({
    file_index: Int32
  })
end