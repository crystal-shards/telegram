# Contains information about a message draft
#
# @reply_to_message_id : Int64 (Identifier of the message to reply to; 0 if none)
# @date : Int32 (Point in time (Unix timestamp) when the draft was created)
# @input_message_text : TD::InputMessageContent (Content of the message draft; must be of the type inputMessageText)

class TD::DraftMessage < TD::Base
  include JSON::Serializable

  object_type "draftMessage"

  object_attributes({
    reply_to_message_id: Int64,
    date: Int32,
    input_message_text: TD::InputMessageContent
  })
end