# Part of the face, relative to which a mask is placed
#
# Forehead: The mask is placed relatively to the forehead
# Eyes: The mask is placed relatively to the eyes
# Mouth: The mask is placed relatively to the mouth
# Chin: The mask is placed relatively to the chin

abstract class TD::MaskPoint < TD::Base
end