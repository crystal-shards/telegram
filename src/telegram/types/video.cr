# Describes a video file
#
# @duration : Int32 (Duration of the video, in seconds; as defined by the sender)
# @width : Int32 (Video width; as defined by the sender)
# @height : Int32 (Video height; as defined by the sender)
# @file_name : String (Original name of the file; as defined by the sender)
# @mime_type : String (MIME type of the file; as defined by the sender)
# @has_stickers : Bool (True, if stickers were added to the video. The list of corresponding sticker sets can be received using getAttachedStickerSets)
# @supports_streaming : Bool (True, if the video is supposed to be streamed)
# @minithumbnail : TD::Minithumbnail? (Video minithumbnail; may be null)
# @thumbnail : TD::Thumbnail? (Video thumbnail in JPEG or MPEG4 format; as defined by the sender; may be null)
# @video : TD::File (File containing the video)

class TD::Video < TD::Base
  include JSON::Serializable

  object_type "video"

  object_attributes({
    duration: Int32,
    width: Int32,
    height: Int32,
    file_name: String,
    mime_type: String,
    has_stickers: Bool,
    supports_streaming: Bool,
    minithumbnail: TD::Minithumbnail?,
    thumbnail: TD::Thumbnail?,
    video: TD::File
  })
end