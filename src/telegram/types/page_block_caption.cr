# Contains a caption of an instant view web page block, consisting of a text and a trailing credit
#
# @text : TD::RichText (Content of the caption)
# @credit : TD::RichText (Block credit (like HTML tag)

class TD::PageBlockCaption < TD::Base
  include JSON::Serializable

  object_type "pageBlockCaption"

  object_attributes({
    text: TD::RichText,
    credit: TD::RichText
  })
end