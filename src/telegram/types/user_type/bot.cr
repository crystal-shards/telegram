# A bot (see https:
#
# @can_join_groups : Bool (True, if the bot can be invited to basic group and supergroup chats)
# @can_read_all_group_messages : Bool (True, if the bot can read all messages in basic group or supergroup chats and not just those addressed to the bot. In private and channel chats a bot can always read all messages)
# @is_inline : Bool (True, if the bot supports inline queries)
# @inline_query_placeholder : String (Placeholder for inline queries (displayed on the application input field))
# @need_location : Bool (True, if the location of the user is expected to be sent with every inline query to this bot)

class TD::UserType::Bot < TD::UserType
  include JSON::Serializable

  object_type "userTypeBot"

  object_attributes({
    can_join_groups: Bool,
    can_read_all_group_messages: Bool,
    is_inline: Bool,
    inline_query_placeholder: String,
    need_location: Bool
  })
end