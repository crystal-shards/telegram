# A regular user
#

class TD::UserType::Regular < TD::UserType
  include JSON::Serializable

  object_type "userTypeRegular"
end