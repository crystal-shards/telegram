# Contains information about a proxy server
#
# @id : Int32 (Unique identifier of the proxy)
# @server : String (Proxy server IP address)
# @port : Int32 (Proxy server port)
# @last_used_date : Int32 (Point in time (Unix timestamp) when the proxy was last used; 0 if never)
# @is_enabled : Bool (True, if the proxy is enabled now)
# @type : TD::ProxyType (Type of the proxy)

class TD::Proxy < TD::Base
  include JSON::Serializable

  object_type "proxy"

  object_attributes({
    id: Int32,
    server: String,
    port: Int32,
    last_used_date: Int32,
    is_enabled: Bool,
    type: TD::ProxyType
  })
end