# Represents the current state of 2-step verification
#
# @has_password : Bool (True, if a 2-step verification password is set)
# @password_hint : String (Hint for the password; may be empty)
# @has_recovery_email_address : Bool (True, if a recovery email is set)
# @has_passport_data : Bool (True, if some Telegram Passport elements were saved)
# @recovery_email_address_code_info : TD::EmailAddressAuthenticationCodeInfo? (Information about the recovery email address to which the confirmation email was sent; may be null)
# @pending_reset_date : Int32 (If not 0, point in time (Unix timestamp) after which the password can be reset immediately using resetPassword)

class TD::PasswordState < TD::Base
  include JSON::Serializable

  object_type "passwordState"

  object_attributes({
    has_password: Bool,
    password_hint: String,
    has_recovery_email_address: Bool,
    has_passport_data: Bool,
    recovery_email_address_code_info: TD::EmailAddressAuthenticationCodeInfo?,
    pending_reset_date: Int32
  })
end