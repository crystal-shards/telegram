# Contains a part of a file
#
# @data : String (File bytes)

class TD::FilePart < TD::Base
  include JSON::Serializable

  object_type "filePart"

  object_attributes({
    data: String
  })
end