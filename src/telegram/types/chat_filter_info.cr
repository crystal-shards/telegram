# Contains basic information about a chat filter
#
# @id : Int32 (Unique chat filter identifier)
# @title : String (The title of the filter; 1-12 characters without line feeds)
# @icon_name : String (The chosen or default icon name for short filter representation. One of "All", "Unread", "Unmuted", "Bots", "Channels", "Groups", "Private", "Custom", "Setup", "Cat", "Crown", "Favorite", "Flower", "Game", "Home", "Love", "Mask", "Party", "Sport", "Study", "Trade", "Travel", "Work")

class TD::ChatFilterInfo < TD::Base
  include JSON::Serializable

  object_type "chatFilterInfo"

  object_attributes({
    id: Int32,
    title: String,
    icon_name: String
  })
end