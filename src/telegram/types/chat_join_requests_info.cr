# Contains information about pending join requests for a chat
#
# @total_count : Int32 (Total number of pending join requests)
# @user_ids : Array(Int64) (Identifiers of at most 3 users sent the newest pending join requests)

class TD::ChatJoinRequestsInfo < TD::Base
  include JSON::Serializable

  object_type "chatJoinRequestsInfo"

  object_attributes({
    total_count: Int32,
    user_ids: Array(Int64)
  })
end