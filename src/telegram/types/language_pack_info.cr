# Contains information about a language pack
#
# @id : String (Unique language pack identifier)
# @base_language_pack_id : String (Identifier of a base language pack; may be empty. If a string is missed in the language pack, then it must be fetched from base language pack. Unsupported in custom language packs)
# @name : String (Language name)
# @native_name : String (Name of the language in that language)
# @plural_code : String (A language code to be used to apply plural forms. See https:)
# @is_official : Bool (True, if the language pack is official)
# @is_rtl : Bool (True, if the language pack strings are RTL)
# @is_beta : Bool (True, if the language pack is a beta language pack)
# @is_installed : Bool (True, if the language pack is installed by the current user)
# @total_string_count : Int32 (Total number of non-deleted strings from the language pack)
# @translated_string_count : Int32 (Total number of translated strings from the language pack)
# @local_string_count : Int32 (Total number of non-deleted strings from the language pack available locally)
# @translation_url : String (Link to language translation interface; empty for custom local language packs)

class TD::LanguagePackInfo < TD::Base
  include JSON::Serializable

  object_type "languagePackInfo"

  object_attributes({
    id: String,
    base_language_pack_id: String,
    name: String,
    native_name: String,
    plural_code: String,
    is_official: Bool,
    is_rtl: Bool,
    is_beta: Bool,
    is_installed: Bool,
    total_string_count: Int32,
    translated_string_count: Int32,
    local_string_count: Int32,
    translation_url: String
  })
end