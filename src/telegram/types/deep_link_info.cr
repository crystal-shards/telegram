# Contains information about a tg: deep link
#
# @text : TD::FormattedText (Text to be shown to the user)
# @need_update_application : Bool (True, if the user must be asked to update the application)

class TD::DeepLinkInfo < TD::Base
  include JSON::Serializable

  object_type "deepLinkInfo"

  object_attributes({
    text: TD::FormattedText,
    need_update_application: Bool
  })
end