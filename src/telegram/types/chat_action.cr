# Describes the different types of activity in a chat
#
# Typing: The user is typing a message
# RecordingVideo: The user is recording a video
# UploadingVideo: The user is uploading a video
# RecordingVoiceNote: The user is recording a voice note
# UploadingVoiceNote: The user is uploading a voice note
# UploadingPhoto: The user is uploading a photo
# UploadingDocument: The user is uploading a document
# ChoosingSticker: The user is picking a sticker to send
# ChoosingLocation: The user is picking a location or venue to send
# ChoosingContact: The user is picking a contact to send
# StartPlayingGame: The user has started to play a game
# RecordingVideoNote: The user is recording a video note
# UploadingVideoNote: The user is uploading a video note
# WatchingAnimations: The user is watching animations sent by the other party by clicking on an animated emoji
# Cancel: The user has canceled the previous action

abstract class TD::ChatAction < TD::Base
end