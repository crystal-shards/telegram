# Contains information about countries
#
# @countries : Array(TD::CountryInfo) (The list of countries)

class TD::Countries < TD::Base
  include JSON::Serializable

  object_type "countries"

  object_attributes({
    countries: Array(TD::CountryInfo)
  })
end