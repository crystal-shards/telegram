# Represents a list of proxy servers
#
# @proxies : Array(TD::Proxy) (List of proxy servers)

class TD::Proxies < TD::Base
  include JSON::Serializable

  object_type "proxies"

  object_attributes({
    proxies: Array(TD::Proxy)
  })
end