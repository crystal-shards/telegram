# Describes the way the text needs to be parsed for TextEntities
#
# Markdown: The text uses Markdown-style formatting
# HTML: The text uses HTML-style formatting. The same as Telegram Bot API "HTML" parse mode

abstract class TD::TextParseMode < TD::Base
end