# Contains information about a forwarded message
#
# @origin : TD::MessageForwardOrigin (Origin of a forwarded message)
# @date : Int32 (Point in time (Unix timestamp) when the message was originally sent)
# @public_service_announcement_type : String (The type of a public service announcement for the forwarded message)
# @from_chat_id : Int64 (For messages forwarded to the chat with the current user (Saved Messages), to the Replies bot chat, or to the channel's discussion group, the identifier of the chat from which the message was forwarded last time; 0 if unknown)
# @from_message_id : Int64 (For messages forwarded to the chat with the current user (Saved Messages), to the Replies bot chat, or to the channel's discussion group, the identifier of the original message from which the new message was forwarded last time; 0 if unknown)

class TD::MessageForwardInfo < TD::Base
  include JSON::Serializable

  object_type "messageForwardInfo"

  object_attributes({
    origin: TD::MessageForwardOrigin,
    date: Int32,
    public_service_announcement_type: String,
    from_chat_id: Int64,
    from_message_id: Int64
  })
end