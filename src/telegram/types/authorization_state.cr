# Represents the current authorization state of the TDLib client
#
# WaitTdlibParameters: TDLib needs TdlibParameters for initialization
# WaitEncryptionKey: TDLib needs an encryption key to decrypt the local database
# WaitPhoneNumber: TDLib needs the user's phone number to authorize. Call `setAuthenticationPhoneNumber` to provide the phone number, or use `requestQrCodeAuthentication`, or `checkAuthenticationBotToken` for other authentication options
# WaitCode: TDLib needs the user's authentication code to authorize
# WaitOtherDeviceConfirmation: The user needs to confirm authorization on another logged in device by scanning a QR code with the provided link
# WaitRegistration: The user is unregistered and need to accept terms of service and enter their first name and last name to finish registration
# WaitPassword: The user has been authorized, but needs to enter a password to start using the application
# Ready: The user has been successfully authorized. TDLib is now ready to answer queries
# LoggingOut: The user is currently logging out
# Closing: TDLib is closing, all subsequent queries will be answered with the error 500. Note that closing TDLib can take a while. All resources will be freed only after authorizationStateClosed has been received
# Closed: TDLib client is in its final state. All databases are closed and all resources are released. No other updates will be received after this. All queries will be responded towith error code 500. To continue working, one must create a new instance of the TDLib client

abstract class TD::AuthorizationState < TD::Base
end