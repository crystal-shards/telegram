# Contains information about found messages sent on a specific day
#
# @total_count : Int32 (Total number of found messages sent on the day)
# @message : TD::Message (First message sent on the day)

class TD::MessageCalendarDay < TD::Base
  include JSON::Serializable

  object_type "messageCalendarDay"

  object_attributes({
    total_count: Int32,
    message: TD::Message
  })
end