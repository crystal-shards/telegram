# Describes a group call
#
# @id : Int32 (Group call identifier)
# @title : String (Group call title)
# @scheduled_start_date : Int32 (Point in time (Unix timestamp) when the group call is supposed to be started by an administrator; 0 if it is already active or was ended)
# @enabled_start_notification : Bool (True, if the group call is scheduled and the current user will receive a notification when the group call will start)
# @is_active : Bool (True, if the call is active)
# @is_joined : Bool (True, if the call is joined)
# @need_rejoin : Bool (True, if user was kicked from the call because of network loss and the call needs to be rejoined)
# @can_be_managed : Bool (True, if the current user can manage the group call)
# @participant_count : Int32 (Number of participants in the group call)
# @loaded_all_participants : Bool (True, if all group call participants are loaded)
# @recent_speakers : Array(TD::GroupCallRecentSpeaker) (At most 3 recently speaking users in the group call)
# @is_my_video_enabled : Bool (True, if the current user's video is enabled)
# @is_my_video_paused : Bool (True, if the current user's video is paused)
# @can_enable_video : Bool (True, if the current user can broadcast video or share screen)
# @mute_new_participants : Bool (True, if only group call administrators can unmute new participants)
# @can_toggle_mute_new_participants : Bool (True, if the current user can enable or disable mute_new_participants setting)
# @record_duration : Int32 (Duration of the ongoing group call recording, in seconds; 0 if none. An updateGroupCall update is not triggered when value of this field changes, but the same recording goes on)
# @is_video_recorded : Bool (True, if a video file is being recorded for the call)
# @duration : Int32 (Call duration, in seconds; for ended calls only)

class TD::GroupCall < TD::Base
  include JSON::Serializable

  object_type "groupCall"

  object_attributes({
    id: Int32,
    title: String,
    scheduled_start_date: Int32,
    enabled_start_notification: Bool,
    is_active: Bool,
    is_joined: Bool,
    need_rejoin: Bool,
    can_be_managed: Bool,
    participant_count: Int32,
    loaded_all_participants: Bool,
    recent_speakers: Array(TD::GroupCallRecentSpeaker),
    is_my_video_enabled: Bool,
    is_my_video_paused: Bool,
    can_enable_video: Bool,
    mute_new_participants: Bool,
    can_toggle_mute_new_participants: Bool,
    record_duration: Int32,
    is_video_recorded: Bool,
    duration: Int32
  })
end