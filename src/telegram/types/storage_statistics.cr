# Contains the exact storage usage statistics split by chats and file type
#
# @size : Int64 (Total size of files, in bytes)
# @count : Int32 (Total number of files)
# @by_chat : Array(TD::StorageStatisticsByChat) (Statistics split by chats)

class TD::StorageStatistics < TD::Base
  include JSON::Serializable

  object_type "storageStatistics"

  object_attributes({
    size: Int64,
    count: Int32,
    by_chat: Array(TD::StorageStatisticsByChat)
  })
end