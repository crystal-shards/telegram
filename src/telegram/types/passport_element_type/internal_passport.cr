# A Telegram Passport element containing the user's internal passport
#

class TD::PassportElementType::InternalPassport < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypeInternalPassport"
end