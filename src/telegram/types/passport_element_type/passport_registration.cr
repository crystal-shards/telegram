# A Telegram Passport element containing the registration page of the user's passport
#

class TD::PassportElementType::PassportRegistration < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypePassportRegistration"
end