# A Telegram Passport element containing the user's temporary registration
#

class TD::PassportElementType::TemporaryRegistration < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypeTemporaryRegistration"
end