# A Telegram Passport element containing the user's rental agreement
#

class TD::PassportElementType::RentalAgreement < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypeRentalAgreement"
end