# A Telegram Passport element containing the user's email address
#

class TD::PassportElementType::EmailAddress < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypeEmailAddress"
end