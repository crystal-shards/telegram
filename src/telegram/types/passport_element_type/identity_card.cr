# A Telegram Passport element containing the user's identity card
#

class TD::PassportElementType::IdentityCard < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypeIdentityCard"
end