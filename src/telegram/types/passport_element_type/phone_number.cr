# A Telegram Passport element containing the user's phone number
#

class TD::PassportElementType::PhoneNumber < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypePhoneNumber"
end