# A Telegram Passport element containing the user's utility bill
#

class TD::PassportElementType::UtilityBill < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypeUtilityBill"
end