# A Telegram Passport element containing the user's address
#

class TD::PassportElementType::Address < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypeAddress"
end