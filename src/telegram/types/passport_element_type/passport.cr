# A Telegram Passport element containing the user's passport
#

class TD::PassportElementType::Passport < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypePassport"
end