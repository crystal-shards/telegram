# A Telegram Passport element containing the user's bank statement
#

class TD::PassportElementType::BankStatement < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypeBankStatement"
end