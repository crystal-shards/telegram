# A Telegram Passport element containing the user's personal details
#

class TD::PassportElementType::PersonalDetails < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypePersonalDetails"
end