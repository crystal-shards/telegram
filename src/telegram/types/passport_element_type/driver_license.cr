# A Telegram Passport element containing the user's driver license
#

class TD::PassportElementType::DriverLicense < TD::PassportElementType
  include JSON::Serializable

  object_type "passportElementTypeDriverLicense"
end