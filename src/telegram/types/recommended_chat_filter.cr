# Describes a recommended chat filter
#
# @filter : TD::ChatFilter (The chat filter)
# @description : String (Chat filter description)

class TD::RecommendedChatFilter < TD::Base
  include JSON::Serializable

  object_type "recommendedChatFilter"

  object_attributes({
    filter: TD::ChatFilter,
    description: String
  })
end