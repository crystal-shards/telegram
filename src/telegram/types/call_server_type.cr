# Describes the type of a call server
#
# TelegramReflector: A Telegram call reflector
# Webrtc: A WebRTC server

abstract class TD::CallServerType < TD::Base
end