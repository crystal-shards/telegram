# The content must be center-aligned
#

class TD::PageBlockHorizontalAlignment::Center < TD::PageBlockHorizontalAlignment
  include JSON::Serializable

  object_type "pageBlockHorizontalAlignmentCenter"
end