# The content must be left-aligned
#

class TD::PageBlockHorizontalAlignment::Left < TD::PageBlockHorizontalAlignment
  include JSON::Serializable

  object_type "pageBlockHorizontalAlignmentLeft"
end