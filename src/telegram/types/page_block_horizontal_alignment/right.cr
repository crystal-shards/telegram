# The content must be right-aligned
#

class TD::PageBlockHorizontalAlignment::Right < TD::PageBlockHorizontalAlignment
  include JSON::Serializable

  object_type "pageBlockHorizontalAlignmentRight"
end