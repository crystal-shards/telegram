# Contains a list of sessions
#
# @sessions : Array(TD::Session) (List of sessions)
# @inactive_session_ttl_days : Int32 (Number of days of inactivity before sessions will automatically be terminated; 1-366 days)

class TD::Sessions < TD::Base
  include JSON::Serializable

  object_type "sessions"

  object_attributes({
    sessions: Array(TD::Session),
    inactive_session_ttl_days: Int32
  })
end