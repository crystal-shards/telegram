# A background from a local file
#
# @background : TD::InputFile (Background file to use. Only inputFileLocal and inputFileGenerated are supported. The file must be in JPEG format for wallpapers and in PNG format for patterns)

class TD::InputBackground::Local < TD::InputBackground
  include JSON::Serializable

  object_type "inputBackgroundLocal"

  object_attributes({
    background: TD::InputFile
  })
end