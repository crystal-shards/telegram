# A background from the server
#
# @background_id : String (The background identifier)

class TD::InputBackground::Remote < TD::InputBackground
  include JSON::Serializable

  object_type "inputBackgroundRemote"

  object_attributes({
    background_id: String
  })
end