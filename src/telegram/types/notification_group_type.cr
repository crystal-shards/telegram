# Describes the type of notifications in a notification group
#
# Messages: A group containing notifications of type notificationTypeNewMessage and notificationTypeNewPushMessage with ordinary unread messages
# Mentions: A group containing notifications of type notificationTypeNewMessage and notificationTypeNewPushMessage with unread mentions of the current user, replies to their messages, or a pinned message
# SecretChat: A group containing a notification of type notificationTypeNewSecretChat
# Calls: A group containing notifications of type notificationTypeNewCall

abstract class TD::NotificationGroupType < TD::Base
end