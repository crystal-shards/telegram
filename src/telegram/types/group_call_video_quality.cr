# Describes the quality of a group call video
#
# Thumbnail: The worst available video quality
# Medium: The medium video quality
# Full: The best available video quality

abstract class TD::GroupCallVideoQuality < TD::Base
end