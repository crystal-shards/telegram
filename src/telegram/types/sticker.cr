# Describes a sticker
#
# @set_id : String (The identifier of the sticker set to which the sticker belongs; 0 if none)
# @width : Int32 (Sticker width; as defined by the sender)
# @height : Int32 (Sticker height; as defined by the sender)
# @emoji : String (Emoji corresponding to the sticker)
# @is_animated : Bool (True, if the sticker is an animated sticker in TGS format)
# @is_mask : Bool (True, if the sticker is a mask)
# @mask_position : TD::MaskPosition? (Position where the mask is placed; may be null)
# @outline : Array(TD::ClosedVectorPath) (Sticker's outline represented as a list of closed vector paths; may be empty. The coordinate system origin is in the upper-left corner)
# @thumbnail : TD::Thumbnail? (Sticker thumbnail in WEBP or JPEG format; may be null)
# @sticker : TD::File (File containing the sticker)

class TD::Sticker < TD::Base
  include JSON::Serializable

  object_type "sticker"

  object_attributes({
    set_id: String,
    width: Int32,
    height: Int32,
    emoji: String,
    is_animated: Bool,
    is_mask: Bool,
    mask_position: TD::MaskPosition?,
    outline: Array(TD::ClosedVectorPath),
    thumbnail: TD::Thumbnail?,
    sticker: TD::File
  })
end