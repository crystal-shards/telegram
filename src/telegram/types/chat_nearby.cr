# Describes a chat located nearby
#
# @chat_id : Int64 (Chat identifier)
# @distance : Int32 (Distance to the chat location, in meters)

class TD::ChatNearby < TD::Base
  include JSON::Serializable

  object_type "chatNearby"

  object_attributes({
    chat_id: Int64,
    distance: Int32
  })
end