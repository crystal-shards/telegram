# Represents a part of the text which must be formatted differently
#
# Mention: A mention of a user by their username
# Hashtag: A hashtag text, beginning with "
# Cashtag: A cashtag text, beginning with "
# BotCommand: A bot command, beginning with "
# Url: An HTTP URL
# EmailAddress: An email address
# PhoneNumber: A phone number
# BankCardNumber: A bank card number. The getBankCardInfo method can be used to get information about the bank card
# Bold: A bold text
# Italic: An italic text
# Underline: An underlined text
# Strikethrough: A strikethrough text
# Code: Text that must be formatted as if inside a code HTML tag
# Pre: Text that must be formatted as if inside a pre HTML tag
# PreCode: Text that must be formatted as if inside pre, and code HTML tags
# TextUrl: A text description shown instead of a raw URL
# MentionName: A text shows instead of a raw mention of the user (e.g., when the user has no username)
# MediaTimestamp: A media timestamp

abstract class TD::TextEntityType < TD::Base
end