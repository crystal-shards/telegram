# A rule to restrict all users from doing something
#

class TD::UserPrivacySettingRule::RestrictAll < TD::UserPrivacySettingRule
  include JSON::Serializable

  object_type "userPrivacySettingRuleRestrictAll"
end