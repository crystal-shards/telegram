# A rule to restrict all members of specified basic groups and supergroups from doing something
#
# @chat_ids : Array(Int64) (The chat identifiers, total number of chats in all rules must not exceed 20)

class TD::UserPrivacySettingRule::RestrictChatMembers < TD::UserPrivacySettingRule
  include JSON::Serializable

  object_type "userPrivacySettingRuleRestrictChatMembers"

  object_attributes({
    chat_ids: Array(Int64)
  })
end