# A rule to restrict all contacts of a user from doing something
#

class TD::UserPrivacySettingRule::RestrictContacts < TD::UserPrivacySettingRule
  include JSON::Serializable

  object_type "userPrivacySettingRuleRestrictContacts"
end