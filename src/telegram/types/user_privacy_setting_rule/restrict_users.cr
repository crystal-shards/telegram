# A rule to restrict all specified users from doing something
#
# @user_ids : Array(Int64) (The user identifiers, total number of users in all rules must not exceed 1000)

class TD::UserPrivacySettingRule::RestrictUsers < TD::UserPrivacySettingRule
  include JSON::Serializable

  object_type "userPrivacySettingRuleRestrictUsers"

  object_attributes({
    user_ids: Array(Int64)
  })
end