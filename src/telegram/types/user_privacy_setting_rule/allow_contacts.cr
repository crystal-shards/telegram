# A rule to allow all of a user's contacts to do something
#

class TD::UserPrivacySettingRule::AllowContacts < TD::UserPrivacySettingRule
  include JSON::Serializable

  object_type "userPrivacySettingRuleAllowContacts"
end