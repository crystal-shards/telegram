# A rule to allow certain specified users to do something
#
# @user_ids : Array(Int64) (The user identifiers, total number of users in all rules must not exceed 1000)

class TD::UserPrivacySettingRule::AllowUsers < TD::UserPrivacySettingRule
  include JSON::Serializable

  object_type "userPrivacySettingRuleAllowUsers"

  object_attributes({
    user_ids: Array(Int64)
  })
end