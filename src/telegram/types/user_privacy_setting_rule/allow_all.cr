# A rule to allow all users to do something
#

class TD::UserPrivacySettingRule::AllowAll < TD::UserPrivacySettingRule
  include JSON::Serializable

  object_type "userPrivacySettingRuleAllowAll"
end