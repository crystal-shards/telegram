# A rule to allow all members of certain specified basic groups and supergroups to doing something
#
# @chat_ids : Array(Int64) (The chat identifiers, total number of chats in all rules must not exceed 20)

class TD::UserPrivacySettingRule::AllowChatMembers < TD::UserPrivacySettingRule
  include JSON::Serializable

  object_type "userPrivacySettingRuleAllowChatMembers"

  object_attributes({
    chat_ids: Array(Int64)
  })
end