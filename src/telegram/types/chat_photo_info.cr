# Contains basic information about the photo of a chat
#
# @small : TD::File (A small (160x160) chat photo variant in JPEG format. The file can be downloaded only before the photo is changed)
# @big : TD::File (A big (640x640) chat photo variant in JPEG format. The file can be downloaded only before the photo is changed)
# @minithumbnail : TD::Minithumbnail? (Chat photo minithumbnail; may be null)
# @has_animation : Bool (True, if the photo has animated variant)

class TD::ChatPhotoInfo < TD::Base
  include JSON::Serializable

  object_type "chatPhotoInfo"

  object_attributes({
    small: TD::File,
    big: TD::File,
    minithumbnail: TD::Minithumbnail?,
    has_animation: Bool
  })
end