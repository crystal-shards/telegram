# An animation in MPEG4 format; must be square, at most 10 seconds long, have width between 160 and 800 and be at most 2MB in size
#
# @animation : TD::InputFile (Animation to be set as profile photo. Only inputFileLocal and inputFileGenerated are allowed)
# @main_frame_timestamp : Float64 (Timestamp of the frame, which will be used as static chat photo)

class TD::InputChatPhoto::Animation < TD::InputChatPhoto
  include JSON::Serializable

  object_type "inputChatPhotoAnimation"

  object_attributes({
    animation: TD::InputFile,
    main_frame_timestamp: Float64
  })
end