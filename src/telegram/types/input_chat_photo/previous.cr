# A previously used profile photo of the current user
#
# @chat_photo_id : String (Identifier of the current user's profile photo to reuse)

class TD::InputChatPhoto::Previous < TD::InputChatPhoto
  include JSON::Serializable

  object_type "inputChatPhotoPrevious"

  object_attributes({
    chat_photo_id: String
  })
end