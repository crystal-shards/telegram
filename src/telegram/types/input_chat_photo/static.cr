# A static photo in JPEG format
#
# @photo : TD::InputFile (Photo to be set as profile photo. Only inputFileLocal and inputFileGenerated are allowed)

class TD::InputChatPhoto::Static < TD::InputChatPhoto
  include JSON::Serializable

  object_type "inputChatPhotoStatic"

  object_attributes({
    photo: TD::InputFile
  })
end