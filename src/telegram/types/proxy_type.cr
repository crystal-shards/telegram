# Describes the type of a proxy server
#
# Socks5: A SOCKS5 proxy server
# Http: A HTTP transparent proxy server
# Mtproto: An MTProto proxy server

abstract class TD::ProxyType < TD::Base
end