# Contains a list of chat invite link counts
#
# @invite_link_counts : Array(TD::ChatInviteLinkCount) (List of invite link counts)

class TD::ChatInviteLinkCounts < TD::Base
  include JSON::Serializable

  object_type "chatInviteLinkCounts"

  object_attributes({
    invite_link_counts: Array(TD::ChatInviteLinkCount)
  })
end