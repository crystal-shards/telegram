# The file is a thumbnail of another file
#

class TD::FileType::Thumbnail < TD::FileType
  include JSON::Serializable

  object_type "fileTypeThumbnail"
end