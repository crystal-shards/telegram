# The file is a wallpaper or a background pattern
#

class TD::FileType::Wallpaper < TD::FileType
  include JSON::Serializable

  object_type "fileTypeWallpaper"
end