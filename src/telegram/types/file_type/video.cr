# The file is a video
#

class TD::FileType::Video < TD::FileType
  include JSON::Serializable

  object_type "fileTypeVideo"
end