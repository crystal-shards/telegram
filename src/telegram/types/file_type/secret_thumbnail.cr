# The file is a thumbnail of a file from a secret chat
#

class TD::FileType::SecretThumbnail < TD::FileType
  include JSON::Serializable

  object_type "fileTypeSecretThumbnail"
end