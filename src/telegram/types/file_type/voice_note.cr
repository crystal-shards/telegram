# The file is a voice note
#

class TD::FileType::VoiceNote < TD::FileType
  include JSON::Serializable

  object_type "fileTypeVoiceNote"
end