# The file was sent to a secret chat (the file type is not known to the server)
#

class TD::FileType::Secret < TD::FileType
  include JSON::Serializable

  object_type "fileTypeSecret"
end