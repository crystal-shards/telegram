# The file is a sticker
#

class TD::FileType::Sticker < TD::FileType
  include JSON::Serializable

  object_type "fileTypeSticker"
end