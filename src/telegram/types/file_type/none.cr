# The data is not a file
#

class TD::FileType::None < TD::FileType
  include JSON::Serializable

  object_type "fileTypeNone"
end