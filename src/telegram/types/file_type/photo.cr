# The file is a photo
#

class TD::FileType::Photo < TD::FileType
  include JSON::Serializable

  object_type "fileTypePhoto"
end