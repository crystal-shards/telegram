# The file is a file from Secure storage used for storing Telegram Passport files
#

class TD::FileType::Secure < TD::FileType
  include JSON::Serializable

  object_type "fileTypeSecure"
end