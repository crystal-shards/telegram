# The file is a document
#

class TD::FileType::Document < TD::FileType
  include JSON::Serializable

  object_type "fileTypeDocument"
end