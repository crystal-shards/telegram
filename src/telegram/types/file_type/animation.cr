# The file is an animation
#

class TD::FileType::Animation < TD::FileType
  include JSON::Serializable

  object_type "fileTypeAnimation"
end