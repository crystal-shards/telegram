# The file is a profile photo
#

class TD::FileType::ProfilePhoto < TD::FileType
  include JSON::Serializable

  object_type "fileTypeProfilePhoto"
end