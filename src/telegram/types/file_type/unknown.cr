# The file type is not yet known
#

class TD::FileType::Unknown < TD::FileType
  include JSON::Serializable

  object_type "fileTypeUnknown"
end