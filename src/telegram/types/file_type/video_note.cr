# The file is a video note
#

class TD::FileType::VideoNote < TD::FileType
  include JSON::Serializable

  object_type "fileTypeVideoNote"
end