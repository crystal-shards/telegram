# The file is an audio file
#

class TD::FileType::Audio < TD::FileType
  include JSON::Serializable

  object_type "fileTypeAudio"
end