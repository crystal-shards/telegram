# Describes a statistical graph
#
# Data: A graph data
# Async: The graph data to be asynchronously loaded through getStatisticalGraph
# Error: An error message to be shown to the user instead of the graph

abstract class TD::StatisticalGraph < TD::Base
end