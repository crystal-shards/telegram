# Represents a command supported by a bot
#
# @command : String (Text of the bot command)
# @description : String (Description of the bot command)

class TD::BotCommand < TD::Base
  include JSON::Serializable

  object_type "botCommand"

  object_attributes({
    command: String,
    description: String
  })
end