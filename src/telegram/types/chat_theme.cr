# Describes a chat theme
#
# @name : String (Theme name)
# @light_settings : TD::ThemeSettings (Theme settings for a light chat theme)
# @dark_settings : TD::ThemeSettings (Theme settings for a dark chat theme)

class TD::ChatTheme < TD::Base
  include JSON::Serializable

  object_type "chatTheme"

  object_attributes({
    name: String,
    light_settings: TD::ThemeSettings,
    dark_settings: TD::ThemeSettings
  })
end