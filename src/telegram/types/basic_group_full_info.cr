# Contains full information about a basic group
#
# @photo : TD::ChatPhoto? (Chat photo; may be null)
# @description : String (Group description. Updated only after the basic group is opened)
# @creator_user_id : Int64 (User identifier of the creator of the group; 0 if unknown)
# @members : Array(TD::ChatMember) (Group members)
# @invite_link : TD::ChatInviteLink? (Primary invite link for this group; may be null. For chat administrators with can_invite_users right only. Updated only after the basic group is opened)
# @bot_commands : Array(TD::BotCommands) (List of commands of bots in the group)

class TD::BasicGroupFullInfo < TD::Base
  include JSON::Serializable

  object_type "basicGroupFullInfo"

  object_attributes({
    photo: TD::ChatPhoto?,
    description: String,
    creator_user_id: Int64,
    members: Array(TD::ChatMember),
    invite_link: TD::ChatInviteLink?,
    bot_commands: Array(TD::BotCommands)
  })
end