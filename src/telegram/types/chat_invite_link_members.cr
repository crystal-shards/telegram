# Contains a list of chat members joined a chat via an invite link
#
# @total_count : Int32 (Approximate total count of chat members found)
# @members : Array(TD::ChatInviteLinkMember) (List of chat members, joined a chat via an invite link)

class TD::ChatInviteLinkMembers < TD::Base
  include JSON::Serializable

  object_type "chatInviteLinkMembers"

  object_attributes({
    total_count: Int32,
    members: Array(TD::ChatInviteLinkMember)
  })
end