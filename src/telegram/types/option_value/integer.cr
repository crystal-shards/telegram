# Represents an integer option
#
# @value : String (The value of the option)

class TD::OptionValue::Integer < TD::OptionValue
  include JSON::Serializable

  object_type "optionValueInteger"

  object_attributes({
    value: String
  })
end