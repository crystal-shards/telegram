# Represents an unknown option or an option which has a default value
#

class TD::OptionValue::Empty < TD::OptionValue
  include JSON::Serializable

  object_type "optionValueEmpty"
end