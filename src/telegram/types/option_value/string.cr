# Represents a string option
#
# @value : String (The value of the option)

class TD::OptionValue::String < TD::OptionValue
  include JSON::Serializable

  object_type "optionValueString"

  object_attributes({
    value: String
  })
end