# Represents a boolean option
#
# @value : Bool (The value of the option)

class TD::OptionValue::Boolean < TD::OptionValue
  include JSON::Serializable

  object_type "optionValueBoolean"

  object_attributes({
    value: Bool
  })
end