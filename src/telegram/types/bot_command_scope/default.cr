# A scope covering all users
#

class TD::BotCommandScope::Default < TD::BotCommandScope
  include JSON::Serializable

  object_type "botCommandScopeDefault"
end