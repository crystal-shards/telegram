# A scope covering all group and supergroup chats
#

class TD::BotCommandScope::AllGroupChats < TD::BotCommandScope
  include JSON::Serializable

  object_type "botCommandScopeAllGroupChats"
end