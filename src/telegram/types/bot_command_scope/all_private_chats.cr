# A scope covering all private chats
#

class TD::BotCommandScope::AllPrivateChats < TD::BotCommandScope
  include JSON::Serializable

  object_type "botCommandScopeAllPrivateChats"
end