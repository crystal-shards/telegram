# A scope covering a member of a chat
#
# @chat_id : Int64 (Chat identifier)
# @user_id : Int64 (User identifier)

class TD::BotCommandScope::ChatMember < TD::BotCommandScope
  include JSON::Serializable

  object_type "botCommandScopeChatMember"

  object_attributes({
    chat_id: Int64,
    user_id: Int64
  })
end