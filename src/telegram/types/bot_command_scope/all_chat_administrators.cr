# A scope covering all group and supergroup chat administrators
#

class TD::BotCommandScope::AllChatAdministrators < TD::BotCommandScope
  include JSON::Serializable

  object_type "botCommandScopeAllChatAdministrators"
end