# A scope covering all members of a chat
#
# @chat_id : Int64 (Chat identifier)

class TD::BotCommandScope::Chat < TD::BotCommandScope
  include JSON::Serializable

  object_type "botCommandScopeChat"

  object_attributes({
    chat_id: Int64
  })
end