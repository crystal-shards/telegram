# A scope covering all administrators of a chat
#
# @chat_id : Int64 (Chat identifier)

class TD::BotCommandScope::ChatAdministrators < TD::BotCommandScope
  include JSON::Serializable

  object_type "botCommandScopeChatAdministrators"

  object_attributes({
    chat_id: Int64
  })
end