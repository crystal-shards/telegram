# Represents a closed vector path. The path begins at the end point of the last command
#
# @commands : Array(TD::VectorPathCommand) (List of vector path commands)

class TD::ClosedVectorPath < TD::Base
  include JSON::Serializable

  object_type "closedVectorPath"

  object_attributes({
    commands: Array(TD::VectorPathCommand)
  })
end