# Describes a reason why an external chat is shown in a chat list
#
# MtprotoProxy: The chat is sponsored by the user's MTProxy server
# PublicServiceAnnouncement: The chat contains a public service announcement

abstract class TD::ChatSource < TD::Base
end