# Describes theme settings
#
# @accent_color : Int32 (Theme accent color in ARGB format)
# @background : TD::Background? (The background to be used in chats; may be null)
# @outgoing_message_fill : TD::BackgroundFill (The fill to be used as a background for outgoing messages)
# @animate_outgoing_message_fill : Bool (If true, the freeform gradient fill needs to be animated on every sent message)
# @outgoing_message_accent_color : Int32 (Accent color of outgoing messages in ARGB format)

class TD::ThemeSettings < TD::Base
  include JSON::Serializable

  object_type "themeSettings"

  object_attributes({
    accent_color: Int32,
    background: TD::Background?,
    outgoing_message_fill: TD::BackgroundFill,
    animate_outgoing_message_fill: Bool,
    outgoing_message_accent_color: Int32
  })
end