# A forwarded message
#
# @from_chat_id : Int64 (Identifier for the chat this forwarded message came from)
# @message_id : Int64 (Identifier of the message to forward)
# @in_game_share : Bool (True, if a game message is being shared from a launched game; applies only to game messages)
# @copy_options : TD::MessageCopyOptions (Options to be used to copy content of the message without reference to the original sender; pass null to forward the message as usual)

class TD::InputMessageContent::Forwarded < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageForwarded"

  object_attributes({
    from_chat_id: Int64,
    message_id: Int64,
    in_game_share: Bool,
    copy_options: TD::MessageCopyOptions
  })
end