# An audio message
#
# @audio : TD::InputFile (Audio file to be sent)
# @album_cover_thumbnail : TD::InputThumbnail (Thumbnail of the cover for the album; pass null to skip thumbnail uploading)
# @duration : Int32 (Duration of the audio, in seconds; may be replaced by the server)
# @title : String (Title of the audio; 0-64 characters; may be replaced by the server)
# @performer : String (Performer of the audio; 0-64 characters, may be replaced by the server)
# @caption : TD::FormattedText (Audio caption; pass null to use an empty caption; 0-GetOption("message_caption_length_max") characters)

class TD::InputMessageContent::Audio < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageAudio"

  object_attributes({
    audio: TD::InputFile,
    album_cover_thumbnail: TD::InputThumbnail,
    duration: Int32,
    title: String,
    performer: String,
    caption: TD::FormattedText
  })
end