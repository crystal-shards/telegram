# A document message (general file)
#
# @document : TD::InputFile (Document to be sent)
# @thumbnail : TD::InputThumbnail (Document thumbnail; pass null to skip thumbnail uploading)
# @disable_content_type_detection : Bool (If true, automatic file type detection will be disabled and the document will be always sent as file. Always true for files sent to secret chats)
# @caption : TD::FormattedText (Document caption; pass null to use an empty caption; 0-GetOption("message_caption_length_max") characters)

class TD::InputMessageContent::Document < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageDocument"

  object_attributes({
    document: TD::InputFile,
    thumbnail: TD::InputThumbnail,
    disable_content_type_detection: Bool,
    caption: TD::FormattedText
  })
end