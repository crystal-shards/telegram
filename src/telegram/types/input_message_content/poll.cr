# A message with a poll. Polls can't be sent to secret chats. Polls can be sent only to a private chat with a bot
#
# @question : String (Poll question; 1-255 characters (up to 300 characters for bots))
# @options : Array(String) (List of poll answer options, 2-10 strings 1-100 characters each)
# @is_anonymous : Bool (True, if the poll voters are anonymous. Non-anonymous polls can't be sent or forwarded to channels)
# @type : TD::PollType (Type of the poll)
# @open_period : Int32? (Amount of time the poll will be active after creation, in seconds; for bots only)
# @close_date : Int32? (Point in time (Unix timestamp) when the poll will automatically be closed; for bots only)
# @is_closed : Bool? (True, if the poll needs to be sent already closed; for bots only)

class TD::InputMessageContent::Poll < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessagePoll"

  object_attributes({
    question: String,
    options: Array(String),
    is_anonymous: Bool,
    type: TD::PollType,
    open_period: Int32?,
    close_date: Int32?,
    is_closed: Bool?
  })
end