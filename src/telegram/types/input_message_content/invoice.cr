# A message with an invoice; can be used only by bots
#
# @invoice : TD::Invoice (Invoice)
# @title : String (Product title; 1-32 characters)
# @description : String (Product description; 0-255 characters)
# @photo_url : String (Product photo URL; optional)
# @photo_size : Int32 (Product photo size)
# @photo_width : Int32 (Product photo width)
# @photo_height : Int32 (Product photo height)
# @payload : String (The invoice payload)
# @provider_token : String (Payment provider token)
# @provider_data : String (JSON-encoded data about the invoice, which will be shared with the payment provider)
# @start_parameter : String (Unique invoice bot deep link parameter for the generation of this invoice. If empty, it would be possible to pay directly from forwards of the invoice message)

class TD::InputMessageContent::Invoice < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageInvoice"

  object_attributes({
    invoice: TD::Invoice,
    title: String,
    description: String,
    photo_url: String,
    photo_size: Int32,
    photo_width: Int32,
    photo_height: Int32,
    payload: String,
    provider_token: String,
    provider_data: String,
    start_parameter: String
  })
end