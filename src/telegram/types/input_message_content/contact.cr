# A message containing a user contact
#
# @contact : TD::Contact (Contact to send)

class TD::InputMessageContent::Contact < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageContact"

  object_attributes({
    contact: TD::Contact
  })
end