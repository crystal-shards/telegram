# A sticker message
#
# @sticker : TD::InputFile (Sticker to be sent)
# @thumbnail : TD::InputThumbnail (Sticker thumbnail; pass null to skip thumbnail uploading)
# @width : Int32 (Sticker width)
# @height : Int32 (Sticker height)
# @emoji : String (Emoji used to choose the sticker)

class TD::InputMessageContent::Sticker < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageSticker"

  object_attributes({
    sticker: TD::InputFile,
    thumbnail: TD::InputThumbnail,
    width: Int32,
    height: Int32,
    emoji: String
  })
end