# A video message
#
# @video : TD::InputFile (Video to be sent)
# @thumbnail : TD::InputThumbnail (Video thumbnail; pass null to skip thumbnail uploading)
# @added_sticker_file_ids : Array(Int32)? (File identifiers of the stickers added to the video, if applicable)
# @duration : Int32 (Duration of the video, in seconds)
# @width : Int32 (Video width)
# @height : Int32 (Video height)
# @supports_streaming : Bool (True, if the video is supposed to be streamed)
# @caption : TD::FormattedText (Video caption; pass null to use an empty caption; 0-GetOption("message_caption_length_max") characters)
# @ttl : Int32 (Video TTL (Time To Live), in seconds (0-60). A non-zero TTL can be specified only in private chats)

class TD::InputMessageContent::Video < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageVideo"

  object_attributes({
    video: TD::InputFile,
    thumbnail: TD::InputThumbnail,
    added_sticker_file_ids: Array(Int32)?,
    duration: Int32,
    width: Int32,
    height: Int32,
    supports_streaming: Bool,
    caption: TD::FormattedText,
    ttl: Int32
  })
end