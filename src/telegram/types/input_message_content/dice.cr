# A dice message
#
# @emoji : String (Emoji on which the dice throw animation is based)
# @clear_draft : Bool (True, if the chat message draft must be deleted)

class TD::InputMessageContent::Dice < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageDice"

  object_attributes({
    emoji: String,
    clear_draft: Bool
  })
end