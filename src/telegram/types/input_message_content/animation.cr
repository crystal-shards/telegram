# An animation message (GIF-style).
#
# @animation : TD::InputFile (Animation file to be sent)
# @thumbnail : TD::InputThumbnail (Animation thumbnail; pass null to skip thumbnail uploading)
# @added_sticker_file_ids : Array(Int32)? (File identifiers of the stickers added to the animation, if applicable)
# @duration : Int32 (Duration of the animation, in seconds)
# @width : Int32 (Width of the animation; may be replaced by the server)
# @height : Int32 (Height of the animation; may be replaced by the server)
# @caption : TD::FormattedText (Animation caption; pass null to use an empty caption; 0-GetOption("message_caption_length_max") characters)

class TD::InputMessageContent::Animation < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageAnimation"

  object_attributes({
    animation: TD::InputFile,
    thumbnail: TD::InputThumbnail,
    added_sticker_file_ids: Array(Int32)?,
    duration: Int32,
    width: Int32,
    height: Int32,
    caption: TD::FormattedText
  })
end