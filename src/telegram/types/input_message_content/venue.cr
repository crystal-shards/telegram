# A message with information about a venue
#
# @venue : TD::Venue (Venue to send)

class TD::InputMessageContent::Venue < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageVenue"

  object_attributes({
    venue: TD::Venue
  })
end