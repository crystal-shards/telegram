# A video note message
#
# @video_note : TD::InputFile (Video note to be sent)
# @thumbnail : TD::InputThumbnail (Video thumbnail; pass null to skip thumbnail uploading)
# @duration : Int32 (Duration of the video, in seconds)
# @length : Int32 (Video width and height; must be positive and not greater than 640)

class TD::InputMessageContent::VideoNote < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageVideoNote"

  object_attributes({
    video_note: TD::InputFile,
    thumbnail: TD::InputThumbnail,
    duration: Int32,
    length: Int32
  })
end