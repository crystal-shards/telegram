# A voice note message
#
# @voice_note : TD::InputFile (Voice note to be sent)
# @duration : Int32 (Duration of the voice note, in seconds)
# @waveform : String (Waveform representation of the voice note, in 5-bit format)
# @caption : TD::FormattedText (Voice note caption; pass null to use an empty caption; 0-GetOption("message_caption_length_max") characters)

class TD::InputMessageContent::VoiceNote < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageVoiceNote"

  object_attributes({
    voice_note: TD::InputFile,
    duration: Int32,
    waveform: String,
    caption: TD::FormattedText
  })
end