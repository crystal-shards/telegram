# A message with a game; not supported for channels or secret chats
#
# @bot_user_id : Int64 (User identifier of the bot that owns the game)
# @game_short_name : String (Short name of the game)

class TD::InputMessageContent::Game < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageGame"

  object_attributes({
    bot_user_id: Int64,
    game_short_name: String
  })
end