# A message with a location
#
# @location : TD::Location (Location to be sent)
# @live_period : Int32 (Period for which the location can be updated, in seconds; must be between 60 and 86400 for a live location and 0 otherwise)
# @heading : Int32 (For live locations, a direction in which the location moves, in degrees; 1-360. Pass 0 if unknown)
# @proximity_alert_radius : Int32 (For live locations, a maximum distance to another chat member for proximity alerts, in meters (0-100000). Pass 0 if the notification is disabled. Can't be enabled in channels and Saved Messages)

class TD::InputMessageContent::Location < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageLocation"

  object_attributes({
    location: TD::Location,
    live_period: Int32,
    heading: Int32,
    proximity_alert_radius: Int32
  })
end