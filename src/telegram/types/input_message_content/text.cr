# A text message
#
# @text : TD::FormattedText (Formatted text to be sent; 1-GetOption("message_text_length_max") characters. Only Bold, Italic, Underline, Strikethrough, Code, Pre, PreCode, TextUrl and MentionName entities are allowed to be specified manually)
# @disable_web_page_preview : Bool (True, if rich web page previews for URLs in the message text must be disabled)
# @clear_draft : Bool (True, if a chat message draft must be deleted)

class TD::InputMessageContent::Text < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessageText"

  object_attributes({
    text: TD::FormattedText,
    disable_web_page_preview: Bool,
    clear_draft: Bool
  })
end