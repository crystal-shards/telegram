# A photo message
#
# @photo : TD::InputFile (Photo to send)
# @thumbnail : TD::InputThumbnail (Photo thumbnail to be sent; pass null to skip thumbnail uploading. The thumbnail is sent to the other party only in secret chats)
# @added_sticker_file_ids : Array(Int32)? (File identifiers of the stickers added to the photo, if applicable)
# @width : Int32 (Photo width)
# @height : Int32 (Photo height)
# @caption : TD::FormattedText (Photo caption; pass null to use an empty caption; 0-GetOption("message_caption_length_max") characters)
# @ttl : Int32 (Photo TTL (Time To Live), in seconds (0-60). A non-zero TTL can be specified only in private chats)

class TD::InputMessageContent::Photo < TD::InputMessageContent
  include JSON::Serializable

  object_type "inputMessagePhoto"

  object_attributes({
    photo: TD::InputFile,
    thumbnail: TD::InputThumbnail,
    added_sticker_file_ids: Array(Int32)?,
    width: Int32,
    height: Int32,
    caption: TD::FormattedText,
    ttl: Int32
  })
end