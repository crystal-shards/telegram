# Describes the current state of the connection to Telegram servers
#
# WaitingForNetwork: Currently waiting for the network to become available. Use setNetworkType to change the available network type
# ConnectingToProxy: Currently establishing a connection with a proxy server
# Connecting: Currently establishing a connection to the Telegram servers
# Updating: Downloading data received while the application was offline
# Ready: There is a working connection to the Telegram servers

abstract class TD::ConnectionState < TD::Base
end