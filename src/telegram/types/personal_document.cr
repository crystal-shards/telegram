# A personal document, containing some information about a user
#
# @files : Array(TD::DatedFile) (List of files containing the pages of the document)
# @translation : Array(TD::DatedFile) (List of files containing a certified English translation of the document)

class TD::PersonalDocument < TD::Base
  include JSON::Serializable

  object_type "personalDocument"

  object_attributes({
    files: Array(TD::DatedFile),
    translation: Array(TD::DatedFile)
  })
end