# Describes a keyboard button type
#
# Text: A simple button, with text that must be sent when the button is pressed
# RequestPhoneNumber: A button that sends the user's phone number when pressed; available only in private chats
# RequestLocation: A button that sends the user's location when pressed; available only in private chats
# RequestPoll: A button that allows the user to create and send a poll when pressed; available only in private chats

abstract class TD::KeyboardButtonType < TD::Base
end