# Contains a list of messages found by a search
#
# @total_count : Int32 (Approximate total count of messages found; -1 if unknown)
# @messages : Array(TD::Message) (List of messages)
# @next_offset : String (The offset for the next request. If empty, there are no more results)

class TD::FoundMessages < TD::Base
  include JSON::Serializable

  object_type "foundMessages"

  object_attributes({
    total_count: Int32,
    messages: Array(TD::Message),
    next_offset: String
  })
end