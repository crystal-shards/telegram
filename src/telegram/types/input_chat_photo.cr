# Describes a photo to be set as a user profile or chat photo
#
# Previous: A previously used profile photo of the current user
# Static: A static photo in JPEG format
# Animation: An animation in MPEG4 format; must be square, at most 10 seconds long, have width between 160 and 800 and be at most 2MB in size

abstract class TD::InputChatPhoto < TD::Base
end