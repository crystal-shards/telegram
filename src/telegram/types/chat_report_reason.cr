# Describes the reason why a chat is reported
#
# Spam: The chat contains spam messages
# Violence: The chat promotes violence
# Pornography: The chat contains pornographic messages
# ChildAbuse: The chat has child abuse related content
# Copyright: The chat contains copyrighted content
# UnrelatedLocation: The location-based chat is unrelated to its stated location
# Fake: The chat represents a fake account
# Custom: A custom reason provided by the user

abstract class TD::ChatReportReason < TD::Base
end