# Contains information about a phone number
#
# @country : TD::CountryInfo? (Information about the country to which the phone number belongs; may be null)
# @country_calling_code : String (The part of the phone number denoting country calling code or its part)
# @formatted_phone_number : String (The phone number without country calling code formatted accordingly to local rules. Expected digits are returned as '-', but even more digits might be entered by the user)

class TD::PhoneNumberInfo < TD::Base
  include JSON::Serializable

  object_type "phoneNumberInfo"

  object_attributes({
    country: TD::CountryInfo?,
    country_calling_code: String,
    formatted_phone_number: String
  })
end