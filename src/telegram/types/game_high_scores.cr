# Contains a list of game high scores
#
# @scores : Array(TD::GameHighScore) (A list of game high scores)

class TD::GameHighScores < TD::Base
  include JSON::Serializable

  object_type "gameHighScores"

  object_attributes({
    scores: Array(TD::GameHighScore)
  })
end