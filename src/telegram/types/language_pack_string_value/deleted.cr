# A deleted language pack string, the value must be taken from the built-in English language pack
#

class TD::LanguagePackStringValue::Deleted < TD::LanguagePackStringValue
  include JSON::Serializable

  object_type "languagePackStringValueDeleted"
end