# An ordinary language pack string
#
# @value : String (String value)

class TD::LanguagePackStringValue::Ordinary < TD::LanguagePackStringValue
  include JSON::Serializable

  object_type "languagePackStringValueOrdinary"

  object_attributes({
    value: String
  })
end