# A language pack string which has different forms based on the number of some object it mentions. See https:
#
# @zero_value : String (Value for zero objects)
# @one_value : String (Value for one object)
# @two_value : String (Value for two objects)
# @few_value : String (Value for few objects)
# @many_value : String (Value for many objects)
# @other_value : String (Default value)

class TD::LanguagePackStringValue::Pluralized < TD::LanguagePackStringValue
  include JSON::Serializable

  object_type "languagePackStringValuePluralized"

  object_attributes({
    zero_value: String,
    one_value: String,
    two_value: String,
    few_value: String,
    many_value: String,
    other_value: String
  })
end