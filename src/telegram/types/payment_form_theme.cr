# Theme colors for a payment form
#
# @background_color : Int32 (A color of the payment form background in the RGB24 format)
# @text_color : Int32 (A color of text in the RGB24 format)
# @hint_color : Int32 (A color of hints in the RGB24 format)
# @link_color : Int32 (A color of links in the RGB24 format)
# @button_color : Int32 (A color of the buttons in the RGB24 format)
# @button_text_color : Int32 (A color of text on the buttons in the RGB24 format)

class TD::PaymentFormTheme < TD::Base
  include JSON::Serializable

  object_type "paymentFormTheme"

  object_attributes({
    background_color: Int32,
    text_color: Int32,
    hint_color: Int32,
    link_color: Int32,
    button_color: Int32,
    button_text_color: Int32
  })
end