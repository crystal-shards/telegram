# Contains a bot's answer to a callback query
#
# @text : String (Text of the answer)
# @show_alert : Bool (True, if an alert must be shown to the user instead of a toast notification)
# @url : String (URL to be opened)

class TD::CallbackQueryAnswer < TD::Base
  include JSON::Serializable

  object_type "callbackQueryAnswer"

  object_attributes({
    text: String,
    show_alert: Bool,
    url: String
  })
end