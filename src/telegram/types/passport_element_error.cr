# Contains the description of an error in a Telegram Passport element
#
# @type : TD::PassportElementType (Type of the Telegram Passport element which has the error)
# @message : String (Error message)
# @source : TD::PassportElementErrorSource (Error source)

class TD::PassportElementError < TD::Base
  include JSON::Serializable

  object_type "passportElementError"

  object_attributes({
    type: TD::PassportElementType,
    message: String,
    source: TD::PassportElementErrorSource
  })
end