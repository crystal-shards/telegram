# Contains the storage usage statistics for a specific file type
#
# @file_type : TD::FileType (File type)
# @size : Int64 (Total size of the files, in bytes)
# @count : Int32 (Total number of files)

class TD::StorageStatisticsByFileType < TD::Base
  include JSON::Serializable

  object_type "storageStatisticsByFileType"

  object_attributes({
    file_type: TD::FileType,
    size: Int64,
    count: Int32
  })
end