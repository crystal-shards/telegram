# A simple object containing a vector of strings; for testing only
#
# @value : Array(String) (Vector of strings)

class TD::TestVectorString < TD::Base
  include JSON::Serializable

  object_type "testVectorString"

  object_attributes({
    value: Array(String)
  })
end