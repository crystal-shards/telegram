# Describes the exact type of a problem with a call
#
# Echo: The user heard their own voice
# Noise: The user heard background noise
# Interruptions: The other side kept disappearing
# DistortedSpeech: The speech was distorted
# SilentLocal: The user couldn't hear the other side
# SilentRemote: The other side couldn't hear the user
# Dropped: The call ended unexpectedly
# DistortedVideo: The video was distorted
# PixelatedVideo: The video was pixelated

abstract class TD::CallProblem < TD::Base
end