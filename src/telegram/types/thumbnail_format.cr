# Describes format of the thumbnail
#
# Jpeg: The thumbnail is in JPEG format
# Png: The thumbnail is in PNG format. It will be used only for background patterns
# Webp: The thumbnail is in WEBP format. It will be used only for some stickers
# Gif: The thumbnail is in static GIF format. It will be used only for some bot inline results
# Tgs: The thumbnail is in TGS format. It will be used only for animated sticker sets
# Mpeg4: The thumbnail is in MPEG4 format. It will be used only for some animations and videos

abstract class TD::ThumbnailFormat < TD::Base
end