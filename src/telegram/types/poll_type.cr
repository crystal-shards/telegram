# Describes the type of a poll
#
# Regular: A regular poll
# Quiz: A poll in quiz mode, which has exactly one correct answer option and can be answered only once

abstract class TD::PollType < TD::Base
end