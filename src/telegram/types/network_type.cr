# Represents the type of a network
#
# None: The network is not available
# Mobile: A mobile network
# MobileRoaming: A mobile roaming network
# WiFi: A Wi-Fi network
# Other: A different network type (e.g., Ethernet network)

abstract class TD::NetworkType < TD::Base
end