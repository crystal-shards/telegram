# Represents the type of a file
#
# None: The data is not a file
# Animation: The file is an animation
# Audio: The file is an audio file
# Document: The file is a document
# Photo: The file is a photo
# ProfilePhoto: The file is a profile photo
# Secret: The file was sent to a secret chat (the file type is not known to the server)
# SecretThumbnail: The file is a thumbnail of a file from a secret chat
# Secure: The file is a file from Secure storage used for storing Telegram Passport files
# Sticker: The file is a sticker
# Thumbnail: The file is a thumbnail of another file
# Unknown: The file type is not yet known
# Video: The file is a video
# VideoNote: The file is a video note
# VoiceNote: The file is a voice note
# Wallpaper: The file is a wallpaper or a background pattern

abstract class TD::FileType < TD::Base
end