# An authorization confirmation dialog needs to be shown to the user
#
# @url : String (An HTTP URL to be opened)
# @domain : String (A domain of the URL)
# @bot_user_id : Int64 (User identifier of a bot linked with the website)
# @request_write_access : Bool (True, if the user needs to be requested to give the permission to the bot to send them messages)

class TD::LoginUrlInfo::RequestConfirmation < TD::LoginUrlInfo
  include JSON::Serializable

  object_type "loginUrlInfoRequestConfirmation"

  object_attributes({
    url: String,
    domain: String,
    bot_user_id: Int64,
    request_write_access: Bool
  })
end