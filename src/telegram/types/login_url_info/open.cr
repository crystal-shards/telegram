# An HTTP url needs to be open
#
# @url : String (The URL to open)
# @skip_confirm : Bool (True, if there is no need to show an ordinary open URL confirm)

class TD::LoginUrlInfo::Open < TD::LoginUrlInfo
  include JSON::Serializable

  object_type "loginUrlInfoOpen"

  object_attributes({
    url: String,
    skip_confirm: Bool
  })
end