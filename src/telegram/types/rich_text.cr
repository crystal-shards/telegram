# Describes a text object inside an instant-view web page
#
# Plain: A plain text
# Bold: A bold rich text
# Italic: An italicized rich text
# Underline: An underlined rich text
# Strikethrough: A strikethrough rich text
# Fixed: A fixed-width rich text
# Url: A rich text URL link
# EmailAddress: A rich text email link
# Subscript: A subscript rich text
# Superscript: A superscript rich text
# Marked: A marked rich text
# PhoneNumber: A rich text phone number
# Icon: A small image inside the text
# Reference: A reference to a richTexts object on the same web page
# Anchor: An anchor
# AnchorLink: A link to an anchor on the same web page
# S: A concatenation of rich texts

abstract class TD::RichText < TD::Base
end