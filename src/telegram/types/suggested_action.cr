# Describes an action suggested to the current user
#
# EnableArchiveAndMuteNewChats: Suggests the user to enable "archive_and_mute_new_chats_from_unknown_users" option
# CheckPassword: Suggests the user to check whether they still remember their 2-step verification password
# CheckPhoneNumber: Suggests the user to check whether authorization phone number is correct and change the phone number if it is inaccessible
# ViewChecksHint: Suggests the user to view a hint about the meaning of one and two check marks on sent messages
# ConvertToBroadcastGroup: Suggests the user to convert specified supergroup to a broadcast group
# SetPassword: Suggests the user to set a 2-step verification password to be able to log in again

abstract class TD::SuggestedAction < TD::Base
end