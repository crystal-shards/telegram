# Describes an item of a list page block
#
# @label : String (Item label)
# @page_blocks : Array(TD::PageBlock) (Item blocks)

class TD::PageBlockListItem < TD::Base
  include JSON::Serializable

  object_type "pageBlockListItem"

  object_attributes({
    label: String,
    page_blocks: Array(TD::PageBlock)
  })
end