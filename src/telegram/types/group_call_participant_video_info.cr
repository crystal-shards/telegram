# Contains information about a group call participant's video channel
#
# @source_groups : Array(TD::GroupCallVideoSourceGroup) (List of synchronization source groups of the video)
# @endpoint_id : String (Video channel endpoint identifier)
# @is_paused : Bool (True if the video is paused. This flag needs to be ignored, if new video frames are received)

class TD::GroupCallParticipantVideoInfo < TD::Base
  include JSON::Serializable

  object_type "groupCallParticipantVideoInfo"

  object_attributes({
    source_groups: Array(TD::GroupCallVideoSourceGroup),
    endpoint_id: String,
    is_paused: Bool
  })
end