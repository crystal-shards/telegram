# Represents a secret chat
#
# @id : Int32 (Secret chat identifier)
# @user_id : Int64 (Identifier of the chat partner)
# @state : TD::SecretChatState (State of the secret chat)
# @is_outbound : Bool (True, if the chat was created by the current user; otherwise false)
# @key_hash : String (Hash of the currently used key for comparison with the hash of the chat partner's key. This is a string of 36 little-endian bytes, which must be split into groups of 2 bits, each denoting a pixel of one of 4 colors FFFFFF, D5E6F3, 2D5775, and 2F99C9.The pixels must be used to make a 12x12 square image filled from left to right, top to bottom. Alternatively, the first 32 bytes of the hash can be converted to the hexadecimal format and printed as 32 2-digit hex numbers)
# @layer : Int32 (Secret chat layer; determines features supported by the chat partner's application. Nested text entities and underline and strikethrough entities are supported if the layer)

class TD::SecretChat < TD::Base
  include JSON::Serializable

  object_type "secretChat"

  object_attributes({
    id: Int32,
    user_id: Int64,
    state: TD::SecretChatState,
    is_outbound: Bool,
    key_hash: String,
    layer: Int32
  })
end