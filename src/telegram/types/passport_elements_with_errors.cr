# Contains information about a Telegram Passport elements and corresponding errors
#
# @elements : Array(TD::PassportElement) (Telegram Passport elements)
# @errors : Array(TD::PassportElementError) (Errors in the elements that are already available)

class TD::PassportElementsWithErrors < TD::Base
  include JSON::Serializable

  object_type "passportElementsWithErrors"

  object_attributes({
    elements: Array(TD::PassportElement),
    errors: Array(TD::PassportElementError)
  })
end