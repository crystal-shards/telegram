# Describes actions which must be possible to do through a chat action bar
#
# ReportSpam: The chat can be reported as spam using the method reportChat with the reason chatReportReasonSpam
# ReportUnrelatedLocation: The chat is a location-based supergroup, which can be reported as having unrelated location using the method reportChat with the reason chatReportReasonUnrelatedLocation
# InviteMembers: The chat is a recently created group chat to which new members can be invited
# ReportAddBlock: The chat is a private or secret chat, which can be reported using the method reportChat, or the other user can be blocked using the method toggleMessageSenderIsBlocked, or the other user can be added to the contact list using the method addContact
# AddContact: The chat is a private or secret chat and the other user can be added to the contact list using the method addContact
# SharePhoneNumber: The chat is a private or secret chat with a mutual contact and the user's phone number can be shared with the other user using the method sharePhoneNumber
# JoinRequest: The chat is a private chat with an administrator of a chat to which the user sent join request

abstract class TD::ChatActionBar < TD::Base
end