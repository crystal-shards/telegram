# Describes a location on planet Earth
#
# @latitude : Float64 (Latitude of the location in degrees; as defined by the sender)
# @longitude : Float64 (Longitude of the location, in degrees; as defined by the sender)
# @horizontal_accuracy : Float64 (The estimated horizontal accuracy of the location, in meters; as defined by the sender. 0 if unknown)

class TD::Location < TD::Base
  include JSON::Serializable

  object_type "location"

  object_attributes({
    latitude: Float64,
    longitude: Float64,
    horizontal_accuracy: Float64
  })
end