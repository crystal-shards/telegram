# Product invoice
#
# @currency : String (ISO 4217 currency code)
# @price_parts : Array(TD::LabeledPricePart) (A list of objects used to calculate the total price of the product)
# @max_tip_amount : Int64 (The maximum allowed amount of tip in the smallest units of the currency)
# @suggested_tip_amounts : Array(Int64) (Suggested amounts of tip in the smallest units of the currency)
# @is_test : Bool (True, if the payment is a test payment)
# @need_name : Bool (True, if the user's name is needed for payment)
# @need_phone_number : Bool (True, if the user's phone number is needed for payment)
# @need_email_address : Bool (True, if the user's email address is needed for payment)
# @need_shipping_address : Bool (True, if the user's shipping address is needed for payment)
# @send_phone_number_to_provider : Bool (True, if the user's phone number will be sent to the provider)
# @send_email_address_to_provider : Bool (True, if the user's email address will be sent to the provider)
# @is_flexible : Bool (True, if the total price depends on the shipping method)

class TD::Invoice < TD::Base
  include JSON::Serializable

  object_type "invoice"

  object_attributes({
    currency: String,
    price_parts: Array(TD::LabeledPricePart),
    max_tip_amount: Int64,
    suggested_tip_amounts: Array(Int64),
    is_test: Bool,
    need_name: Bool,
    need_phone_number: Bool,
    need_email_address: Bool,
    need_shipping_address: Bool,
    send_phone_number_to_provider: Bool,
    send_email_address_to_provider: Bool,
    is_flexible: Bool
  })
end