# Contains a list of language pack strings
#
# @strings : Array(TD::LanguagePackString) (A list of language pack strings)

class TD::LanguagePackStrings < TD::Base
  include JSON::Serializable

  object_type "languagePackStrings"

  object_attributes({
    strings: Array(TD::LanguagePackString)
  })
end