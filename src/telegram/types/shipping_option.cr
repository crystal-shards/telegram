# One shipping option
#
# @id : String (Shipping option identifier)
# @title : String (Option title)
# @price_parts : Array(TD::LabeledPricePart) (A list of objects used to calculate the total shipping costs)

class TD::ShippingOption < TD::Base
  include JSON::Serializable

  object_type "shippingOption"

  object_attributes({
    id: String,
    title: String,
    price_parts: Array(TD::LabeledPricePart)
  })
end