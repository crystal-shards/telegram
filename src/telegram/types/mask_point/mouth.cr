# The mask is placed relatively to the mouth
#

class TD::MaskPoint::Mouth < TD::MaskPoint
  include JSON::Serializable

  object_type "maskPointMouth"
end