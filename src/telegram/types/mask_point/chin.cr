# The mask is placed relatively to the chin
#

class TD::MaskPoint::Chin < TD::MaskPoint
  include JSON::Serializable

  object_type "maskPointChin"
end