# The mask is placed relatively to the eyes
#

class TD::MaskPoint::Eyes < TD::MaskPoint
  include JSON::Serializable

  object_type "maskPointEyes"
end