# The mask is placed relatively to the forehead
#

class TD::MaskPoint::Forehead < TD::MaskPoint
  include JSON::Serializable

  object_type "maskPointForehead"
end