# Contains the result of a payment request
#
# @success : Bool (True, if the payment request was successful; otherwise the verification_url will be non-empty)
# @verification_url : String (URL for additional payment credentials verification)

class TD::PaymentResult < TD::Base
  include JSON::Serializable

  object_type "paymentResult"

  object_attributes({
    success: Bool,
    verification_url: String
  })
end