# Describes the types of chats to which notification settings are relevant
#
# PrivateChats: Notification settings applied to all private and secret chats when the corresponding chat setting has a default value
# GroupChats: Notification settings applied to all basic groups and supergroups when the corresponding chat setting has a default value
# ChannelChats: Notification settings applied to all channels when the corresponding chat setting has a default value

abstract class TD::NotificationSettingsScope < TD::Base
end