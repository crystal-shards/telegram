# Contains a chat invite link
#
# @invite_link : String (Chat invite link)
# @name : String (Name of the link)
# @creator_user_id : Int64 (User identifier of an administrator created the link)
# @date : Int32 (Point in time (Unix timestamp) when the link was created)
# @edit_date : Int32 (Point in time (Unix timestamp) when the link was last edited; 0 if never or unknown)
# @expiration_date : Int32 (Point in time (Unix timestamp) when the link will expire; 0 if never)
# @member_limit : Int32 (The maximum number of members, which can join the chat using the link simultaneously; 0 if not limited. Always 0 if the link requires approval)
# @member_count : Int32 (Number of chat members, which joined the chat using the link)
# @pending_join_request_count : Int32 (Number of pending join requests created using this link)
# @creates_join_request : Bool (True, if the link only creates join request. If true, total number of joining members will be unlimited)
# @is_primary : Bool (True, if the link is primary. Primary invite link can't have name, expiration date, or usage limit. There is exactly one primary invite link for each administrator with can_invite_users right at a given time)
# @is_revoked : Bool (True, if the link was revoked)

class TD::ChatInviteLink < TD::Base
  include JSON::Serializable

  object_type "chatInviteLink"

  object_attributes({
    invite_link: String,
    name: String,
    creator_user_id: Int64,
    date: Int32,
    edit_date: Int32,
    expiration_date: Int32,
    member_limit: Int32,
    member_count: Int32,
    pending_join_request_count: Int32,
    creates_join_request: Bool,
    is_primary: Bool,
    is_revoked: Bool
  })
end