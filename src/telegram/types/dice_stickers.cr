# Contains animated stickers which must be used for dice animation rendering
#
# Regular: A regular animated sticker
# SlotMachine: Animated stickers to be combined into a slot machine

abstract class TD::DiceStickers < TD::Base
end