# A video chat participant volume level was changed
#
# @participant_id : TD::MessageSender (Identifier of the affected group call participant)
# @volume_level : Int32 (New value of volume_level; 1-20000 in hundreds of percents)

class TD::ChatEventAction::VideoChatParticipantVolumeLevelChanged < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventVideoChatParticipantVolumeLevelChanged"

  object_attributes({
    participant_id: TD::MessageSender,
    volume_level: Int32
  })
end