# The mute_new_participants setting of a video chat was toggled
#
# @mute_new_participants : Bool (New value of the mute_new_participants setting)

class TD::ChatEventAction::VideoChatMuteNewParticipantsToggled < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventVideoChatMuteNewParticipantsToggled"

  object_attributes({
    mute_new_participants: Bool
  })
end