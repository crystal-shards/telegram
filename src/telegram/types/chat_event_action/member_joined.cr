# A new member joined the chat
#

class TD::ChatEventAction::MemberJoined < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventMemberJoined"
end