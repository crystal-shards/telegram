# The chat description was changed
#
# @old_description : String (Previous chat description)
# @new_description : String (New chat description)

class TD::ChatEventAction::DescriptionChanged < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventDescriptionChanged"

  object_attributes({
    old_description: String,
    new_description: String
  })
end