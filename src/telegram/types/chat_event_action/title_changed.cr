# The chat title was changed
#
# @old_title : String (Previous chat title)
# @new_title : String (New chat title)

class TD::ChatEventAction::TitleChanged < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventTitleChanged"

  object_attributes({
    old_title: String,
    new_title: String
  })
end