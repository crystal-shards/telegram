# A video chat participant was muted or unmuted
#
# @participant_id : TD::MessageSender (Identifier of the affected group call participant)
# @is_muted : Bool (New value of is_muted)

class TD::ChatEventAction::VideoChatParticipantIsMutedToggled < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventVideoChatParticipantIsMutedToggled"

  object_attributes({
    participant_id: TD::MessageSender,
    is_muted: Bool
  })
end