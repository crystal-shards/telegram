# A new chat member was invited
#
# @user_id : Int64 (New member user identifier)
# @status : TD::ChatMemberStatus (New member status)

class TD::ChatEventAction::MemberInvited < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventMemberInvited"

  object_attributes({
    user_id: Int64,
    status: TD::ChatMemberStatus
  })
end