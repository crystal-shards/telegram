# The chat photo was changed
#
# @old_photo : TD::ChatPhoto? (Previous chat photo value; may be null)
# @new_photo : TD::ChatPhoto? (New chat photo value; may be null)

class TD::ChatEventAction::PhotoChanged < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventPhotoChanged"

  object_attributes({
    old_photo: TD::ChatPhoto?,
    new_photo: TD::ChatPhoto?
  })
end