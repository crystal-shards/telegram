# A video chat was created
#
# @group_call_id : Int32 (Identifier of the video chat. The video chat can be received through the method getGroupCall)

class TD::ChatEventAction::VideoChatCreated < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventVideoChatCreated"

  object_attributes({
    group_call_id: Int32
  })
end