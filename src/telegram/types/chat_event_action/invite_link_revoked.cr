# A chat invite link was revoked
#
# @invite_link : TD::ChatInviteLink (The invite link)

class TD::ChatEventAction::InviteLinkRevoked < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventInviteLinkRevoked"

  object_attributes({
    invite_link: TD::ChatInviteLink
  })
end