# The can_invite_users permission of a supergroup chat was toggled
#
# @can_invite_users : Bool (New value of can_invite_users permission)

class TD::ChatEventAction::InvitesToggled < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventInvitesToggled"

  object_attributes({
    can_invite_users: Bool
  })
end