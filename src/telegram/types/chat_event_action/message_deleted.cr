# A message was deleted
#
# @message : TD::Message (Deleted message)

class TD::ChatEventAction::MessageDeleted < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventMessageDeleted"

  object_attributes({
    message: TD::Message
  })
end