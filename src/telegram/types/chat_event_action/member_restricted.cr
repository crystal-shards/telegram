# A chat member was restricted
#
# @member_id : TD::MessageSender (Affected chat member identifier)
# @old_status : TD::ChatMemberStatus (Previous status of the chat member)
# @new_status : TD::ChatMemberStatus (New status of the chat member)

class TD::ChatEventAction::MemberRestricted < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventMemberRestricted"

  object_attributes({
    member_id: TD::MessageSender,
    old_status: TD::ChatMemberStatus,
    new_status: TD::ChatMemberStatus
  })
end