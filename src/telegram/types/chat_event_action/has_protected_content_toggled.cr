# The has_protected_content setting of a channel was toggled
#
# @has_protected_content : Bool (New value of has_protected_content)

class TD::ChatEventAction::HasProtectedContentToggled < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventHasProtectedContentToggled"

  object_attributes({
    has_protected_content: Bool
  })
end