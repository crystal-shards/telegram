# A message was unpinned
#
# @message : TD::Message (Unpinned message)

class TD::ChatEventAction::MessageUnpinned < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventMessageUnpinned"

  object_attributes({
    message: TD::Message
  })
end