# The chat username was changed
#
# @old_username : String (Previous chat username)
# @new_username : String (New chat username)

class TD::ChatEventAction::UsernameChanged < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventUsernameChanged"

  object_attributes({
    old_username: String,
    new_username: String
  })
end