# A member left the chat
#

class TD::ChatEventAction::MemberLeft < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventMemberLeft"
end