# A chat member has gained
#
# @user_id : Int64 (Affected chat member user identifier)
# @old_status : TD::ChatMemberStatus (Previous status of the chat member)
# @new_status : TD::ChatMemberStatus (New status of the chat member)

class TD::ChatEventAction::MemberPromoted < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventMemberPromoted"

  object_attributes({
    user_id: Int64,
    old_status: TD::ChatMemberStatus,
    new_status: TD::ChatMemberStatus
  })
end