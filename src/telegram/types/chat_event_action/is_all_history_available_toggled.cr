# The is_all_history_available setting of a supergroup was toggled
#
# @is_all_history_available : Bool (New value of is_all_history_available)

class TD::ChatEventAction::IsAllHistoryAvailableToggled < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventIsAllHistoryAvailableToggled"

  object_attributes({
    is_all_history_available: Bool
  })
end