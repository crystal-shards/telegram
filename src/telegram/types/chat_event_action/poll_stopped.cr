# A poll in a message was stopped
#
# @message : TD::Message (The message with the poll)

class TD::ChatEventAction::PollStopped < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventPollStopped"

  object_attributes({
    message: TD::Message
  })
end