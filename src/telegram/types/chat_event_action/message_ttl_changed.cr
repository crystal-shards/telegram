# The message TTL was changed
#
# @old_message_ttl : Int32 (Previous value of message_ttl)
# @new_message_ttl : Int32 (New value of message_ttl)

class TD::ChatEventAction::MessageTtlChanged < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventMessageTtlChanged"

  object_attributes({
    old_message_ttl: Int32,
    new_message_ttl: Int32
  })
end