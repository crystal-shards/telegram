# The supergroup location was changed
#
# @old_location : TD::ChatLocation? (Previous location; may be null)
# @new_location : TD::ChatLocation? (New location; may be null)

class TD::ChatEventAction::LocationChanged < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventLocationChanged"

  object_attributes({
    old_location: TD::ChatLocation?,
    new_location: TD::ChatLocation?
  })
end