# The linked chat of a supergroup was changed
#
# @old_linked_chat_id : Int64 (Previous supergroup linked chat identifier)
# @new_linked_chat_id : Int64 (New supergroup linked chat identifier)

class TD::ChatEventAction::LinkedChatChanged < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventLinkedChatChanged"

  object_attributes({
    old_linked_chat_id: Int64,
    new_linked_chat_id: Int64
  })
end