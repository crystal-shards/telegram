# The sign_messages setting of a channel was toggled
#
# @sign_messages : Bool (New value of sign_messages)

class TD::ChatEventAction::SignMessagesToggled < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventSignMessagesToggled"

  object_attributes({
    sign_messages: Bool
  })
end