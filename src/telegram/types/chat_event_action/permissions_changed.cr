# The chat permissions was changed
#
# @old_permissions : TD::ChatPermissions (Previous chat permissions)
# @new_permissions : TD::ChatPermissions (New chat permissions)

class TD::ChatEventAction::PermissionsChanged < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventPermissionsChanged"

  object_attributes({
    old_permissions: TD::ChatPermissions,
    new_permissions: TD::ChatPermissions
  })
end