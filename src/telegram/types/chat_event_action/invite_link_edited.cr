# A chat invite link was edited
#
# @old_invite_link : TD::ChatInviteLink (Previous information about the invite link)
# @new_invite_link : TD::ChatInviteLink (New information about the invite link)

class TD::ChatEventAction::InviteLinkEdited < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventInviteLinkEdited"

  object_attributes({
    old_invite_link: TD::ChatInviteLink,
    new_invite_link: TD::ChatInviteLink
  })
end