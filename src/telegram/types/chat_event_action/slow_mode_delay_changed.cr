# The slow_mode_delay setting of a supergroup was changed
#
# @old_slow_mode_delay : Int32 (Previous value of slow_mode_delay, in seconds)
# @new_slow_mode_delay : Int32 (New value of slow_mode_delay, in seconds)

class TD::ChatEventAction::SlowModeDelayChanged < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventSlowModeDelayChanged"

  object_attributes({
    old_slow_mode_delay: Int32,
    new_slow_mode_delay: Int32
  })
end