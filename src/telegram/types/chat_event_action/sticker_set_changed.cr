# The supergroup sticker set was changed
#
# @old_sticker_set_id : String (Previous identifier of the chat sticker set; 0 if none)
# @new_sticker_set_id : String (New identifier of the chat sticker set; 0 if none)

class TD::ChatEventAction::StickerSetChanged < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventStickerSetChanged"

  object_attributes({
    old_sticker_set_id: String,
    new_sticker_set_id: String
  })
end