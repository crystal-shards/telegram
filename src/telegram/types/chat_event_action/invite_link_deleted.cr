# A revoked chat invite link was deleted
#
# @invite_link : TD::ChatInviteLink (The invite link)

class TD::ChatEventAction::InviteLinkDeleted < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventInviteLinkDeleted"

  object_attributes({
    invite_link: TD::ChatInviteLink
  })
end