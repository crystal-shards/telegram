# A new member was accepted to the chat by an administrator
#
# @approver_user_id : Int64 (User identifier of the chat administrator, approved user join request)
# @invite_link : TD::ChatInviteLink? (Invite link used to join the chat; may be null)

class TD::ChatEventAction::MemberJoinedByRequest < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventMemberJoinedByRequest"

  object_attributes({
    approver_user_id: Int64,
    invite_link: TD::ChatInviteLink?
  })
end