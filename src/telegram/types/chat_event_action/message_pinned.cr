# A message was pinned
#
# @message : TD::Message (Pinned message)

class TD::ChatEventAction::MessagePinned < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventMessagePinned"

  object_attributes({
    message: TD::Message
  })
end