# A video chat was ended
#
# @group_call_id : Int32 (Identifier of the video chat. The video chat can be received through the method getGroupCall)

class TD::ChatEventAction::VideoChatEnded < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventVideoChatEnded"

  object_attributes({
    group_call_id: Int32
  })
end