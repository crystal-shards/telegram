# A new member joined the chat via an invite link
#
# @invite_link : TD::ChatInviteLink (Invite link used to join the chat)

class TD::ChatEventAction::MemberJoinedByInviteLink < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventMemberJoinedByInviteLink"

  object_attributes({
    invite_link: TD::ChatInviteLink
  })
end