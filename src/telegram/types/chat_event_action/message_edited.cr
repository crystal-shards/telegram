# A message was edited
#
# @old_message : TD::Message (The original message before the edit)
# @new_message : TD::Message (The message after it was edited)

class TD::ChatEventAction::MessageEdited < TD::ChatEventAction
  include JSON::Serializable

  object_type "chatEventMessageEdited"

  object_attributes({
    old_message: TD::Message,
    new_message: TD::Message
  })
end