# Contains information about notification settings for a chat
#
# @use_default_mute_for : Bool (If true, mute_for is ignored and the value for the relevant type of chat is used instead)
# @mute_for : Int32 (Time left before notifications will be unmuted, in seconds)
# @use_default_sound : Bool (If true, sound is ignored and the value for the relevant type of chat is used instead)
# @sound : String (The name of an audio file to be used for notification sounds; only applies to iOS applications)
# @use_default_show_preview : Bool (If true, show_preview is ignored and the value for the relevant type of chat is used instead)
# @show_preview : Bool (True, if message content must be displayed in notifications)
# @use_default_disable_pinned_message_notifications : Bool (If true, disable_pinned_message_notifications is ignored and the value for the relevant type of chat is used instead)
# @disable_pinned_message_notifications : Bool (If true, notifications for incoming pinned messages will be created as for an ordinary unread message)
# @use_default_disable_mention_notifications : Bool (If true, disable_mention_notifications is ignored and the value for the relevant type of chat is used instead)
# @disable_mention_notifications : Bool (If true, notifications for messages with mentions will be created as for an ordinary unread message)

class TD::ChatNotificationSettings < TD::Base
  include JSON::Serializable

  object_type "chatNotificationSettings"

  object_attributes({
    use_default_mute_for: Bool,
    mute_for: Int32,
    use_default_sound: Bool,
    sound: String,
    use_default_show_preview: Bool,
    show_preview: Bool,
    use_default_disable_pinned_message_notifications: Bool,
    disable_pinned_message_notifications: Bool,
    use_default_disable_mention_notifications: Bool,
    disable_mention_notifications: Bool
  })
end