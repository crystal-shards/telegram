# Represents the categories of chats for which a list of frequently used chats can be retrieved
#
# Users: A category containing frequently used private chats with non-bot users
# Bots: A category containing frequently used private chats with bot users
# Groups: A category containing frequently used basic groups and supergroups
# Channels: A category containing frequently used channels
# InlineBots: A category containing frequently used chats with inline bots sorted by their usage in inline mode
# Calls: A category containing frequently used chats used for calls
# ForwardChats: A category containing frequently used chats used to forward messages

abstract class TD::TopChatCategory < TD::Base
end