# The best available video quality
#

class TD::GroupCallVideoQuality::Full < TD::GroupCallVideoQuality
  include JSON::Serializable

  object_type "groupCallVideoQualityFull"
end