# The worst available video quality
#

class TD::GroupCallVideoQuality::Thumbnail < TD::GroupCallVideoQuality
  include JSON::Serializable

  object_type "groupCallVideoQualityThumbnail"
end