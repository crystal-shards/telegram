# The medium video quality
#

class TD::GroupCallVideoQuality::Medium < TD::GroupCallVideoQuality
  include JSON::Serializable

  object_type "groupCallVideoQualityMedium"
end