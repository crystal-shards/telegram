# Contains a list of websites the current user is logged in with Telegram
#
# @websites : Array(TD::ConnectedWebsite) (List of connected websites)

class TD::ConnectedWebsites < TD::Base
  include JSON::Serializable

  object_type "connectedWebsites"

  object_attributes({
    websites: Array(TD::ConnectedWebsite)
  })
end