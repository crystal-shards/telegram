# The secret chat is ready to use
#

class TD::SecretChatState::Ready < TD::SecretChatState
  include JSON::Serializable

  object_type "secretChatStateReady"
end