# The secret chat is not yet created; waiting for the other user to get online
#

class TD::SecretChatState::Pending < TD::SecretChatState
  include JSON::Serializable

  object_type "secretChatStatePending"
end