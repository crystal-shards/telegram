# The secret chat is closed
#

class TD::SecretChatState::Closed < TD::SecretChatState
  include JSON::Serializable

  object_type "secretChatStateClosed"
end