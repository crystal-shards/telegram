# Contains statistics about number of new members invited by a user
#
# @user_id : Int64 (User identifier)
# @added_member_count : Int32 (Number of new members invited by the user)

class TD::ChatStatisticsInviterInfo < TD::Base
  include JSON::Serializable

  object_type "chatStatisticsInviterInfo"

  object_attributes({
    user_id: Int64,
    added_member_count: Int32
  })
end