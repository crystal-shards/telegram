# Contains information about a Telegram Passport authorization form that was requested
#
# @id : Int32 (Unique identifier of the authorization form)
# @required_elements : Array(TD::PassportRequiredElement) (Telegram Passport elements that must be provided to complete the form)
# @privacy_policy_url : String (URL for the privacy policy of the service; may be empty)

class TD::PassportAuthorizationForm < TD::Base
  include JSON::Serializable

  object_type "passportAuthorizationForm"

  object_attributes({
    id: Int32,
    required_elements: Array(TD::PassportRequiredElement),
    privacy_policy_url: String
  })
end