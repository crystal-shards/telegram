# Contains the user's personal details
#
# @first_name : String (First name of the user written in English; 1-255 characters)
# @middle_name : String (Middle name of the user written in English; 0-255 characters)
# @last_name : String (Last name of the user written in English; 1-255 characters)
# @native_first_name : String (Native first name of the user; 1-255 characters)
# @native_middle_name : String (Native middle name of the user; 0-255 characters)
# @native_last_name : String (Native last name of the user; 1-255 characters)
# @birthdate : TD::Date (Birthdate of the user)
# @gender : String (Gender of the user, "male" or "female")
# @country_code : String (A two-letter ISO 3166-1 alpha-2 country code of the user's country)
# @residence_country_code : String (A two-letter ISO 3166-1 alpha-2 country code of the user's residence country)

class TD::PersonalDetails < TD::Base
  include JSON::Serializable

  object_type "personalDetails"

  object_attributes({
    first_name: String,
    middle_name: String,
    last_name: String,
    native_first_name: String,
    native_middle_name: String,
    native_last_name: String,
    birthdate: TD::Date,
    gender: String,
    country_code: String,
    residence_country_code: String
  })
end