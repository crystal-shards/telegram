# Animated variant of a chat photo in MPEG4 format
#
# @length : Int32 (Animation width and height)
# @file : TD::File (Information about the animation file)
# @main_frame_timestamp : Float64 (Timestamp of the frame, used as a static chat photo)

class TD::AnimatedChatPhoto < TD::Base
  include JSON::Serializable

  object_type "animatedChatPhoto"

  object_attributes({
    length: Int32,
    file: TD::File,
    main_frame_timestamp: Float64
  })
end