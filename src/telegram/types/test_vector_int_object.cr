# A simple object containing a vector of objects that hold a number; for testing only
#
# @value : Array(TD::TestInt) (Vector of objects)

class TD::TestVectorIntObject < TD::Base
  include JSON::Serializable

  object_type "testVectorIntObject"

  object_attributes({
    value: Array(TD::TestInt)
  })
end