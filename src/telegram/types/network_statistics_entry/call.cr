# Contains information about the total amount of data that was used for calls
#
# @network_type : TD::NetworkType (Type of the network the data was sent through. Call setNetworkType to maintain the actual network type)
# @sent_bytes : Int64 (Total number of bytes sent)
# @received_bytes : Int64 (Total number of bytes received)
# @duration : Float64 (Total call duration, in seconds)

class TD::NetworkStatisticsEntry::Call < TD::NetworkStatisticsEntry
  include JSON::Serializable

  object_type "networkStatisticsEntryCall"

  object_attributes({
    network_type: TD::NetworkType,
    sent_bytes: Int64,
    received_bytes: Int64,
    duration: Float64
  })
end