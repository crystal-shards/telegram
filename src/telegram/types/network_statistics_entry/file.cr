# Contains information about the total amount of data that was used to send and receive files
#
# @file_type : TD::FileType (Type of the file the data is part of; pass null if the data isn't related to files)
# @network_type : TD::NetworkType (Type of the network the data was sent through. Call setNetworkType to maintain the actual network type)
# @sent_bytes : Int64 (Total number of bytes sent)
# @received_bytes : Int64 (Total number of bytes received)

class TD::NetworkStatisticsEntry::File < TD::NetworkStatisticsEntry
  include JSON::Serializable

  object_type "networkStatisticsEntryFile"

  object_attributes({
    file_type: TD::FileType,
    network_type: TD::NetworkType,
    sent_bytes: Int64,
    received_bytes: Int64
  })
end