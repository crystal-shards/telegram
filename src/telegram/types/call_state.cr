# Describes the current call state
#
# Pending: The call is pending, waiting to be accepted by a user
# ExchangingKeys: The call has been answered and encryption keys are being exchanged
# Ready: The call is ready to use
# HangingUp: The call is hanging up after discardCall has been called
# Discarded: The call has ended successfully
# Error: The call has ended with an error

abstract class TD::CallState < TD::Base
end