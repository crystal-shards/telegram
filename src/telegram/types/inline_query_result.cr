# Represents a single result of an inline query
#
# Article: Represents a link to an article or web page
# Contact: Represents a user contact
# Location: Represents a point on the map
# Venue: Represents information about a venue
# Game: Represents information about a game
# Animation: Represents an animation file
# Audio: Represents an audio file
# Document: Represents a document
# Photo: Represents a photo
# Sticker: Represents a sticker
# Video: Represents a video
# VoiceNote: Represents a voice note

abstract class TD::InlineQueryResult < TD::Base
end