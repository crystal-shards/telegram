# Contains statistics about network usage
#
# File: Contains information about the total amount of data that was used to send and receive files
# Call: Contains information about the total amount of data that was used for calls

abstract class TD::NetworkStatisticsEntry < TD::Base
end