# The content of a message to send
#
# Text: A text message
# Animation: An animation message (GIF-style).
# Audio: An audio message
# Document: A document message (general file)
# Photo: A photo message
# Sticker: A sticker message
# Video: A video message
# VideoNote: A video note message
# VoiceNote: A voice note message
# Location: A message with a location
# Venue: A message with information about a venue
# Contact: A message containing a user contact
# Dice: A dice message
# Game: A message with a game; not supported for channels or secret chats
# Invoice: A message with an invoice; can be used only by bots
# Poll: A message with a poll. Polls can't be sent to secret chats. Polls can be sent only to a private chat with a bot
# Forwarded: A forwarded message

abstract class TD::InputMessageContent < TD::Base
end