# A cubic Bézier curve to a given point
#
# @start_control_point : TD::Point (The start control point of the curve)
# @end_control_point : TD::Point (The end control point of the curve)
# @end_point : TD::Point (The end point of the curve)

class TD::VectorPathCommand::CubicBezierCurve < TD::VectorPathCommand
  include JSON::Serializable

  object_type "vectorPathCommandCubicBezierCurve"

  object_attributes({
    start_control_point: TD::Point,
    end_control_point: TD::Point,
    end_point: TD::Point
  })
end