# A straight line to a given point
#
# @end_point : TD::Point (The end point of the straight line)

class TD::VectorPathCommand::Line < TD::VectorPathCommand
  include JSON::Serializable

  object_type "vectorPathCommandLine"

  object_attributes({
    end_point: TD::Point
  })
end