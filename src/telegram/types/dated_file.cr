# File with the date it was uploaded
#
# @file : TD::File (The file)
# @date : Int32 (Point in time (Unix timestamp) when the file was uploaded)

class TD::DatedFile < TD::Base
  include JSON::Serializable

  object_type "datedFile"

  object_attributes({
    file: TD::File,
    date: Int32
  })
end