# Contains a description of the required Telegram Passport element that was requested by a service
#
# @suitable_elements : Array(TD::PassportSuitableElement) (List of Telegram Passport elements any of which is enough to provide)

class TD::PassportRequiredElement < TD::Base
  include JSON::Serializable

  object_type "passportRequiredElement"

  object_attributes({
    suitable_elements: Array(TD::PassportSuitableElement)
  })
end