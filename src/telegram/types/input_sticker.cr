# Describes a sticker that needs to be added to a sticker set
#
# Static: A static sticker in PNG format, which will be converted to WEBP server-side
# Animated: An animated sticker in TGS format

abstract class TD::InputSticker < TD::Base
end