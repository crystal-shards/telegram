# Describes a group of video synchronization source identifiers
#
# @semantics : String (The semantics of sources, one of "SIM" or "FID")
# @source_ids : Array(Int32) (The list of synchronization source identifiers)

class TD::GroupCallVideoSourceGroup < TD::Base
  include JSON::Serializable

  object_type "groupCallVideoSourceGroup"

  object_attributes({
    semantics: String,
    source_ids: Array(Int32)
  })
end