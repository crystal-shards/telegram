# Describes the type of an inline keyboard button
#
# Url: A button that opens a specified URL
# LoginUrl: A button that opens a specified URL and automatically authorize the current user if allowed to do so
# Callback: A button that sends a callback query to a bot
# CallbackWithPassword: A button that asks for password of the current user and then sends a callback query to a bot
# CallbackGame: A button with a game that sends a callback query to a bot. This button must be in the first column and row of the keyboard and can be attached only to a message with content of the type messageGame
# SwitchInline: A button that forces an inline query to the bot to be inserted in the input field
# Buy: A button to buy something. This button must be in the first column and row of the keyboard and can be attached only to a message with content of the type messageInvoice
# User: A button with a user reference to be handled in the same way as textEntityTypeMentionName entities

abstract class TD::InlineKeyboardButtonType < TD::Base
end