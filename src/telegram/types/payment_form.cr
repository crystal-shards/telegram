# Contains information about an invoice payment form
#
# @id : String (The payment form identifier)
# @invoice : TD::Invoice (Full information of the invoice)
# @url : String (Payment form URL)
# @seller_bot_user_id : Int64 (User identifier of the seller bot)
# @payments_provider_user_id : Int64 (User identifier of the payment provider bot)
# @payments_provider : TD::PaymentsProviderStripe? (Information about the payment provider, if available, to support it natively without the need for opening the URL; may be null)
# @saved_order_info : TD::OrderInfo? (Saved server-side order information; may be null)
# @saved_credentials : TD::SavedCredentials? (Information about saved card credentials; may be null)
# @can_save_credentials : Bool (True, if the user can choose to save credentials)
# @need_password : Bool (True, if the user will be able to save credentials protected by a password they set up)

class TD::PaymentForm < TD::Base
  include JSON::Serializable

  object_type "paymentForm"

  object_attributes({
    id: String,
    invoice: TD::Invoice,
    url: String,
    seller_bot_user_id: Int64,
    payments_provider_user_id: Int64,
    payments_provider: TD::PaymentsProviderStripe?,
    saved_order_info: TD::OrderInfo?,
    saved_credentials: TD::SavedCredentials?,
    can_save_credentials: Bool,
    need_password: Bool
  })
end