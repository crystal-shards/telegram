# Describes a poll
#
# @id : String (Unique poll identifier)
# @question : String (Poll question; 1-300 characters)
# @options : Array(TD::PollOption) (List of poll answer options)
# @total_voter_count : Int32 (Total number of voters, participating in the poll)
# @recent_voter_user_ids : Array(Int64) (User identifiers of recent voters, if the poll is non-anonymous)
# @is_anonymous : Bool (True, if the poll is anonymous)
# @type : TD::PollType (Type of the poll)
# @open_period : Int32 (Amount of time the poll will be active after creation, in seconds)
# @close_date : Int32 (Point in time (Unix timestamp) when the poll will automatically be closed)
# @is_closed : Bool (True, if the poll is closed)

class TD::Poll < TD::Base
  include JSON::Serializable

  object_type "poll"

  object_attributes({
    id: String,
    question: String,
    options: Array(TD::PollOption),
    total_voter_count: Int32,
    recent_voter_user_ids: Array(Int64),
    is_anonymous: Bool,
    type: TD::PollType,
    open_period: Int32,
    close_date: Int32,
    is_closed: Bool
  })
end