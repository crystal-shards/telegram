# Contains a list of chat events
#
# @events : Array(TD::ChatEvent) (List of events)

class TD::ChatEvents < TD::Base
  include JSON::Serializable

  object_type "chatEvents"

  object_attributes({
    events: Array(TD::ChatEvent)
  })
end