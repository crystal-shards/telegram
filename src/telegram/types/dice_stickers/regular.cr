# A regular animated sticker
#
# @sticker : TD::Sticker (The animated sticker with the dice animation)

class TD::DiceStickers::Regular < TD::DiceStickers
  include JSON::Serializable

  object_type "diceStickersRegular"

  object_attributes({
    sticker: TD::Sticker
  })
end