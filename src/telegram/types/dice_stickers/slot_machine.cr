# Animated stickers to be combined into a slot machine
#
# @background : TD::Sticker (The animated sticker with the slot machine background. The background animation must start playing after all reel animations finish)
# @lever : TD::Sticker (The animated sticker with the lever animation. The lever animation must play once in the initial dice state)
# @left_reel : TD::Sticker (The animated sticker with the left reel)
# @center_reel : TD::Sticker (The animated sticker with the center reel)
# @right_reel : TD::Sticker (The animated sticker with the right reel)

class TD::DiceStickers::SlotMachine < TD::DiceStickers
  include JSON::Serializable

  object_type "diceStickersSlotMachine"

  object_attributes({
    background: TD::Sticker,
    lever: TD::Sticker,
    left_reel: TD::Sticker,
    center_reel: TD::Sticker,
    right_reel: TD::Sticker
  })
end