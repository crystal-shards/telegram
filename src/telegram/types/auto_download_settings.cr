# Contains auto-download settings
#
# @is_auto_download_enabled : Bool (True, if the auto-download is enabled)
# @max_photo_file_size : Int32 (The maximum size of a photo file to be auto-downloaded, in bytes)
# @max_video_file_size : Int32 (The maximum size of a video file to be auto-downloaded, in bytes)
# @max_other_file_size : Int32 (The maximum size of other file types to be auto-downloaded, in bytes)
# @video_upload_bitrate : Int32 (The maximum suggested bitrate for uploaded videos, in kbit)
# @preload_large_videos : Bool (True, if the beginning of video files needs to be preloaded for instant playback)
# @preload_next_audio : Bool (True, if the next audio track needs to be preloaded while the user is listening to an audio file)
# @use_less_data_for_calls : Bool (True, if "use less data for calls" option needs to be enabled)

class TD::AutoDownloadSettings < TD::Base
  include JSON::Serializable

  object_type "autoDownloadSettings"

  object_attributes({
    is_auto_download_enabled: Bool,
    max_photo_file_size: Int32,
    max_video_file_size: Int32,
    max_other_file_size: Int32,
    video_upload_bitrate: Int32,
    preload_large_videos: Bool,
    preload_next_audio: Bool,
    use_less_data_for_calls: Bool
  })
end