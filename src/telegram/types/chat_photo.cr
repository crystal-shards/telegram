# Describes a chat or user profile photo
#
# @id : String (Unique photo identifier)
# @added_date : Int32 (Point in time (Unix timestamp) when the photo has been added)
# @minithumbnail : TD::Minithumbnail? (Photo minithumbnail; may be null)
# @sizes : Array(TD::PhotoSize) (Available variants of the photo in JPEG format, in different size)
# @animation : TD::AnimatedChatPhoto? (Animated variant of the photo in MPEG4 format; may be null)

class TD::ChatPhoto < TD::Base
  include JSON::Serializable

  object_type "chatPhoto"

  object_attributes({
    id: String,
    added_date: Int32,
    minithumbnail: TD::Minithumbnail?,
    sizes: Array(TD::PhotoSize),
    animation: TD::AnimatedChatPhoto?
  })
end