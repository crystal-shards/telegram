# Describes a game
#
# @id : String (Game ID)
# @short_name : String (Game short name. To share a game use the URL https:)
# @title : String (Game title)
# @text : TD::FormattedText (Game text, usually containing scoreboards for a game)
# @description : String (Game description)
# @photo : TD::Photo (Game photo)
# @animation : TD::Animation? (Game animation; may be null)

class TD::Game < TD::Base
  include JSON::Serializable

  object_type "game"

  object_attributes({
    id: String,
    short_name: String,
    title: String,
    text: TD::FormattedText,
    description: String,
    photo: TD::Photo,
    animation: TD::Animation?
  })
end