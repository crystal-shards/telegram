# Contains a list of chat members
#
# @total_count : Int32 (Approximate total count of chat members found)
# @members : Array(TD::ChatMember) (A list of chat members)

class TD::ChatMembers < TD::Base
  include JSON::Serializable

  object_type "chatMembers"

  object_attributes({
    total_count: Int32,
    members: Array(TD::ChatMember)
  })
end