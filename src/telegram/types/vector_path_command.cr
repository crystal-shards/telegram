# Represents a vector path command
#
# Line: A straight line to a given point
# CubicBezierCurve: A cubic Bézier curve to a given point

abstract class TD::VectorPathCommand < TD::Base
end