# Information about a bank card
#
# @title : String (Title of the bank card description)
# @actions : Array(TD::BankCardActionOpenUrl) (Actions that can be done with the bank card number)

class TD::BankCardInfo < TD::Base
  include JSON::Serializable

  object_type "bankCardInfo"

  object_attributes({
    title: String,
    actions: Array(TD::BankCardActionOpenUrl)
  })
end