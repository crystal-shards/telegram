# Contains a list of message positions
#
# @total_count : Int32 (Total count of messages found)
# @positions : Array(TD::MessagePosition) (List of message positions)

class TD::MessagePositions < TD::Base
  include JSON::Serializable

  object_type "messagePositions"

  object_attributes({
    total_count: Int32,
    positions: Array(TD::MessagePosition)
  })
end