# Describes a user contact
#
# @phone_number : String (Phone number of the user)
# @first_name : String (First name of the user; 1-255 characters in length)
# @last_name : String (Last name of the user)
# @vcard : String (Additional data about the user in a form of vCard; 0-2048 bytes in length)
# @user_id : Int64 (Identifier of the user, if known; otherwise 0)

class TD::Contact < TD::Base
  include JSON::Serializable

  object_type "contact"

  object_attributes({
    phone_number: String,
    first_name: String,
    last_name: String,
    vcard: String,
    user_id: Int64
  })
end