# Contains approximate storage usage statistics, excluding files of unknown file type
#
# @files_size : Int64 (Approximate total size of files, in bytes)
# @file_count : Int32 (Approximate number of files)
# @database_size : Int64 (Size of the database)
# @language_pack_database_size : Int64 (Size of the language pack database)
# @log_size : Int64 (Size of the TDLib internal log)

class TD::StorageStatisticsFast < TD::Base
  include JSON::Serializable

  object_type "storageStatisticsFast"

  object_attributes({
    files_size: Int64,
    file_count: Int32,
    database_size: Int64,
    language_pack_database_size: Int64,
    log_size: Int64
  })
end