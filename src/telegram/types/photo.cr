# Describes a photo
#
# @has_stickers : Bool (True, if stickers were added to the photo. The list of corresponding sticker sets can be received using getAttachedStickerSets)
# @minithumbnail : TD::Minithumbnail? (Photo minithumbnail; may be null)
# @sizes : Array(TD::PhotoSize) (Available variants of the photo, in different sizes)

class TD::Photo < TD::Base
  include JSON::Serializable

  object_type "photo"

  object_attributes({
    has_stickers: Bool,
    minithumbnail: TD::Minithumbnail?,
    sizes: Array(TD::PhotoSize)
  })
end