# Represents a list of chats
#
# @total_count : Int32 (Approximate total count of chats found)
# @chat_ids : Array(Int64) (List of chat identifiers)

class TD::Chats < TD::Base
  include JSON::Serializable

  object_type "chats"

  object_attributes({
    total_count: Int32,
    chat_ids: Array(Int64)
  })
end