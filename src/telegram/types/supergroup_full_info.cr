# Contains full information about a supergroup or channel
#
# @photo : TD::ChatPhoto? (Chat photo; may be null)
# @description : String (Supergroup or channel description)
# @member_count : Int32 (Number of members in the supergroup or channel; 0 if unknown)
# @administrator_count : Int32 (Number of privileged users in the supergroup or channel; 0 if unknown)
# @restricted_count : Int32 (Number of restricted users in the supergroup; 0 if unknown)
# @banned_count : Int32 (Number of users banned from chat; 0 if unknown)
# @linked_chat_id : Int64 (Chat identifier of a discussion group for the channel, or a channel, for which the supergroup is the designated discussion group; 0 if none or unknown)
# @slow_mode_delay : Int32 (Delay between consecutive sent messages for non-administrator supergroup members, in seconds)
# @slow_mode_delay_expires_in : Float64 (Time left before next message can be sent in the supergroup, in seconds. An updateSupergroupFullInfo update is not triggered when value of this field changes, but both new and old values are non-zero)
# @can_get_members : Bool (True, if members of the chat can be retrieved)
# @can_set_username : Bool (True, if the chat username can be changed)
# @can_set_sticker_set : Bool (True, if the supergroup sticker set can be changed)
# @can_set_location : Bool (True, if the supergroup location can be changed)
# @can_get_statistics : Bool (True, if the supergroup or channel statistics are available)
# @is_all_history_available : Bool (True, if new chat members will have access to old messages. In public or discussion groups and both public and private channels, old messages are always available, so this option affects only private supergroups without a linked chat. The value of this field is only available for chat administrators)
# @sticker_set_id : String (Identifier of the supergroup sticker set; 0 if none)
# @location : TD::ChatLocation? (Location to which the supergroup is connected; may be null)
# @invite_link : TD::ChatInviteLink? (Primary invite link for this chat; may be null. For chat administrators with can_invite_users right only)
# @bot_commands : Array(TD::BotCommands) (List of commands of bots in the group)
# @upgraded_from_basic_group_id : Int64 (Identifier of the basic group from which supergroup was upgraded; 0 if none)
# @upgraded_from_max_message_id : Int64 (Identifier of the last message in the basic group from which supergroup was upgraded; 0 if none)

class TD::SupergroupFullInfo < TD::Base
  include JSON::Serializable

  object_type "supergroupFullInfo"

  object_attributes({
    photo: TD::ChatPhoto?,
    description: String,
    member_count: Int32,
    administrator_count: Int32,
    restricted_count: Int32,
    banned_count: Int32,
    linked_chat_id: Int64,
    slow_mode_delay: Int32,
    slow_mode_delay_expires_in: Float64,
    can_get_members: Bool,
    can_set_username: Bool,
    can_set_sticker_set: Bool,
    can_set_location: Bool,
    can_get_statistics: Bool,
    is_all_history_available: Bool,
    sticker_set_id: String,
    location: TD::ChatLocation?,
    invite_link: TD::ChatInviteLink?,
    bot_commands: Array(TD::BotCommands),
    upgraded_from_basic_group_id: Int64,
    upgraded_from_max_message_id: Int64
  })
end