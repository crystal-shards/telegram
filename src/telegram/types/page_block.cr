# Describes a block of an instant view web page
#
# Title: The title of a page
# Subtitle: The subtitle of a page
# AuthorDate: The author and publishing date of a page
# Header: A header
# Subheader: A subheader
# Kicker: A kicker
# Paragraph: A text paragraph
# Preformatted: A preformatted text paragraph
# Footer: The footer of a page
# Divider: An empty block separating a page
# Anchor: An invisible anchor on a page, which can be used in a URL to open the page from the specified anchor
# List: A list of data blocks
# BlockQuote: A block quote
# PullQuote: A pull quote
# Animation: An animation
# Audio: An audio file
# Photo: A photo
# Video: A video
# VoiceNote: A voice note
# Cover: A page cover
# Embedded: An embedded web page
# EmbeddedPost: An embedded post
# Collage: A collage
# Slideshow: A slideshow
# ChatLink: A link to a chat
# Table: A table
# Details: A collapsible block
# RelatedArticles: Related articles
# Map: A map

abstract class TD::PageBlock < TD::Base
end