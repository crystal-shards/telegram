# Returns information about the availability of a temporary password, which can be used for payments
#
# @has_password : Bool (True, if a temporary password is available)
# @valid_for : Int32 (Time left before the temporary password expires, in seconds)

class TD::TemporaryPasswordState < TD::Base
  include JSON::Serializable

  object_type "temporaryPasswordState"

  object_attributes({
    has_password: Bool,
    valid_for: Int32
  })
end