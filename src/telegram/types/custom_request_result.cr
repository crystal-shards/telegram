# Contains the result of a custom request
#
# @result : String (A JSON-serialized result)

class TD::CustomRequestResult < TD::Base
  include JSON::Serializable

  object_type "customRequestResult"

  object_attributes({
    result: String
  })
end