# Contains a globally unique push receiver identifier, which can be used to identify which account has received a push notification
#
# @id : String (The globally unique identifier of push notification subscription)

class TD::PushReceiverId < TD::Base
  include JSON::Serializable

  object_type "pushReceiverId"

  object_attributes({
    id: String
  })
end