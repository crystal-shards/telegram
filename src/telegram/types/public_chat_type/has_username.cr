# The chat is public, because it has username
#

class TD::PublicChatType::HasUsername < TD::PublicChatType
  include JSON::Serializable

  object_type "publicChatTypeHasUsername"
end