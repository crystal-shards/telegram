# The chat is public, because it is a location-based supergroup
#

class TD::PublicChatType::IsLocationBased < TD::PublicChatType
  include JSON::Serializable

  object_type "publicChatTypeIsLocationBased"
end