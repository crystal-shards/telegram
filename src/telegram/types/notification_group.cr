# Describes a group of notifications
#
# @id : Int32 (Unique persistent auto-incremented from 1 identifier of the notification group)
# @type : TD::NotificationGroupType (Type of the group)
# @chat_id : Int64 (Identifier of a chat to which all notifications in the group belong)
# @total_count : Int32 (Total number of active notifications in the group)
# @notifications : Array(TD::Notification) (The list of active notifications)

class TD::NotificationGroup < TD::Base
  include JSON::Serializable

  object_type "notificationGroup"

  object_attributes({
    id: Int32,
    type: TD::NotificationGroupType,
    chat_id: Int64,
    total_count: Int32,
    notifications: Array(TD::Notification)
  })
end