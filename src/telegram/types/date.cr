# Represents a date according to the Gregorian calendar
#
# @day : Int32 (Day of the month; 1-31)
# @month : Int32 (Month; 1-12)
# @year : Int32 (Year; 1-9999)

class TD::Date < TD::Base
  include JSON::Serializable

  object_type "date"

  object_attributes({
    day: Int32,
    month: Int32,
    year: Int32
  })
end