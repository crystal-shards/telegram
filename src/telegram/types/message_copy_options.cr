# Options to be used when a message content is copied without reference to the original sender. Service messages and messageInvoice can't be copied
#
# @send_copy : Bool (True, if content of the message needs to be copied without reference to the original sender. Always true if the message is forwarded to a secret chat or is local)
# @replace_caption : Bool (True, if media caption of the message copy needs to be replaced. Ignored if send_copy is false)
# @new_caption : TD::FormattedText (New message caption; pass null to copy message without caption. Ignored if replace_caption is false)

class TD::MessageCopyOptions < TD::Base
  include JSON::Serializable

  object_type "messageCopyOptions"

  object_attributes({
    send_copy: Bool,
    replace_caption: Bool,
    new_caption: TD::FormattedText
  })
end