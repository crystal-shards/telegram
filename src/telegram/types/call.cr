# Describes a call
#
# @id : Int32 (Call identifier, not persistent)
# @user_id : Int64 (Peer user identifier)
# @is_outgoing : Bool (True, if the call is outgoing)
# @is_video : Bool (True, if the call is a video call)
# @state : TD::CallState (Call state)

class TD::Call < TD::Base
  include JSON::Serializable

  object_type "call"

  object_attributes({
    id: Int32,
    user_id: Int64,
    is_outgoing: Bool,
    is_video: Bool,
    state: TD::CallState
  })
end