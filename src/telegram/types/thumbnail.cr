# Represents a thumbnail
#
# @format : TD::ThumbnailFormat (Thumbnail format)
# @width : Int32 (Thumbnail width)
# @height : Int32 (Thumbnail height)
# @file : TD::File (The thumbnail)

class TD::Thumbnail < TD::Base
  include JSON::Serializable

  object_type "thumbnail"

  object_attributes({
    format: TD::ThumbnailFormat,
    width: Int32,
    height: Int32,
    file: TD::File
  })
end