# Notification settings applied to all channels when the corresponding chat setting has a default value
#

class TD::NotificationSettingsScope::ChannelChats < TD::NotificationSettingsScope
  include JSON::Serializable

  object_type "notificationSettingsScopeChannelChats"
end