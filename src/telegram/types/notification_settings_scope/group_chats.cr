# Notification settings applied to all basic groups and supergroups when the corresponding chat setting has a default value
#

class TD::NotificationSettingsScope::GroupChats < TD::NotificationSettingsScope
  include JSON::Serializable

  object_type "notificationSettingsScopeGroupChats"
end