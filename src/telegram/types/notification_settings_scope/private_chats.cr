# Notification settings applied to all private and secret chats when the corresponding chat setting has a default value
#

class TD::NotificationSettingsScope::PrivateChats < TD::NotificationSettingsScope
  include JSON::Serializable

  object_type "notificationSettingsScopePrivateChats"
end