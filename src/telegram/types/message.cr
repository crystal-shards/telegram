# Describes a message
#
# @id : Int64 (Message identifier; unique for the chat to which the message belongs)
# @sender_id : TD::MessageSender (Identifier of the sender of the message)
# @chat_id : Int64 (Chat identifier)
# @sending_state : TD::MessageSendingState? (The sending state of the message; may be null)
# @scheduling_state : TD::MessageSchedulingState? (The scheduling state of the message; may be null)
# @is_outgoing : Bool (True, if the message is outgoing)
# @is_pinned : Bool (True, if the message is pinned)
# @can_be_edited : Bool (True, if the message can be edited. For live location and poll messages this fields shows whether editMessageLiveLocation or stopPoll can be used with this message by the application)
# @can_be_forwarded : Bool (True, if the message can be forwarded)
# @can_be_saved : Bool (True, if content of the message can be saved locally or copied)
# @can_be_deleted_only_for_self : Bool (True, if the message can be deleted only for the current user while other users will continue to see it)
# @can_be_deleted_for_all_users : Bool (True, if the message can be deleted for all users)
# @can_get_statistics : Bool (True, if the message statistics are available)
# @can_get_message_thread : Bool (True, if the message thread info is available)
# @can_get_viewers : Bool (True, if chat members already viewed the message can be received through getMessageViewers)
# @can_get_media_timestamp_links : Bool (True, if media timestamp links can be generated for media timestamp entities in the message text, caption or web page description)
# @has_timestamped_media : Bool (True, if media timestamp entities refers to a media in this message as opposed to a media in the replied message)
# @is_channel_post : Bool (True, if the message is a channel post. All messages to channels are channel posts, all other messages are not channel posts)
# @contains_unread_mention : Bool (True, if the message contains an unread mention for the current user)
# @date : Int32 (Point in time (Unix timestamp) when the message was sent)
# @edit_date : Int32 (Point in time (Unix timestamp) when the message was last edited)
# @forward_info : TD::MessageForwardInfo? (Information about the initial message sender; may be null)
# @interaction_info : TD::MessageInteractionInfo? (Information about interactions with the message; may be null)
# @reply_in_chat_id : Int64 (If non-zero, the identifier of the chat to which the replied message belongs; Currently, only messages in the Replies chat can have different reply_in_chat_id and chat_id)
# @reply_to_message_id : Int64 (If non-zero, the identifier of the message this message is replying to; can be the identifier of a deleted message)
# @message_thread_id : Int64 (If non-zero, the identifier of the message thread the message belongs to; unique within the chat to which the message belongs)
# @ttl : Int32 (For self-destructing messages, the message's TTL (Time To Live), in seconds; 0 if none. TDLib will send updateDeleteMessages or updateMessageContent once the TTL expires)
# @ttl_expires_in : Float64 (Time left before the message expires, in seconds. If the TTL timer isn't started yet, equals to the value of the ttl field)
# @via_bot_user_id : Int64 (If non-zero, the user identifier of the bot through which this message was sent)
# @author_signature : String (For channel posts and anonymous group messages, optional author signature)
# @media_album_id : String (Unique identifier of an album this message belongs to. Only audios, documents, photos and videos can be grouped together in albums)
# @restriction_reason : String (If non-empty, contains a human-readable description of the reason why access to this message must be restricted)
# @content : TD::MessageContent (Content of the message)
# @reply_markup : TD::ReplyMarkup? (Reply markup for the message; may be null)

class TD::Message < TD::Base
  include JSON::Serializable

  object_type "message"

  object_attributes({
    id: Int64,
    sender_id: TD::MessageSender,
    chat_id: Int64,
    sending_state: TD::MessageSendingState?,
    scheduling_state: TD::MessageSchedulingState?,
    is_outgoing: Bool,
    is_pinned: Bool,
    can_be_edited: Bool,
    can_be_forwarded: Bool,
    can_be_saved: Bool,
    can_be_deleted_only_for_self: Bool,
    can_be_deleted_for_all_users: Bool,
    can_get_statistics: Bool,
    can_get_message_thread: Bool,
    can_get_viewers: Bool,
    can_get_media_timestamp_links: Bool,
    has_timestamped_media: Bool,
    is_channel_post: Bool,
    contains_unread_mention: Bool,
    date: Int32,
    edit_date: Int32,
    forward_info: TD::MessageForwardInfo?,
    interaction_info: TD::MessageInteractionInfo?,
    reply_in_chat_id: Int64,
    reply_to_message_id: Int64,
    message_thread_id: Int64,
    ttl: Int32,
    ttl_expires_in: Float64,
    via_bot_user_id: Int64,
    author_signature: String,
    media_album_id: String,
    restriction_reason: String,
    content: TD::MessageContent,
    reply_markup: TD::ReplyMarkup?
  })
end