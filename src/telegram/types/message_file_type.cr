# Contains information about a file with messages exported from another app
#
# Private: The messages was exported from a private chat
# Group: The messages was exported from a group chat
# Unknown: The messages was exported from a chat of unknown type

abstract class TD::MessageFileType < TD::Base
end