# Specifies the kind of chat members to return in searchChatMembers
#
# Contacts: Returns contacts of the user
# Administrators: Returns the owner and administrators
# Members: Returns all chat members, including restricted chat members
# Mention: Returns users which can be mentioned in the chat
# Restricted: Returns users under certain restrictions in the chat; can be used only by administrators in a supergroup
# Banned: Returns users banned from the chat; can be used only by administrators in a supergroup or in a channel
# Bots: Returns bot members of the chat

abstract class TD::ChatMembersFilter < TD::Base
end