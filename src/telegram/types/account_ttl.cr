# Contains information about the period of inactivity after which the current user's account will automatically be deleted
#
# @days : Int32 (Number of days of inactivity before the account will be flagged for deletion; 30-366 days)

class TD::AccountTtl < TD::Base
  include JSON::Serializable

  object_type "accountTtl"

  object_attributes({
    days: Int32
  })
end