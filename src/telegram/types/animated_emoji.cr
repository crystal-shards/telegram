# Describes an animated representation of an emoji
#
# @sticker : TD::Sticker (Animated sticker for the emoji)
# @fitzpatrick_type : Int32 (Emoji modifier fitzpatrick type; 0-6; 0 if none)
# @sound : TD::File? (File containing the sound to be played when the animated emoji is clicked if any; may be null. The sound is encoded with the Opus codec, and stored inside an OGG container)

class TD::AnimatedEmoji < TD::Base
  include JSON::Serializable

  object_type "animatedEmoji"

  object_attributes({
    sticker: TD::Sticker,
    fitzpatrick_type: Int32,
    sound: TD::File?
  })
end