# An object of this type can be returned on every function call, in case of an error
#
# @code : Int32 (Error code; subject to future changes. If the error code is 406, the error message must not be processed in any way and must not be displayed to the user)
# @message : String (Error message; subject to future changes)

class TD::Error < TD::Base
  include JSON::Serializable

  object_type "error"

  object_attributes({
    code: Int32,
    message: String
  })
end