# Contains the description of an error in a Telegram Passport element; for bots only
#
# @type : TD::PassportElementType (Type of Telegram Passport element that has the error)
# @message : String (Error message)
# @source : TD::InputPassportElementErrorSource (Error source)

class TD::InputPassportElementError < TD::Base
  include JSON::Serializable

  object_type "inputPassportElementError"

  object_attributes({
    type: TD::PassportElementType,
    message: String,
    source: TD::InputPassportElementErrorSource
  })
end