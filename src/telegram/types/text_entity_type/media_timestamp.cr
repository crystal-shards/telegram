# A media timestamp
#
# @media_timestamp : Int32 (Timestamp from which a video)

class TD::TextEntityType::MediaTimestamp < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeMediaTimestamp"

  object_attributes({
    media_timestamp: Int32
  })
end