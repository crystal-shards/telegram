# A cashtag text, beginning with "
#

class TD::TextEntityType::Cashtag < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeCashtag"
end