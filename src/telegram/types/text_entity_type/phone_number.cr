# A phone number
#

class TD::TextEntityType::PhoneNumber < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypePhoneNumber"
end