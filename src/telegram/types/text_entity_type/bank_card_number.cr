# A bank card number. The getBankCardInfo method can be used to get information about the bank card
#

class TD::TextEntityType::BankCardNumber < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeBankCardNumber"
end