# A text description shown instead of a raw URL
#
# @url : String (HTTP or tg:)

class TD::TextEntityType::TextUrl < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeTextUrl"

  object_attributes({
    url: String
  })
end