# Text that must be formatted as if inside a code HTML tag
#

class TD::TextEntityType::Code < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeCode"
end