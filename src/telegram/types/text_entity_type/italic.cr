# An italic text
#

class TD::TextEntityType::Italic < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeItalic"
end