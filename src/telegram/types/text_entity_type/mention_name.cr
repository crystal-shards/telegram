# A text shows instead of a raw mention of the user (e.g., when the user has no username)
#
# @user_id : Int64 (Identifier of the mentioned user)

class TD::TextEntityType::MentionName < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeMentionName"

  object_attributes({
    user_id: Int64
  })
end