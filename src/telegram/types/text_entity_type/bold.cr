# A bold text
#

class TD::TextEntityType::Bold < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeBold"
end