# An email address
#

class TD::TextEntityType::EmailAddress < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeEmailAddress"
end