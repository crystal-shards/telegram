# Text that must be formatted as if inside a pre HTML tag
#

class TD::TextEntityType::Pre < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypePre"
end