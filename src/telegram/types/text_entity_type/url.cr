# An HTTP URL
#

class TD::TextEntityType::Url < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeUrl"
end