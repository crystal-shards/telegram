# Text that must be formatted as if inside pre, and code HTML tags
#
# @language : String (Programming language of the code; as defined by the sender)

class TD::TextEntityType::PreCode < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypePreCode"

  object_attributes({
    language: String
  })
end