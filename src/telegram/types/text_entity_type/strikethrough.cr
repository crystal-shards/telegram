# A strikethrough text
#

class TD::TextEntityType::Strikethrough < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeStrikethrough"
end