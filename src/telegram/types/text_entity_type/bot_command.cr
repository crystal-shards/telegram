# A bot command, beginning with "
#

class TD::TextEntityType::BotCommand < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeBotCommand"
end