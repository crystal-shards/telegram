# A mention of a user by their username
#

class TD::TextEntityType::Mention < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeMention"
end