# A hashtag text, beginning with "
#

class TD::TextEntityType::Hashtag < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeHashtag"
end