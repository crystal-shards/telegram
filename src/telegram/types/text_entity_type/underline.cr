# An underlined text
#

class TD::TextEntityType::Underline < TD::TextEntityType
  include JSON::Serializable

  object_type "textEntityTypeUnderline"
end