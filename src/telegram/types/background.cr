# Describes a chat background
#
# @id : String (Unique background identifier)
# @is_default : Bool (True, if this is one of default backgrounds)
# @is_dark : Bool (True, if the background is dark and is recommended to be used with dark theme)
# @name : String (Unique background name)
# @document : TD::Document? (Document with the background; may be null. Null only for filled backgrounds)
# @type : TD::BackgroundType (Type of the background)

class TD::Background < TD::Base
  include JSON::Serializable

  object_type "background"

  object_attributes({
    id: String,
    is_default: Bool,
    is_dark: Bool,
    name: String,
    document: TD::Document?,
    type: TD::BackgroundType
  })
end