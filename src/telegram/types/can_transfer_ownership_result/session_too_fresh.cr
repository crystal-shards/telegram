# The session was created recently, user needs to wait
#
# @retry_after : Int32 (Time left before the session can be used to transfer ownership of a chat, in seconds)

class TD::CanTransferOwnershipResult::SessionTooFresh < TD::CanTransferOwnershipResult
  include JSON::Serializable

  object_type "canTransferOwnershipResultSessionTooFresh"

  object_attributes({
    retry_after: Int32
  })
end