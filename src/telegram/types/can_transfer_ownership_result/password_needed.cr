# The 2-step verification needs to be enabled first
#

class TD::CanTransferOwnershipResult::PasswordNeeded < TD::CanTransferOwnershipResult
  include JSON::Serializable

  object_type "canTransferOwnershipResultPasswordNeeded"
end