# The 2-step verification was enabled recently, user needs to wait
#
# @retry_after : Int32 (Time left before the session can be used to transfer ownership of a chat, in seconds)

class TD::CanTransferOwnershipResult::PasswordTooFresh < TD::CanTransferOwnershipResult
  include JSON::Serializable

  object_type "canTransferOwnershipResultPasswordTooFresh"

  object_attributes({
    retry_after: Int32
  })
end