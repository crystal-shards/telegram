# The session can be used
#

class TD::CanTransferOwnershipResult::Ok < TD::CanTransferOwnershipResult
  include JSON::Serializable

  object_type "canTransferOwnershipResultOk"
end