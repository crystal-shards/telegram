# Contains a list of requests to join a chat
#
# @total_count : Int32 (Approximate total count of requests found)
# @requests : Array(TD::ChatJoinRequest) (List of the requests)

class TD::ChatJoinRequests < TD::Base
  include JSON::Serializable

  object_type "chatJoinRequests"

  object_attributes({
    total_count: Int32,
    requests: Array(TD::ChatJoinRequest)
  })
end