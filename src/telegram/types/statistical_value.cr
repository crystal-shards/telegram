# A value with information about its recent changes
#
# @value : Float64 (The current value)
# @previous_value : Float64 (The value for the previous day)
# @growth_rate_percentage : Float64 (The growth rate of the value, as a percentage)

class TD::StatisticalValue < TD::Base
  include JSON::Serializable

  object_type "statisticalValue"

  object_attributes({
    value: Float64,
    previous_value: Float64,
    growth_rate_percentage: Float64
  })
end