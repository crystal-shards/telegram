# Contains content of a push message notification
#
# Hidden: A general message with hidden content
# Animation: An animation message (GIF-style).
# Audio: An audio message
# Contact: A message with a user contact
# ContactRegistered: A contact has registered with Telegram
# Document: A document message (a general file)
# Game: A message with a game
# GameScore: A new high score was achieved in a game
# Invoice: A message with an invoice from a bot
# Location: A message with a location
# Photo: A photo message
# Poll: A message with a poll
# ScreenshotTaken: A screenshot of a message in the chat has been taken
# Sticker: A message with a sticker
# Text: A text message
# Video: A video message
# VideoNote: A video note message
# VoiceNote: A voice note message
# BasicGroupChatCreate: A newly created basic group
# ChatAddMembers: New chat members were invited to a group
# ChatChangePhoto: A chat photo was edited
# ChatChangeTitle: A chat title was edited
# ChatSetTheme: A chat theme was edited
# ChatDeleteMember: A chat member was deleted
# ChatJoinByLink: A new member joined the chat via an invite link
# ChatJoinByRequest: A new member was accepted to the chat by an administrator
# MessageForwards: A forwarded messages
# MediaAlbum: A media album

abstract class TD::PushMessageContent < TD::Base
end