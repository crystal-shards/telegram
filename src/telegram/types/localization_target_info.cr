# Contains information about the current localization target
#
# @language_packs : Array(TD::LanguagePackInfo) (List of available language packs for this application)

class TD::LocalizationTargetInfo < TD::Base
  include JSON::Serializable

  object_type "localizationTargetInfo"

  object_attributes({
    language_packs: Array(TD::LanguagePackInfo)
  })
end