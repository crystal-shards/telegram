# Describes a video note. The video must be equal in width and height, cropped to a circle, and stored in MPEG4 format
#
# @duration : Int32 (Duration of the video, in seconds; as defined by the sender)
# @length : Int32 (Video width and height; as defined by the sender)
# @minithumbnail : TD::Minithumbnail? (Video minithumbnail; may be null)
# @thumbnail : TD::Thumbnail? (Video thumbnail in JPEG format; as defined by the sender; may be null)
# @video : TD::File (File containing the video)

class TD::VideoNote < TD::Base
  include JSON::Serializable

  object_type "videoNote"

  object_attributes({
    duration: Int32,
    length: Int32,
    minithumbnail: TD::Minithumbnail?,
    thumbnail: TD::Thumbnail?,
    video: TD::File
  })
end