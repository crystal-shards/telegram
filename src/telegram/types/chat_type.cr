# Describes the type of a chat
#
# Private: An ordinary chat with a user
# BasicGroup: A basic group (a chat with 0-200 other users)
# Supergroup: A supergroup or channel (with unlimited members)
# Secret: A secret chat with a user

abstract class TD::ChatType < TD::Base
end