# Represents a payload of a callback query
#
# Data: The payload for a general callback button
# DataWithPassword: The payload for a callback button requiring password
# Game: The payload for a game callback button

abstract class TD::CallbackQueryPayload < TD::Base
end