# Contains a list of chat lists
#
# @chat_lists : Array(TD::ChatList) (List of chat lists)

class TD::ChatLists < TD::Base
  include JSON::Serializable

  object_type "chatLists"

  object_attributes({
    chat_lists: Array(TD::ChatList)
  })
end