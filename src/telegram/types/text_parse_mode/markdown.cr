# The text uses Markdown-style formatting
#
# @version : Int32 (Version of the parser: 0 or 1 - Telegram Bot API "Markdown" parse mode, 2 - Telegram Bot API "MarkdownV2" parse mode)

class TD::TextParseMode::Markdown < TD::TextParseMode
  include JSON::Serializable

  object_type "textParseModeMarkdown"

  object_attributes({
    version: Int32
  })
end