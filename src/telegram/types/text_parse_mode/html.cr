# The text uses HTML-style formatting. The same as Telegram Bot API "HTML" parse mode
#

class TD::TextParseMode::HTML < TD::TextParseMode
  include JSON::Serializable

  object_type "textParseModeHTML"
end