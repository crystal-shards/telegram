# Contains information about one website the current user is logged in with Telegram
#
# @id : String (Website identifier)
# @domain_name : String (The domain name of the website)
# @bot_user_id : Int64 (User identifier of a bot linked with the website)
# @browser : String (The version of a browser used to log in)
# @platform : String (Operating system the browser is running on)
# @log_in_date : Int32 (Point in time (Unix timestamp) when the user was logged in)
# @last_active_date : Int32 (Point in time (Unix timestamp) when obtained authorization was last used)
# @ip : String (IP address from which the user was logged in, in human-readable format)
# @location : String (Human-readable description of a country and a region, from which the user was logged in, based on the IP address)

class TD::ConnectedWebsite < TD::Base
  include JSON::Serializable

  object_type "connectedWebsite"

  object_attributes({
    id: String,
    domain_name: String,
    bot_user_id: Int64,
    browser: String,
    platform: String,
    log_in_date: Int32,
    last_active_date: Int32,
    ip: String,
    location: String
  })
end