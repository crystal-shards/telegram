# The chat is sponsored by the user's MTProxy server
#

class TD::ChatSource::MtprotoProxy < TD::ChatSource
  include JSON::Serializable

  object_type "chatSourceMtprotoProxy"
end