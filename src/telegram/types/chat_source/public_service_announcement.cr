# The chat contains a public service announcement
#
# @type : String (The type of the announcement)
# @text : String (The text of the announcement)

class TD::ChatSource::PublicServiceAnnouncement < TD::ChatSource
  include JSON::Serializable

  object_type "chatSourcePublicServiceAnnouncement"

  object_attributes({
    type: String,
    text: String
  })
end