# Contains full information about a user
#
# @photo : TD::ChatPhoto? (User profile photo; may be null)
# @is_blocked : Bool (True, if the user is blocked by the current user)
# @can_be_called : Bool (True, if the user can be called)
# @supports_video_calls : Bool (True, if a video call can be created with the user)
# @has_private_calls : Bool (True, if the user can't be called due to their privacy settings)
# @has_private_forwards : Bool (True, if the user can't be linked in forwarded messages due to their privacy settings)
# @need_phone_number_privacy_exception : Bool (True, if the current user needs to explicitly allow to share their phone number with the user when the method addContact is used)
# @bio : String (A short user bio)
# @share_text : String (For bots, the text that is shown on the bot's profile page and is sent together with the link when users share the bot)
# @description : String (For bots, the text shown in the chat with the bot if the chat is empty)
# @group_in_common_count : Int32 (Number of group chats where both the other user and the current user are a member; 0 for the current user)
# @commands : Array(TD::BotCommand) (For bots, list of the bot commands)

class TD::UserFullInfo < TD::Base
  include JSON::Serializable

  object_type "userFullInfo"

  object_attributes({
    photo: TD::ChatPhoto?,
    is_blocked: Bool,
    can_be_called: Bool,
    supports_video_calls: Bool,
    has_private_calls: Bool,
    has_private_forwards: Bool,
    need_phone_number_privacy_exception: Bool,
    bio: String,
    share_text: String,
    description: String,
    group_in_common_count: Int32,
    commands: Array(TD::BotCommand)
  })
end