# Describes an image in JPEG format
#
# @type : String (Image type (see https:)
# @photo : TD::File (Information about the image file)
# @width : Int32 (Image width)
# @height : Int32 (Image height)
# @progressive_sizes : Array(Int32) (Sizes of progressive JPEG file prefixes, which can be used to preliminarily show the image; in bytes)

class TD::PhotoSize < TD::Base
  include JSON::Serializable

  object_type "photoSize"

  object_attributes({
    type: String,
    photo: TD::File,
    width: Int32,
    height: Int32,
    progressive_sizes: Array(Int32)
  })
end