# The log is written to a file
#
# @path : String (Path to the file to where the internal TDLib log will be written)
# @max_file_size : Int64 (The maximum size of the file to where the internal TDLib log is written before the file will automatically be rotated, in bytes)
# @redirect_stderr : Bool (Pass true to additionally redirect stderr to the log file. Ignored on Windows)

class TD::LogStream::File < TD::LogStream
  include JSON::Serializable

  object_type "logStreamFile"

  object_attributes({
    path: String,
    max_file_size: Int64,
    redirect_stderr: Bool
  })
end