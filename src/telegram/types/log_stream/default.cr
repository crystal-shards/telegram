# The log is written to stderr or an OS specific log
#

class TD::LogStream::Default < TD::LogStream
  include JSON::Serializable

  object_type "logStreamDefault"
end