# Describes a user that sent a join request and waits for administrator approval
#
# @user_id : Int64 (User identifier)
# @date : Int32 (Point in time (Unix timestamp) when the user sent the join request)
# @bio : String (A short bio of the user)

class TD::ChatJoinRequest < TD::Base
  include JSON::Serializable

  object_type "chatJoinRequest"

  object_attributes({
    user_id: Int64,
    date: Int32,
    bio: String
  })
end