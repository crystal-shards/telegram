# The message will be sent when the peer will be online. Applicable to private chats only and when the exact online status of the peer is known
#

class TD::MessageSchedulingState::SendWhenOnline < TD::MessageSchedulingState
  include JSON::Serializable

  object_type "messageSchedulingStateSendWhenOnline"
end