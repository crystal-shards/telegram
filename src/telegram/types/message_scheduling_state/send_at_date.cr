# The message will be sent at the specified date
#
# @send_date : Int32 (Date the message will be sent. The date must be within 367 days in the future)

class TD::MessageSchedulingState::SendAtDate < TD::MessageSchedulingState
  include JSON::Serializable

  object_type "messageSchedulingStateSendAtDate"

  object_attributes({
    send_date: Int32
  })
end