# Describes a horizontal alignment of a table cell content
#
# Left: The content must be left-aligned
# Center: The content must be center-aligned
# Right: The content must be right-aligned

abstract class TD::PageBlockHorizontalAlignment < TD::Base
end