# Represents a data needed to subscribe for push notifications through registerDevice method. To use specific push notification service, the correct application platform must be specified and a valid server authentication data must be uploaded at https:
#
# FirebaseCloudMessaging: A token for Firebase Cloud Messaging
# ApplePush: A token for Apple Push Notification service
# ApplePushVoIP: A token for Apple Push Notification service VoIP notifications
# WindowsPush: A token for Windows Push Notification Services
# MicrosoftPush: A token for Microsoft Push Notification Service
# MicrosoftPushVoIP: A token for Microsoft Push Notification Service VoIP channel
# WebPush: A token for web Push API
# SimplePush: A token for Simple Push API for Firefox OS
# UbuntuPush: A token for Ubuntu Push Client service
# BlackBerryPush: A token for BlackBerry Push Service
# TizenPush: A token for Tizen Push Service

abstract class TD::DeviceToken < TD::Base
end