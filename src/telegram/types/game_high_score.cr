# Contains one row of the game high score table
#
# @position : Int32 (Position in the high score table)
# @user_id : Int64 (User identifier)
# @score : Int32 (User score)

class TD::GameHighScore < TD::Base
  include JSON::Serializable

  object_type "gameHighScore"

  object_attributes({
    position: Int32,
    user_id: Int64,
    score: Int32
  })
end