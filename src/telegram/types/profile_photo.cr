# Describes a user profile photo
#
# @id : String (Photo identifier; 0 for an empty photo. Can be used to find a photo in a list of user profile photos)
# @small : TD::File (A small (160x160) user profile photo. The file can be downloaded only before the photo is changed)
# @big : TD::File (A big (640x640) user profile photo. The file can be downloaded only before the photo is changed)
# @minithumbnail : TD::Minithumbnail? (User profile photo minithumbnail; may be null)
# @has_animation : Bool (True, if the photo has animated variant)

class TD::ProfilePhoto < TD::Base
  include JSON::Serializable

  object_type "profilePhoto"

  object_attributes({
    id: String,
    small: TD::File,
    big: TD::File,
    minithumbnail: TD::Minithumbnail?,
    has_animation: Bool
  })
end