# Represents a list of animations
#
# @animations : Array(TD::Animation) (List of animations)

class TD::Animations < TD::Base
  include JSON::Serializable

  object_type "animations"

  object_attributes({
    animations: Array(TD::Animation)
  })
end