# The user is unregistered and need to accept terms of service and enter their first name and last name to finish registration
#
# @terms_of_service : TD::TermsOfService (Telegram terms of service)

class TD::AuthorizationState::WaitRegistration < TD::AuthorizationState
  include JSON::Serializable

  object_type "authorizationStateWaitRegistration"

  object_attributes({
    terms_of_service: TD::TermsOfService
  })
end