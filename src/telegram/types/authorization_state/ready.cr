# The user has been successfully authorized. TDLib is now ready to answer queries
#

class TD::AuthorizationState::Ready < TD::AuthorizationState
  include JSON::Serializable

  object_type "authorizationStateReady"
end