# The user needs to confirm authorization on another logged in device by scanning a QR code with the provided link
#
# @link : String (A tg:)

class TD::AuthorizationState::WaitOtherDeviceConfirmation < TD::AuthorizationState
  include JSON::Serializable

  object_type "authorizationStateWaitOtherDeviceConfirmation"

  object_attributes({
    link: String
  })
end