# TDLib needs TdlibParameters for initialization
#

class TD::AuthorizationState::WaitTdlibParameters < TD::AuthorizationState
  include JSON::Serializable

  object_type "authorizationStateWaitTdlibParameters"
end