# TDLib needs the user's authentication code to authorize
#
# @code_info : TD::AuthenticationCodeInfo (Information about the authorization code that was sent)

class TD::AuthorizationState::WaitCode < TD::AuthorizationState
  include JSON::Serializable

  object_type "authorizationStateWaitCode"

  object_attributes({
    code_info: TD::AuthenticationCodeInfo
  })
end