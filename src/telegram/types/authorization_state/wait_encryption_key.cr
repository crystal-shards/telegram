# TDLib needs an encryption key to decrypt the local database
#
# @is_encrypted : Bool (True, if the database is currently encrypted)

class TD::AuthorizationState::WaitEncryptionKey < TD::AuthorizationState
  include JSON::Serializable

  object_type "authorizationStateWaitEncryptionKey"

  object_attributes({
    is_encrypted: Bool
  })
end