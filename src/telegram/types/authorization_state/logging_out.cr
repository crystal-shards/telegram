# The user is currently logging out
#

class TD::AuthorizationState::LoggingOut < TD::AuthorizationState
  include JSON::Serializable

  object_type "authorizationStateLoggingOut"
end