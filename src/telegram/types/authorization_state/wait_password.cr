# The user has been authorized, but needs to enter a password to start using the application
#
# @password_hint : String (Hint for the password; may be empty)
# @has_recovery_email_address : Bool (True, if a recovery email address has been set up)
# @recovery_email_address_pattern : String (Pattern of the email address to which the recovery email was sent; empty until a recovery email has been sent)

class TD::AuthorizationState::WaitPassword < TD::AuthorizationState
  include JSON::Serializable

  object_type "authorizationStateWaitPassword"

  object_attributes({
    password_hint: String,
    has_recovery_email_address: Bool,
    recovery_email_address_pattern: String
  })
end