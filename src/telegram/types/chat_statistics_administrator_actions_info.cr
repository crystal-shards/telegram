# Contains statistics about administrator actions done by a user
#
# @user_id : Int64 (Administrator user identifier)
# @deleted_message_count : Int32 (Number of messages deleted by the administrator)
# @banned_user_count : Int32 (Number of users banned by the administrator)
# @restricted_user_count : Int32 (Number of users restricted by the administrator)

class TD::ChatStatisticsAdministratorActionsInfo < TD::Base
  include JSON::Serializable

  object_type "chatStatisticsAdministratorActionsInfo"

  object_attributes({
    user_id: Int64,
    deleted_message_count: Int32,
    banned_user_count: Int32,
    restricted_user_count: Int32
  })
end