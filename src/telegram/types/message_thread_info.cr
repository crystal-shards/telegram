# Contains information about a message thread
#
# @chat_id : Int64 (Identifier of the chat to which the message thread belongs)
# @message_thread_id : Int64 (Message thread identifier, unique within the chat)
# @reply_info : TD::MessageReplyInfo (Information about the message thread)
# @unread_message_count : Int32 (Approximate number of unread messages in the message thread)
# @messages : Array(TD::Message) (The messages from which the thread starts. The messages are returned in a reverse chronological order (i.e., in order of decreasing message_id))
# @draft_message : TD::DraftMessage? (A draft of a message in the message thread; may be null)

class TD::MessageThreadInfo < TD::Base
  include JSON::Serializable

  object_type "messageThreadInfo"

  object_attributes({
    chat_id: Int64,
    message_thread_id: Int64,
    reply_info: TD::MessageReplyInfo,
    unread_message_count: Int32,
    messages: Array(TD::Message),
    draft_message: TD::DraftMessage?
  })
end