# Represents a chat event
#
# @id : String (Chat event identifier)
# @date : Int32 (Point in time (Unix timestamp) when the event happened)
# @member_id : TD::MessageSender (Identifier of the user or chat who performed the action)
# @action : TD::ChatEventAction (The action)

class TD::ChatEvent < TD::Base
  include JSON::Serializable

  object_type "chatEvent"

  object_attributes({
    id: String,
    date: Int32,
    member_id: TD::MessageSender,
    action: TD::ChatEventAction
  })
end