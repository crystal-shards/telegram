# Contains detailed information about a notification
#
# NewMessage: New message was received
# NewSecretChat: New secret chat was created
# NewCall: New call was received
# NewPushMessage: New message was received through a push notification

abstract class TD::NotificationType < TD::Base
end