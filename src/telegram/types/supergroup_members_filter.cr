# Specifies the kind of chat members to return in getSupergroupMembers
#
# Recent: Returns recently active users in reverse chronological order
# Contacts: Returns contacts of the user, which are members of the supergroup or channel
# Administrators: Returns the owner and administrators
# Search: Used to search for supergroup or channel members via a (string) query
# Restricted: Returns restricted supergroup members; can be used only by administrators
# Banned: Returns users banned from the supergroup or channel; can be used only by administrators
# Mention: Returns users which can be mentioned in the supergroup
# Bots: Returns bot members of the supergroup or channel

abstract class TD::SupergroupMembersFilter < TD::Base
end