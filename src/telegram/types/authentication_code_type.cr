# Provides information about the method by which an authentication code is delivered to the user
#
# TelegramMessage: An authentication code is delivered via a private Telegram message, which can be viewed from another active session
# Sms: An authentication code is delivered via an SMS message to the specified phone number
# Call: An authentication code is delivered via a phone call to the specified phone number
# FlashCall: An authentication code is delivered by an immediately canceled call to the specified phone number. The phone number that calls is the code that must be entered automatically
# MissedCall: An authentication code is delivered by an immediately canceled call to the specified phone number. The last digits of the phone number that calls are the code that must be entered manually by the user

abstract class TD::AuthenticationCodeType < TD::Base
end