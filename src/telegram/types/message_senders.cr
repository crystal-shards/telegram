# Represents a list of message senders
#
# @total_count : Int32 (Approximate total count of messages senders found)
# @senders : Array(TD::MessageSender) (List of message senders)

class TD::MessageSenders < TD::Base
  include JSON::Serializable

  object_type "messageSenders"

  object_attributes({
    total_count: Int32,
    senders: Array(TD::MessageSender)
  })
end