# Contains statistics about messages sent by a user
#
# @user_id : Int64 (User identifier)
# @sent_message_count : Int32 (Number of sent messages)
# @average_character_count : Int32 (Average number of characters in sent messages; 0 if unknown)

class TD::ChatStatisticsMessageSenderInfo < TD::Base
  include JSON::Serializable

  object_type "chatStatisticsMessageSenderInfo"

  object_attributes({
    user_id: Int64,
    sent_message_count: Int32,
    average_character_count: Int32
  })
end