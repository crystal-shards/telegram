# A PNG or TGV (gzipped subset of SVG with MIME type "application
#
# @fill : TD::BackgroundFill (Fill of the background)
# @intensity : Int32 (Intensity of the pattern when it is shown above the filled background; 0-100.)
# @is_inverted : Bool (True, if the background fill must be applied only to the pattern itself. All other pixels are black in this case. For dark themes only)
# @is_moving : Bool (True, if the background needs to be slightly moved when device is tilted)

class TD::BackgroundType::Pattern < TD::BackgroundType
  include JSON::Serializable

  object_type "backgroundTypePattern"

  object_attributes({
    fill: TD::BackgroundFill,
    intensity: Int32,
    is_inverted: Bool,
    is_moving: Bool
  })
end