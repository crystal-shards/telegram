# A wallpaper in JPEG format
#
# @is_blurred : Bool (True, if the wallpaper must be downscaled to fit in 450x450 square and then box-blurred with radius 12)
# @is_moving : Bool (True, if the background needs to be slightly moved when device is tilted)

class TD::BackgroundType::Wallpaper < TD::BackgroundType
  include JSON::Serializable

  object_type "backgroundTypeWallpaper"

  object_attributes({
    is_blurred: Bool,
    is_moving: Bool
  })
end