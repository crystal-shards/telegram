# A filled background
#
# @fill : TD::BackgroundFill (The background fill)

class TD::BackgroundType::Fill < TD::BackgroundType
  include JSON::Serializable

  object_type "backgroundTypeFill"

  object_attributes({
    fill: TD::BackgroundFill
  })
end