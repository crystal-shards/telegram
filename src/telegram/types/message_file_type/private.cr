# The messages was exported from a private chat
#
# @name : String (Name of the other party; may be empty if unrecognized)

class TD::MessageFileType::Private < TD::MessageFileType
  include JSON::Serializable

  object_type "messageFileTypePrivate"

  object_attributes({
    name: String
  })
end