# The messages was exported from a chat of unknown type
#

class TD::MessageFileType::Unknown < TD::MessageFileType
  include JSON::Serializable

  object_type "messageFileTypeUnknown"
end