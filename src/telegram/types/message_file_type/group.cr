# The messages was exported from a group chat
#
# @title : String (Title of the group chat; may be empty if unrecognized)

class TD::MessageFileType::Group < TD::MessageFileType
  include JSON::Serializable

  object_type "messageFileTypeGroup"

  object_attributes({
    title: String
  })
end