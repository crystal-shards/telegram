# Contains encrypted Telegram Passport data credentials
#
# @data : String (The encrypted credentials)
# @hash : String (The decrypted data hash)
# @secret : String (Secret for data decryption, encrypted with the service's public key)

class TD::EncryptedCredentials < TD::Base
  include JSON::Serializable

  object_type "encryptedCredentials"

  object_attributes({
    data: String,
    hash: String,
    secret: String
  })
end