# An error message to be shown to the user instead of the graph
#
# @error_message : String (The error message)

class TD::StatisticalGraph::Error < TD::StatisticalGraph
  include JSON::Serializable

  object_type "statisticalGraphError"

  object_attributes({
    error_message: String
  })
end