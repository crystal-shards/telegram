# The graph data to be asynchronously loaded through getStatisticalGraph
#
# @token : String (The token to use for data loading)

class TD::StatisticalGraph::Async < TD::StatisticalGraph
  include JSON::Serializable

  object_type "statisticalGraphAsync"

  object_attributes({
    token: String
  })
end