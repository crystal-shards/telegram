# A graph data
#
# @json_data : String (Graph data in JSON format)
# @zoom_token : String (If non-empty, a token which can be used to receive a zoomed in graph)

class TD::StatisticalGraph::Data < TD::StatisticalGraph
  include JSON::Serializable

  object_type "statisticalGraphData"

  object_attributes({
    json_data: String,
    zoom_token: String
  })
end