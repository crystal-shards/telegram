# Contains a list of chat invite links
#
# @total_count : Int32 (Approximate total count of chat invite links found)
# @invite_links : Array(TD::ChatInviteLink) (List of invite links)

class TD::ChatInviteLinks < TD::Base
  include JSON::Serializable

  object_type "chatInviteLinks"

  object_attributes({
    total_count: Int32,
    invite_links: Array(TD::ChatInviteLink)
  })
end