# Contains information about a successful payment
#
# @title : String (Product title)
# @description : String (Product description)
# @photo : TD::Photo? (Product photo; may be null)
# @date : Int32 (Point in time (Unix timestamp) when the payment was made)
# @seller_bot_user_id : Int64 (User identifier of the seller bot)
# @payments_provider_user_id : Int64 (User identifier of the payment provider bot)
# @invoice : TD::Invoice (Information about the invoice)
# @order_info : TD::OrderInfo? (Order information; may be null)
# @shipping_option : TD::ShippingOption? (Chosen shipping option; may be null)
# @credentials_title : String (Title of the saved credentials chosen by the buyer)
# @tip_amount : Int64 (The amount of tip chosen by the buyer in the smallest units of the currency)

class TD::PaymentReceipt < TD::Base
  include JSON::Serializable

  object_type "paymentReceipt"

  object_attributes({
    title: String,
    description: String,
    photo: TD::Photo?,
    date: Int32,
    seller_bot_user_id: Int64,
    payments_provider_user_id: Int64,
    invoice: TD::Invoice,
    order_info: TD::OrderInfo?,
    shipping_option: TD::ShippingOption?,
    credentials_title: String,
    tip_amount: Int64
  })
end