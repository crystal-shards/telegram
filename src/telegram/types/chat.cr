# A chat. (Can be a private chat, basic group, supergroup, or secret chat)
#
# @id : Int64 (Chat unique identifier)
# @type : TD::ChatType (Type of the chat)
# @title : String (Chat title)
# @photo : TD::ChatPhotoInfo? (Chat photo; may be null)
# @permissions : TD::ChatPermissions (Actions that non-administrator chat members are allowed to take in the chat)
# @last_message : TD::Message? (Last message in the chat; may be null)
# @positions : Array(TD::ChatPosition) (Positions of the chat in chat lists)
# @message_sender_id : TD::MessageSender? (Identifier of a user or chat that is selected to send messages in the chat; may be null if the user can't change message sender)
# @has_protected_content : Bool (True, if chat content can't be saved locally, forwarded, or copied)
# @is_marked_as_unread : Bool (True, if the chat is marked as unread)
# @is_blocked : Bool (True, if the chat is blocked by the current user and private messages from the chat can't be received)
# @has_scheduled_messages : Bool (True, if the chat has scheduled messages)
# @can_be_deleted_only_for_self : Bool (True, if the chat messages can be deleted only for the current user while other users will continue to see the messages)
# @can_be_deleted_for_all_users : Bool (True, if the chat messages can be deleted for all users)
# @can_be_reported : Bool (True, if the chat can be reported to Telegram moderators through reportChat or reportChatPhoto)
# @default_disable_notification : Bool (Default value of the disable_notification parameter, used when a message is sent to the chat)
# @unread_count : Int32 (Number of unread messages in the chat)
# @last_read_inbox_message_id : Int64 (Identifier of the last read incoming message)
# @last_read_outbox_message_id : Int64 (Identifier of the last read outgoing message)
# @unread_mention_count : Int32 (Number of unread messages with a mention)
# @notification_settings : TD::ChatNotificationSettings (Notification settings for this chat)
# @message_ttl : Int32 (Current message Time To Live setting (self-destruct timer) for the chat; 0 if not defined. TTL is counted from the time message or its content is viewed in secret chats and from the send date in other chats)
# @theme_name : String (If non-empty, name of a theme, set for the chat)
# @action_bar : TD::ChatActionBar? (Information about actions which must be possible to do through the chat action bar; may be null)
# @video_chat : TD::VideoChat (Information about video chat of the chat)
# @pending_join_requests : TD::ChatJoinRequestsInfo? (Information about pending join requests; may be null)
# @reply_markup_message_id : Int64 (Identifier of the message from which reply markup needs to be used; 0 if there is no default custom reply markup in the chat)
# @draft_message : TD::DraftMessage? (A draft of a message in the chat; may be null)
# @client_data : String (Application-specific data associated with the chat. (For example, the chat scroll position or local chat notification settings can be stored here.) Persistent if the message database is used)

class TD::Chat < TD::Base
  include JSON::Serializable

  object_type "chat"

  object_attributes({
    id: Int64,
    type: TD::ChatType,
    title: String,
    photo: TD::ChatPhotoInfo?,
    permissions: TD::ChatPermissions,
    last_message: TD::Message?,
    positions: Array(TD::ChatPosition),
    message_sender_id: TD::MessageSender?,
    has_protected_content: Bool,
    is_marked_as_unread: Bool,
    is_blocked: Bool,
    has_scheduled_messages: Bool,
    can_be_deleted_only_for_self: Bool,
    can_be_deleted_for_all_users: Bool,
    can_be_reported: Bool,
    default_disable_notification: Bool,
    unread_count: Int32,
    last_read_inbox_message_id: Int64,
    last_read_outbox_message_id: Int64,
    unread_mention_count: Int32,
    notification_settings: TD::ChatNotificationSettings,
    message_ttl: Int32,
    theme_name: String,
    action_bar: TD::ChatActionBar?,
    video_chat: TD::VideoChat,
    pending_join_requests: TD::ChatJoinRequestsInfo?,
    reply_markup_message_id: Int64,
    draft_message: TD::DraftMessage?,
    client_data: String
  })
end