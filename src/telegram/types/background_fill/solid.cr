# Describes a solid fill of a background
#
# @color : Int32 (A color of the background in the RGB24 format)

class TD::BackgroundFill::Solid < TD::BackgroundFill
  include JSON::Serializable

  object_type "backgroundFillSolid"

  object_attributes({
    color: Int32
  })
end