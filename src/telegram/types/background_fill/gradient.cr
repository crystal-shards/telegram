# Describes a gradient fill of a background
#
# @top_color : Int32 (A top color of the background in the RGB24 format)
# @bottom_color : Int32 (A bottom color of the background in the RGB24 format)
# @rotation_angle : Int32 (Clockwise rotation angle of the gradient, in degrees; 0-359. Must be always divisible by 45)

class TD::BackgroundFill::Gradient < TD::BackgroundFill
  include JSON::Serializable

  object_type "backgroundFillGradient"

  object_attributes({
    top_color: Int32,
    bottom_color: Int32,
    rotation_angle: Int32
  })
end