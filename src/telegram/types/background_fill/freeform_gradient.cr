# Describes a freeform gradient fill of a background
#
# @colors : Array(Int32) (A list of 3 or 4 colors of the freeform gradients in the RGB24 format)

class TD::BackgroundFill::FreeformGradient < TD::BackgroundFill
  include JSON::Serializable

  object_type "backgroundFillFreeformGradient"

  object_attributes({
    colors: Array(Int32)
  })
end