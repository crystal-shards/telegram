# The content must be bottom-aligned
#

class TD::PageBlockVerticalAlignment::Bottom < TD::PageBlockVerticalAlignment
  include JSON::Serializable

  object_type "pageBlockVerticalAlignmentBottom"
end