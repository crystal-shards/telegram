# The content must be middle-aligned
#

class TD::PageBlockVerticalAlignment::Middle < TD::PageBlockVerticalAlignment
  include JSON::Serializable

  object_type "pageBlockVerticalAlignmentMiddle"
end