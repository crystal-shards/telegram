# The content must be top-aligned
#

class TD::PageBlockVerticalAlignment::Top < TD::PageBlockVerticalAlignment
  include JSON::Serializable

  object_type "pageBlockVerticalAlignmentTop"
end