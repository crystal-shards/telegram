# Describes an internal https:
#
# ActiveSessions: The link is a link to the active sessions section of the app. Use getActiveSessions to handle the link
# AuthenticationCode: The link contains an authentication code. Call checkAuthenticationCode with the code if the current authorization state is authorizationStateWaitCode
# Background: The link is a link to a background. Call searchBackground with the given background name to process the link
# BotStart: The link is a link to a chat with a Telegram bot. Call searchPublicChat with the given bot username, check that the user is a bot, show START button in the chat with the bot,and then call sendBotStartMessage with the given start parameter after the button is pressed
# BotStartInGroup: The link is a link to a Telegram bot, which is supposed to be added to a group chat. Call searchPublicChat with the given bot username, check that the user is a bot and can be added to groups,ask the current user to select a group to add the bot to, and then call sendBotStartMessage with the given start parameter and the chosen group chat. Bots can be added to a public group only by administrators of the group
# ChangePhoneNumber: The link is a link to the change phone number section of the app
# ChatInvite: The link is a chat invite link. Call checkChatInviteLink with the given invite link to process the link
# FilterSettings: The link is a link to the filter settings section of the app
# Game: The link is a link to a game. Call searchPublicChat with the given bot username, check that the user is a bot, ask the current user to select a chat to send the game, and then call sendMessage with inputMessageGame
# LanguagePack: The link is a link to a language pack. Call getLanguagePackInfo with the given language pack identifier to process the link
# Message: The link is a link to a Telegram message. Call getMessageLinkInfo with the given URL to process the link
# MessageDraft: The link contains a message draft text. A share screen needs to be shown to the user, then the chosen chat must be opened and the text is added to the input field
# PassportDataRequest: The link contains a request of Telegram passport data. Call getPassportAuthorizationForm with the given parameters to process the link if the link was received from outside of the app, otherwise ignore it
# PhoneNumberConfirmation: The link can be used to confirm ownership of a phone number to prevent account deletion. Call sendPhoneNumberConfirmationCode with the given hash and phone number to process the link
# Proxy: The link is a link to a proxy. Call addProxy with the given parameters to process the link and add the proxy
# PublicChat: The link is a link to a chat by its username. Call searchPublicChat with the given chat username to process the link
# QrCodeAuthentication: The link can be used to login the current user on another device, but it must be scanned from QR-code using in-app camera. An alert similar to"This code can be used to allow someone to log in to your Telegram account. To confirm Telegram login, please go to Settings
# Settings: The link is a link to app settings
# StickerSet: The link is a link to a sticker set. Call searchStickerSet with the given sticker set name to process the link and show the sticker set
# Theme: The link is a link to a theme. TDLib has no theme support yet
# ThemeSettings: The link is a link to the theme settings section of the app
# UnknownDeepLink: The link is an unknown tg: link. Call getDeepLinkInfo to process the link
# UnsupportedProxy: The link is a link to an unsupported proxy. An alert can be shown to the user
# VideoChat: The link is a link to a video chat. Call searchPublicChat with the given chat username, and then joinGoupCall with the given invite hash to process the link

abstract class TD::InternalLinkType < TD::Base
end