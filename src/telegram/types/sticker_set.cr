# Represents a sticker set
#
# @id : String (Identifier of the sticker set)
# @title : String (Title of the sticker set)
# @name : String (Name of the sticker set)
# @thumbnail : TD::Thumbnail? (Sticker set thumbnail in WEBP or TGS format with width and height 100; may be null. The file can be downloaded only before the thumbnail is changed)
# @thumbnail_outline : Array(TD::ClosedVectorPath) (Sticker set thumbnail's outline represented as a list of closed vector paths; may be empty. The coordinate system origin is in the upper-left corner)
# @is_installed : Bool (True, if the sticker set has been installed by the current user)
# @is_archived : Bool (True, if the sticker set has been archived. A sticker set can't be installed and archived simultaneously)
# @is_official : Bool (True, if the sticker set is official)
# @is_animated : Bool (True, is the stickers in the set are animated)
# @is_masks : Bool (True, if the stickers in the set are masks)
# @is_viewed : Bool (True for already viewed trending sticker sets)
# @stickers : Array(TD::Sticker) (List of stickers in this set)
# @emojis : Array(TD::Emojis) (A list of emoji corresponding to the stickers in the same order. The list is only for informational purposes, because a sticker is always sent with a fixed emoji from the corresponding Sticker object)

class TD::StickerSet < TD::Base
  include JSON::Serializable

  object_type "stickerSet"

  object_attributes({
    id: String,
    title: String,
    name: String,
    thumbnail: TD::Thumbnail?,
    thumbnail_outline: Array(TD::ClosedVectorPath),
    is_installed: Bool,
    is_archived: Bool,
    is_official: Bool,
    is_animated: Bool,
    is_masks: Bool,
    is_viewed: Bool,
    stickers: Array(TD::Sticker),
    emojis: Array(TD::Emojis)
  })
end