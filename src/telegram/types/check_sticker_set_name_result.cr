# Represents result of checking whether a name can be used for a new sticker set
#
# Ok: The name can be set
# NameInvalid: The name is invalid
# NameOccupied: The name is occupied

abstract class TD::CheckStickerSetNameResult < TD::Base
end