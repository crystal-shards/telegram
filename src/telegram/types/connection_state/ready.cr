# There is a working connection to the Telegram servers
#

class TD::ConnectionState::Ready < TD::ConnectionState
  include JSON::Serializable

  object_type "connectionStateReady"
end