# Currently waiting for the network to become available. Use setNetworkType to change the available network type
#

class TD::ConnectionState::WaitingForNetwork < TD::ConnectionState
  include JSON::Serializable

  object_type "connectionStateWaitingForNetwork"
end