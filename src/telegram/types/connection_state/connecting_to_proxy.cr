# Currently establishing a connection with a proxy server
#

class TD::ConnectionState::ConnectingToProxy < TD::ConnectionState
  include JSON::Serializable

  object_type "connectionStateConnectingToProxy"
end