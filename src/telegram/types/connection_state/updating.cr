# Downloading data received while the application was offline
#

class TD::ConnectionState::Updating < TD::ConnectionState
  include JSON::Serializable

  object_type "connectionStateUpdating"
end