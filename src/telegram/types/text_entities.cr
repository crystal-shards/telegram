# Contains a list of text entities
#
# @entities : Array(TD::TextEntity) (List of text entities)

class TD::TextEntities < TD::Base
  include JSON::Serializable

  object_type "textEntities"

  object_attributes({
    entities: Array(TD::TextEntity)
  })
end