# A static sticker in PNG format, which will be converted to WEBP server-side
#
# @sticker : TD::InputFile (PNG image with the sticker; must be up to 512 KB in size and fit in a 512x512 square)
# @emojis : String (Emojis corresponding to the sticker)
# @mask_position : TD::MaskPosition (For masks, position where the mask is placed; pass null if unspecified)

class TD::InputSticker::Static < TD::InputSticker
  include JSON::Serializable

  object_type "inputStickerStatic"

  object_attributes({
    sticker: TD::InputFile,
    emojis: String,
    mask_position: TD::MaskPosition
  })
end