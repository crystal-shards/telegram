# An animated sticker in TGS format
#
# @sticker : TD::InputFile (File with the animated sticker. Only local or uploaded within a week files are supported. See https:)
# @emojis : String (Emojis corresponding to the sticker)

class TD::InputSticker::Animated < TD::InputSticker
  include JSON::Serializable

  object_type "inputStickerAnimated"

  object_attributes({
    sticker: TD::InputFile,
    emojis: String
  })
end