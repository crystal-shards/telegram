# Contains a list of chat or user profile photos
#
# @total_count : Int32 (Total number of photos)
# @photos : Array(TD::ChatPhoto) (List of photos)

class TD::ChatPhotos < TD::Base
  include JSON::Serializable

  object_type "chatPhotos"

  object_attributes({
    total_count: Int32,
    photos: Array(TD::ChatPhoto)
  })
end