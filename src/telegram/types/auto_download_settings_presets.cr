# Contains auto-download settings presets for the current user
#
# @low : TD::AutoDownloadSettings (Preset with lowest settings; supposed to be used by default when roaming)
# @medium : TD::AutoDownloadSettings (Preset with medium settings; supposed to be used by default when using mobile data)
# @high : TD::AutoDownloadSettings (Preset with highest settings; supposed to be used by default when connected on Wi-Fi)

class TD::AutoDownloadSettingsPresets < TD::Base
  include JSON::Serializable

  object_type "autoDownloadSettingsPresets"

  object_attributes({
    low: TD::AutoDownloadSettings,
    medium: TD::AutoDownloadSettings,
    high: TD::AutoDownloadSettings
  })
end