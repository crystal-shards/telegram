# Contains information about a Telegram Passport element that was requested by a service
#
# @type : TD::PassportElementType (Type of the element)
# @is_selfie_required : Bool (True, if a selfie is required with the identity document)
# @is_translation_required : Bool (True, if a certified English translation is required with the document)
# @is_native_name_required : Bool (True, if personal details must include the user's name in the language of their country of residence)

class TD::PassportSuitableElement < TD::Base
  include JSON::Serializable

  object_type "passportSuitableElement"

  object_attributes({
    type: TD::PassportElementType,
    is_selfie_required: Bool,
    is_translation_required: Bool,
    is_native_name_required: Bool
  })
end