# Represents the results of the inline query. Use sendInlineQueryResultMessage to send the result of the query
#
# @inline_query_id : String (Unique identifier of the inline query)
# @next_offset : String (The offset for the next request. If empty, there are no more results)
# @results : Array(TD::InlineQueryResult) (Results of the query)
# @switch_pm_text : String (If non-empty, this text must be shown on the button, which opens a private chat with the bot and sends the bot a start message with the switch_pm_parameter)
# @switch_pm_parameter : String (Parameter for the bot start message)

class TD::InlineQueryResults < TD::Base
  include JSON::Serializable

  object_type "inlineQueryResults"

  object_attributes({
    inline_query_id: String,
    next_offset: String,
    results: Array(TD::InlineQueryResult),
    switch_pm_text: String,
    switch_pm_parameter: String
  })
end