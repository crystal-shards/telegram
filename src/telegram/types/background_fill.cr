# Describes a fill of a background
#
# Solid: Describes a solid fill of a background
# Gradient: Describes a gradient fill of a background
# FreeformGradient: Describes a freeform gradient fill of a background

abstract class TD::BackgroundFill < TD::Base
end