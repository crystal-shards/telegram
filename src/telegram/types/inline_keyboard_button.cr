# Represents a single button in an inline keyboard
#
# @text : String (Text of the button)
# @type : TD::InlineKeyboardButtonType (Type of the button)

class TD::InlineKeyboardButton < TD::Base
  include JSON::Serializable

  object_type "inlineKeyboardButton"

  object_attributes({
    text: String,
    type: TD::InlineKeyboardButtonType
  })
end