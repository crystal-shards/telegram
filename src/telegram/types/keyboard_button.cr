# Represents a single button in a bot keyboard
#
# @text : String (Text of the button)
# @type : TD::KeyboardButtonType (Type of the button)

class TD::KeyboardButton < TD::Base
  include JSON::Serializable

  object_type "keyboardButton"

  object_attributes({
    text: String,
    type: TD::KeyboardButtonType
  })
end