# Contains information about replies to a message
#
# @reply_count : Int32 (Number of times the message was directly or indirectly replied)
# @recent_replier_ids : Array(TD::MessageSender) (Identifiers of at most 3 recent repliers to the message; available in channels with a discussion supergroup. The users and chats are expected to be inaccessible: only their photo and name will be available)
# @last_read_inbox_message_id : Int64 (Identifier of the last read incoming reply to the message)
# @last_read_outbox_message_id : Int64 (Identifier of the last read outgoing reply to the message)
# @last_message_id : Int64 (Identifier of the last reply to the message)

class TD::MessageReplyInfo < TD::Base
  include JSON::Serializable

  object_type "messageReplyInfo"

  object_attributes({
    reply_count: Int32,
    recent_replier_ids: Array(TD::MessageSender),
    last_read_inbox_message_id: Int64,
    last_read_outbox_message_id: Int64,
    last_message_id: Int64
  })
end