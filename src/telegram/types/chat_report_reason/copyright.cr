# The chat contains copyrighted content
#

class TD::ChatReportReason::Copyright < TD::ChatReportReason
  include JSON::Serializable

  object_type "chatReportReasonCopyright"
end