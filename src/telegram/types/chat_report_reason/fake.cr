# The chat represents a fake account
#

class TD::ChatReportReason::Fake < TD::ChatReportReason
  include JSON::Serializable

  object_type "chatReportReasonFake"
end