# A custom reason provided by the user
#

class TD::ChatReportReason::Custom < TD::ChatReportReason
  include JSON::Serializable

  object_type "chatReportReasonCustom"
end