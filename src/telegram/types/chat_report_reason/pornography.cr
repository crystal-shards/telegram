# The chat contains pornographic messages
#

class TD::ChatReportReason::Pornography < TD::ChatReportReason
  include JSON::Serializable

  object_type "chatReportReasonPornography"
end