# The chat contains spam messages
#

class TD::ChatReportReason::Spam < TD::ChatReportReason
  include JSON::Serializable

  object_type "chatReportReasonSpam"
end