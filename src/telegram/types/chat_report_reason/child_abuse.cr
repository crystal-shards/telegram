# The chat has child abuse related content
#

class TD::ChatReportReason::ChildAbuse < TD::ChatReportReason
  include JSON::Serializable

  object_type "chatReportReasonChildAbuse"
end