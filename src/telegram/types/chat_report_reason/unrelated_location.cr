# The location-based chat is unrelated to its stated location
#

class TD::ChatReportReason::UnrelatedLocation < TD::ChatReportReason
  include JSON::Serializable

  object_type "chatReportReasonUnrelatedLocation"
end