# The chat promotes violence
#

class TD::ChatReportReason::Violence < TD::ChatReportReason
  include JSON::Serializable

  object_type "chatReportReasonViolence"
end