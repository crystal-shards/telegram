# A photo
#
# @photo : TD::Photo? (Photo file; may be null)
# @caption : TD::PageBlockCaption (Photo caption)
# @url : String (URL that needs to be opened when the photo is clicked)

class TD::PageBlock::Photo < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockPhoto"

  object_attributes({
    photo: TD::Photo?,
    caption: TD::PageBlockCaption,
    url: String
  })
end