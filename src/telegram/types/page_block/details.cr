# A collapsible block
#
# @header : TD::RichText (Always visible heading for the block)
# @page_blocks : Array(TD::PageBlock) (Block contents)
# @is_open : Bool (True, if the block is open by default)

class TD::PageBlock::Details < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockDetails"

  object_attributes({
    header: TD::RichText,
    page_blocks: Array(TD::PageBlock),
    is_open: Bool
  })
end