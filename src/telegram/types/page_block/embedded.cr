# An embedded web page
#
# @url : String? (Web page URL, if available)
# @html : String (HTML-markup of the embedded page)
# @poster_photo : TD::Photo? (Poster photo, if available; may be null)
# @width : Int32 (Block width; 0 if unknown)
# @height : Int32 (Block height; 0 if unknown)
# @caption : TD::PageBlockCaption (Block caption)
# @is_full_width : Bool (True, if the block must be full width)
# @allow_scrolling : Bool (True, if scrolling needs to be allowed)

class TD::PageBlock::Embedded < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockEmbedded"

  object_attributes({
    url: String?,
    html: String,
    poster_photo: TD::Photo?,
    width: Int32,
    height: Int32,
    caption: TD::PageBlockCaption,
    is_full_width: Bool,
    allow_scrolling: Bool
  })
end