# A block quote
#
# @text : TD::RichText (Quote text)
# @credit : TD::RichText (Quote credit)

class TD::PageBlock::BlockQuote < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockBlockQuote"

  object_attributes({
    text: TD::RichText,
    credit: TD::RichText
  })
end