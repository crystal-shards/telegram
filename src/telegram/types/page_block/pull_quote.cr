# A pull quote
#
# @text : TD::RichText (Quote text)
# @credit : TD::RichText (Quote credit)

class TD::PageBlock::PullQuote < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockPullQuote"

  object_attributes({
    text: TD::RichText,
    credit: TD::RichText
  })
end