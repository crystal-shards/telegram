# A list of data blocks
#
# @items : Array(TD::PageBlockListItem) (The items of the list)

class TD::PageBlock::List < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockList"

  object_attributes({
    items: Array(TD::PageBlockListItem)
  })
end