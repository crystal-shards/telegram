# An audio file
#
# @audio : TD::Audio? (Audio file; may be null)
# @caption : TD::PageBlockCaption (Audio file caption)

class TD::PageBlock::Audio < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockAudio"

  object_attributes({
    audio: TD::Audio?,
    caption: TD::PageBlockCaption
  })
end