# An empty block separating a page
#

class TD::PageBlock::Divider < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockDivider"
end