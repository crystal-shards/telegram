# A slideshow
#
# @page_blocks : Array(TD::PageBlock) (Slideshow item contents)
# @caption : TD::PageBlockCaption (Block caption)

class TD::PageBlock::Slideshow < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockSlideshow"

  object_attributes({
    page_blocks: Array(TD::PageBlock),
    caption: TD::PageBlockCaption
  })
end