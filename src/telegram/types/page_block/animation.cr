# An animation
#
# @animation : TD::Animation? (Animation file; may be null)
# @caption : TD::PageBlockCaption (Animation caption)
# @need_autoplay : Bool (True, if the animation must be played automatically)

class TD::PageBlock::Animation < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockAnimation"

  object_attributes({
    animation: TD::Animation?,
    caption: TD::PageBlockCaption,
    need_autoplay: Bool
  })
end