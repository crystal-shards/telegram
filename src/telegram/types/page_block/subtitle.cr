# The subtitle of a page
#
# @subtitle : TD::RichText (Subtitle)

class TD::PageBlock::Subtitle < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockSubtitle"

  object_attributes({
    subtitle: TD::RichText
  })
end