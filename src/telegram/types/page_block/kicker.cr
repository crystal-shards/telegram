# A kicker
#
# @kicker : TD::RichText (Kicker)

class TD::PageBlock::Kicker < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockKicker"

  object_attributes({
    kicker: TD::RichText
  })
end