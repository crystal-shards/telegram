# The footer of a page
#
# @footer : TD::RichText (Footer)

class TD::PageBlock::Footer < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockFooter"

  object_attributes({
    footer: TD::RichText
  })
end