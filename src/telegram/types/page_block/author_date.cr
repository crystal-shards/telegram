# The author and publishing date of a page
#
# @author : TD::RichText (Author)
# @publish_date : Int32 (Point in time (Unix timestamp) when the article was published; 0 if unknown)

class TD::PageBlock::AuthorDate < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockAuthorDate"

  object_attributes({
    author: TD::RichText,
    publish_date: Int32
  })
end