# Related articles
#
# @header : TD::RichText (Block header)
# @articles : Array(TD::PageBlockRelatedArticle) (List of related articles)

class TD::PageBlock::RelatedArticles < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockRelatedArticles"

  object_attributes({
    header: TD::RichText,
    articles: Array(TD::PageBlockRelatedArticle)
  })
end