# A map
#
# @location : TD::Location (Location of the map center)
# @zoom : Int32 (Map zoom level)
# @width : Int32 (Map width)
# @height : Int32 (Map height)
# @caption : TD::PageBlockCaption (Block caption)

class TD::PageBlock::Map < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockMap"

  object_attributes({
    location: TD::Location,
    zoom: Int32,
    width: Int32,
    height: Int32,
    caption: TD::PageBlockCaption
  })
end