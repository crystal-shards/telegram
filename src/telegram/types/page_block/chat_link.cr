# A link to a chat
#
# @title : String (Chat title)
# @photo : TD::ChatPhotoInfo? (Chat photo; may be null)
# @username : String (Chat username, by which all other information about the chat can be resolved)

class TD::PageBlock::ChatLink < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockChatLink"

  object_attributes({
    title: String,
    photo: TD::ChatPhotoInfo?,
    username: String
  })
end