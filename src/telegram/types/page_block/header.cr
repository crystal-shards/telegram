# A header
#
# @header : TD::RichText (Header)

class TD::PageBlock::Header < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockHeader"

  object_attributes({
    header: TD::RichText
  })
end