# A preformatted text paragraph
#
# @text : TD::RichText (Paragraph text)
# @language : String (Programming language for which the text needs to be formatted)

class TD::PageBlock::Preformatted < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockPreformatted"

  object_attributes({
    text: TD::RichText,
    language: String
  })
end