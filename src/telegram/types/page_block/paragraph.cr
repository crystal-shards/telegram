# A text paragraph
#
# @text : TD::RichText (Paragraph text)

class TD::PageBlock::Paragraph < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockParagraph"

  object_attributes({
    text: TD::RichText
  })
end