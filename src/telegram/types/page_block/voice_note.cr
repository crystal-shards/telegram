# A voice note
#
# @voice_note : TD::VoiceNote? (Voice note; may be null)
# @caption : TD::PageBlockCaption (Voice note caption)

class TD::PageBlock::VoiceNote < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockVoiceNote"

  object_attributes({
    voice_note: TD::VoiceNote?,
    caption: TD::PageBlockCaption
  })
end