# An embedded post
#
# @url : String (Web page URL)
# @author : String (Post author)
# @author_photo : TD::Photo? (Post author photo; may be null)
# @date : Int32 (Point in time (Unix timestamp) when the post was created; 0 if unknown)
# @page_blocks : Array(TD::PageBlock) (Post content)
# @caption : TD::PageBlockCaption (Post caption)

class TD::PageBlock::EmbeddedPost < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockEmbeddedPost"

  object_attributes({
    url: String,
    author: String,
    author_photo: TD::Photo?,
    date: Int32,
    page_blocks: Array(TD::PageBlock),
    caption: TD::PageBlockCaption
  })
end