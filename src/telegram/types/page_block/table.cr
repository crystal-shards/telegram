# A table
#
# @caption : TD::RichText (Table caption)
# @cells : Array(Array(TD::PageBlockTableCell)) (Table cells)
# @is_bordered : Bool (True, if the table is bordered)
# @is_striped : Bool (True, if the table is striped)

class TD::PageBlock::Table < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockTable"

  object_attributes({
    caption: TD::RichText,
    cells: Array(Array(TD::PageBlockTableCell)),
    is_bordered: Bool,
    is_striped: Bool
  })
end