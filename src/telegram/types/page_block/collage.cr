# A collage
#
# @page_blocks : Array(TD::PageBlock) (Collage item contents)
# @caption : TD::PageBlockCaption (Block caption)

class TD::PageBlock::Collage < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockCollage"

  object_attributes({
    page_blocks: Array(TD::PageBlock),
    caption: TD::PageBlockCaption
  })
end