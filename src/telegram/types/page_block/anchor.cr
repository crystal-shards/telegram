# An invisible anchor on a page, which can be used in a URL to open the page from the specified anchor
#
# @name : String (Name of the anchor)

class TD::PageBlock::Anchor < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockAnchor"

  object_attributes({
    name: String
  })
end