# A video
#
# @video : TD::Video? (Video file; may be null)
# @caption : TD::PageBlockCaption (Video caption)
# @need_autoplay : Bool (True, if the video must be played automatically)
# @is_looped : Bool (True, if the video must be looped)

class TD::PageBlock::Video < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockVideo"

  object_attributes({
    video: TD::Video?,
    caption: TD::PageBlockCaption,
    need_autoplay: Bool,
    is_looped: Bool
  })
end