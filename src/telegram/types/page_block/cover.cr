# A page cover
#
# @cover : TD::PageBlock (Cover)

class TD::PageBlock::Cover < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockCover"

  object_attributes({
    cover: TD::PageBlock
  })
end