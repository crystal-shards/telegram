# The title of a page
#
# @title : TD::RichText (Title)

class TD::PageBlock::Title < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockTitle"

  object_attributes({
    title: TD::RichText
  })
end