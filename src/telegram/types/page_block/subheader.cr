# A subheader
#
# @subheader : TD::RichText (Subheader)

class TD::PageBlock::Subheader < TD::PageBlock
  include JSON::Serializable

  object_type "pageBlockSubheader"

  object_attributes({
    subheader: TD::RichText
  })
end