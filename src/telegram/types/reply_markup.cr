# Contains a description of a custom keyboard and actions that can be done with it to quickly reply to bots
#
# RemoveKeyboard: Instructs application to remove the keyboard once this message has been received. This kind of keyboard can't be received in an incoming message; instead, UpdateChatReplyMarkup with message_id
# ForceReply: Instructs application to force a reply to this message
# ShowKeyboard: Contains a custom keyboard layout to quickly reply to bots
# InlineKeyboard: Contains an inline keyboard layout

abstract class TD::ReplyMarkup < TD::Base
end