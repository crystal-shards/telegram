# Represents a list of chats located nearby
#
# @users_nearby : Array(TD::ChatNearby) (List of users nearby)
# @supergroups_nearby : Array(TD::ChatNearby) (List of location-based supergroups nearby)

class TD::ChatsNearby < TD::Base
  include JSON::Serializable

  object_type "chatsNearby"

  object_attributes({
    users_nearby: Array(TD::ChatNearby),
    supergroups_nearby: Array(TD::ChatNearby)
  })
end