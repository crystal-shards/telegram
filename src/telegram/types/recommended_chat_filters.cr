# Contains a list of recommended chat filters
#
# @chat_filters : Array(TD::RecommendedChatFilter) (List of recommended chat filters)

class TD::RecommendedChatFilters < TD::Base
  include JSON::Serializable

  object_type "recommendedChatFilters"

  object_attributes({
    chat_filters: Array(TD::RecommendedChatFilter)
  })
end