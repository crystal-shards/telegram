# Contains a list of t.me URLs
#
# @urls : Array(TD::TMeUrl) (List of URLs)

class TD::TMeUrls < TD::Base
  include JSON::Serializable

  object_type "tMeUrls"

  object_attributes({
    urls: Array(TD::TMeUrl)
  })
end