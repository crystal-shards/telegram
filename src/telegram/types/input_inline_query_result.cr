# Represents a single result of an inline query; for bots only
#
# Animation: Represents a link to an animated GIF or an animated (i.e., without sound) H.264
# Article: Represents a link to an article or web page
# Audio: Represents a link to an MP3 audio file
# Contact: Represents a user contact
# Document: Represents a link to a file
# Game: Represents a game
# Location: Represents a point on the map
# Photo: Represents link to a JPEG image
# Sticker: Represents a link to a WEBP or TGS sticker
# Venue: Represents information about a venue
# Video: Represents a link to a page containing an embedded video player or a video file
# VoiceNote: Represents a link to an opus-encoded audio file within an OGG container, single channel audio

abstract class TD::InputInlineQueryResult < TD::Base
end