# A text with some entities
#
# @text : String (The text)
# @entities : Array(TD::TextEntity) (Entities contained in the text. Entities can be nested, but must not mutually intersect with each other.Pre, Code and PreCode entities can't contain other entities. Bold, Italic, Underline and Strikethrough entities can contain and to be contained in all other entities. All other entities can't contain each other)

class TD::FormattedText < TD::Base
  include JSON::Serializable

  object_type "formattedText"

  object_attributes({
    text: String,
    entities: Array(TD::TextEntity)
  })
end