# Contains some text
#
# @text : String (Text)

class TD::Text < TD::Base
  include JSON::Serializable

  object_type "text"

  object_attributes({
    text: String
  })
end