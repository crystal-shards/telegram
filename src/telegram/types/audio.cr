# Describes an audio file. Audio is usually in MP3 or M4A format
#
# @duration : Int32 (Duration of the audio, in seconds; as defined by the sender)
# @title : String (Title of the audio; as defined by the sender)
# @performer : String (Performer of the audio; as defined by the sender)
# @file_name : String (Original name of the file; as defined by the sender)
# @mime_type : String (The MIME type of the file; as defined by the sender)
# @album_cover_minithumbnail : TD::Minithumbnail? (The minithumbnail of the album cover; may be null)
# @album_cover_thumbnail : TD::Thumbnail? (The thumbnail of the album cover in JPEG format; as defined by the sender. The full size thumbnail is supposed to be extracted from the downloaded file; may be null)
# @audio : TD::File (File containing the audio)

class TD::Audio < TD::Base
  include JSON::Serializable

  object_type "audio"

  object_attributes({
    duration: Int32,
    title: String,
    performer: String,
    file_name: String,
    mime_type: String,
    album_cover_minithumbnail: TD::Minithumbnail?,
    album_cover_thumbnail: TD::Thumbnail?,
    audio: TD::File
  })
end