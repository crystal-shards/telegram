@[Link("tdjson")]

lib TDLib
  alias Status = LibC::Int
  alias VBLevel = LibC::Int
  alias Double = LibC::Double

  fun create = td_json_client_create() : Void*
  fun destroy = td_json_client_destroy(Void*) : Void

  fun send = td_json_client_send(Void*, UInt8*) : Void*
  fun receive = td_json_client_receive(Void*, Double) : UInt8*

  fun set_log_verbosity_level = td_set_log_verbosity_level(VBLevel) : Void
  fun set_log_file_path = td_set_log_file_path(UInt8*) : Void
end
