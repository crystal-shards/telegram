class TD::Client
  module Incoming
    CALLBACKS = {} of String => Array(String)
    METHOD_NAMES = [] of Hash(String, String)

    macro after_initialize(&block)
      def after_initialize
        {{block.body}}
      end
    end

    macro on(incoming_type, &block)
      {% incoming_name = incoming_type.id.stringify.underscore.gsub(/::/, "_") %}
      {% uniq_postfix = (1..1000).to_a.shuffle[0..3].join("_") %}

      CALLBACKS[{{incoming_name}}] ||= [] of String

      {% method_name = "#{incoming_name.id}_#{uniq_postfix.id}" %}
      CALLBACKS[{{incoming_name}}].push({{method_name}})
      {% METHOD_NAMES.push({"method" => method_name, "type" => incoming_type.id.stringify}) %}
      
      def {{method_name.id}}({{block.args[0]}} : {{incoming_type.id}})
        {{block.body}}
      end

      def execute_method(method_name : String, __message__ : TD::Base)
        {% begin %}
        case method_name
        {% for method in METHOD_NAMES %}
        when {{method["method"]}} then {{method["method"].id}}(__message__.as({{method["type"].id}}))
        {% end %}
        end
        {% end %}
      end

      def process(__message__ : TD::Base)
        __message_name__ = __message__.class.name.underscore.gsub(/::/, "_")
        if CALLBACKS[__message_name__]?
          CALLBACKS[__message_name__].each { |method| execute_method(method, __message__) }
        end
      end
    end

    def process(__message__ : TD::Base); end
    def after_initialize; end
  end
end
