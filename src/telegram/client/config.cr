class TD::Client
  class Config
    include JSON::Serializable

    property null_updates_sleep_interval, login_as_bot, request_timeout, api_id, api_hash
    property phone_number, bot_token, database_directory, files_directory
    property use_file_database, use_chat_info_database, use_message_database, use_secret_chats
    property device_model, system_language_code, system_version, application_version
    property use_test_dc, enable_storage_optimizer, ignore_file_names

    def initialize
      @null_updates_sleep_interval = 0.05
      @login_as_bot = false
      @request_timeout = 20
      @api_id = 0
      @api_hash = ""
      @phone_number = ""
      @bot_token = ""
      @database_directory = "#{Dir.current}/.tdlib/default/base"
      @files_directory = "#{Dir.current}/.tdlib/default/files"
      @use_file_database = false
      @use_chat_info_database = false
      @use_message_database = false
      @use_secret_chats = false
      @device_model = "Crystal language client #{TD::VERSION}"
      @system_language_code = "EN"
      @system_version = "undefined"
      @application_version = TD::VERSION
      @use_test_dc = false
      @enable_storage_optimizer = false
      @ignore_file_names = false
    end
  end
end
