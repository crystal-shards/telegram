require "future"

class TD::Client
  class Connection
    include TD::Functions

    class Error < Exception
    end

    class Timeout < Exception
    end

    @status = uninitialized TD::AuthorizationState
    @responses : Hash(String, Channel(JSON::Any | Timeout)) = {} of String => Channel(JSON::Any | Timeout)

    getter client, status

    def initialize(@client : TD::Client)
      @connected = false
      @td_client = TDLib.create

      spawn { get_updates! }
      make_connection!
    end

    def config
      @client.config
    end

    def send(json : String, timeout : Int32 = config.request_timeout)
      request_id = UUID.random.to_s
      channel = Channel(JSON::Any | Timeout).new(capacity: 1)
      json = json.gsub(/}$/, ", \"@extra\": \"#{request_id}\"}")
      @responses[request_id] = channel
      TDLib.send(@td_client.not_nil!, json)

      delay(timeout) { @responses[request_id].send(Timeout.new) if @responses[request_id]? }

      response = channel.receive
      @responses[request_id].close
      @responses.delete(request_id)

      raise(response) if response.is_a?(Timeout)
      klass = TD::Types::TABLE[response["@type"].as_s]
      klass.new(response)
    end

    def receive(wait : Int32 = 0)
      raw = TDLib.receive(@td_client.not_nil!, 0)
      return if raw.null?

      update = String.new(raw)
      JSON.parse(update)
    end

    def destroy!
      @connected = false
      @td_client = TDLib.destroy(@td_client.not_nil!)
    end

    private def get_updates!
      loop do
        result = receive
        if result
          spawn do
            begin
              
              request_id = result.not_nil!["@extra"]?
              if request_id
                channel = @responses[request_id]?
                next unless channel
                channel.send(result.not_nil!)
              else
                next unless @connected
                klass = TD::Types::TABLE[result.not_nil!["@type"].as_s]
                @client.process(klass.new(result))
              end
            rescue e
              TD::Log.fatal {"ERROR: #{e.message}\nBACKTRACE:\n#{e.backtrace.join("\n")}"}
            end
          end
        else
          sleep(config.null_updates_sleep_interval)
        end

        next
      end
    end

    private def make_connection!
      @status = get_authorization_state
      TD::Log.info {"#{config.api_id}: #{@status.class}"}

      case @status
      when TD::AuthorizationState::WaitTdlibParameters
        parameters = TD::TdlibParameters.new(JSON.parse(config.to_json))
        set_tdlib_parameters(parameters) && make_connection!
      when TD::AuthorizationState::WaitEncryptionKey
        check_database_encryption_key("") && make_connection!
      when TD::AuthorizationState::WaitPhoneNumber
        if config.login_as_bot
          check_authentication_bot_token(config.bot_token) && make_connection!
        else
          settings = TD::PhoneNumberAuthenticationSettings.new(
            allow_flash_call: false,
            is_current_phone_number: true,
            allow_sms_retriever_api: false
          )
          
          set_authentication_phone_number(config.phone_number, settings) && make_connection!
        end
      when TD::AuthorizationState::WaitCode
        if ARGV.any? && ARGV[0].split("=").any? && ARGV[0].split("=")[0] == "TG_CODE"
          code = ARGV[0].split("=")[1]
          check_authentication_code(code) && make_connection!
        else
          TD::Log.info {"#{config.api_id}: restart program with first argument TG_CODE=88495 (sample)"}

          exit(0)
        end
      when TD::AuthorizationState::Ready
        if ARGV.any? && ARGV[0].split("=").any? && ARGV[0].split("=")[0] == "TG_CODE"
          TD::Log.info {"#{config.api_id}: authorization complete. Restart program without arg TG_CODE"}

          exit(0)
        else
          @connected = true
          return
        end
      else
        raise Error.new "#{config.api_id}: authorization failed (#{@status.class.name})"
      end
    end
  end
end
