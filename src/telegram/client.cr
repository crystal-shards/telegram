require "./client/**"

module TD
  class Client
    include TD::Functions
    include Incoming

    class ResponseError < Exception
      getter response

      def message
        "#{response.code}: #{response.message}"
      end

      def initialize(@response : TD::Error)
      end
    end

    @connection : Connection?
    @config = uninitialized Config

    property connection, config

    def self.run!
      instance.connect
      instance.after_initialize
    end

    def self.instance
      @@instance ||= new
    end

    def send(json : String, timeout : Int32 = config.request_timeout)
      connection.not_nil!.send(json, timeout)
    end

    def connect
      @connection ||= Connection.new(client: self)
    end

    def disconnect
      connection.destroy! if connection
      @connection = nil
    end

    def self.configure(&block)
      instance.config = Config.new
      yield instance.config
    end
  end
end
