require "uuid"
require "json"

require "./telegram/types.cr"
require "./telegram/base.cr"
require "./telegram/types/**"
require "./telegram/functions.cr"
require "./telegram/client.cr"
require "./telegram/tdlib.cr"

module TD
  VERSION = "1.8.0"
  Log = ::Log.for("telegram")

  def self.set_log_level(level : Int32)
    TDLib.set_log_verbosity_level(level)
  end
end
