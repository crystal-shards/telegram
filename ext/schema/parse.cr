require "file_utils"

DESCRIPTION_SCAN_REGEX = /@([\w\d]+)\s([\w\s.,;`'\"?{}()!:\-\d+]+)/
SPLIT_CLASSES = { 
  "ChatEventAction" => "ChatEvent",
  "MessageContent"  => "Message",
  "InputMessageContent" => "InputMessage"
}



abstract_types = [] of AbstractType
types = [] of Type
functions = [] of Function
read_context = false

temp_comment_description = uninitialized String?
temp_comment_attributes = {} of String => String
temp_nilable_fields = [] of String
previous_line = ""

class Type
  property name, description, parent, short_name, class_name, attributes

  def initialize(@name : String, @description : String, @parent : AbstractType?, @class_name : String? = nil, @short_name : String? = nil, @attributes = [] of Attribute); end
end

class AbstractType
  property name, description, children

  def initialize(@name : String, @description : String, @children = [] of Type); end
end

class Attribute
  property name, description, type

  def initialize(@name : String, @description : String, @type : String); end
end

class Function
  property name, description, attributes, returned_type

  def initialize(@name : String, @description : String, @returned_type : String, @attributes = [] of Attribute); end
end

def extract_type(raw : String)
  case raw
  when "int32"  then "Int32"
  when "int53"  then "Int64"
  when "int64"  then "String"
  when "double" then "Float64"
  when "string" then "String"
  when "bytes"  then "String"
  when "Bool"   then "Bool"
  when /vector<[\w\d<>]+>/
    kls = raw.match(/vector<([\w\d<>]+)>/).not_nil![1]
    "Array(#{extract_type(kls)})"
  else "TD::#{raw.camelcase}"
  end
end


clear_after_parse = -> do
  temp_comment_description = nil
  temp_comment_attributes = {} of String => String
  temp_nilable_fields = [] of String
  previous_line = ""

  read_context = false
end

parse_descriptions = -> (line : String) do
  if line.starts_with?("//-")
    line = previous_line + line.gsub("//-", "")
  end

  scanned = line.scan(DESCRIPTION_SCAN_REGEX)
  hash = {} of String => String
  return unless scanned.any?

  scanned.each do |l|
    l = l.to_a.map(&.not_nil!)
    l.shift
    hash[l[0]] = l[1]
  end

  description = hash.delete("description")
  if description
    read_context = true
    description = description.strip
    temp_comment_description = description
  end

  hash.each do |k, v|
    k = "description" if k == "param_description" # fuckin tg developers
    temp_comment_attributes[k.strip] = v.strip
    temp_nilable_fields << k.strip if v.includes?("may be null") || v.includes?("if applicable") || v.includes?("if available") || v.includes?("for bots only")
  end
  
  temp_nilable_fields.uniq!
  previous_line = line
end



# ---------------------------------------------------------------------------------------------------------

puts "------- Telegram Schema Parser --------"
puts "---> Parse abstract types..."
File.each_line("td_api.tl") do |line|
  break if line == "---functions---"
  next unless line.starts_with?("//@class")
  read_context = false

  scanned = line.scan(DESCRIPTION_SCAN_REGEX)
  hash = {} of String => String
  next unless scanned.any?

  scanned.each do |l|
    l = l.to_a.map(&.not_nil!)
    l.shift
    hash[l[0]] = l[1]
  end

  klass = hash.delete("class").not_nil!.strip
  description = hash.delete("description").not_nil!.strip
  abstract_types << AbstractType.new(name: klass, description: description)
rescue e
  puts "\nERROR WHILE PARSING:"
  puts "error: #{e}"
  puts "--------"
  puts "FAILED LINE: #{line}"
end

puts "---> done"
puts "---> Parse types..."

File.each_line("td_api.tl") do |line|
  break if line == "---functions---"
  next if line.empty? || line.starts_with?("//@class")
  
  if line.starts_with?("//")
    parse_descriptions.call(line)
  else
    next unless read_context

    scanned = line.match(/^([\w]+)\s/)
    next unless scanned
    type_name = scanned[1]
    attrs = [] of String
  
    scanned = line.scan(/([\w]+:[\w\d<>]+)/)
    if scanned
      raw_attrs = scanned.map(&.[1])
      attrs.concat raw_attrs
    end

    raw_parent_class = line.match(/=\s([\w]+);/)
    raw_parent_class = raw_parent_class[1].not_nil! if raw_parent_class

    primary = type_name.camelcase == raw_parent_class
    abstract_type = unless primary
      abstract_types.select { |el| el.name == raw_parent_class }.first
    end

    type = Type.new(name: type_name, description: temp_comment_description.not_nil!, parent: abstract_type)

    attrs.each do |raw_attr|
      attr_name, raw_attr_type = raw_attr.split(":")
      attr_type = extract_type raw_attr_type
      attr_type = attr_type + "?" if temp_nilable_fields.includes?(attr_name)
      attr_description = temp_comment_attributes[attr_name]

      attr = Attribute.new(name: attr_name, type: attr_type, description: attr_description)
      type.attributes << attr
    end

    types << type
    abstract_type.children << type if abstract_type
    clear_after_parse.call
  end
rescue e
  puts "\nERROR WHILE PARSING:"
  puts "error: #{e}"
  puts "--------"
  puts "FAILED LINE: #{line}"
end

puts "---> done"
puts "---> Create type.short_name for namespacing..."

types.each do |type|
  next unless type.parent
  camelize = type.name.camelcase
  parent_name = type.parent.not_nil!.name

  if SPLIT_CLASSES[parent_name]?
    parent_name = SPLIT_CLASSES[parent_name]
  end

  new_camelize = camelize.gsub(/^#{parent_name}/, "")

  if new_camelize == camelize
    puts "WARNING: #{camelize} not have parent name from #{parent_name}"
  end

  type.short_name = new_camelize
end

puts "---> done"
puts "---> Parse functions..."

functions_finded = false
File.each_line("td_api.tl") do |line|
  if line == "---functions---"
    functions_finded = true
    next
  end

  next unless functions_finded

  if line.starts_with?("//")
    parse_descriptions.call(line)
  else
    next unless read_context

    scanned = line.match(/^([\w]+)\s/)
    next unless scanned
    function_name = scanned[1]
    attrs = [] of String
  
    scanned = line.scan(/([\w]+:[\w\d<>]+)/)
    if scanned
      raw_attrs = scanned.map(&.[1])
      attrs.concat raw_attrs
    end

    returned_type = line.match(/=\s([\w]+);/).not_nil!
    returned_type = returned_type[1].not_nil!

    function = Function.new(name: function_name, description: temp_comment_description.not_nil!, returned_type: returned_type)

    attrs.each do |raw_attr|
      attr_name, raw_attr_type = raw_attr.split(":")
      attr_type = extract_type raw_attr_type
      attr_type = attr_type + "?" if temp_nilable_fields.includes?(attr_name)
      attr_description = temp_comment_attributes[attr_name]

      attr = Attribute.new(name: attr_name, type: attr_type, description: attr_description)
      function.attributes << attr
    end

    functions << function
    clear_after_parse.call
  end
rescue e
  puts "\nERROR WHILE PARSING:"
  puts "error: #{e}"
  puts "--------"
  puts "FAILED LINE: #{line}"
end
puts "---> done"

# ---------------------------------------------------------------------------------------------------------
puts "---> Creating abstract classes..."

path_prefix = "../../src/telegram/types"

FileUtils.rm_rf(path_prefix)
FileUtils.mkdir(path_prefix)

abstract_types.each do |type|
  File.open("#{path_prefix}/#{type.name.underscore}.cr", "w") do |f|
    f << "# #{type.description}\n"
    f << "#\n"
    type.children.each do |child|
      name = (child.short_name || child.name).camelcase
      f << "# #{name}: #{child.description}\n"
    end
    f << "\nabstract class TD::#{type.name.camelcase} < TD::Base\n"
    f << "end"
  end
end

puts "---> done"
puts "---> Creating classes..."

types.each do |type|
  parent = type.parent
  parent_class = parent ? parent.name : "Base"
  
  name = (type.short_name || type.name).camelcase
  name_path = name.underscore
  parent_folder = "/#{parent.name.underscore}" if parent
  file_name = "#{path_prefix}#{parent_folder}/#{name_path}.cr"

  if parent_folder
    FileUtils.mkdir_p("#{path_prefix}#{parent_folder}")
  end

  class_name = "TD::#{"#{parent.name}::" if parent}#{name}"
  type.class_name = class_name

  File.open(file_name, "w") do |f|
    f << "# #{type.description}\n"
    f << "#\n"
    type.attributes.each do |attr|
      f << "# @#{attr.name} : #{attr.type} (#{attr.description})\n"
    end

    f << "\nclass #{class_name} < TD::#{parent_class}\n"
    f << "  include JSON::Serializable\n\n"
    f << "  object_type \"#{type.name}\"\n"
    if type.attributes.any?
      f << "\n"
      f << "  object_attributes({\n"
      f << type.attributes.map {|attr| "    #{attr.name}: #{attr.type}" }.join(",\n")
      f << "\n  })\n"
    end
    f << "end"
  end
end

puts "---> done"
puts "---> Creating reference types table..."

types_file = "../../src/telegram/types.cr"
FileUtils.rm_rf(types_file)
File.open(types_file, "w") do |f|
  f << "class TD::Types\n"
  f << "  ABSTRACT_TYPES = {\n"
  abstract_types.each do |type|
    f << "    TD::#{type.name} => true,\n"
  end
  f << "  }\n\n"
  f << "  TABLE = {\n"
  types.each do |type|
    f << "    \"#{type.name}\" => #{type.class_name.not_nil!},\n"
  end
  f << "  }\n"
  f << "end"
end

puts "---> done"
puts "---> Creating functions..."

functions_file = "../../src/telegram/functions.cr"
FileUtils.rm_rf(functions_file)
File.open(functions_file, "w") do |f|
  f << "module TD::Functions\n"
  functions.each do |func|
    params = [] of String
    structure = [] of String
    structure = ["      \"@type\": \"#{func.name}\""]
    f << "  # #{func.description}\n"
    func.attributes.each do |attr|
      params << "#{attr.name} : #{attr.type}? = nil"
      structure << "      \"#{attr.name}\": #{attr.name}"
      f << "  # @#{attr.name} : #{attr.type} (#{attr.description})\n"
    end

    f << "  def #{func.name.underscore}(#{params.join(", ")}) : #{func.returned_type}\n"
    f << "    result = send({\n"
    f << structure.join(",\n")
    f << "\n    }.to_json)\n"
    f << "\n    result.is_a?(TD::Error) ? raise(TD::Client::ResponseError.new(response: result)) : result.as(#{func.returned_type})\n"
    f << "  end\n\n"
  end
  f << "end"
end
puts "---> done"