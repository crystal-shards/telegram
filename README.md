# telegram

## Installation

1. Add the dependency to your `shard.yml`:
```yaml
dependencies:
  telegram:
    git: https://gitlab.com/crystal-shards/telegram
```
2. Run `shards install`

## Usage

```crystal
require "telegram"
```

## Required TDLIB tag

https://github.com/tdlib/td/tree/v1.8.0
